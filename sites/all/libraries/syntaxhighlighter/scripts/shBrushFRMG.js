SyntaxHighlighter.brushes.Custom = function()
{
  var keywords    = 'class node desc value father path template \\$';
  this.regexList = [
      // keywords
      { regex: new RegExp(this.getKeywords(keywords), 'g'),             css: 'keyword' },
      // resources
      { regex: /(^|&nbsp;|\s)+(\+|-)(\s|&nbsp;)*(\w+::)*(\w+);/g,     func:getVariable5,     css: 'color3'},
      // comments
      { regex: /%+(.*)$/gm,                                               css: 'comments' },
      // classnames
      {regex: /(&lt;:|class)(\s+|&nbsp;+)(<a[^>]*>)*(\w+)(<\/a>)*/g, func:getVariable4, css:'string'},
      // variables
      {regex: /node(\(|\s+|&nbsp;+)(?:\w+::)*(\w+)(\)|)/g, func:getVariable2, css:'variable'}, 
      // variables 
      { regex: /(::)*([^&nbsp;|\.]*\w+)(\s|&nbsp;|(=|\+|=>|=&gt;|<|>>|>>+|&gt;&gt;\+|&gt;&gt;|&lt;|&gt;))/g,    func:getVariable2,            css: 'variable'},
      { regex: /(\s|&nbsp;)*(=|>>|&gt;&gt;|&gt;&gt;\+|&lt;)(\s|&nbsp;)*([\$]?\w+::)*([^&nbsp;]*\w+);/g, func:getVariable5,            css: 'variable'},
      // operators
      {regex : /(?:&nbsp;*)\+(?:&nbsp;*)|(?:&nbsp;*)\-(?:&nbsp;*)|&gt;&gt;\+|&gt;&gt;|=|\||~|&lt;:|&lt;|&gt;|::/g, css: 'preprocessor'},
      // attributes
      {regex : /[^node](\[|\.|\s+|&nbsp;+|,)(\w+)(&nbsp;)*(\:([^\:])+?)/g, func:getVariable2, css:'color2'},
      // attributes
      {regex : /\.(\w+)/g, css:'color2'},
      // namespaces
      {regex: /(\w+)::/g,  func:getVariable1, css:'color1'}
      ];
};
 
SyntaxHighlighter.brushes.Custom.prototype = new SyntaxHighlighter.Highlighter();
SyntaxHighlighter.brushes.Custom.aliases  = ['frmg','FRMG'];


function getVar(indexOfMath){
  function getFunction(match,regexinfo){
    match.index += match[0].indexOf(match[indexOfMatch]);
    return match[indexOfMatch];
  };
  return getFunction;
}

function getVariable(match, indexOfMatch, regexinfo){
  match.index += match[0].indexOf(match[indexOfMatch]);
  return match[indexOfMatch];
}

function getVariableTest(match, regexinfo){
  alert(match[0]);
  match.index += match[0].indexOf(match[5]);
  return match[5];
}

function getVariable4(match,regexinfo){
  return getVariable(match,4,regexinfo);
}

function getVariable5(match,regexinfo){
  return getVariable(match,5,regexinfo);
}

function getVariable2(match, regexinfo){
  return getVariable(match,2,regexinfo);
}

function getVariable3(match, regexinfo){
  return getVariable(match,3,regexinfo);
}

function getVariable1(match, regexinfo){
  return getVariable(match,1,regexinfo);
}
