(function($,undefined) {
  $( document ).ready(function() {
    
    $('.edit_section_header').mouseenter(function(){
      $('.hide-in-toc').show();
    });
    
    $('.edit_section_header').mouseleave(function(){
      $('.hide-in-toc').hide();
    });
    
  });
})(jQuery);