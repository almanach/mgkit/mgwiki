<?php

$item = $data;
$params = $options;

$label_style;
$action;
$label;
$table_style = 'margin:0;';

if($item['target_id'] == 'extra_field'){
  $table_style = 'border-top:1px solid black; margin:10px 0px 0px 0px;';
  $label = '<input type="textarea" />';
  $action =  '<input type="submit" value="Assign Class" onclick="alert(\'remove from manually assigned class\');"/>';
}else{

  $label = $item['entity']->title;
  
  if(isset($item['manual'])){
    if(isset($item['auto_missing'])){
      $label_style = 'color:red;';
    }else{
      $label_style = 'color:green;';
    }
    $action = '<div id="ucf-remove" onclick="alert(\'remove from manually assigned class\');"></div>'; 
  }else{
    $action = '<input style="margin:0;" type="submit" value="Assign Class" onclick="alert(\'assign class\');"/>';
  }
}


?>

<table style=" <?php echo $table_style; ?>">
  <tr>
    <td style="width:70%; padding:0;">
      <?php echo $label; ?>            
    </td>
    <td style="padding:0;">
      <?php echo $action; ?>
    </td>
  </tr>
</table>