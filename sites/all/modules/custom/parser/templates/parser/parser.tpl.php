<?php 
$parserpath = drupal_get_path('module','parser') ;
drupal_add_js($parserpath . "/js/parser/build/parser.min.js");
drupal_add_css($parserpath . '/js/parser/build/style/style.css');

global $base_url;

global $user;

$connected_user = "";
if($user->uid != 0){
  $connected_user = $user->uid;
}

$defaultoptions = array(
  "base_url"=>$base_url,
  "parser_url"=>$base_url."/new_frmgserver_proxy",
  "graph_url"=>$base_url.'/d3js/ws_post',
  "parser_proxy"=>$base_url."/frmgserver_proxy",
  "parser_ws"=>$base_url."/parser/ws",
  "url_register"=>$base_url."/user/register",
  "user"=>$connected_user,
  "url_help_depgraph"=>$base_url."/content/helpdepgraph",
  "url_help_frmg"=>$base_url."/wiki/lanalyseur-syntaxique-frmg",
);

$options = array_merge($options,$defaultoptions);
$json_options = json_encode($options);

$id = uniqid();

?>
<div id="parser-ui-<?php echo $id;?>"></div>
<script>
(function(){
	var parser = new mgwiki.parser.MainForm(jQuery('#parser-ui-<?php echo $id;?>'),<?php echo $json_options;?>);
	parser.view.render();
}());

</script>

