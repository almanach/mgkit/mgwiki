(function(mgwiki){
  
  /**
   * Parser module.
   * This module is the ui for frmg parser provided to mgwiki.
   * @type {Object}
   */
  mgwiki.parser = {};

}(window.mgwiki = window.mgwiki || {}));