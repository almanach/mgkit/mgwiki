(function(mgwiki,$){
  
  mgwiki.parser.MainForm = function($el,options){
    this.options = options || {};
    if(options.light_ui){
      this.view = new mgwiki.parser.QuickFormView(this,$el,{});
    }else{
      this.view = new mgwiki.parser.MainFormView(this,$el,{});  
    }
    
  };

  mgwiki.parser.MainForm.prototype.parse = function(sentence,options){
    var me = this;
    var params = options || {};
    params.format = params.format || 'depxml';
    var dataType = 'json';
    /*if(params.format == 'passage'){
      dataType = 'text';
    }*/
    jQuery.ajax({
      type: 'POST', 
      url: me.options.parser_url,
      data: {
        sentence: sentence, 
        options: params,
      },
      dataType : dataType,
      success: function(data, textStatus, jqXHR) {
        if(dataType == 'text'){
          me.displayResult(sentence,data,params);
          $('.parser-ui-main-throbber',me.view.$el).addClass('parser-ui-throbber-success');
          $('.parser-ui-main-throbber',me.view.$el).fadeOut(2000);
        }else if(!data.error){
          me.displayResult(sentence,data,params);
          $('.parser-ui-main-throbber',me.view.$el).addClass('parser-ui-throbber-success');
          $('.parser-ui-main-throbber',me.view.$el).fadeOut(2000);
        }else{
          alert("La phrase n'a pas pu être analysée.");
          $('.parser-ui-main-throbber',me.view.$el).addClass('parser-ui-throbber-error');
          $('.parser-ui-main-throbber',me.view.$el).fadeOut(2000);
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        alert(textStatus);
        $('.parser-ui-main-throbber',me.view.$el).addClass('parser-ui-throbber-error');
        $('.parser-ui-main-throbber',me.view.$el).fadeOut(2000);
      }
    });
  };

  mgwiki.parser.MainForm.prototype.displayResult = function(sentence,data,options){
    if(this.options.light_ui){
      this.overlayDisplayResult(sentence,data,options);
    }else{
      this.normalDisplayResult(sentence,data,options);
    }
  };

   mgwiki.parser.MainForm.prototype.normalDisplayResult = function(sentence,data,options){

    var $el = $('<div></div>');
    this.view.$el.find('.parser-mf-results').prepend($el);
    var result = new mgwiki.parser.Result({sentence:sentence,data:data,initialOptions:options.frmg_options,format:options.format},$el,{base_url:this.options.base_url,parser:this,parser_ws:this.options.parser_ws,graph_url:this.options.graph_url});
    result.view.render();

  };

   mgwiki.parser.MainForm.prototype.overlayDisplayResult = function(sentence,data,options){
    var width = $(window).width();
    var height = $(window).height();
    var box = new depgraphlib.Box({closeButton:true,draggable:true,position:{x:100,y:100}});
    box.setMaxSize(width-200,height-200);
    box.open();
    box.setHeader(sentence);

    var resultDiv = $('<div></div>');
    var resultHeaderDiv = $('<div></div>');
    var resultFooterDiv = $('<div></div>');
    var resultHeader = new mgwiki.parser.ResultViewHeader(resultHeaderDiv,this.options);
    resultHeader.render();
    box.setContent(resultHeaderDiv);
    box.setContent(resultDiv);
    box.setContent(resultFooterDiv);

    var $el = resultDiv//$('.depgraphlib-box-content',box.object);
    var result = new mgwiki.parser.Result({sentence:sentence,data:data,initialOptions:options.frmg_options,format:options.format},$el,{base_url:this.options.base_url,box:box,parser:this,parser_ws:this.options.parser_ws,graph_url:this.options.graph_url});
    result.view.render();
    
    box.options.onclose = function(){
      result.destroy();
    };
  };

}(window.mgwiki = window.mgwiki || {},jQuery));