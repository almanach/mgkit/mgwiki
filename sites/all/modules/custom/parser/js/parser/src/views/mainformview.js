(function(mgwiki,$){
  
  mgwiki.parser.MainFormView = function(model,$el,options){
    this.model = model;
    this.el = ($el)?$el[0]:$('<div></div>');
    this.$el = $(this.el);
    this.initialized = false;
  };

  mgwiki.parser.MainFormView.prototype.render = function(){
    var me = this;
    if(this.model){
      if(!this.initialized){
        this.$el.html(mgwiki.parser.MainFormView.template);
        var resultHeaderDiv = $('<div></div>');
        var resultHeader = new mgwiki.parser.ResultViewHeader(resultHeaderDiv,this.model.options);
        this.$el.find('.parser-mf-results').prepend(resultHeaderDiv);
        resultHeader.render();
        
        this.$el.find('.parser-mf-submit').click(function(){
          var throbber = jQuery('.parser-ui-main-throbber',me.$el);
          if(!throbber.is(':hidden')){
            return;  
          }
          throbber.removeClass('parser-ui-throbber-success');
          throbber.removeClass('parser-ui-throbber-error');
          throbber.show();
          throbber.css('display','inline-block');

          var sentence = me.$el.find('.parser-mf-st-input').val();
          var format = me.$el.find('.parser-mf-option-format').val();
          var robust = me.$el.find('.parser-mf-option-robust').prop('checked');
          var exotic = me.$el.find('.parser-mf-option-exotic').prop('checked');
          var transform = me.$el.find('.parser-mf-option-transform').prop('checked');
          var rename = me.$el.find('.parser-mf-option-rename').prop('checked');
          var frmg_options = '';
          if(robust){
            frmg_options += 'robust ';
          }
          if(rename){
            frmg_options += 'rename ';
          }
          if(exotic){
            frmg_options += 'exotic ';
          }
          if(transform){
            frmg_options += 'transform ';
          }
          var options = {
            format:format,
            frmg_options:frmg_options
          }
          me.model.parse(sentence,options);
        });
        this.initialized = true;
      }
    }
  };

  mgwiki.parser.MainFormView.template = '<div class="parser-mf-container">'+
    '<label>Analyser une phrase (français) : </label>'+
    '<textarea class="parser-mf-st-input" rows="5" cols="200"></textarea>'+
    '<label>Options : </label>'+
    '<span class="frmg-parser-option">Format : <select class="parser-mf-option-format">'+
    '<option selected="true" value="depxml">depxml</option>'+
    '<option value="depconll">depconll</option>'+
    '<option value="conll">conll</option>'+
    '<option value="passage">passage</option>'+
    '</select></span>'+
    '<span class="frmg-parser-option">Robust :<input class="parser-mf-option-robust" type="checkbox" name="robust"></span>'+
    '<span class="frmg-parser-option">Transform : <input class="parser-mf-option-transform" type="checkbox" name="transform"></span>'+
    '<span class="frmg-parser-option">Rename : <input class="parser-mf-option-rename" type="checkbox" name="rename"></span>'+
    '<span class="frmg-parser-option">Exotic :<input class="parser-mf-option-exotic" type="checkbox" name="exotic"></span>'+
    '<hr>'+
    '<input class="parser-mf-submit" type="button" value="Lancer"><div class="parser-ui-main-throbber parser-ui-throbber" style="display:none;"></div>'+
    '<hr>'+
    '<div class="parser-mf-results"></div>'+
  '</div>';

}(window.mgwiki = window.mgwiki || {},jQuery));
