CKEDITOR.plugins.add( 'sentence_example', {
    icons: 'sentence_example',
    init: function( editor ) {
        
      CKEDITOR.scriptLoader.load(this.path + 'test.js',function(success){});
      
      var cssPath = this.path + 'sentence_example.css';
      editor.on('instanceReady', function () {
        this.document.appendStyleSheet(cssPath);
      });
      editor.addCommand( 'insertSentenceDialog', new CKEDITOR.dialogCommand( 'insertSentenceDialog' ));
      editor.addCommand( 'editSentenceDialog', new CKEDITOR.dialogCommand( 'editSentenceDialog' ));
      
      editor.ui.addButton( 'sentence_example', {
        label: 'Insert new sentence',
        command: 'insertSentenceDialog',
        toolbar: 'insert'
    });
      
      if ( editor.contextMenu ) {
        editor.addMenuGroup( 'sentenceExampleGrp' );
        editor.addMenuItem( 'sentenceExampleItem', {
            label: 'Edit Sentence',
            icon: this.path + 'icons/sentence_example.png',
            command: 'editSentenceDialog',
            group: 'sentenceExampleGrp'
        });
        editor.contextMenu.addListener( function( element ) {
          if ( element.getAscendant( 'st', true ) ) {
              return { sentenceExampleItem: CKEDITOR.TRISTATE_OFF };
          }
      });
      }
      
      CKEDITOR.dialog.add( 'insertSentenceDialog', this.path + 'dialogs/insertSentence.js' );
      CKEDITOR.dialog.add( 'editSentenceDialog', this.path + 'dialogs/editSentence.js' );
    }
});