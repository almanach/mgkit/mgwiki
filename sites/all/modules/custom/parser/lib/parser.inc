<?php

class Sentence {
  public $sentence;
  public $parser_outputs = array();
  public $default_parser_output;
  public $assigned_classes = array();
  public $used_classes = array();
  public $used_trees = array();
  
  
  public function __construct($sentence = ""){
    $this->sentence = $sentence;
  }
  
  public function loadFromDB($node_id){
    $node = node_load($node_id);
    $this->sentence = $node->body['und'][0]['value'];
    $this->used_classes = array();
    if($node->parser_output_used_classes!=null){
      foreach($node->parser_output_used_classes['und'] as $used_class){
        $this->used_classes[] = $used_class['target_id'];
      }
    }
    $this->used_trees = array();
    if($node->parser_output_used_trees!=null){
      foreach($node->parser_output_used_trees['und'] as $used_tree){
        $this->used_trees[] = $used_tree['target_id'];
      }
    }
    $this->parser_outputs = array();
    if($node->sentence_parser_outputs!=null){
      foreach($node->sentence_parser_outputs['und'] as $parser_output){
        $parser_output_obj = new ParserOutput();
        $parser_output_obj->loadFromDB($parser_output['target_id']);
        $this->parser_outputs[] = $parser_output_obj;
      }
    }
    if($node->sentence_main_parser_output!=null){
      $parser_output_obj = new ParserOutput();
      $parser_output_obj->loadFromDB($node->sentence_main_parser_output['und'][0]['target_id']);
      $this->default_parser_output = $parser_output_obj;
    }
  }
  
  /**
   * 
   * @param unknown $simplexml
   * @return multitype:NULL
   * Returns an array with keys 'classes' and 'trees'
   * which are array to id of tree and classes used by the parse result.
   */
  public static function getRelatedClassesAndTrees($xmldata){
    if(is_string($xmldata)){
      $xmldata = new SimpleXMLElement($xmldata);
    }
    
    $treesInfo = &drupal_static(__FUNCTION__ . ':trees',array());
    /*
    if(!count($treesInfo)){
      $trees = getNodes(null,"frmg_tree");
      foreach ($trees as $tid) {
        $tree = node_load($tid);
        $body = $tree->body[$tree->language][0]['value'];
        $body = explode(" ", $body);
        for ($i=1; $i < count($body) ; $i++) { 
          $body_part = $body[$i];
          if(!isset($treesInfo[$tid])){
            $treesInfo[$tid] = array();
          }
          if(!isset($treesInfo[$tid][$body_part])){
            $treesInfo[$tid][$body_part]=true;
          }
        }
      }
    }
    */
   
   if(!count($treesInfo)){
      $trees = getNodes(null,"frmg_tree");
      foreach ($trees as $tid) {
        $tree = node_load($tid);
        $body = $tree->body[$tree->language][0]['value'];
        $body = explode(" ", $body);
        array_shift($body);
        $treeclasses = implode(" ", $body);
        $treesInfo[$treeclasses]=$tid;
      }
    }
    



    

    $used_trees = array();
    $used_classes = array();
    $info = array('classes' => &$used_classes, 'trees' => &$used_trees);
    foreach($xmldata->node as $node){
      $table = explode(" ",$node['tree']);
      if(strcmp($table[0],"lexical")==0)
        continue;
      $tree = $table[0];
      //
      $tree_classes = $table;
      array_shift($tree_classes);
      $tree_node_id = null;
      $treeclasses = implode(" ", $tree_classes);

      if(isset($treesInfo[$treeclasses])){
              
        $tree_node_id = $treesInfo[$treeclasses];
      }
      //$n = count($table);
      /*$tree = null;
      $treeCount = 0;
      foreach($treesInfo as $tid => $treeInfo){
        $tc = 0;
        for($i = 1; $i<$n; $i++){
          $item = $table[$i];
          if(isset($treeInfo[$item])){
            $tc++;
          }
        }
        if($tc>=$treeCount){
          $treeCount = $tc;
          $tree = $tid;
        }
      }
      
      $tree_node_id = $tree;*/
      //$tree_node_id = getNodeID("tree $tree","frmg_tree");
      //
      if($tree_node_id!=null && !existIn($tree_node_id,$used_trees, null)){
        $used_trees[] = $tree_node_id;
      }
      $n = count($table);
      for($i = 1; $i<$n; $i++){
        $item = $table[$i];
        $sub_items = explode(":",$item);
        $class = $sub_items[count($sub_items)-1];
        $class_node_id = getNodeID($class,"frmg_class");
        if(!existIn($class_node_id,$used_classes, null)){
          if($class_node_id != null){
            $used_classes[] = $class_node_id;
          }
        }
      }
  
    }
    return $info;
  }

  public function getParserOutputs(){
    
  }
  
  public function parse(){
    $this->default_parser_output = new ParserOutput($this->sentence);
    $entity = $this->default_parser_output->parse();
    return $entity != null;
    //$this->parseXMLDep($this->default_parser_output->xml_source);
  }
  
  /**
   * OBSOLETE not used since the fields class used and tree used are related to manually user assigned items 
   * Parse the the xmldep format of the parser output to populate class and tree used by the sentence parse
   * @param unknown $xmldep
   * @return boolean
   * Returns true if success, false otherwise
   */
  public function parseXMLDep($xmldep){
    module_load_include("module", "frmg");
    if($xmldep == null){
      return false;
    }
    $xmldata = new SimpleXMLElement($xmldep);
    foreach($xmldata->node as $node){
      $table = explode(" ",$node['tree']);
      if(!existIn($table[0],$this->used_trees, null)){
        if(strcmp($table[0],"lexical")==0)
          continue;
        $this->used_trees[] = $table[0];
        $n = count($table);
        for($i = 1; $i<$n; $i++){
          $item = $table[$i];
          $sub_items = explode(":",$item);
          $class = $sub_items[count($sub_items)-1];
          if(!existIn($class, $this->used_classes, null)){
            $this->used_classes[] = $class;
          }
        }
  
      }
    }
    return true;
  }
  
  
  public function createNode(){
    $node = new stdClass();
    $node->type = "sentence";
    $node->title = "Sentence " . uniqid();
    $node->body['und'][0]['value'] = $this->sentence;
    $node->body['und'][0]['format'] = 'full_html';
    $node->language = LANGUAGE_NONE;
    $node->sentence_main_parser_output['und'][0]['target_id']=$this->default_parser_output->gid;
    foreach($this->used_trees as $tree){
      $tree_node_id = getNodeID("tree $tree","frmg_tree");
      if($tree_node_id != null){
        $node->parser_output_used_trees['und'][]['target_id'] = $tree_node_id;
      }
    }
    foreach($this->used_classes as $class){
      $class_node_id = getNodeID($class,"frmg_class");
      if($class_node_id != null){
        if(!isset($node->parser_output_used_classes['und']) || !existIn($class_node_id, $node->parser_output_used_classes['und'], 'target_id')){
          $node->parser_output_used_classes['und'][]['target_id'] = $class_node_id;
        }
      }
    }
    node_object_prepare($node);
    $node = node_submit($node);
    node_save($node);
    return $node;
  }
}

class ParserOutput{
  public $sentence_nid;
  public $sentence;
  public $xml_source;
  public $params;
  public $gid = null;
  
  
  public function __construct($sentence = "", $params = array()){
    $this->sentence = $sentence;
    $this->params = $params;
  }
  
  public function loadFromDB($entity_id){
    $query_results = db_query("SELECT gid, data FROM d3js WHERE gid = $entity_id");
    foreach($query_results as $row){
      $this->xml_source = $row->data;
      $this->gid = $row->gid;
    }
  }
  
  /**
   * Call the parser server
   * @param string $param
   * @return NULL|unknown
   * Returns the id (gid) of the parse output resulting of the call to the parser server.
   * Returns null on failure.
   */
  public function parse($param = ''){
    $sentence = urlencode($this->sentence);
    $this->xml_source = file_get_contents (
        "http://alpage.inria.fr/alpes/parser.pl?sentence=$sentence&forest=xmldep&grammar=frmgtelr&Action=Submit&disambiguate=yes",  // page url
        false,
        null);
    if($this->xml_source === false){
      return null;
    }
    $id = uniqid();
    $xml = new SimpleXMLElement($this->xml_source);
    $xml->addAttribute("sentence",htmlentities($this->sentence));
    $xml->addAttribute("uid",$id);
    $this->xml_source = $xml->asXML();
    $entity = entity_get_controller('d3js')->create('parser_output_graph');
    $entity->title = "Parser Output " . $id;
    $entity->data = $this->xml_source;
    entity_get_controller('d3js')->save($entity);
    $this->gid = $entity->gid;
    return $entity;
  }
  



}







