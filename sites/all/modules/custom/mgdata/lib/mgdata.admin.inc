<?php

/**
 * Defines the steps of the sync update process
 * @return multitype:multitype:string
 */
function _mgdata_management_steps() {
  return array(
      1 => array(
          'form' => 'mgdata_add_source_page_form',
          'name' => 'Add Source',
      ),
      2 => array(
          'form' => 'mgdata_overview_page_form',
          'name' => 'Overview',
      ),
      3 => array(
          'form' => 'mgdata_merge_page_form',
          'name' => 'Merge Page'
      ),
  );
}


/**
 * Add this file menu items to the module menu items
 * @param unknown $items
 * @return unknown
 */
function _mgdata_admin_menu(&$items){
  return $items;
}

/**
 * Entry point for the multi step form wizard
 * @param unknown $form
 * @param unknown $form_state
 * @return unknown
 */
function mgdata_management_form_wizard($form, &$form_state) {
  module_load_include("inc", "mgdata", "lib/mgdata");
  if (empty($form_state['step'])) {
    $repos = MGDataRepository::getSVNRepository();
    if(count($repos) == 0){
      $form_state['step'] = 1;
    }else{
      $form_state['step'] = 2;
    }

    $form_state['step_information'] = _mgdata_management_steps();
  }

  $step = &$form_state['step'];

  drupal_set_title(t('MGData Management : @step', array('@step' => $form_state['step_information'][$step]['name'])));

  $form = $form_state['step_information'][$step]['form']($form, $form_state);

  return $form;
}

/**
 * Overview of the meta grammars and the updates
 * @param unknown $form
 * @param unknown $form_state
 * @return multitype:string multitype:string  Ambigous <The, string, A, Optional>
 */
function mgdata_overview_page_form($form, &$form_state){
  module_load_include("inc","mgdata","lib/mgdata");
  // loads all available repository
  $repos = MGDataRepository::getSVNRepository(); 
  
  $form['v_tabs'] = array(
    '#type' => 'vertical_tabs',
  );
  
  foreach($repos as $repo){
    // loads from db the metagrammar defined by the repository, using the proper metagrammar manager
    $module_name = strtoupper($repo->manager_module);

    // switch case between existent and non-existent version of the metagrammar
    $currentRevisionLoaded = $repo->getCurrentMGRevisionImported();
    if($currentRevisionLoaded != 0){
      $title = t('Current Version of ') . $repo->name . ' : ' . $currentRevisionLoaded;
    }
    else{
      $title = t('There is no version of ') . $repo->name . t(' loaded in this wiki.');
    }
    
    $form[$repo->machine_name] = array(
        '#type' => 'fieldset',
        '#title' => $repo->name,
        '#group' => 'v_tabs');

    $form[$repo->machine_name]['mgdata_info'] = array(
        '#type' => 'item',
        '#title' => $title,
    );
    
    // if a hook is set, then this test could be removed
    $new_rev = (method_exists($module_name, "getUpdateStatus")) ? call_user_func($module_name . '::getUpdateStatus') : $repo->getUpdateStatus();

    // check for some possible failure
    
    if($new_rev == 0){
      watchdog('error', t('Failed use of svn, make sure subversion is installed on the server and that the directory of the repository is writable'));
    }else if($new_rev != $currentRevisionLoaded){
      $form[$repo->machine_name]['mgdata_update'] = array(
          '#type' => 'item',
          '#title' => t('New version of ') . $repo->name . t(' available! : ') . $new_rev,
      );
      $form[$repo->machine_name]['mgdata_new_revision'] = array(
          '#type' => 'textfield',
          '#size' => 10,
          '#title' => 'Update to revision : ',
          '#default_value' => $new_rev,
          '#value' => $new_rev,
          '#attributes' => array('style'=>'width:50px;')
      );
      $form[$repo->machine_name]['update'] = array(
          '#type' => 'submit',
          '#value' => t('Update to new revision'),
          '#submit' => array('mgdata_overview_page_update'),
        );
    }else{
      $form[$repo->machine_name]['mgdata_new_revision'] = array(
          '#type' => 'textfield',
          '#size' => 10,
          '#title' => 'Update to revision : ',
          '#default_value' => $new_rev,
          '#value' => $new_rev,
          '#attributes' => array('style'=>'width:50px;')
      );
      $form[$repo->machine_name]['update'] = array(
          '#type' => 'submit',
          '#value' => t('Update to new revision'),
          '#submit' => array('mgdata_overview_page_update'),
      );
    }
  }
  
   $form['add_source'] = array(
       '#type' => 'submit',
       '#value' => t('Add new Meta Grammar Source'),
       '#submit' => array('mgdata_goto_add_source'),
   );
  return $form;
}

/**
 * Load the new version of the metagrammar and compute the diff between the one in db
 * @param unknown $form
 * @param unknown $form_state
 * @return string
 */
function mgdata_overview_page_update($form, &$form_state){
  module_load_include("inc","mgdata","lib/mgdata");

  // get the current selected repository
  $active_tab = $form_state['input']['v_tabs__active_tab'];
  $repo_machine_name = substr($active_tab, 5);
  $repo = MGDataRepository::getSVNRepository($repo_machine_name);


  // instanciate the manager associated with the repository
  $manager_module = strtoupper($repo->manager_module);

  $current_step = &$form_state['step'];
  $form_state['step_information'][$current_step]['stored_values']=$form_state['input'];
  
  if(!isset($form_state['step_information'][3]['computed_diff'])){
    $new_revision = $form_state['step_information'][2]['stored_values']['mgdata_new_revision'];

    // new mgdata object   
    $mgdata = new $manager_module();
    // update repository to new revision
    $repo->updateTo($new_revision);
    // compute diff with the mgdata object
    $diff = $mgdata->diff($repo);

    $form_state['step_information'][3]['computed_diff'] = $diff;
  }

  $form_state['step'] = 3;
  $form_state['current_repo'] = $repo;
  $form_state['rebuild'] = TRUE;
}

/**
 * Overview of the result of loading the new version of the metagrammar
 * @param unknown $form
 * @param unknown $form_state
 * @return Ambigous <multitype:string multitype:string  The , multitype:string boolean The multitype: , multitype:string boolean Ambigous <The, string, A, Optional> >
 */
function mgdata_merge_page_form($form, &$form_state){
  $diff=$form_state['step_information'][3]['computed_diff'];
  $repo = $form_state['current_repo'];
  $manager_module = strtoupper($repo->manager_module);
  
  $form['diff'] = $manager_module::showDiffForm($diff);
  
  $form['apply'] = array(
      '#type' => 'submit',
      '#value' => t('Submit new revision'),
      '#submit' => array('mgdata_apply_update_selected'),
      '#attributes' => array('onclick' => 'if(!confirm("Be sure you linked properly removed classes if needed!")){return false;}'),
  );


  return $form;
}

/**
 * Submit function that achieve the saving of the new version of the metagrammar
 * @param unknown $form
 * @param unknown $form_state
 */
function mgdata_apply_update_selected($form, &$form_state){
  module_load_include("inc", "mgdata", "lib/mgdata");

  $repo = $form_state['current_repo'];
  $manager_module = strtoupper($repo->manager_module);
  $mgdata = new $manager_module();

  try{
    set_time_limit(0);
    $diff = &$form_state['step_information'][3]['computed_diff'];
    

    $mgdata->prepareUpdate($repo,$diff,$form_state);

    $batch = array(
        'title' => t('Exporting'),
        'operations' => array(
            array(strtolower($manager_module) . "::update_db", array($diff,$mgdata)),
        ),
        'finished' => strtolower($manager_module) . '::finish_update',
        'file' => drupal_get_path('module', strtolower($manager_module)),
    );
    batch_set($batch);
    
    $repo->updateCurrentRev();
  }
  catch(Exception $e){
    $repo->revertSVNToMGLoadedVersion();
    drupal_set_message(t('Something wrong happend while trying to apply the new changes, nothing was changed'), 'error');
  }
}


/**
 * Overview of the svn source available
 * @param unknown $form
 * @param unknown $form_state
 * @return multitype:string multitype:string  Ambigous <The, string, A, Optional>
 */
function mgdata_add_source_page_form($form, &$form_state){
  module_load_include("inc", "mgdata", "lib/mgdata");
  
  // liste des managers avec message avertissement si non valides
  $managers = MGDataRepository::scanMGManagerModules();
  $header = array(
      'name' => t('Name'),
      'svn' => t('SVN'),
      'version' => t('Version'),
  );
  $options = array();
  foreach ($managers as $manager){
    $options[] = array(
        'name' => $manager['name'],
        'svn' => $manager['svn_url'],
        'version' => $manager['loaded_svn_revision'],
        );
  }
  $form['managers_table'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => t('There is no Metagrammar manager enabled.')
  );
  
  $form['howto'] = array(
      '#type' => 'item',
      '#title' => t('To add a source, you must create a php module that will handle the
          the metagrammar, from business logic to display logic, with the proper implementation of
          required methods. Help will come soon. For the moment you can contact the webmaster of
          this site for further information.')
  );        

  $form['back']=array(
      '#type'=>'submit',
      '#value' => t('back'),
      '#submit' => array('mgdata_source_submit'),
  );
  return $form;
}



/**
 * Returns to the overview step
 * @param unknown $form
 * @param unknown $form_state
 */
function mgdata_source_submit($form, &$form_state){
  $current_step = &$form_state['step'];

  $form_state['step'] = 2;
  $form_state['rebuild'] = TRUE;
}




/**
 * Goto the page source overview step
 * @param unknown $form
 * @param unknown $form_state
 */
function mgdata_goto_add_source($form, &$form_state){
  $form_state['step'] = 1;
  $form_state['rebuild'] = TRUE;
}

/**
 * Not used?
 * @param unknown $form_id
 * @param unknown $form_values
 */
function mgdata_management_form_submit($form_id, &$form_values){
  module_load_include("inc","mgdata","lib/mgdata");
  $form_values['rebuild'] = true;
  /*$mgdata = new mgdata();
   $mgdata->load(mgdata::$frgram_file_path, mgdata::$frgram_raw_file_path);
  $mgdata->loadTAG(mgdata::$frgram_tag_file_path);
  $mgdata->createNodes();*/
}
