<?php

define("BASE_SVN_PATH", "sites/default/files/repos/");
define("BASE_MANAGER_PATH", "managers");


/**
 * Class that abstact the svn repository
 * Comment : Should add hook that manager module of the metagrammar data could use
 * @author buiquang
 *
 */
class MGDataRepository {
  public $attributes; 
  
  function __construct(){
    $this->attributes = func_get_args();
  }
  
  function __get($name){
    foreach($this->attributes as $attribute){
      if(is_array($attribute)){
        if (array_key_exists($name, $attribute)) {
          return $attribute[$name];
        }
      }else if(is_object($attribute)){
        return $attribute->$name;
      }
    }
  }    
  
  function __set($name,$value){
    foreach($this->attributes as $attribute){
      if(is_array($attribute)){
        if (array_key_exists($name, $attribute)) {
          $attribute[$name] = $value;
        }
      }else if(is_object($attribute)){
        $attribute->$name = $value;
      }
    }
  }

  /**
   * @return the current loaded verion of the metagrammar (from previous import),
   * often equal to the current svn revision status.
   */
  function getCurrentMGRevisionImported(){
    return $this->loaded_svn_revision;
  }
  
  
  function updateCurrentRev(){
    $this->loaded_svn_revision = $this->getCurrentSVNStatusRevision();
    drupal_write_record('mgdata', $this->attributes[0], 'mgid');
  }
  
 
  /**
   * 
   * @return the current revision number of the svn repository
   */
  function getCurrentSVNStatusRevision(){
    $path = DRUPAL_ROOT . '/' . BASE_SVN_PATH . $this->machine_name;
    $output = shell_exec("export LANGUAGE=en_US.utf8; svn info $path");
    if($output == null){
      return 0;
    }
    $table = explode("\n", $output);
    foreach($table as $line){
      if(substr($line, 0, 8)=="Revision")
        return trim(substr($line,9));
    }
  }
  
  /**
   * 
   * @return the revision number of the latest changes in the svn
   * //todo
   * create a hook for metagrammar managers to tell which files specifically look for change
   * current version watch for any file change
   */
  function getUpdateStatus(){
    $cmd = "export LANGUAGE=en_US.utf8; svn st -u " . DRUPAL_ROOT . '/' . BASE_SVN_PATH . $this->machine_name;
    $output = shell_exec($cmd);
    if($output==null){
      $this->initSVNRepository();
      $output = shell_exec($cmd);
    }
    $output_lines = explode("\n", $output);
    $m = count($output_lines);
    Logger::Log($output_lines);
    while($m>1 && ($new_revision=$output_lines[$m-1])==""){
      $m--;
    }
    $matches = array();
    $found = preg_match("/\d+/", $new_revision, $matches);
    if($found){
      $new_revision = $matches[0];
    }
    return $new_revision;
  }
  
  /**
   * init the svn repository
   * @param string $svn_url
   */
  function initSVNRepository(){
    $svn_dir = DRUPAL_ROOT . '/' . BASE_SVN_PATH . $this->machine_name;
    $svn_url = $this->svn_url;
    shell_exec("mkdir -p $svn_dir");  
    $output = shell_exec("svn co $svn_url $svn_dir");
  }
  
  function removeSVNRepository(){
    $svn_dir = DRUPAL_ROOT . '/' . BASE_SVN_PATH . $this->machine_name;
    $svn_url = $this->svn_url;
    $output = shell_exec("rm -fr $svn_dir");
  }
  
  function updateTo($revision){
    $path = DRUPAL_ROOT . '/' . BASE_SVN_PATH . $this->machine_name;
    $to_rev = "";
    if($revision != null){
      $to_rev = "-r $revision";
    }
    $output = shell_exec("svn update $path $to_rev");
    
    $callback = "do_after_svn_update";
    $manager_module = strtoupper($this->manager_module);
    if(method_exists($manager_module, $callback)){
      $manager_module::$callback($path,$revision);
    }
  }
  
  function revertSVNToMGLoadedVersion(){
    $this->updateTo($this->loaded_svn_revision);
  }
  
  /*************************************/
  /*            STATICS                */
  /*************************************/
  
  static function getSVNRepository($name = null){
    module_load_include("inc", "mgwiki", "mgwiki.utils");
    $result;
    if($name != null){
      $machine_name = toMachineName($name);
      $result = db_query("SELECT * FROM mgdata WHERE machine_name = :machine_name",
          array(
              ':machine_name' => $machine_name,
          )
      );
    }else{
      $result = db_query("SELECT * FROM mgdata",
          array(
          )
      );
    }
  
    $repos = array();
    foreach($result as $row){
      $repos[] = new MGDataRepository($row);
    }
  
    $repo = $repos;
    if($name!=null && count($repos) > 0){
      $repo = $repos[0];
    }
    
    return $repo;
  }
  
  static function getSVNRepositoryByManagerModule($module){
    $result = db_query("SELECT * FROM mgdata WHERE manager_module = :module_name",
        array(
            ':module_name' => $module,
        )
      );
    
    $repos = array();
    foreach($result as $row){
      $repos[] = new MGDataRepository($row);
    }
  
    return $repos;
  }
  
  static function autoFillRepoRequiredAttr(&$obj){
    if(is_object($obj)){
      if(!isset($obj->loaded_svn_revision)){
        $obj->loaded_svn_revision = 0;
      }
      if(!isset($obj->machine_name)){
        $obj->machine_name = toMachineName($obj->name);
      }
    }else{
      if(!isset($obj['loaded_svn_revision'])){
        $obj['loaded_svn_revision'] = 0;
      }
      if(!isset($obj['machine_name'])){
        $obj['machine_name'] = toMachineName($obj['name']);
      }
    }
    return $obj;
  }
  
  static function scanMGManagerModules(){
    module_load_include("inc", "mgwiki", "mgwiki.utils");
    
    //TODO Replace 'mgdata' mb with __FILE__ to get module's name or smthing else than hard code.. :/
    $mg_modules = array();
    $module_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'mgdata') . '/' . BASE_MANAGER_PATH; 
    $list = glob_recursive($module_path . "/*.module");
    foreach ($list as $file) {
      require_once ($file);
      
      $manager_name = getFileNameWithoutExtension($file);
      $info_callback = $manager_name . '_' . 'mgdata_info';
      if (function_exists($info_callback)){
        $info = $info_callback();
        $info = MGDataRepository::autoFillRepoRequiredAttr($info);
        $valid = method_exists($manager_name, "showDiffForm") &&
          method_exists($manager_name, "prepareUpdate") &&
          method_exists($manager_name, "serialize") &&
          method_exists($manager_name, "unserialize") &&
          method_exists($manager_name, "diff") &&
          method_exists($manager_name,'update_db');
        if($valid){
          $mg_modules[] = $info;
        }
      }
    }
    return $mg_modules;
  }
  
  static function registerMGManager($module){
    module_load_include("inc", "mgwiki", "mgwiki.utils");
    
    $file = DRUPAL_ROOT . '/' . drupal_get_path('module', 'mgdata') . '/' . BASE_MANAGER_PATH . "/$module/$module" .".module";
    require_once($file);
    $info_callback = $module . '_' . 'mgdata_info';
    if (function_exists($info_callback)){
      $info = $info_callback();
      $info = MGDataRepository::autoFillRepoRequiredAttr($info);
      $repo = new MGDataRepository($info);
      $repo_stdobject = array_to_object($info); 
      $repos = MGDataRepository::getSVNRepositoryByManagerModule($repo->manager_module);
      if(count($repos)>0){
        $repo->mgid = $repos[0]->mgid;
        drupal_write_record('mgdata', $repo_stdobject, 'mgid');
      }else{
        drupal_write_record('mgdata', $repo_stdobject);
        $repo->initSVNRepository();
      }
    }
  }
  
  /**
   * Called when a manager module is disabled or uninstalled
   * @param unknown $module
   */
  static function unregisterMGManager($module){
    $num_deleted = db_delete('mgdata')
    ->condition('manager_module', $module)
    ->execute();
  }
  
}

interface Manager {
  /**
   * @desc returns a drupal form to show the diff or let the user interact with it
   * @param unknown $diff
   * @return unknown a drupal form
   */
  static function showDiffForm($diff);
  
  /**
   * @desc called at the end of the update operation
   * @param unknown $success
   * @param unknown $result
   * @param unknown $operations
   */
  static function finish_update($success,$result,$operations);
  
  /**
   * @desc update the data
   * @param unknown $diff the diff produced by the manager object
   * @param unknown $mgdata the manager object after prepareUpdate has been called
   * @param unknown $context this function is called repeatedly until $context['finished'] == 1 
   */
  static function update_db($diff,$mgdata,&$context);
  
  /**
   * @desc prepare this manager object to update the data from a repository set to the new version,
   * the diff object and the form_state resulting of showDiffForm
   * @param unknown $repository
   * @param unknown $diff
   * @param unknown $form_state
   */
  function prepareUpdate($repository,$diff,$form_state);
  
  /**
   *
   * @param Manager $manager
   * The other manager loaded with a database version or any svn repository version
   * @return mixed array
   * An array with the following keys :
   * - 'added' : an array containing arrays for id/name and type of the added element
   * - 'removed' : an array containing arrays for id/name and type of the removed element
   * - 'modified' : an array containing arrays for id/name and type of the modified element
  */
  function diff($manager);

  /**
   *
   * @param string $id
   * The id of the element to retrieve
   * @param string $type
   * The type of the element to retrieve
   * @return object
   * The object instance of the element to retrieve
  */
  function getItem($id,$type);

  /**
   * Save into database a element
   *
   * @param string $id
   * The id of the element to retrieve
   * @param string $type
   * The type of the element to retrieve
  */
  function saveItem($id,$type);
}

