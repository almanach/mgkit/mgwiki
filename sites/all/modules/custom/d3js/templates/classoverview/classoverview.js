(function(depgraphlib){
  
  depgraphlib.ClassOverviewApp = function(container,data,options){
    this.options = options;
    this.createView(container,options);
    this.classoverview = new depgraphlib.ClassOverview(this.chart,data,options);

    var jcontainer = jQuery('#'+container);
    var ajaxLoader = jQuery('#classoverview-ajax-loader');
    ajaxLoader.width(jcontainer.width());
    ajaxLoader.height(jcontainer.height());


    var me = this;
    jQuery("#classoverviewappinput").change(function(){
      me.classoverview.showOnlySimilar(this.value);
    });
    
    jQuery('#classoverview-fullscreen').click(function(){
      me.fullScreen();
    });

    jQuery('#classoverview-help').click(function(){
      jQuery.ajax({
        type: 'POST', 
        url: me.options.help_url,
        data: {
          app:"classoverview"
        },
        dataType : 'html',
        success: function(data, textStatus, jqXHR) {
          var div = data;
          var width = 300;
          var height = 300;
          var ww = jQuery(window).width();
          var wh = jQuery(window).height();
          var point = {x:(ww-width)/2,y:(wh-height)/2};
          var box = new depgraphlib.Box({closeButton:true,autodestroy:true,position:point}).setContent(div).open();
          box.setFixedSize(width,height);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          alert(textStatus);
        }
      });
    });

    if(options.frmg_class){
      me.classoverview.showOnlyStrict(options.frmg_class);
    }
    
  };

  depgraphlib.ClassOverviewApp.prototype.fullScreen = function(){
    var me = this;
    var width = jQuery(window).width()-200;
    var height = jQuery(window).height()-200;
    var prevWidth = this.classoverview.chart.width();
    var prevHeight = this.classoverview.chart.height();
    var point = {x:100,y:100};

    jQuery('#classoverview-fullscreen').hide();
    this.classoverview.resize(width-20,height);
    //this.detachableContainer.detach();
    
    var div = this.detachableContainer;
    var box = new depgraphlib.Box({closeButton:true,draggable:true,onclose:function(){
        me.container.append(me.detachableContainer);
        me.classoverview.resize(prevWidth,prevHeight);
        jQuery('#classoverview-fullscreen').show();
        jQuery('#classoverview-fullscreen').click(function(){
          me.fullScreen();
        });
      }}).setContent(div).open(point).setFixedSize(width,height);
  };
  
  depgraphlib.ClassOverviewApp.prototype.createView = function(container,options){
    var container = this.container = jQuery('#'+container);
    container.addClass('classoverview-container');
    
    var detachableContainer = this.detachableContainer = jQuery('<div id="classoverview-detachable-container"></div>');
    container.append(detachableContainer);
    var absoluteRef = jQuery('<div style="position:absolute; width:100%;"></div>');
    detachableContainer.append(absoluteRef);

    var chart = this.chart = jQuery('<div id="classoverview-chart"></div>');
    var mainpanel = this.mainpanel = jQuery('<div id="classoverview-mainpanel"></div>');
    detachableContainer.append('<div id="classoverview-ajax-loader"></div>');
    absoluteRef.append('<div id="classoverview-fullscreen"></div>');
    absoluteRef.append('<div id="classoverview-help"></div>');
    detachableContainer.append(mainpanel);
    
    var table = jQuery('<table class="classoverview"><tbody><tr></tr></tbody></table>');
    var cell1 = jQuery('<td class="classoverview" width="75%"></td>');
    var cell2;
    if(!options.widget){
      cell2 = jQuery('<td class="classoverview" width="25%"></td>');
      cell2.append(depgraphlib.ClassOverviewApp.sidebar());
    }
    
    cell1.append(chart);
    
    jQuery('tr',table).append(cell1);
    if(!options.widget){
      jQuery('tr',table).append(cell2);  
    }
    
    mainpanel.append(table);
    
    
  };
  
  depgraphlib.ClassOverviewApp.sidebar = function(){
    var sidebar = jQuery('<div id="classoverview-sidebar"></div>');
    //sidebar.append('<input type="button" onclick="depgraphlib.exportLayout(depgraphlib.overview);" value="Export Layout">');
    sidebar.append('<div><input type="text" name="test" id="classoverviewappinput"/></div>');
    
    
    return sidebar;
  };
  
  depgraphlib.exportLayout = function(overview){
    jQuery.ajax({
      type: 'POST', 
      url: 'save_layout',
      data: {
        data:overview.classoverview.data,
      },
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        if(data && data.success){
          alert('saved');
        }else{
          alert('error');
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        alert(textStatus);
      }
    });

  };
  

  depgraphlib.ClassOverview = function(chart,data,options){

    this.options = options;
    this.originalData = depgraphlib.clone(data);
    this.chart = chart; 

    var width = this.width = chart.width(),
    height = this.height = options.height || 600;
    
    chart.width(width);
    chart.height(height);

    var svg = this.svg = d3.select(chart[0]).append("svg")
        .attr("width", width)
        .attr("height", height);
    
    this.defs = this.svg.append("defs");
    this.defs.append('marker')
      .attr('id','arrow-end')
      .attr('viewBox',"0 0 10 10")
      .attr('refX','24')
      .attr('refY','5')
      .attr('markerUnits','strokeWidth')
      .attr('orient','auto')
      .attr('markerWidth','5')
      .attr('markerHeight','5')
      .append('polyline')
        .attr('points','0,0 10,5 0,10');
    this.defs.append('marker')
    .attr('id','arrow-start')
    .attr('viewBox',"0 0 10 10")
    .attr('refX','-14')
    .attr('refY','5')
    .attr('markerUnits','strokeWidth')
    .attr('orient','auto')
    .attr('markerWidth','5')
    .attr('markerHeight','5')
    .append('polyline')
      .attr('points','0,5 10,0 10,10');
    
    this.frame = svg.append('g');
    this.vis = this.frame.append('g');
    
    this.reset(data);

  };

  depgraphlib.ClassOverview.prototype.resize = function(width,height){
    
    this.width = width;
    this.chart.width(width);
    this.height = height;
    this.chart.height(height);

    this.svg
        .attr("width", width)
        .attr("height", height);

    this.fitScaleToContainerSize();

  };



  depgraphlib.ClassOverview.prototype.showOnly = function(input,testfunction,extrafunction){
    var me = this;
    var newdata = {'nodes':[],'links':[],'ressourcelinks':[]};
    for(var i=0; i < this.originalData['nodes'].length; ++i){
      var originalNode = this.originalData['nodes'][i];
      if(testfunction(input,originalNode)){
        var node = depgraphlib.clone(this.originalData['nodes'][i]);
        node.originalIndex = i;
        newdata.nodes.push(node);
      };
    }
    var n = 0;
    n = addRelatedLinksAndNodes('links','parent',n);
    addRelatedLinksAndNodes('ressourcelinks','ressource',n);
    this.reset(newdata);
    
    if(extrafunction){
      extrafunction(input);
    }
    
    function addRelatedLinksAndNodes(path,type,n){
      me.originalData[path].forEach(function(d,i){
        var sourceIndex = null;
        var targetIndex = null;
        var nodeRef = null;
        for(var i=0; i< newdata.nodes.length; i++){
          if(d.source == newdata.nodes[i].originalIndex){
            sourceIndex = i;
            nodeRef = newdata.nodes[i];
          }
          if(d.target == newdata.nodes[i].originalIndex){
            targetIndex = i;
            nodeRef = newdata.nodes[i];
          }
          if(sourceIndex !== null && targetIndex !== null){
            newdata[path].push({source:sourceIndex,target:targetIndex,type:type});
            break;
          }
        }
        if(targetIndex === null && sourceIndex !== null){
          newdata[path].push({source:sourceIndex,target:newdata['nodes'].length,type:type});
          var newnode = depgraphlib.clone(me.originalData['nodes'][d.target]);
          if(nodeRef){
            var offset = positionNodeAround(n,100);
            n++;
            newnode.x = nodeRef.x + offset.x;
            newnode.y = nodeRef.y + offset.y;
          }
          if(type == "parent"){
            newnode.relation = "parent";  
          }else if(type == "ressource"){
            newnode.relation = "consumer";
          }
          newdata['nodes'].push(newnode);
        }else if(targetIndex !== null && sourceIndex === null){
          newdata[path].push({source:newdata['nodes'].length,target:targetIndex,type:type});
          var newnode = depgraphlib.clone(me.originalData['nodes'][d.source]);
          if(nodeRef){
            var offset = positionNodeAround(n,100);
            n++;
            newnode.x = nodeRef.x + offset.x;
            newnode.y = nodeRef.y + offset.y;
          }
          if(type == "parent"){
            newnode.relation = "child";
          }else if(type == "ressource"){
            newnode.relation = "provider";
          }
          newdata['nodes'].push(newnode);
        }
      });
      return n;
    }
    
    function addRelatedLinks(path,type){
      me.originalData[path].forEach(function(d,i){
        var sourceIndex = null;
        var targetIndex = null;
        for(var i=0; i< newdata.nodes.length; i++){
          if(d.source == newdata.nodes[i].originalIndex){
            sourceIndex = i;
          }
          if(d.target == newdata.nodes[i].originalIndex){
            targetIndex = i;
          }
          if(sourceIndex !== null && targetIndex !== null){
            newdata[path].push({source:sourceIndex,target:targetIndex,type:type});
            break;
          }
        }
      });
    }
  };

  function positionNodeAround(n,r){
    var positionOffset = {x:0,y:0};
    var p = Math.floor(n/2);
    var i = 0;
    while(p!=0){
      p=Math.floor(p/2);
      i++;
    }
    var d = Math.pow(2,i);
    var j = n%4;
    var radianOffset = 0;
    var base = 3.14*(1+2*(Math.floor((n-d)/4)))/d;
    if(j==0){
      radianOffset = 0;
    }else if(j==1){
      radianOffset = 1;
    }else if(j==2){
      radianOffset = -1/2;
    }else{
      radianOffset = 3/2;
    }
    var radianPosition = base + 3.14*radianOffset;

    if(n==2){
      radianPosition = 3.14/2;
    }else if(n==3){
      radianPosition = 3.14*3/2;
    }

    positionOffset.x = r*Math.cos(radianPosition);
    positionOffset.y = r*Math.sin(radianPosition);
    return positionOffset;
  }
  
  depgraphlib.ClassOverview.prototype.showOnlySimilar = function(entrystring){
    var me = this;
    var regex = new RegExp(entrystring,"gim"); 
    this.showOnly(entrystring,function(input,node){
      return regex.test(node.name);
    },function(input){
      if(input !== ''){
        me.showallresources = true;
        me.vis.selectAll('.ressource').attr('class','link ressource show');
      }else{
        me.showallresources = false;
        me.vis.selectAll('.ressource').attr('class','link ressource');
      }
    });
  };
  
  depgraphlib.ClassOverview.prototype.showOnlyStrict = function(entrystring){
    this.showOnlySimilar('^'+entrystring+'$');
  };
  
  depgraphlib.ClassOverview.prototype.showOnlyList = function(list){
    this.showOnly(list,function(input,node){
      for(var i=0;i<input.length;i++){
        if(input[i]==node.name){
          return true;
        }
      }
      return false;
    });
  };

  depgraphlib.ClassOverview.prototype.reset = function(data){
    var me = this; 
    
    this.data = data;
    this.vis.selectAll('.link').remove();
    this.vis.selectAll('.node').remove();
    
    var links = this.vis.selectAll(".link"),
    nodes = this.vis.selectAll(".node");
    
    var force = d3.layout.force()
    .size([this.width, this.height])
    .charge(-400)
    .linkDistance(40)
    .on("tick", tick);
    
    var drag = force.drag();

    drag.on("dragstart", dragstart);
    
    force
    .nodes(data.nodes)
    .links(data.links)
    .start();

    /*setTimeout(function(){
      force.stop();
      me.fitScaleToContainerSize();
      ajaxLoader(false);
      }
    ,data.nodes.length*1000);
    */
  
    

    function tick() {
      var alpha = force.alpha();
      if(alpha < 0.07){
        force.stop();
        me.fitScaleToContainerSize();
        ajaxLoader(false);
      }
      links.attr("x1", function(d) { return d.source.x; })
          .attr("y1", function(d) { return d.source.y; })
          .attr("x2", function(d) { return d.target.x; })
          .attr("y2", function(d) { return d.target.y; });
    
      nodes.attr("transform", function(d) { return 'translate('+d.x+","+d.y+')'; });
      
    }


    
    function dragstart(d) {
      d.fixed = true;
      d3.select(this).classed("fixed", true);
    }
    
    data.ressourcelinks = data.ressourcelinks || []; 
      
    data.ressourcelinks.forEach(function(d,i){
      d.source = data.nodes[d.source];
      d.target = data.nodes[d.target];
    });
    
      
    var alllinks = data.links.concat(data.ressourcelinks);
    
    links = links.data(alllinks);
    
    links.enter().append("line")
        .attr('class',function(d){return "link " + (d.type || '');})
        .attr('marker-end',function(d){if(d.type == 'parent'){return 'url(#arrow-end)';}else{return null;}})
        .attr('marker-start',function(d){if(d.type == 'ressource'){return 'url(#arrow-start)';}else{return null;}});
    
    links.exit().remove();
    
    nodes = nodes.data(data.nodes);
    var nodesgroup = nodes.enter()
      .append('g').attr("class", function(d){
        var relation ="";
        if(d.name==me.options.frmg_class){
          relation=" node-main";
        }else{
          relation = " node-"+d.relation;
        }
        return "node"+relation;}
      );
    
    nodes.exit().remove();
    
    nodesgroup.append("circle")      
    .attr("r", 12)
    .call(drag)
    .on('mouseover',nodemouseover)
    .on('mouseout',nodemouseout)
    .on('click',nodemouseclick);
    
    nodesgroup.append('text').text(function(d){return d.name;})
      .on("click",function(d){
        var dir = "class";
        /*if(d.relation == "consumer" || d.relation == "provider"){
          dir = "resource";
        }*/
        window.location.href=me.options.baseUrl+"/mgdata/"+dir+"/"+d.name;
        });
    
    
    this.svg.call(d3.behavior.zoom().on("zoom", redraw));
    
    var vis = this.vis;
    var frame = this.frame;
    
    function redraw(){
      frame.attr("transform",
          "translate(" + d3.event.translate + ")"
          + " scale(" + d3.event.scale + ")");
    }
    
    function setNodeMaterial(d,i){
      var node = d3.select(this);
      
      node.append('text').text(d.name);
    }
    
    
    
    function nodemouseover(d,i){
      if(!me.showallresources){
        vis.selectAll('.ressource').attr('class',function(ld,li){
          if(ld.source.index == i || ld.target.index == i){
            return 'link ressource show';
          }else{
            return 'link ressource';
          }
        });
      }
     }
    
    function nodemouseout(d,i){
      if(!me.showallresources){
        vis.selectAll('.ressource').attr('class',function(ld,li){
            return 'link ressource';
          }
        );
      }
    }
    
    function nodemouseclick(d,i){
      //me.showOnlyStrict(d.name);
      var dir = "class";
      /*if(d.relation == "consumer" || d.relation == "provider"){
        dir = "resource";
      }*/
      window.location.href=me.options.baseUrl+"/mgdata/"+dir+"/"+d.name;
    }
  


  };

  function ajaxLoader(state){
    if(state){
      jQuery('#classoverview-ajax-loader').show();
    }else{
      jQuery('#classoverview-ajax-loader').hide();
    }
  }
  
  depgraphlib.ClassOverview.prototype.fitScaleToContainerSize = function(){
    var me = this;
    var bbox = me.vis.node().getBBox();
    var wscale = me.width/(bbox.width+20);
    var hscale = me.height/(bbox.height+20);
    var scale = Math.min(wscale,hscale);
    me.frame.attr('transform',null);
    var offset = {x:me.width/2-bbox.width*scale/2,y:me.height/2-bbox.height*scale/2};
    me.vis.attr('transform','translate('+(offset.x-bbox.x*scale)+','+(offset.y-bbox.y*scale)+') scale('+scale+')');
  };
  
}(window.depgraphlib = window.depgraphlib || {}));