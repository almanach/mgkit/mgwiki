<?php 

$basepath = drupal_get_path('module','d3js') . "/templates/";
drupal_add_js($basepath . "treeview/d3js-tree-view.js");
drupal_add_css($basepath . "treeview/tree_view.css");
drupal_add_library('system', 'ui');
module_load_include("inc", "d3js", "templates/treeview/d3js-tree-view");

$uid = uniqid();
$ajson = array();
$data = new SimpleXMLElement($data['data']);
$nodeInfo = new DOMDocument();
$debugData = buildJSONTree($uid, $data->node ,$nodeInfo,$ajson);
$json_data = json_encode($ajson);
$options['treeHeight']=count($debugData);
$options['maxNodePerDepth']=max($debugData);
/*$options['miniature']=true;
$options['height']='100px';
$options['width']='100px';*/
$json_options = json_encode($options);

if(isset($options['link']) && $options['link']){
  echo '<a class="colorbox-inline" href="?inline=true#d3js-panel">View graph</a>
    <div id="shadowbox-content" style="display:none;">
  ';
}

?>

<div style="display: none;">
 <div id="node-info-<?php echo $uid;?>">
  <?php 
  $nodeInfo;
  $str = $nodeInfo->saveHTML();
  $str = str_replace("\n", "", $str);
  echo $str;
  ?>
 </div>
 <div id="ht-content-<?php echo $uid;?>" class="ht-content">
  <?php 
  if(isset($data->ht)){
  	    $xslt_xml = new DOMDocument();
  	    $xslt_xml->load(dirname(__FILE__) . "/tree_meta_info.xsl");
  	    $xslt = new XSLTProcessor();
  	    $xslt->importStylesheet($xslt_xml);
  	    $formatedHTML = $xslt->transformToXml($data->ht);
  	    $html = explode("\n", $formatedHTML);
  	    echo "'" . str_replace("'", "\\'", $html[1]) . "'";
  	}
  	?>
 </div>
</div>
<div id="graph-container-<?php echo $uid;?>"></div>
  <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="0" height="0">
    <defs>
      <linearGradient id="allguards" x1="0%" y1="100%" x2="100%" y2="0%">
        <stop offset="0%" style="stop-color:#fc4e4a; stop-opacity:1" />
        <stop offset="100%" style="stop-color:#33ff24;stop-opacity:1" />
      </linearGradient>
    </defs>
  </svg>

<script>
  var graphViewer = new depgraphlib.GraphViewer(jQuery("#graph-container-<?php echo $uid;?>"),"<?php echo $uid;?>");
  graphViewer.debugMode(true);
  var treeGraph = new treegraphlib.TreeGraph(graphViewer,<?php echo $json_data ?>,<?php echo $json_options;?>);
  treeGraph.update(treeGraph.root);
</script>

<?php 

if(isset($options['link']) && $options['link']){
  echo '</div>';
}
?>
