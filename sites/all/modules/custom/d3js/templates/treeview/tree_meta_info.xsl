<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:strip-space elements="*"/>

<xsl:template match="/">
  <xsl:for-each select="//ht">
   <xsl:apply-templates select="."/>
   <br/>
  </xsl:for-each>
</xsl:template>

<xsl:template match="narg" mode="narg">
  <tr class="fval">
    <td class="feature">
      <xsl:value-of select="@type"/>
    </td>
    <td class="fvalue">
      <xsl:apply-templates/>
    </td>
  </tr>
</xsl:template>

<xsl:template match="fs">
  <table class="fs">
    <xsl:apply-templates select="f"/>
  </table>
</xsl:template >

<xsl:template match="fs/f">
  <tr class="fval">
    <td class="feature">
      <xsl:value-of select="@name"/>
    </td>
    <td class="fvalue">
      <xsl:apply-templates/>
    </td>
  </tr>
</xsl:template >

<xsl:template match="sym|symbol">
  <xsl:value-of select="@value"/>
</xsl:template >

<xsl:template match="var">
  <table class="var">
    <td class="varname">
      <xsl:value-of select="@name"/>
    </td>
  </table>
</xsl:template>

<xsl:template match="var[./*]">
  <table class="var varwithvalue">
    <td class="varname">
      <xsl:value-of select="@name"/>
    </td>
    <td class="fvalue varvalue">
      <xsl:apply-templates/>
    </td>
  </table>
</xsl:template>

<xsl:template match="vAlt">
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="not(position()=last())"><span class="vAlt">|</span></xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template match="not">
  <span class="notop">~</span>
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="plus">
  <xsl:text>+</xsl:text>
</xsl:template>

<xsl:template match="minus">
  <xsl:text>-</xsl:text>
</xsl:template>

<xsl:template match="*[@id and guards]" mode="guard">
    <td>
      <div id="guard{generate-id(.)}">
  <table  class="guards">
    <caption>
      <xsl:value-of select="@id"/>
    </caption>
    <td>
      <xsl:apply-templates select="guards" mode="guard"/>
    </td>
  </table>
      </div>
  </td>
</xsl:template>

<xsl:template match="guards[@rel='+']" mode="guard">
  <table class="pguard">
    <xsl:apply-templates mode="guard"/>
  </table>
</xsl:template>

<xsl:template match="guards[@rel='-']" mode="guard">
  <table class="nguard">
    <xsl:apply-templates mode="guard"/>
  </table>
</xsl:template>

<xsl:template match="guard" mode="guard">
  <tr>
    <td>
      <xsl:apply-templates select="*[1]"/>
    </td>
    <td>
      =
    </td>
    <td>
      <xsl:apply-templates select="*[2]"/>
    </td>
  </tr>
</xsl:template>

<xsl:template match="xguard" mode="guard">
  <tr class="xguard">
    <xsl:for-each select="*">
      <td>
        <xsl:apply-templates select="."/>
      </td>
    </xsl:for-each>    
  </tr>
</xsl:template>


<xsl:template match="and" mode="guard">
  <xsl:apply-templates mode="guard"/>
</xsl:template>

<xsl:template match="or" mode="guard">
  <xsl:apply-templates mode="guard"/>
</xsl:template>

<xsl:template match="or[count(*) > 1]" mode="guard">
  <tr>
    <td> OR </td>
    <td colspan="2">
      <table class="orguard">
        <xsl:for-each select="*">
          <tr>
            <td>
              <table class="orblock">
                <xsl:apply-templates select="." mode="guard"/>
              </table>
            </td>
          </tr>
        </xsl:for-each>
      </table>
    </td>
  </tr>
</xsl:template>


</xsl:stylesheet>
