(function(mgwiki,$){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.SearchResult = function($el,settings){
    var config = settings || {};
    this.ws_url = config.ws_url;
    this.proxy_url = config.proxy_url;
    this.targetCorpus = config.targetCorpus;
    this.data = config.data || {};
    this.query = config.query || {};
    this.app = config.app;
    this.from = 0;
    this.finished = false;
    this.total = getTotal(this.data);
    this.e_total = 0;
    this.max = mgwiki.cma.SearchService.max;
    this.view = new mgwiki.cma.SearchResultView(this);
    $el.prepend(this.view.$el);
    this.view.render();
    this.fetchResult(0);
  };

  function getTotal(data){
    return data.total;
  }

  mgwiki.cma.SearchResult.prototype.next = function(callbackSuccess,callbackFailure){
    this.goTo(Math.ceil(this.from/this.max)+1,callbackSuccess,callbackFailure);
  };

  mgwiki.cma.SearchResult.prototype.prev = function(callbackSuccess,callbackFailure){
    this.goTo(Math.ceil(this.from/this.max)-1,callbackSuccess,callbackFailure);
  };

  mgwiki.cma.SearchResult.prototype.fetchResult = function(index){
    var me = this;
    var interval = me.fetchInterval = setInterval(function(){fetchResult()}, 2000);
    
    function fetchResult(){
      var searchInfo = me.app.getSearchInfo(me.targetCorpus);
      var data = {
        query:me.query,
        max:me.max
      };
      if(searchInfo.cmaCorpus){
        data['cma-reloaded']=true;
      }
      $.ajax({
        type: 'POST', 
        url: me.proxy_url + searchInfo.url,
        data: data,
        dataType : 'json',
        success: function(data, textStatus, jqXHR) {
          console.log(data);
          if(data.error){
            me.app.errorLogger.log(data.message);
            clearInterval(interval);
          }else{
            if(me.from == 0){
              me.data = data;
            }
            if(data.total != me.total || data.e_total != me.e_total){
              me.total = getTotal(data);
              me.e_total = data.e_total;
              me.view.render();
            }
            if(data.status != 0){
              me.from = me.max*index;
              me.finished = true;
              me.view.render();
              clearInterval(interval);
              delete me.fetchInterval;
            }
          }
        },
        error: function(jqXHR, textStatus, errorThrown) {
          clearInterval(interval);
        }
      });
      
    }
  };

  mgwiki.cma.SearchResult.prototype.abortSearch = function(){
    var me = this;
    var searchInfo = me.app.getSearchInfo(me.targetCorpus);
    var data = {
      query:me.query,
      abort:true
    };
    if(searchInfo.cmaCorpus){
      data['cma-reloaded']=true;
    }
    $.ajax({
      type: 'POST', 
      url: me.proxy_url + searchInfo.url,
      data: data,
    });
  };

  mgwiki.cma.SearchResult.prototype.goTo = function(index,callbackSuccess,callbackFailure){
    var me = this;
    var searchInfo = me.app.getSearchInfo(me.targetCorpus);
    var data = {
      query:me.query,
      max:me.max,
      from:index*me.max
    };
    if(searchInfo.cmaCorpus){
      data['cma-reloaded']=true;
    }
    $.ajax({
      type: 'POST', 
      url: me.proxy_url + searchInfo.url,
      data: data,
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        console.log(data);
        if(data.error){
          me.app.errorLogger.log(data.message);
          callbackFailure.call();
        }else{
          me.from = me.max*index;
          me.data = data;
          me.view.render();
          callbackSuccess.call(me,data);  
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        callbackFailure.call(me,jqXHR);
      }
    });
  };


  mgwiki.cma.SearchResult.prototype.showGraph = function(sid,did,cname){
    var me = this;
    var divloader = '<div style="width:300px; height:150px;"><div class="cma-graph-loading"></div></div>';
    var width = $(window).width()-50;
    var height = $(window).height()-50;
    var box = (new depgraphlib.Box({closeButton:true,draggable:true})).defaultInit().open();
    
    
    box.setHeader(sid+"("+cname+"|"+did+")");
    $.ajax({
      type: 'POST', 
      url: me.proxy_url + "sc/"+cname+"/_graph",
      data: {
        sid:sid,
        depgraphsrc:true
      },
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        
        
        var uniqid = Date.now();
        var div = jQuery('<div><div id="graph-container-'+uniqid+'"></div></div>');
      
        box.resetContent();
        box.setContent(div);
        var depGraph = new depgraphlib.DepGraph(jQuery("#graph-container-"+uniqid),data,{"uid":uniqid,viewmode:"full",maxwidth:width});
        depGraph.wsurl = me.ws_url+"_graph";
        var viewer = depGraph.viewer;
        viewer.noBorders();

        var boxcontent = jQuery('.depgraphlib-box-content',box.object);
        var dgwidth = viewer.mainpanel.width();
        var dgheight = viewer.mainpanel.height();
        boxcontent.width(dgwidth+16);
        boxcontent.height(dgheight+16);
        boxcontent.resizable({
          stop:function(e,data){
            depGraph.setWidthLimitedViewMode(data.size.width-16,true);
            viewer.setHeight(data.size.height-46);
          }
        });

        var point = {x:width/2-dgwidth/2,y:height/2-dgheight/2};
        box.move(point);
      },
      error: function(jqXHR, textStatus, errorThrown) {
      }
    });
  };

  // save the query (call dpathlib forms)
  mgwiki.cma.SearchResult.prototype.save = function(){

  };

  mgwiki.cma.SearchResult.prototype.export = function(){
    var me = this;
    var searchInfo = me.app.getSearchInfo(me.targetCorpus);
    var data = {
      query:me.query,
      export:true
    };
    if(searchInfo.cmaCorpus){
      data['cma-reloaded']=true;
    }
    var postVal = {export:true,query:me.query};
    depgraphlib.windowOpenPost(data,me.proxy_url + searchInfo.url);
  };

  mgwiki.cma.SearchResult.prototype.destroy = function(){
    var me = this;
    if(me.fetchInterval){
      clearInterval(me.fetchInterval);
    }
    this.abortSearch();
    this.app.dismissResult(this);
  };
  
}(window.mgwiki = window.mgwiki || {},jQuery));