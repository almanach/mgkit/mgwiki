(function(mgwiki,$){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.CorpusListItemView = function(model,$el,settings){
    this.model = model;
    this.$el = $el || $('<div class="cma-corpus-item"></div>');      

  };

  mgwiki.cma.CorpusListItemView.prototype.render = function(){
    var me = this;
    this.$el.html(this.model.item.name);
    this.$el.on("click",function(){
      me.model.app.use(me.model.item);
    });
  }

  
}(window.mgwiki = window.mgwiki || {},jQuery));