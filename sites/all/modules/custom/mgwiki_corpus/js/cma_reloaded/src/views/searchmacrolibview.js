(function(mgwiki,$){
  

  mgwiki.cma.SearchMacroLibView = function(model,$el){
    this.model = model;
    this.$el = $el || $('<div></div>');
    this.initiated = false;
  };


  mgwiki.cma.SearchMacroLibView.template = '<div class="cma-macrolib-container">'+
    '<div class="cma-macrolib-header">'+
      // title
    '</div>'+
    '<div class="cma-macrolib-content">'+
    '</div>'+
    '<div class="cma-macrolib-actions">'+
      '<span class="cma-macrolib-insert"></span>'+
    '</div>'+
  '</div>';
  
}(window.mgwiki = window.mgwiki || {},jQuery));