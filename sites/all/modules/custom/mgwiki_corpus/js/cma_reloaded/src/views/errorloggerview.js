(function(mgwiki,$){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.ErrorLoggerView = function(model,$el){
    this.model = model;
    this.$el = $el || $('<div></div>');
  };

  mgwiki.cma.ErrorLoggerView.prototype.render = function(){
    if(this.model.messages.length){
      this.$el.prepend(this.model.messages[this.model.messages.length-1]);
    }
  };


  

  
}(window.mgwiki = window.mgwiki || {},jQuery));