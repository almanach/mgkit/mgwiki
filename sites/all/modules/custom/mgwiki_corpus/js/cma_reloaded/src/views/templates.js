(function(mgwiki){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.templates = {};

  
}(window.mgwiki = window.mgwiki || {}));