(function(mgwiki){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.SearchServiceView = function(model,$el,settings){
    this.model = model;
    this.$el = $el || jQuery('<div></div>');
    this.$el.append('<div style="width:100%; height:150px; text-align:top;">Loading Corpus, please wait.<div class="cma-graph-loading"></div></div>');
  };

  mgwiki.cma.SearchServiceView.prototype.render = function(){
    var me = this;
    this.$el.html(mgwiki.cma.SearchServiceView.template);

    var $options = this.$el.find('.cma-search-options');

    var corpusOption = 'Select corpus : <select name="type">';
    for(var i=0;i<this.model.corpusList.length;i++){
      var corpusInfo = this.model.corpusList[i];
      var selected = '';
      if(this.model.targetCorpus == corpusInfo.name){
        selected = ' selected'
      }
      corpusOption += '<option value="'+corpusInfo.name+'"'+selected+'>'+corpusInfo.name+'</option>';
    }
    corpusOption += '</select>';


    $options.append(corpusOption);

    $options.find('select').on('change',function(e){
      var corpus = e.target.value;
      me.model.targetCorpus = e.target.value;
      me.model.getTagset();
    });

    var helpDiv = 'Le format de requête est de la forme suivante :<br>'+
      'nodes|edges|root ((target|source|next|prev|out|in)* ([@attr="value"])*)+';
    var foldableDiv = new mgwiki.cma.ui.FoldableDiv(helpDiv);
    foldableDiv.setTitle('Help');
    $options.after(foldableDiv.view);

    var strategies = this.autocompleteStrategies();
    var textarea = this.textarea = this.$el.find('.cma-search-input');
    var parent = jQuery(this.$el);
    textarea.width(parent.width()-70);
    textarea.textcomplete(strategies);

    var submitButton = new mgwiki.cma.ui.CallbackButton("Submit search",{});
    $options.append(submitButton.view);
    submitButton.init(function(data){
      var button = this;
      var textarea = data.$el.find('.cma-search-input');
      var select = data.$el.find('.cma-search-options').find('select');
      me.model.targetCorpus = select[0].options[select[0].selectedIndex].value;
      me.model.search(textarea[0].textContent,function(){button.success();},function(){button.error();});
    },this);

    /*this.macroLib = new cma.toolbox.SearchLib(textarea);
    this.history = new cma.toolbox.SearchHistory(this);
*/
    jQuery('.cma-search-insert-macro',this.view).click(function(){
      me.model.searchlib.show();
    });

    jQuery('.cma-search-history',this.view).click(function(){
      me.model.history.show();
    });

    jQuery('.cma-search-visual',this.view).click(function(){
      me.visualSignatureForm();
    });

    jQuery('.cma-search-save-macro',this.view).click(function(){
      var textarea = me.$el.find('.cma-search-input');
      var val = textarea[0].textContent;
      me.model.searchlib.elementForm({query:val});
    });

  };


  mgwiki.cma.SearchServiceView.prototype.autocompleteStrategies = function(){
    var me = this;
    var strategies = [];
    strategies.push({
      match: /@(\w+)\s*=(")?(\w*)$/,
      index:3,
      search: function (term, callback) {
        var prop = this.currentMatch[1];
        var propVal = [];
        if(me.model.tagset && me.model.tagset.edge[prop]){
          propVal = me.model.tagset.edge[prop];
        }else if(me.model.tagset && me.model.tagset.node[prop]){
          propVal = me.model.tagset.node[prop];
        }
        
        callback(jQuery.map(propVal, function (element) {
                return element.indexOf(term) === 0 ? element : null;
            }));
      },
      replace: function (value) {
        return '@$1="' + value +'"';
      },
      cache: true,
      maxCount:99
    });
    strategies.push({
      match: /(\$)(\w*)$/,
      search: function (term, callback) {
        var macros = me.model.searchlib.macros;
        callback(jQuery.map(macros, function (element) {
                return element.indexOf(term) === 0 ? element : null;
            }));
      },
      replace: function (value) {
        return '$1' + value + ' ';
      },
      cache: true,
      maxCount:99
    });
    strategies.push({
      match: /(@)(\w*)$/,
      search: function (term, callback) {
        var props = me.model.props || [];
        callback(jQuery.map(props, function (element) {
                return element.indexOf(term) === 0 ? element : null;
            }));
      },
      replace: function (value) {
        return '$1' + value ;
      },
      cache: true,
      maxCount:99
    });

    return strategies;
  };

  mgwiki.cma.SearchServiceView.prototype.visualSignatureForm = function(){
    var me = this;
    var width = jQuery('body').width();
    var height = jQuery('body').height();
    var offset = jQuery('body').offset();

    var uniqid = Date.now();
    var div = jQuery('<div><div id="graph-container-'+uniqid+'"></div></div>');
  
    // Box || r.window => base sentence
    // data creation (+datamodel)
    
    var sentence = prompt("Please enter a sentence.","");

    if (sentence==null || sentence == ""){
      sentence =".";
    }

    var data = {
      graph:{
        words:[]
      }
    };


    var words = sentence.split(/\s+/);
    for (var i = 0 ; i < words.length ; i++) {
      data.graph.words.push({id:i+1,label:words[i]});
    };
    

    var box = new depgraphlib.Box({closeButton:true,draggable:true}).setContent(div).open();
    box.setHeader("Visual Query (over highlighted graph parts)");
    var depGraph = new depgraphlib.DepGraph(jQuery("#graph-container-"+uniqid),data,{"uid":uniqid,viewmode:"full"});
    var viewer = depGraph.viewer;
    depGraph.wsurl = this.model.settings.ws_url+"graph";
    if(depGraph.options.viewmode == 'full'){
      viewer.noBorders();
    }

    var boxcontent = jQuery('.depgraphlib-box-content',box.object);
    var dgwidth = viewer.mainpanel.width();
    var dgheight = viewer.mainpanel.height();
    boxcontent.width(dgwidth+16);
    boxcontent.height(dgheight+16);
    boxcontent.resizable({
      stop:function(e,data){
        depGraph.setWidthLimitedViewMode(data.size.width-16,true);
        viewer.setHeight(data.size.height-46);
      }
    });

    var point = {x:offset.left+width/2-dgwidth/2,y:50};
    box.move(point);

    depGraph.sentenceLink = '#';

    depGraph.sentence = 'sentence content here';
    depGraph.dataFormat = 'json';

    var pos = me.model.tagset.node["pos"] || me.model.tagset.node["cat"];
    var dataModel = {
      words:[
      {name:"label",view:'label'},
      {name:"lemma",view:"sublabel/0"},
      {name:"pos",values:pos,view:"sublabel/1"}
      ],
      links:[
      {name:"label",values:me.model.tagset.edge["label"],view:"label"},
      ],
    };

    depGraph.editObject.setDataModel(dataModel);
    

    viewer.setFixedToolbar();
    var formatTmp = (me.model.tagset.node["pos"])?"conll":"depxml";
    viewer.addToolbarItem({name:'export_signature',callback:function(){
      var ddata = depGraph.cleanData();
      jQuery.ajax({
        type: 'POST', 
        url: me.model.settings.ws_url+"graph",
        data: {
          action:'_getSig',
          data:ddata,
          format: formatTmp
        },
        dataType : 'text',
        success: function(data, textStatus, jqXHR) {
          console.log(data);
          var prev = me.textarea.html();
          me.textarea.html(prev + data);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          alert(textStatus);
        }
      });
    },tooltip:'Export Signature',style:'mgwiki-export','static':true});

    depGraph.editObject.setEditMode('default',false);
  };

  mgwiki.cma.SearchServiceView.template = '<table><tbody><tr><td><div class="cma-search-input" contenteditable=true></div><div class="cma-search-options"></div></td><td style="vertical-align:top;"><div class="cma-search-tools"><div class="cma-search-insert-macro"></div><div class="cma-search-save-macro"></div><div class="cma-search-visual"></div><div class="cma-search-history"></div></div></td></tr></tbody></table>';
  
}(window.mgwiki = window.mgwiki || {}));