(function(cma){
  
  cma.ui = cma.ui || {};

  cma.ui.ToolBox = function(){
    this.view = jQuery(cma.template.toolbox);
    this.toolboxitems = {};
    this.detachedBox = jQuery(cma.template.toolboxitem_outset);
    jQuery('body').append(this.detachedBox);
    var width = cma.TheApp.view.width();
    this.detachedBox.css('top',50);
    this.detachedBox.css('left',50);
    this.detachedBox.css('width',width-100);
    this.detachedBoxRef = null;
  };

  cma.ui.ToolBox.prototype.addBoxItem = function(name,content,title,options){
    options = options || {};
    this.toolboxitems[name] = new cma.ui.ToolBoxItem(name,this,options);
    this.toolboxitems[name].setContent(content);
    if(!title){
      title = name;
    }
    this.toolboxitems[name].setTitle(title);
    jQuery('.cma-toolboxitems-container',this.view).append(this.toolboxitems[name].view);

  };

  cma.ui.ToolBox.prototype.detachBoxItem = function(name){
    this.ratachBoxItem();

    var content = this.toolboxitems[name].view.children();
    this.toolboxitems[name].content.show();
    jQuery('.cma-toolbox-item-expand',content).hide();
    content.detach();
    this.detachedBox.append(content);
    this.detachedBoxRef = name;
    this.detachedBox.show();
  };

  cma.ui.ToolBox.prototype.ratachBoxItem = function(){
    if(this.detachedBoxRef){
      var prevContent = this.detachedBox.children();
      jQuery('.cma-toolbox-item-expand',prevContent).show();
      prevContent.detach();
      this.toolboxitems[this.detachedBoxRef].view.append(prevContent);
      if(!this.toolboxitems[this.detachedBoxRef].expanded){
        this.toolboxitems[this.detachedBoxRef].content.hide();
      }
      this.detachedBoxRef = null;
      this.detachedBox.hide();
    }
  }

  cma.ui.ToolBoxItem = function(name,toolboxcontainer,options){
    var me = this;
    this.name = name;
    this.toolboxcontainer = toolboxcontainer;
    this.view = jQuery(cma.template.toolboxitem_inset);
    this.view.attr("name",name);
    this.content = jQuery('.cma-toolboxitem-content',this.view);
    if(options.startOpen){
      this.expanded = true;
    }else{
      this.expanded = false;  
      this.content.hide();  
    }
    this.detached = false;

    jQuery('.cma-toolbox-item-detach',this.view).click(function(){
      if(me.detached){
        me.toolboxcontainer.ratachBoxItem();
      }else{
        me.toolboxcontainer.detachBoxItem(me.name);
      }
      me.detached = !me.detached;
    });
    
    jQuery('.cma-toolbox-item-expand',this.view).click(function(){
      if(me.expanded){
        me.content.hide();
      }else{
        me.content.show();  
      }
      me.expanded = !me.expanded;
    });
  }

  cma.ui.ToolBoxItem.prototype.setContent = function(content){
    this.content.html(content);
  }

  cma.ui.ToolBoxItem.prototype.setTitle = function(title){
    jQuery('.cma-toolbox-item-title',this.view).html(title);
  }
  
  
  
}(window.cma = window.cma || {}));