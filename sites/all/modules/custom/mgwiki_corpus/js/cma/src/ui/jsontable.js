(function(cma){

  cma.ui = cma.ui || {};

  cma.ui.JSONTable = function(data,style){
    this.view = jQuery('<table></table>');
    this.style = style || {};
    this.setData(data);
    return this.view;
  }



  cma.ui.JSONTable.prototype.update = function(data){
    var styleth = (this.style.th)?' class="'+this.style.th+'" ':"";
    var stylethr = (this.style.thr)?' class="'+this.style.thr+'" ':"";
    var styletr0 = (this.style.tr0)?' class="'+this.style.tr0+'" ':"";
    var styletr1 = (this.style.tr1)?' class="'+this.style.tr1+'" ':"";
    var styletd = (this.style.td)?' class="'+this.style.tr1+'" ':"";
    var headers = {};
    var k = 0;
    for(var i =0;i<data.length;i++){
      for(var name in data[i]){
        if(!headers[name]){
          headers[name]=k;
          k++;
        }
      }
    }

    var tableheader = '<tr'+stylethr+'>';
    for(var i in headers){
      tableheader += '<th'+styleth+'></th>';
    }
    tableheader += '</tr>';
    tableheader = jQuery(tableheader);
    var tableheadercells = tableheader.children();
    for(var i in headers){
      var cellIndex = headers[i];
      jQuery(tableheadercells[cellIndex]).html(i);
    }

    this.view.append(tableheader);
    for(var i =0;i<data.length;i++){
      var row = '<tr'+((i%2)?styletr1:styletr0)+'>';
      for(var k=0;k<ncols;k++){
        row += '<td'+styletd+'></td>';
      }
      row += '</tr>';
      row = jQuery(row);
      var cells = row.children();
      for(var name in data[i]){
        var cellIndex = headers[name];
        jQuery(cells[cellIndex]).html(data[i][name]);
      } 
      this.view.append(row);
    }
  }
  
  
}(window.cma = window.cma || {}));