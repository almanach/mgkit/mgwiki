(function(cma){
  
  cma.Log = function(message){
    cma.ui.get("cma-status-bar-content").html(message);
  }

  cma.utils = {};

  cma.utils.getTextareaCarretPosition = function(textarea){
    
  };

  /**
   * @function promise
   * 
   * @desc call a function when requested data are defined
   * @param callback the function to be called
   * @param requested_data array of requested data definition {object:object,field:string}
   * @param timeout (optional, default = 2000 ms) when to stop trying to wait for requested data to be defined
   * @param interval (optional, default = 200ms) interval whithin trials.
   * @memberof utils#
   */
  cma.utils.promise = function(callback,requested_data,timeout,interval){
    if(!interval){
      interval = 200;
    }
    if(timeout == undefined){
      timeout = 2000;
    }
    if(timeout<=0){
      return;
    }
    for(var i=0;i<requested_data.length;++i){
      if(requested_data[i].object[requested_data[i].field] == undefined){
        setTimeout(function() {cma.utils.promise(callback,requested_data,timeout-interval,interval)}, interval);
        return;
      }
    }
    callback.call();
  }
  
  
  
}(window.cma = window.cma || {}));