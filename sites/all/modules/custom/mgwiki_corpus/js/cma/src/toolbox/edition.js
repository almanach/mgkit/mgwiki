(function(cma){
  
  cma.toolbox = cma.toolbox || {};

  cma.toolbox.Edition = function(){
    var me = this;
    this.initView();
    cma.TheApp.websocketAddListener(
      "onmessage",
      "editBroadcast",
      function(data){
        var json = JSON.parse(data);
        if(json.type == "depgraphbroadcast"){
          if(cma.TheApp.corpus.doc.openedGraphs[json.id]){
            cma.TheApp.corpus.doc.openedGraphs[json.id].editObject.pushAction(json.action,true);
          }
        }else if(json.type == "saveNewRevisionsResult"){

        }
      }
    );
  }

  cma.toolbox.Edition.prototype.updateRevisionList = function(){
    var me = this;
    cma.TheApp.corpus.getAndDo('docRevs',{did:cma.TheApp.corpus.doc.did},function(data){
      console.log(data);
      me.revisionList.update(data);
    },function(){alert('error while fetching doc revs')},'json');
  }
  
  cma.toolbox.Edition.prototype.initView = function(){
    var me = this;
    this.view = jQuery(cma.toolbox.Edition.templateMain);

    this.revisionList = new cma.ui.Table({headers:[
      //{title:"rID",nameid:"_id",format:formatRID,unformat:unformatRID},
      {title:"date",nameid:"timedate",format:formatDate,unformat:unformatDate},
      {title:"name",nameid:"title"},
      {title:"user",nameid:"author"},
      {title:"tags",nameid:"tags",format:formatTAGs,unformat:unformatTAGs},
    ]});

    cma.ui.get('cma-edit-revisions',this.view).html(this.revisionList.view);

    this.editSaveButton = new cma.ui.CallbackButton("Save New Revision");
    cma.ui.get('cma-edit-save-submit',this.view).append(this.editSaveButton.view);
  }
  
  cma.toolbox.Edition.prototype.init = function(){
    var me = this;

    this.updateRevisionList();


    cma.ui.get('cma-edit-submit').click(function(){
      var val = cma.ui.get('cma-edit-input').val();
      var message = {data:val,authkey:cma.TheApp.authkey};
      cma.TheApp.socket.send(JSON.stringify(message));
    });
    cma.TheApp.corpus.permissions.haveRights('edit',function(){
    },function(){
      cma.ui.get('cma-edit-mode-switch').hide();
      cma.ui.get('cma-edit-input').hide();
      me.editSaveButton.view.hide();
    });
    cma.ui.get('cma-edit-mode-switch').click(function(){
      var button  = jQuery(this);
      cma.TheApp.corpus.permissions.haveRights('edit',function(){
        if(cma.TheApp.corpus.doc.editMode){
          if(cma.TheApp.corpus.doc.stopEditMode()){
            button.removeClass("cma-edit-mode-on").addClass("cma-edit-mode-off")
            button.html('<div style="padding-top:7px">off</div>');  
          }
        }else{
          if(cma.TheApp.corpus.doc.startEditMode()){
            var message = {authkey:cma.TheApp.authkey};
            cma.TheApp.socket.send(JSON.stringify(message));
            button.removeClass("cma-edit-mode-off").addClass("cma-edit-mode-on");
            button.html('<div style="padding-top:7px">on</div>'); 
          }
        }
      },function(){
        alert("you don't have the rights to edit documents in this corpus!");
      });
    });

    this.editSaveButton.init(function(button,caller){
      var name = cma.ui.get('cma-edit-input',caller.view).val();
      cma.TheApp.callProxy('corpus/'+cma.TheApp.corpus.cid+'/document/'+cma.TheApp.corpus.doc.did+'/_save',{name:name},function(data){
        if(data.success){
          cma.ui.get('cma-edit-input',caller.view).val("");
          button.success();
          caller.updateRevisionList();
        }else{
          button.error();
        }
      },function(){
        button.error();
      },'json');
    },this);
  }

  cma.toolbox.Edition.templateMain = '<div id="cma-tmp">'+
    '<div id="cma-edit-mode-switch" class="cma-edit-mode-off"><div style="padding-top:7px">off</div></div>'+
    '<div id="cma-edit-revisions"></div>'+
    '<input id="cma-edit-input" type="text">'+
    '<span id="cma-edit-save-submit" ></span>'+
    '</div>';

  function formatDate(name,value,data){
    return (new Date(value)).toString();
  }

  function unformatDate(value){
    return (new Date(value)).getTime();
  }

  function formatTAGs(name,value,data){
    if(value)
      return value.join(' | ');
    return "";
  }

  function unformatTAGs(value){
    if(value)
      return value.split('|');
    return null;
  }

}(window.cma = window.cma || {}));