(function(cma){
  
  cma.Document = function(did){
    this.did = did;
    this.editMode = false;
    this.openedGraphs = {};
  }

  cma.Document.prototype.initView = function(filename){
    var cmaContentContainer = cma.ui.get('cma-content');
    var height = cma.ui.get('cma-app-container').height();
    //cmaContentContainer.height(height-100);

    // set the title
    cma.AppView.setTitle('<h1>'+cma.TheApp.corpus.name+' | '+filename+'</h1>');
    
    var layout = [];
    var row = [{name:"firstCol",width:80},{name:"secondCol",width:15}];
    layout.push(row);
    cma.AppView.setMainLayout(layout);

    this.document = jQuery('<div id="cma-document"></div>');
    cma.AppView.getMainLayoutCell(0,0).html(this.document);
    this.document.after(cma.Document.templateLegend);

    this.initToolBox();

    

  }

  cma.Document.prototype.init = function(data){
    var me = this;
    var height = cma.TheApp.view.height();
    this.document.height(height - 220);
    this.document.html(data);

    jQuery(".cma-sentence",this.document).on("click",function(){
      if(!me.preventShowGraph){
        me.showGraph(me.did,jQuery(this).attr("id"));  
      }
    });
    jQuery(this.document).ready(function(){
      jQuery(me.document).bind("mouseup", function(event){
        
        var text = me.getSelectionText();
        if(text != ''){
          me.showFragment(event,text,me.did);
          me.preventShowGraph = true;
        }else{
          me.preventShowGraph = false;
        }
      });
    });
    
    var def = {
      "infos":{
        menu:"Infos",
        click:function(element){
          var infos = jQuery('<div />').text(element.attr('infos')).html();
          var width = cma.TheApp.view.width();
          var height = cma.TheApp.view.height();
          var offset = cma.TheApp.view.offset();
          var point = {x:offset.left+width/2-400,y:offset.top+height/2-200};
          var box = new depgraphlib.Box({closeButton:true,draggable:true}).setContent(infos).open(point);
        }
      },
      "revisions":{
        menu:"Revisions",
        click:function(element){
          me.showGraphRevisions(element.attr('id'));
          console.log(element);
          //alert('show panel with revisions list for this sentence');
        }
      },
    };
    /*for(menu in this.mode[this.editMode].onLinkContext){
      def[menu] = {
        menu : menu,
        click: function(element) {  // element is the jquery obj clicked on when context menu launched
          onContextClick('onLinkContext',this,element);
        },
        klass: "menu-item-1" // a custom css class for this menu item (usable for styling)
      };
    }*/
    jQuery(".cma-sentence",this.document).contextMenu('sentence-context-menu', def);

    
  }

  cma.Document.prototype.initToolBox = function(){

    var me = this; 
    var cell = cma.AppView.getMainLayoutCell(0,1);

    var toolboxobj = new cma.ui.ToolBox();
    cell.html(toolboxobj.view);

    var search = new cma.toolbox.Search();
    var edition = this.edition = new cma.toolbox.Edition();

    toolboxobj.addBoxItem('document-search',search.view,'Search',{startOpen:true});
    toolboxobj.addBoxItem('document-edition',edition.view,'Edition',{startOpen:true});
    
    search.init(this.did);
    edition.init();

    

  };

  cma.Document.prototype.showGraphRevisions = function(id){
    var me = this;
    var html = "<ul>";
    html += '<li ref="'+id+'">0</li>';
    var revisions = this.edition.revisionList.originalData;
    for(var i in revisions){
      var rev = revisions[i];
      for(var change in rev.state){
        if(change==id){
          html += '<li ref="'+change+'">'+rev.state[id]+'</li>';
        }
      }
    }
    html += "</ul>";

    var div = jQuery(html);
    jQuery('li',div).click(function(element){
      if(!me.preventShowGraph){
        me.showGraph(me.did,jQuery(this).attr("ref"),jQuery(this).html());  
      }
    })

    var width = cma.TheApp.view.width();
    var height = cma.TheApp.view.height();
    var offset = cma.TheApp.view.offset();
    var point = {x:offset.left+width/2-400,y:offset.top+height/2-200};
    var box = new depgraphlib.Box({closeButton:true,draggable:true}).setContent(div).open(point);
  }


  cma.Document.prototype.displayGraph = function(data,id,box){
    var me = this;
    var width = cma.TheApp.view.width();
    var height = cma.TheApp.view.height();
    var offset = cma.TheApp.view.offset();
    
    var uniqid = Date.now();
    var div = jQuery('<div><div id="graph-container-'+uniqid+'"></div></div>');
  
    if(!box){
      box = new depgraphlib.Box({closeButton:true,draggable:true}).setContent(div).open();
    }else{
      box.resetContent().setContent(div);
    }
    box.setHeader(id);
    var depGraph = new depgraphlib.DepGraph(jQuery("#graph-container-"+uniqid),data,{"uid":uniqid,viewmode:"full",maxwidth:cma.TheApp.view.width()-100});
    var viewer = depGraph.viewer;
    depGraph.wsurl = cma.TheApp.ws_url+"graph";
    if(depGraph.options.viewmode == 'full'){
      viewer.noBorders();
    }

    var boxcontent = jQuery('.depgraphlib-box-content',box.object);
    var dgwidth = viewer.mainpanel.width();
    var dgheight = viewer.mainpanel.height();
    boxcontent.width(dgwidth+16);
    boxcontent.height(dgheight+16);
    boxcontent.resizable({
      stop:function(e,data){
        depGraph.setWidthLimitedViewMode(data.size.width-16,true);
        viewer.setHeight(data.size.height-46);
      }
    });

    var point = {x:width/2-dgwidth/2,y:height/2-dgheight/2};
    box.move(point);



    viewer.addToolbarItem({name:'saveInMgwiki',callback:function(){
        var ddata = depGraph.cleanData();
        jQuery.ajax({
          type: 'POST', 
          url: cma.TheApp.ws_url+"graph",
          data: {
            format:'json',
            action:'mgwiki-import',
            options: '',
            data:ddata,
          },
          dataType : 'json',
          success: function(data, textStatus, jqXHR) {
            alert("Successfully imported to mgwiki.")
            console.log(data);
          },
          error: function(jqXHR, textStatus, errorThrown) {
            alert(textStatus);
          }
        });
      },style:'mgwiki-export','static':true});
    

    if(this.editMode){
      depGraph.sentenceLink = '#';

      depGraph.editObject.mode['default'].save = depgraphlib.EditObject.DefaultModeLib.save;
      depGraph.dataFormat = 'json';
      viewer.setFixedToolbar();
      if(cma.TheApp.corpus.graphFormat == 'depxml'){
        frmgEditMode = new depgraphlib.FRMGEditMode('frmgserver_url_here');
        depGraph.editObject.addEditMode(frmgEditMode.mode);
        depGraph.editObject.setEditMode('frmg');
        depGraph.editObject.addEditModeSwitcher();
      }else{
        depGraph.editObject.setEditMode('default');
      }



      depGraph.editObject.mode[depGraph.editObject.editMode].broadcast = function(action){
        var message = {type:"depgraphbroadcast",id:id,action:action};
        cma.TheApp.socket.send(JSON.stringify(message));
      };

      // @todo : pb server side => convert to conll everytime
      depGraph.editObject.mode[depGraph.editObject.editMode].save = function(depgraph){
        var ddata = depgraph.cleanData();
        jQuery.ajax({
          type: 'POST', 
          url: cma.TheApp.ws_url+"graph",
          data: {
            action:'save',
            format:depgraph.dataFormat,
            options: '',
            gid:id,
            cid:cma.TheApp.corpus.cid,
            did:cma.TheApp.corpus.doc.did,
            data:ddata,
            uid:depgraph.options.uid
          },
          dataType : 'json',
          success: function(data, textStatus, jqXHR) {
            depgraph.editObject.lastSavedPtr = depgraph.editObject.currentPtr;
            depgraph.editObject.needToSaveState = false;
            depgraph.editObject.updateSaveState();
          },
          error: function(jqXHR, textStatus, errorThrown) {
            alert(textStatus);
          }
        });
      }



      point.x = offset.left+width/2-viewer._width/2;
      point.y = offset.top+height/2-viewer._height/2;
      box.move(point); 

           
    }

    box.options.onClose = function(){
        me.openedGraphs[id] = null;
      }

    return depGraph;
  };

  cma.Document.prototype.showGraph = function(did,id,vid){
    var me = this;
    var box = new depgraphlib.Box({closeButton:true,draggable:true}).defaultInit().open();
    

    cma.TheApp.callProxy('sc/'+did+'/_graph',{sid:id,depgraphsrc:true},function(data){
      var depGraph = me.displayGraph(data,id,box);
      me.openedGraphs[id] = depGraph;
    });
  };


  cma.Document.prototype.showFragment = function(event,textSelection,did){
    var me = this;

    var tsInfo;
    var teInfo;
    var slist = [];

    var aTokens = getToken(textSelection.anchorNode,textSelection.anchorOffset);
    var fTokens = getToken(textSelection.focusNode,textSelection.focusOffset);


    var relativePosition = aTokens.prevToken.compareDocumentPosition( fTokens.prevToken );
    if(relativePosition == 2){
      tsInfo = {tid:jQuery(fTokens.nextToken).attr('ref'),sid:fTokens.nextToken.parentNode.id,token:fTokens.nextToken};
      teInfo = {tid:jQuery(aTokens.prevToken).attr('ref'),sid:aTokens.prevToken.parentNode.id,token:aTokens.prevToken};
    }else if(relativePosition == 4){
      tsInfo = {tid:jQuery(aTokens.nextToken).attr('ref'),sid:aTokens.nextToken.parentNode.id,token:aTokens.nextToken};
      teInfo = {tid:jQuery(fTokens.prevToken).attr('ref'),sid:fTokens.prevToken.parentNode.id,token:fTokens.prevToken};
    }else{
      alert("error");
      return;
    }

    var sStart = tsInfo.token.parentNode;
    var sEnd = teInfo.token.parentNode;
    var sentence = sStart;
    slist.push(sentence.id);
    while(sentence.id!=sEnd.id){
      sentence = sentence.nextSibling;
      while(!jQuery(sentence).hasClass("cma-sentence")){
        sentence = sentence.nextSibling;
      }
      slist.push(sentence.id);
    }
    

    console.log(tsInfo);
    console.log(teInfo);
    console.log(slist);
    var box = new depgraphlib.Box({closeButton:true,draggable:true}).defaultInit().open();
    cma.TheApp.corpus.getAndDo("fragment",{ts:tsInfo.tid,te:teInfo.tid,slist:slist,did:did},function(data){
        me.displayGraph(data,slist,box);

      },null);


    function getToken(node,offset){
      if(node.id == "cma-document"){
        node = node.childNodes[offset];
      }
      if(node.parentNode.id == "cma-document"){ // case outside a sentence
        var nextNode = node;
        while(!jQuery(nextNode).hasClass("cma-sentence")){
          nextNode = nextNode.nextSibling;
        }
        var nextToken;
        for(var i=0;i<nextNode.childNodes.length;i++){
          if(nextNode.childNodes[i].nodeName.toUpperCase()=="TOKEN"){
            nextToken = nextNode.childNodes[i];
            break;
          }
        }

        var prevNode = node;
        while(!jQuery(prevNode).hasClass("cma-sentence")){
          prevNode = prevNode.previousSibling;
        }
        var prevToken;
        for(var i=prevNode.childNodes.length-1;i>=0;i--){
          if(prevNode.childNodes[i].nodeName.toUpperCase()=="TOKEN"){
            prevToken = prevNode.childNodes[i];
            break;
          }
        }
        return {prevToken:prevToken,nextToken:nextToken};
      }else if(jQuery(node.parentNode).hasClass("cma-sentence")){ // case token or random node (br txtnode etc..)
        var nextToken = node;
        while(nextToken.nodeName.toUpperCase()!="TOKEN"){
          nextToken = nextToken.nextSibling;
        }
        var prevToken = node;
        while(prevToken.nodeName.toUpperCase()!="TOKEN"){
          prevToken = prevToken.nextSibling;
        }
        return {prevToken:prevToken,nextToken:nextToken};
      }else{ // case txtnode inside token
        return {prevToken:node.parentNode,nextToken:node.parentNode};
      }
    }
              
  }

  cma.Document.prototype.getSelectionText = function() {
    var t = '';
    if(window.getSelection){
      t = window.getSelection();
    }else if(document.getSelection){
      t = document.getSelection();
    }else if(document.selection){
      t = document.selection.createRange().text;
    }
    return t;
 };


  cma.Document.prototype.startEditMode = function(){
    if(!cma.TheApp.websocket_enabled){
      alert('web socket are not enabled. please use a more recent browser version');
      return;
    }

    

    this.editMode = true;
    return true;
  };

  cma.Document.prototype.stopEditMode = function(){
    if(depgraphlib.Box.instances.length>0){
      alert("close all graph windows before");
      return false;
    }
    /*for(var i=0;i< depgraphlib.Box.instances.length;i++){
      depgraphlib.Box.instances[i].close(true);
    }
    var r=confirm("Press a button");
        if (r==true)
          {
          x="You pressed OK!";
          }
        else
          {
          x="You pressed Cancel!";
          }*/

    this.editMode = false;
    return true;
  };

  cma.Document.templateLegend = '<div id="cma-document-legend">Legend : '+
    '<span class="cma-document-legend-item"><span style="color:red;">red</span> : failed parse</span>'+
    '<span class="cma-document-legend-item"><span style="color:blue;">blue</span> : partial parse</span>'+
    '<span class="cma-document-legend-item"><span style="border-bottom:1px dotted black;">underlined</span> : edited sentence</span>'+
    '</div>';
  
}(window.cma = window.cma || {}));
