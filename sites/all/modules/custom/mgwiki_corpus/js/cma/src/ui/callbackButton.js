(function(cma){
  
  cma.ui = cma.ui || {};

  cma.ui.CallbackButton = function(name,options){
    var style = "";
    if(options && options.style){
      style = options.style;
    }
    this.view = jQuery('<span><input type="button" '+style+' value="'+name+'"><div class="cma-throbber hidden"></div></span>');
  }



  cma.ui.CallbackButton.prototype.init = function(callback,data){
    var me = this;
    jQuery('input',this.view).click(function(){
      var throbber = jQuery('.cma-throbber',me.view);
      if(!throbber.is(':hidden')){
        return;  
      }
      throbber.removeClass('cma-throbber-success');
      jQuery('.cma-throbber',me.view).removeClass('cma-throbber-error');
      jQuery('.cma-throbber',me.view).show();
      jQuery('.cma-throbber',me.view).css('display','inline-block');
      callback.call(null,me,data);
    });
  }

  cma.ui.CallbackButton.prototype.success = function(){
    jQuery('.cma-throbber',this.view).addClass('cma-throbber-success');
    jQuery('.cma-throbber',this.view).fadeOut(2000);
  }

  cma.ui.CallbackButton.prototype.error = function(){
    jQuery('.cma-throbber',this.view).addClass('cma-throbber-error');
    jQuery('.cma-throbber',this.view).fadeOut(2000);
  }

  
  
}(window.cma = window.cma || {}));