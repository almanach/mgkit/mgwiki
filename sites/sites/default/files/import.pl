 #!/usr/bin/perl

use strict;
use warnings;
use AppConfig;

use Cwd 'abs_path';

my $config = AppConfig->new('format=s',
                              'folder=f');

$config->args;

my $directory = abs_path($config->folder);

print $config->format;
print $directory;

opendir (DIR, $directory) or die $!;

while (my $file = readdir(DIR)) {
  print "$file\n";
}

closedir(DIR);


use LWP::UserAgent;
 
my $ua = LWP::UserAgent->new;
 
my $server_endpoint = "http://localhost:8082/sc/_create";
 
my $req = HTTP::Request->new(POST => $server_endpoint);
$req->header('content-type' => 'application/json');
 
my $post_data = '{ "name": "test2" }';
$req->content($post_data);
 
my $resp = $ua->request($req);
if ($resp->is_success) {
    my $message = $resp->decoded_content;
    print "Received reply: $message\n";
}
else {
    print "HTTP POST error code: ", $resp->code, "\n";
    print "HTTP POST error message: ", $resp->message, "\n";
}
