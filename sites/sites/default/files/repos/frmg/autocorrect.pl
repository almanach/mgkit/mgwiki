#!/usr/bin/perl

use 5.006;
use strict;
use warnings;
use Carp;

use AppConfig qw/:argcount :expand/;

use locale;

#use IO::Handle;

my $home = $ENV{HOME};

my $config = AppConfig->new(
                            "verbose|v!" => {DEFAULT => 1},
			    "easydir=f"  => {DEFAULT => "$home/.build/lexiques/easy"},
                           );

$config->args;

my $easydir = $config->easydir();
my $adjl    = "$easydir/adj.l";
my $adjlex  = "$easydir/adj.lex";
my $noml    = "$easydir/nom.l";
my $nomlex  = "$easydir/nom.lex";

my %suffixes = ( 'al'   => 'adj_a-l',
		 'teur' => 'adj_t-eur',
		 'eur'  => 'adj_eu-r',
		 'eux'  => 'adj_u-x',
		 'aux'  => 'adj_fau-x',
		 'tif'  => 'adj-f',
		 'ien'  => 'adj_n',
		 'e'    => 'adj',
		 'el'   => 'adj_l',
		 's'    => 'adj_s',
		 'er'   => 'adj-er',
		 'o'    => 'adj-5',
		 'eau'  => 'adj-5'
	       );

my $pattern = join('|',sort {length($b) <=> length($a)} keys %suffixes);

my %convert = ( 'nc/m-4' => 'adj-4' );

my %candidates = ();

while(<>) {
  my ($word,$occ) = (split(/\s+/,$_))[0,4];
##  print "Handling $word $occ\n";
  next unless ($occ > 5);
  next unless ($word =~ /^[\w-]+$/);
  my $info = `grep "^$word\t" $nomlex`;
  next unless $info;
  next if `grep "^$word\t" $adjlex`;
  $info =~ /pred=.(\S+?)</;
  my $lemma = $1;
  $info = `grep "^$lemma\t" $noml`;
  chomp $info;
  ($lemma,$info) = split(/\s+/,$info);
  next unless ($lemma && $info);
##  print "$lemma: $info\n";
  my $kind = 'adj';
  $kind = $suffixes{$1} if ($lemma =~ /($pattern)$/);
##  print "$lemma $kind\n";
  $candidates{$lemma} ||= $kind;
}

print "\n## Start Addition\n\n";

foreach (sort keys %candidates) {
  print "$_\t$candidates{$_}\n";
}

print "\n## End Addition\n";


=head1 NAME

autocorrect.pl

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2004-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=cut
