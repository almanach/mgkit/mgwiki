/*
  javascrip functions to help visualizing TAG trees
*/

function toggleVisibility(elt) 
{
    var table = elt.parentNode.getElementsByTagName("table")[0];
    if (table.style.visibility != "hidden" ) {
        table.style.visibility = "hidden";
    } else {
        table.style.visibility = "visible";
    }
}

var borders;

function toggleDisplay(elt) 
{
    var table = elt.parentNode.getElementsByTagName("tr")[0];
    if (table.style.display != "none" ) {
        table.style.display = "none";
        borders = elt.style.border;
        elt.style.borderBottom = "thick red solid";
    } else {
        table.style.display = "table-row";
        elt.style.borderBottom = "";
    }
}

var NodeInfo = Class.create();
NodeInfo.prototype = {

  initialize: function(element) {
        var options = Object.extend({
              default_css: false,
              margin: "0px",
              padding: "5px",
              backgroundColor: "#d6d6fc",
              min_distance_x: 10,
              min_distance_y: 10,
              delta_x: 0,
              delta_y: 0,
              zindex: 1000,
              opacity: .95
            }, arguments[2] || {});
        
        this.element      = $(element);
        this.options      = options;
        
        this.key = element.getAttribute("id");
        this.guard = document.getElementById("guard"+this.key);
        this.decoration = document.getElementById("decoration"+this.key);

   
        this.eventClick = this.toggleGuard.bindAsEventListener(this);
        this.eventMouseOver = this.showDecoration.bindAsEventListener(this);
        this.eventMouseOut   = this.hideAll.bindAsEventListener(this);
        
        this.registerEvents();
  },

  destroy: function() {
        Event.stopObserving(this.element, "mouseover", this.eventMouseOver);
        Event.stopObserving(this.element, "mouseout", this.eventMouseOut);
        Event.stopObserving(this.element, "click", this.eventClick);
    },
  
  registerEvents: function() {
        Event.observe(this.element, "mouseover", this.eventMouseOver);
        Event.observe(this.element, "mouseout", this.eventMouseOut);
        Event.observe(this.element, "click", this.eventClick);
    },

  moveInfo: function(event,info){
        Event.stop(event);

            // save info about info
        var caption = info.getElementsByTagName("caption")[0];
        Element.hide(caption);
                
            // get Mouse position
        var mouse_x = Event.pointerX(event);
        var mouse_y = Event.pointerY(event);

            // get Node position relative to viewport
        var node_pos = Position.page(this.element);
        var node_dim = Element.getDimensions(this.element);
        var node_x = node_pos[0] + node_dim.width/2;
        var node_y = node_pos[1] + node_dim.height/2;
	
            // decide if wee need to switch sides for the tooltip
        var dimensions = Element.getDimensions( info );
        var element_width = dimensions.width;
        var element_height = dimensions.height;
        
//        mouse_x -= element_width/4;
        
        if ( (element_width + node_x) >= ( this.getWindowWidth() - this.options.min_distance_x) ){ // too big for X
            mouse_x -= element_width +this.options.min_distance_x;
        } else {
            mouse_x +=  this.options.min_distance_x;
        }
	
        if ( (element_height + node_y) >= ( this.getWindowHeight() - this.options.min_distance_y) ){ // too big for Y
            mouse_y -=  element_height + this.options.min_distance_y;
        } else {
            mouse_y +=  this.options.min_distance_y;
        } 
	
            // now set the right styles
        this.setStyles(info,mouse_x, mouse_y);
    },

  setStyles: function(info,x, y){
            // set the right styles to position the tool tip
        Element.setStyle(info,{ position:'absolute',
                                          top:y + this.options.delta_y + "px",
                                          left:x + this.options.delta_x + "px",
                                zindex:this.options.zindex,
                                opacity:  this.options.opacity
            });
    },

  toggleGuard: function(event) {
        Event.stop(event);
        var guard = this.guard;
        if (guard) {
            if (guard.style.position != "absolute") {
                this.restaure(this.decoration);
                this.moveInfo(event,guard);
            } else {
                this.showDecoration(event);
            }
        }
    },
  
  showDecoration: function(event) {
        Event.stop(event);
        var decoration = this.decoration;
        if (decoration && decoration.style.position != "absolute") {
            this.restaure(this.guard);
            this.moveInfo(event,decoration);
        }
    },

  restaure: function(info) {
        if (info && info.style.position == "absolute"){
            var caption = info.getElementsByTagName("caption")[0];
            Element.show(caption);
            info.style.position = "static";
            info.style.opacity = 1;
        }
  },
  
  hideGuard: function(event){
        this.restaure(this.guard);
    },
  
  hideDecoration: function(event){
        this.restaure(this.decoration);
    },

  hideAll: function(event) 
  {
      this.hideGuard(event);
      this.hideDecoration(event);
  },
  
  getWindowHeight: function(){
        var innerHeight;
        if (navigator.appVersion.indexOf('MSIE')>0) {
            innerHeight = document.body.clientHeight;
        } else {
            innerHeight = window.innerHeight;
        }
        return innerHeight;	
    },
 
  getWindowWidth: function(){
        var innerWidth;
        if (navigator.appVersion.indexOf('MSIE')>0) {
            innerWidth = document.body.clientWidth;
        } else {
            innerWidth = window.innerWidth;
        }
        return innerWidth;	
    }
  
};

/*
window.onload = function ()
{
    document.getElementsByClassName("nodeid").each(
        function (node) {
            var key = node.getAttribute("id");
            var guard = document.getElementById("guard"+key);
            var decoration = document.getElementById("decoration"+key);
            
            if (guard) {
                node.eventHandler = function (event) {
                    popupGuard(event,key);
                }
                node.onclick = node.eventHandler.bindAsEventListener(node);
            }
            if (decoration) {
                node.eventHandler = function (event) {
                    popupDecoration(event,key);
                }
                node.onmouseover = node.eventHandler.bindAsEventListener(node);
            }
            if (guard || decoration) {
                node.eventHandler = function () {
                    nodeExit(key);
                }
                node.onmouseout = node.eventHandler;
            }
        }
                                                   );
    
    document.getElementsByClassName("nodelabel").each(
        function (node) {
            if (node.getAttribute("internal") == "yes") {
                node.eventHandler = function () {
                    toggleDisplay(this);
                }
                node.ondblclick = node.eventHandler.bindAsEventListener(node);
            }
        }
                                                      );
    
}
*/  


window.onload = function ()
{
    document.getElementsByClassName("nodeid").each(
        function (node) {
            var key = node.getAttribute("id");
            var guard = document.getElementById("guard"+key);
            var decoration = document.getElementById("decoration"+key);
            
            if (guard || decoration) {
                new NodeInfo(node);
            }
            
        }
                                                   );
    
    document.getElementsByClassName("nodelabel").each(
        function (node) {
            if (node.getAttribute("internal") == "yes") {
                node.eventHandler = function () {
                    toggleDisplay(this);
                }
                node.ondblclick = node.eventHandler.bindAsEventListener(node);
            }
        }
                                                      );
    
}



/*
 */
