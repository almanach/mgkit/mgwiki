## My completion file for adding missing features to words
## beware of tabs as separator (not blanks)

## souvent	adv	[adv_kind=freq]

@adj sigmo�de +s + +s
@adj lin�aire +s + +s
@adj ombelliforme +s + +s
@adj bifurqu� +s +e +es
@adj polygame +s + +s
@adj distique +s + +s
##@adj appr�me +s + +s
@adj voyant +s +e +es
@adj minuscule +s + +s
@adj lisse +s + +s
@adj rampant +s +e +es
@adj pubescent +s +e +es
@adj t�tram�re +s + +s
@adj pentam�re +s + +s
@adj terminal terminaux terminale terminales
@adj violet +s +te +tes
@adj oblique  +s + +s
@adj r�ticul� +s +e +es
@adj super +s + +s

@adj structuraliste +s + +s

@adj tomodensitom�trique +s + +s

basai	adj	[pred="basai",cat=adj,@s]	basai____1	Default

@nc stylopode +s @
@nc staminode +s @
@nc testa +s @
@nc m�ricarpe +s @m
@nc exocarpe +s @m
@nc convergente +s @f
@nc pistillode +s @

@nc trilogue +s @m

Roms	np	[cat=np,@p]	Rom	Default

## @nc dignitaire +s @


_DIMENSION	np	[cat=np,@p]	_DIMENSION	Default

dens�ment	adv	[]	dens�ment____20	Default

##ce		pro	[pred="ce____3",number=sg]	ce_____3	Default


##comment		pri	[pred="commentComp?_____1",case= comp]	commentComp?_____1	Default 
quel		pri	[pred="quel?_____1",case= comp,@ms]	quel?_____1	Default 
quelle		pri	[pred="quel?_____1",case= comp,@fs]	quel?_____1	Default 
quels		pri	[pred="quel?_____1",case= comp,@mp]	quel?_____1	Default 
quelles		pri	[pred="quel?_____1",case= comp,@fp]	quel?_____1	Default 
qui		pri	[pred="quiComp?_____1",case= comp]	quiComp?_____1	Default
que		pri	[pred="queComp?_____1",case= comp]	queComp?_____1	Default 
qu'		pri	[pred="queComp?_____1",case= comp]	queComp?_____1	Default 
lequel		pri	[pred="lequelComp?_____1",case= comp,@ms]	lequelComp?_____1	Default 
lesquels	pri	[pred="lequelComp?_____1",case= comp,@mp]	lequelComp?_____1	Default 
laquelle	pri	[pred="lequelComp?_____1",case= comp,@fs]	lequelComp?_____1	Default 
lesquelles	pri	[pred="lequelComp?_____1",case= comp,@fp]	lequelComp?_____1	Default 



lequel		pri	[pred="lequel?_____1",case=nom|acc,@ms]	quel?_____1	Default 
laquelle		pri	[pred="lequel?_____1",case=nom|acc,@fs]	quel?_____1	Default 
lesquels		pri	[pred="lequel?_____1",case=nom|acc,@mp]	quel?_____1	Default 
lesquelles		pri	[pred="lequel?_____1",case=nom|acc,@fp]	quel?_____1	Default 

proto-_		advPref	[pred="proto-______2<Suj:(sn)>",cat=adv]	proto-______2	Default

## Un	det	[det=+,define=-,@ms]	un_____1	Default	ms
## Un	pro	[pred="un_____1<Objde:(de-sn)>",define=-,@ms]	un_____1	Default	ms

pas encore	advneg	[pred="pas encore_____1",cat=advneg]	pas encore_____1	Default 
pr�s de	adv	[pred="pr�s de_____1",cat=adv,adv_kind=modnc]	pr�s de_____1	Default
pr�s d"	adv	[pred="pr�s de_____1",cat=adv,adv_kind=modnc]	pr�s de_____1	Default
:	poncts	[]	:_____2	Default	%default
Wikip�dia	np	[cat=np]	Wikip�dia_____1	Default
DGSE	np	[cat=np,@fs]	DGSE_____1	Default
DST	np	[cat=np,@fs]	DST_____1	Default

eux-m�mes	100	xpro	[pred="lui-m�me_____2",@3mp]	lui-m�me_____1	Default	%default
elles-m�mes	100	xpro	[pred="lui-m�me_____2",@3fp]	lui-m�me_____1	Default	%default
lui-m�me	100	xpro	[pred="lui-m�me_____2",@3ms]	lui-m�me_____1	Default	%default
elle-m�me	100	xpro	[pred="lui-m�me_____2",@3fs]	lui-m�me_____1	Default	%default


## comme det: divers diff�rent
## Quid

Quid	np	[cat=np]	Quid_____1	Default
Gaza	np	[cat=np]	Gaza_____1	Default
Fermat	np	[cat=np]	Gaza_____1	Default
Dinant	np	[cat=np]	Gaza_____1	Default
Octave	np	[cat=np,@s]	Gaza_____1	Default
Martial	np	[cat=np,@ms]	Gaza_____1	Default
Pluton	100	np	[pred="Pluton",cat=np,@ms]	Pluton_____1	Default	ms	 %default

JO	100	np	[pred="Journal_Officiel",cat=np,@ms]	Journal Officiel_____1	Default	ms	%default

@nc finalisation +s @f
@nc d�chetterie +s @f
@nc commetant +s @m
@nc mod�lisme +s @m
@nc boite +s @f
@nc conscrit +s @m
@nc shogunat +s @m
@nc blasonnement +s @m
@nc r�am�nagement +s @m
@nc babouschka + @f
@nc cr�nelage +s @m
@nc d�sossage +s @m
@nc requ�rant +s @m
@nc requ�rante +s @f
@nc v�rificateur +s @m
@nc r�prouv� +s @m
@nc r�prouv�e +s @m
@nc d�put�e +s @f
@nc quartet +s @m
@nc phtalate +s @f
@nc constitutionalisme +s @m
@nc constitutionalisation +s @f
@nc hooliganisme +s @m
@nc taliban + @m
@nc lobbyiste +s @
@nc reculons + @m
@nc chromatographe +s @m
@nc ovoproduit +s @m
@nc multilinguisme +s @m
@nc rembours + @m

@adj nitrique +s + +s
@adj massique +s + +s
@adj proc�dural proc�duraux proc�durale proc�durales
@adj notifiant +s +e +es
@adj natif natifs native natives
@adj tier tiers tierce tierces
@adj m�solithique +s + +s
@adj naufrag� +s +e +es
@adj fr�quentiel +s +le +les
@adj antidopage +s + +s
@adj juridictionel +s +le +les

@adj rus� +s +e +es

chut	pres	[pred="chut_____1"]	chut_____1	Default

divers	100	det	[define=-,det=+,@m]	divers_____1	Default m	%default
diverse	100	det	[define=-,det=+,@fs]	divers_____1	Default fs	%default
diverses	100	det	[define=-,det=+,@fp]	divers_____1	Default fp	%default

diff�rent	100	det	[define=-,det=+,@ms]	diff�rent_____1	Default ms	%default
diff�rents	100	det	[define=-,det=+,@mp]	diff�rent_____1	Default mp	%default
diff�rente	100	det	[define=-,det=+,@fs]	diff�rent_____1	Default fs	%default
diff�rentes	100	det	[define=-,det=+,@fp]	diff�rent_____1	Default fp	%default

## Construction en "X de" comme det

peu	100	predet	[pred="peu_____1",predet_kind=adv,quantity=peu]	peu_____1	Default	%default
beaucoup	100	predet	[pred="beaucoup_____1",predet_kind=adv,quantity=beaucoup]	beaucoup_____1	Default	%default
tant	100	predet	[pred="tant_____1",predet_kind=adv,quantity=beaucoup]	tant_____1	Default	%default
nombre	100	predet	[pred="nombre_____1",predet_kind=nc]	nombre_____1	Default	%default
�norm�ment	100	predet	[pred="�norm�ment_____1",predet_kind=adv,quantity=beaucoup]	�norm�ment_____1	Default	%default
tellement	100	predet	[pred="tellement_____1",predet_kind=adv,quantity=beaucoup]	tellement_____1	Default	%default
immens�ment	100	predet	[pred="immens�ment_____1",predet_kind=adv,quantity=beaucoup]	immens�ment_____1	Default	%default
quantit�	100	predet	[pred="quantit�_____1",predet_kind=nc]	quantit�_____1	Default	%default
plein	100	predet	[pred="plein_____1",predet_kind=adv]	plein_____1	Default	%default
combien	100	predet	[pred="combien_____1",predet_kind=nc,wh=+]	combien_____1	Default	%default
suffisamment	100	predet	[pred="suffisamment_____1",predet_kind=adv,quantity=beaucoup]	suffisamment_____1	Default	%default
assez	100	predet	[pred="assez_____1",predet_kind=adv,quantity=beaucoup]	assez_____1	Default	%default
trop	100	predet	[pred="trop_____1",predet_kind=adv,quantity=beaucoup]	trop_____1	Default	%default
nul	100	predet	[pred="nul_____1",predet_kind=nc]	nul_____1	Default	%default
plus	100	predet	[pred="plus_____1",predet_kind=adv,quantity=beaucoup]	plus_____1	Default	%default
moins	100	predet	[pred="moins_____1",predet_kind=adv,quantity=beaucoup]	moins_____1	Default	%default
davantage	100	predet	[pred="davantage_____1",predet_kind=adv,quantity=beaucoup]	davantage_____1	Default	%default
## pas is only possible in negative contexte
## il ne veut (pas de) pomme.
## il ne veut presque (pas de) pomme.
## il veut une part avec (pas de) pommes.
pas	100	predet	[pred="pas_____1",predet_kind=advneg]	pas_____1	Default	%default
gu�re	100	predet	[pred="gu�re_____1",predet_kind=advneg]	gu�re_____1	Default	%default

gaiement	100	advm	[pred="gaiement_____1",cat=adv,clive=+]	gaiement_____1	Default	%default

## Actually, voici and voil� are in Lefff as v, but with a special subcat
## No subj
##voici	100	v	[pred="voici_____1<Suj:cln|sn,Obj:(cla|sn|scompl)>",cat=v,@imperative,@Y2s]	voici_____1	Imperative	Y2s	%actif
##revoici	100	v	[pred="revoici_____1<Suj:cln|sn,Obj:(cla|sn|scompl)>",cat=v,@imperative,@Y2s]	revoici_____1	Imperative	Y2s	%actif
##voil�	100	v	[pred="voil�_____1<Suj:cln|sn,Obj:(cla|sn|scompl)>",cat=v,@imperative,@Y2s]	voil�_____1	Imperative	Y2s	%actif
##revoil�	100	v	[pred="revoil�_____1<Suj:cln|sn,Obj:(cla|sn|scompl)>",cat=v,@imperative,@Y2s]	revoil�_____1	Imperative	Y2s	%actif
##quid	100	v	[pred="quid_____1<Suj:cln|sn,Objde:(de-sinf|de-sn)>",cat=v,@imperative,@Y2s]	quid_____1	Imperative	Y2s	%actif

## avec	120	prep	[pred="avec_____1<Obj:(sa|sadv|sn)>",pcas = avec]	avec_____1	Default	%default

## fait	100	v	[pred="faire_____1<Suj:cln|sn,Obj:sinfcaus>",@pers,cat=v,@P3s]	faire_____1	ThirdSing	P3s	%actif

## Tmp: en attendant de completer Lefff
soulign�	100	v	[pred="souligner_____1<Obl2:(par-sn),Suj:cln|sn|sinf|scompl|qcompl>",@passive,@pers,cat=v,@Kms]	souligner_____1	PastParticiple	Kms	%passif
soulign�e	100	v	[pred="souligner_____1<Obl2:(par-sn),Suj:cln|sn|sinf|scompl>",@passive,@pers,cat=v,@Kfs]	souligner_____1	PastParticiple	Kfs	%passif
soulign�s	100	v	[pred="souligner_____1<Obl2:(par-sn),Suj:cln|sn|sinf|scompl>",@passive,@pers,cat=v,@Kmp]	souligner_____1	PastParticiple	Kmp	%passif    
soulign�es	100	v	[pred="souligner_____1<Obl2:(par-sn),Suj:cln|sn|sinf|scompl>",@passive,@pers,cat=v,@Kfp]	souligner_____1	PastParticiple	Kfp	%passif

que	100	csu	[wh=que]	que_____1	Default	%default:
qu'	100	csu	[wh=que]	que_____1	Default	%default
    
_NUMBER	number	[cat=number]	_NUMBER	Default
SHQ:	epsilon	[cat=epsilon]	SHQ:____1	Default
:SHQ	epsilon	[]	:SHQ____1	Default
PRED:	epsilon	[]	PRED:____1	Default
:PRED	epsilon	[]	:PRED____1	Default
AUT CL:	epsilon	[]	AUTCL:____1	Default
:AUT CL	epsilon	[]	:AUTCL____1	Default
AUT:	epsilon	[]	AUT:____1	Default
:AUT	epsilon	[]	:AUT____1	Default
SQ:	epsilon	[cat=epsilon]	SQ:____1	Default
:SQ	epsilon	[]	:SQ____1	Default
STQ:	epsilon	[cat=epsilon]	STQ:____1	Default
:STQ	epsilon	[]	:STQ____1	Default
HQ:	epsilon	[cat=epsilon]	HQ:____1	Default
:HQ	epsilon	[]	:HQ____1	Default
TQ:	epsilon	[cat=epsilon]	TQ:____1	Default
:TQ	epsilon	[]	:TQ____1	Default
DQ:	epsilon	[cat=epsilon]	DQ:____1	Default
:DQ	epsilon	[]	:DQ____1	Default


## Pb with SxPipe: Dieu (incorrectly) corrected into dieu
dieu	100	np	[cat=np,@ms]	Dieu_____1	Default


nul	100	det	[define=-,det=+,@ms]	nul_____1	Default	ms	%default
nuls	100	det	[define=-,det=+,@mp]	nul_____1		Default	mp	%default
nulle	100	det	[define=-,det=+,@fs]	nul_____1		Default	fs	%default
nulles	100	det	[define=-,det=+,@fp]	nul_____1		Default	fp	%default

si	100	adv	[pred="si_____1",adv_kind=intensive,cat=adv]	si_____1	Default	%default
tellement	100	adv	[pred="tellement_____1",adv_kind=intensive,cat=adv]	tellement_____1	Default	%default
plut�t	100	adv	[pred="plut�t_____1",adv_kind=intens,cat=adv]	plut�t_____1	Default	%default

## nouns witch scompl arg
motif	100	nc	[pred="motif_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@ms]	motif_____1	Default	ms	%default
souhait	100	nc	[pred="souhait_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@ms]	souhait_____1	Default	ms	%default
d�sir	100	nc	[pred="d�sir_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@ms]	d�sir_____1	Default	ms	%default
accord	100	nc	[pred="accord_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@ms]	accord_____1	Default	ms	%default
constat	100	nc	[pred="constat_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@ms]	constat_____1	Default	ms	%default
rappel	100	nc	[pred="rappel_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@ms]	rappel_____1	Default	ms	%default
sentiment	100	nc	[pred="sentiment_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@ms]	sentiment_____1	Default	ms	%default
doute	100	nc	[pred="doute_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@ms]	doute_____1	Default	ms	%default
principe	100	nc	[pred="principe_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@ms]	principe_____1	Default	ms	%default

sorte	100	nc	[pred="sorte_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@fs]	sorte_____1	Default	fs	%default
envie	100	nc	[pred="envie_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@fs]	envie_____1	Default	fs	%default
impression	100	nc	[pred="impression_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@fs]	impression_____1	Default	fs	%default
proposition	100	nc	[pred="proposition_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@fs]	proposition_____1	Default	fs	%default
croyance	100	nc	[pred="croyance_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@fs]	croyance_____1	Default	fs	%default
assurance	100	nc	[pred="assurance_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@fs]	assurance_____1	Default	fs	%default
crainte	100	nc	[pred="crainte_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@fs]	crainte_____1	Default	fs	%default
inqui�tude	100	nc	[pred="inqui�tude_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@fs]	inqui�tude_____1	Default	fs	%default
pens�e	100	nc	[pred="pens�e_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@fs]	pens�e_____1	Default	fs	%default
condition	100	nc	[pred="condition_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@fs]	condition_____1	Default	fs	%default
annonce	100	nc	[pred="annonce_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@fs]	annonce_____1	Default	fs	%default
conclusion	100	nc	[pred="conclusion_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@fs]	conclusion_____1	Default	fs	%default


super	100	advm	[pred="super_____1",cat=adv,adv_kind=intens]	super_____1	Default	%default

@title ma�tre +s @m
@title Me +s @
@title ma�tresse +s @f
@title docteur +s @
@title professeur +s @
@title madame mesdames @f
@title mademoiselle mesdemoiselles @f
@title monsieur messieurs @m
@title Mr MM. @m
@title M. MM. @m
@title M MM @m
@title Mlle Mlles @f
@title Mme Mmes @f
@title Monseigneur Messeigneurs @m
@title Mgr +s @m
@title Dame Dames @f
@title Sir Sirs @m
@title Lady Ladies @f
@title Mess Mess @m
@title R�v�rend R�v @m
@title colonel colonel @m
@title Miss Misses @f
@title baron barons @m

aussi	adv	[pred="aussi_____1",cat=adv,adv_kind=modpro]	aussi_____1	Default
m�me	adv	[pred="m�me_____1",cat=adv,adv_kind=modpro]	m�me_____1	Default
non plus	adv	[pred="nonplus_____1",cat=adv,adv_kind=modpro]	non plus_____1	Default

## plut�t que	prep	[pred="plut�t que_____1<Obj:sn|sa|sadv|sinf>"]	plut�t que_____1	Default	%default

quiconque	100	prel	[pred="quiconque_____1",@pro_nom]	quiconque_____1	Default	%default

## Pb dans Europarl
@redirect cur coeur
@redirect curs coeurs
@redirect nud noeud
@redirect nuds noeuds
@redirect surs soeurs
@redirect consur consoeur
@redirect consurs consoeurs
@redirect belle-sur belle-soeur
@redirect belle-surs belle-soeurs
@redirect demi-sur demi-soeur
@redirect demi-surs demi-soeurs
@redirect chur choeur
@redirect churs choeurs
@redirect contrecur contrecoeur
@redirect contrecurs contrecoeurs
@redirect uvre oeuvre
@redirect uvres oeuvres
@redirect uvrer oeuvrer
@redirect uvrent oeuvrent
@redirect uvr� oeuvr�
@redirect uvr�e oeuvr�e
@redirect uvr�s oeuvr�s
@redirect uvr�es oeuvr�es
@redirect main-d'uvre main-d'oeuvre
@redirect main-d'uvres main-d'oeuvres
@redirect chef-d'uvre chef-d'oeuvre
@redirect chef-d'uvres chef-d'oeuvres
@redirect uf oeuf
@redirect ufs oeufs
@redirect buf boeuf
@redirect bufs boeufs
@redirect manuvre manoeuvre
@redirect manuvres manoeuvres
@redirect manuvrer manoeuvrer
@redirect manuvrent manoeuvrent
@redirect manuvr� manoeuvr�
@redirect manuvr�e manoeuvr�e
@redirect manuvr�s manoeuvr�s
@redirect manuvr�es manoeuvr�es
@redirect �curer �coeurer
@redirect �cure �coeure
@redirect �cures �coeures
@redirect �curent �coeurent
@redirect �cur� �coeur�
@redirect �cur�e �coeur�e
@redirect �cur�s �coeur�s
@redirect �cur�es �coeur�es
@redirect sis situ�
@redirect sise situ�e
@redirect sises situ�es
@redirect r�gne r�gne

## missing in sxpipe/lefff (maybe a bug) 2010/09/28 Eric
@redirect _LOCATION2 _LOCATION
@redirect _PERSON2 _PERSON
@redirect _PERSON_f2 _PERSON_f
@redirect _PERSON_m2 _PERSON_m
@redirect _ORGANIZATION2 _ORGANIZATION
@redirect trad. traduction
@redirect r��d. r��dition
@redirect ed. �diteur

@redirect �v�nement �v�nement

quinzaine	100	nc	[pred="quinzaine",cat=nc,@fs,@time]	quinzaine____1	Default	%default
burqa	100	nc	[pred="burqa",cat=nc,@f]	burqa____1	Default	%default

@nc procureure +s @f
@nc proviseure +s @f

@nc quint� +s @m
@nc trader +s @m
@nc hacker +s @m
@nc rappeur +s @m
@nc rappeuse +s @f
@nc surfeur +s @m
@nc surfeuse +s @f

@nc marqueur +s @m

@adj lampass� +s +e +es
@nc despotat +s @m

@nc polym�rase +s @f
#@nc laitier +s @m
@nc vicomt� +s @f

@nc bassidji +s @m 

@nc f�ria +s @f
@nc capitanat +s @m
@nc greffi�re +s @f
@nc gouverneure +s @f
@nc cadencement +s @m
@nc connection +s @f

@adj lorientais + +e +es
@adj rennais    + +e +es
@adj sedanais   + +e +es
@adj perpignanais + +e +es
@adj saskatchewanais + +e +es

@adj �tats-unien  +s +ne +nes
@adj internet + + +

@adj grandissime +s + +s

@nc sprinteur +s @m
@nc paparazzi + @m
@nc sinistrose +s @f

@nc tie-break +s @m

@nc performeur +s @m
@nc performeuse +s @f

@adj za�dite +s + +s
@adj vallonn� +s +e +es

@nc derby derbies @m

@adj celsius + + +

@adj d�sob�isseur +s d�sob�isseuse +s

@nc r�f�rencement +s @m

@adj inarr�table +s + +s

@nc volte-face +s @f

@nc t�l�chargement +s @m
@nc vert�br� +s @m
@nc invert�br� +s @m
@nc naufrag� +s @m
@nc naufrag�e +s @f

@adj proleptique +s + +s
@nc dioxyg�ne +s @m
@adj songha� + + +
@adj burgonde +s + +s
@adj almohade +s + +s
@adj �th�rique +s + +s
@adj fr�quentiel +s +le +les
@adj quadratique +s + +s

@adj diagonalisable +s + +s
@nc d�groupage +s @m

@nc baseballeur +s @m
@nc baseballeuse +s @f

@nc num�rologie +s @f

@nc amine +s @f

@nc alien +s @m

@nc centre-ville +s @m

@adj s�n�galais + +e +es

@adj radiatif +s radiative radiatives
@nc d�senfumage +s @m

@nc diabolisation +s @f
@adj sph�ro�dal sph�ro�daux sph�ro�dale sph�ro�dales
@nc tryptamine +s @f
@adj palois + +e +es
@nc insurgent +s @m
@nc remixe +s @m

@adj quitte +s + +s

chemin	100	cfi	[pred="chemin_____1<Suj:cln|sn>",lightverb=rebrousser]	chemin_____1	Default	%default
main-forte	100	cfi	[pred="main-forte_____1<Suj:cln|sn>",lightverb=pr�ter]	main-forte_____1	Default	%default
main-forte	100	cfi	[pred="main-forte_____1<Suj:cln|sn>",lightverb=demander]	main-forte_____1	Default	%default
main-forte	100	cfi	[pred="main-forte_____1<Suj:cln|sn>",lightverb=chercher]	main-forte_____1	Default	%default
main-forte	100	cfi	[pred="main-forte_____1<Suj:cln|sn>",lightverb=qu�rir]	main-forte_____1	Default	%default
main-forte	100	cfi	[pred="main-forte_____1<Suj:cln|sn>",lightverb=requ�rir]	main-forte_____1	Default	%default
gain de cause	100	cfi	[pred="gain de cause_____1<Suj:cln|sn>",lightverb=obtenir]	gain de cause_____1	Default	%default
probl�me	100	cfi	[pred="probl�me_____1<Suj:cln|sn,Obj�:(�-sn|cld)>",lightverb=poser]	probl�me_____1	Default	%default
## gaffe	100	cfi	[pred="gaffe_____1<Suj:cln|sn,Obj�:(�-sn|y|�-sinf|scompl|de-sinf)>",lightverb=faire]	gaffe_____1	Default	%default

pas grand chose	100	pro	[pred="pas grand-chose_____1<Objde:(de-sn|de-sa)>",define=-,@s]	pas grand-chose_____1	Default	%default

cens�	100	v	[pred="censer_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@�tre,cat=v,@Kms]	censer_____1	PastParticiple	Kms	%actif
cens�e	100	v	[pred="censer_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@�tre,cat=v,@Kfs]	censer_____1	PastParticiple	Kfs	%actif
cens�s	100	v	[pred="censer_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@�tre,cat=v,@Kmp]	censer_____1	PastParticiple	Kmp	%actif
cens�es	100	v	[pred="censer_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@�tre,cat=v,@Kfp]	censer_____1	PastParticiple Kfp	%actif

suppos�	100	v	[pred="supposer_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@�tre,cat=v,@Kms]	supposer_____1	PastParticiple	Kms	%actif
suppos�e	100	v	[pred="supposer_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@�tre,cat=v,@Kfs]	supposer_____1	PastParticiple	Kfs	%actif
suppos�s	100	v	[pred="supposer_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@�tre,cat=v,@Kmp]	supposer_____1	PastParticiple	Kmp	%actif
suppos�es	100	v	[pred="supposer_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@�tre,cat=v,@Kfp]	supposer_____1	PastParticiple Kfp	%actif

r�put�	100	v	[pred="r�puter_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@�tre,cat=v,@Kms]	r�puter_____1	PastParticiple	Kms	%actif
r�put�e	100	v	[pred="r�puter_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@�tre,cat=v,@Kfs]	r�puter_____1	PastParticiple	Kfs	%actif
r�put�s	100	v	[pred="r�puter_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@�tre,cat=v,@Kmp]	r�puter_____1	PastParticiple	Kmp	%actif
r�put�es	100	v	[pred="r�puter_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@�tre,cat=v,@Kfp]	r�puter_____1	PastParticiple Kfp	%actif

intervenu	100	v	[pred="intervenir_____1<Suj:cln|sn,Loc:(loc-sn|y)>",@active,@pers,@�tre,cat=v,@Kms]	intervenir_____1	PastParticiple	Kms	%actif
intervenue	100	v	[pred="intervenir_____1<Suj:cln|sn,Loc:(loc-sn|y)>",@active,@pers,@�tre,cat=v,@Kfs]	intervenir_____1	PastParticiple	Kfs	%actif
intervenus	100	v	[pred="intervenir_____1<Suj:cln|sn,Loc:(loc-sn|y)>",@active,@pers,@�tre,cat=v,@Kmp]	intervenir_____1	PastParticiple	Kmp	%actif
intervenues	100	v	[pred="intervenir_____1<Suj:cln|sn,Loc:(loc-sn|y)>",@active,@pers,@�tre,cat=v,@Kfp]	intervenir_____1	PastParticiple Kfp	%actif


paru	100	v	[pred="para�tre_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kms]	para�tre_____1	PastParticiple	Kms	%actif
parue	100	v	[pred="para�tre_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kfs]	para�tre_____1	PastParticiple	Kfs	%actif
parus	100	v	[pred="para�tre_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kmp]	para�tre_____1	PastParticiple	Kmp	%actif
parues	100	v	[pred="para�tre_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kfp]	para�tre_____1	PastParticiple Kfp	%actif

survenu	100	v	[pred="survenir_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kms]	survenir_____1	PastParticiple	Kms	%actif
survenue	100	v	[pred="survenir_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kfs]	survenir_____1	PastParticiple	Kfs	%actif
survenus	100	v	[pred="survenir_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kmp]	survenir_____1	PastParticiple	Kmp	%actif
survenues	100	v	[pred="survenir_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kfp]	survenir_____1	PastParticiple Kfp	%actif

apparu	100	v	[pred="appara�tre_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kms]	appara�tre_____1	PastApparticiple	Kms	%actif
apparue	100	v	[pred="appara�tre_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kfs]	appara�tre_____1	PastApparticiple	Kfs	%actif
apparus	100	v	[pred="appara�tre_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kmp]	appara�tre_____1	PastApparticiple	Kmp	%actif
apparues	100	v	[pred="appara�tre_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kfp]	appara�tre_____1	PastApparticiple Kfp	%actif

av�r�	100	v	[pred="av�rer_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kms]	av�rer_____1	PastParticiple	Kms	%actif
av�r�e	100	v	[pred="av�rer_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kfs]	av�rer_____1	PastParticiple	Kfs	%actif
av�r�s	100	v	[pred="av�rer_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kmp]	av�rer_____1	PastParticiple	Kmp	%actif
av�r�es	100	v	[pred="av�rer_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kfp]	av�rer_____1	PastParticiple Kfp	%actif


d�battu	100	v	[pred="d�battre_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kms]	d�battre_____1	PastParticiple	Kms	%passif
d�battue	100	v	[pred="d�battre_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kfs]	d�battre_____1	PastParticiple	Kfs	%passif
d�battus	100	v	[pred="d�battre_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kmp]	d�battre_____1	PastParticiple	Kmp	%passif
d�battues	100	v	[pred="d�battre_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kfp]	d�battre_____1	PastParticiple Kfp	%passif

paniqu�	100	v	[pred="paniquer_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kms]	paniquer_____1	PastParticiple	Kms	%passif
paniqu�e	100	v	[pred="paniquer_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kfs]	paniquer_____1	PastParticiple	Kfs	%passif
paniqu�s	100	v	[pred="paniquer_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kmp]	paniquer_____1	PastParticiple	Kmp	%passif
paniqu�es	100	v	[pred="paniquer_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kfp]	paniquer_____1	PastParticiple Kfp	%passif

autoproclam�	100	v	[pred="autoproclamer_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kms]	autoproclamer_____1	PastParticiple	Kms	%passif
autoproclam�e	100	v	[pred="autoproclamer_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kfs]	autoproclamer_____1	PastParticiple	Kfs	%passif
autoproclam�s	100	v	[pred="autoproclamer_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kmp]	autoproclamer_____1	PastParticiple	Kmp	%passif
autoproclam�es	100	v	[pred="autoproclamer_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kfp]	autoproclamer_____1	PastParticiple Kfp	%passif

surnomm�	100	v	[pred="surnommer_____1<Obl2:(par-sn),Suj:cln|sn,Att:(sa|sn)>",@passive,@pers,cat=v,@Kms]	surnommer_____1	PastParticiple	Kms	%passif
surnomm�e	100	v	[pred="surnommer_____1<Obl2:(par-sn),Suj:cln|sn,Att:(sa|sn)>",@passive,@pers,cat=v,@Kfs]	surnommer_____1	PastParticiple	Kfs	%passif
surnomm�s	100	v	[pred="surnommer_____1<Obl2:(par-sn),Suj:cln|sn,Att:(sa|sn)>",@passive,@pers,cat=v,@Kmp]	surnommer_____1	PastParticiple	Kmp	%passif
surnomm�es	100	v	[pred="surnommer_____1<Obl2:(par-sn),Suj:cln|sn,Att:(sa|sn)>",@passive,@pers,cat=v,@Kfp]	surnommer_____1	PastParticiple Kfp	%passif


modulo	120	prep	[pred="modulo_____1<Obj:(sa|sadv|sn)>",pcas = modulo]	modulo_____1	Default	%default

@redirect commes comme

@nc ataman +s @m
@nc courtine +s @f
@redirect M'sieur Monsieur


#avant	120	prep	[pred="avant_____1",pcas = loc]	avant_____1	Default	%default
#apr�s	120	prep	[pred="apr�s_____1",pcas = loc]	apr�s_____1	Default	%default    
#derri�re	120	prep	[pred="derri�re_____1",pcas = loc]	derri�re_____1	Default	%default

@redirect anni anniversaire

@nc accompagnant +s @m
@nc accompagnante +s @f

moyen	100	cfi	[pred="moyen_____1<Suj:cln|sn,Objde:(de-sinf|scompl)>",lightverb=avoir]	moyen_____1	Default	%default
moyen	100	cfi	[pred="moyen_____1<Suj:cln|sn,Objde:(de-sinf|scompl)>",lightverb=trouver]	moyen_____1	Default	%default

peur	100	cfi	[pred="peur_____1<Suj:cln|sn,Objde:(de-sinf|scompl|de-sn)>",lightverb=avoir]	peur_____1	Default	%default

pr�texte	100	nc	[pred="pr�texte_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@ms]	pr�texte_____1	Default	ms	%default

trace	100	cfi	[pred="trace_____1<Suj:cln|sn,Objde:(de-sn)>",lightverb=porter]	trace_____1	Default	%default
trace	100	cfi	[pred="trace_____1<Suj:cln|sn,Objde:(de-sn)>",lightverb=garder]	trace_____1	Default	%default

p�nitence	100	cfi	[pred="p�nitence_____1<Suj:cln|sn,Objde:(de-sn)>",lightverb=faire]	p�nitence_____1	Default	%default

garde	100	cfi	[pred="garde_____1<Suj:cln|sn,Objde:de-sn|�-sn>",lightverb=avoir]	garde_____1	Default	%default
raison	100	cfi	[pred="raison_____1<Suj:cln|sn,Objde:(de-sn)>",lightverb=avoir]	raison_____1	Default	%default

ripailles	100	cfi	[pred="ripailles_____1<Suj:cln|sn>",lightverb=faire]	ripailles_____1	Default

semblant	100	cfi	[pred="semblant_____1<Suj:cln|sn,Objde:(de-sinf)>",lightverb=faire]	semblant_____1	Default

compte	100	cfi	[pred="compte_____1<Suj:cln|sn,Objde:(de-sn|scompl),Obj�:(�-sn)>",lightverb=rendre]	compte_____1	Default	%default

@nc sicav + @f

# SVP		[cat=nc,@ms]	svp_____1	Default
# STP		[cat=nc,@ms]	svp_____1	Default
# svp		[cat=nc,@ms]	svp_____1	Default
# stp		[cat=nc,@ms]	svp_____1	Default

oh	pres	[pred="oh_____1"]	oh_____1	Default
ho	pres	[pred="ho_____1"]	ho_____1	Default
eh	pres	[pred="eh_____1"]	eh_____1	Default
he	pres	[pred="oh_____1"]	he_____1	Default
ha	pres	[pred="ha_____1"]	ha_____1	Default

pour	120	prep	[pred="pour_____1",pcas = pour]	pour_____1	Default	%default
contre	120	prep	[pred="contre_____1",pcas = loc|contre]	contre_____1	Default	%default

quitte	100	cfi	[pred="quitte_____1<Suj:cln|sn,Obj:(sa|sn),Objde:(de-sn|en)>",lightverb=tenir]	tenir_____1	Default	%default

presque	advPref	[pred="presque_____1",cat=adv]	presque_____1	Default 
presque	adjPref	[pred="presque_____1<Suj:(sn)>",cat=adj]	presque_____1	Default 

## should be handled by suffix
@adj russophone +s + +s
@adj n�erlandophone +s + +s
@adj lusophone +s + +s
@adj italophone +s + +s
@adj hispanophone +s + +s
@adj kurdophone +s + +s
@adj magyarophone +s + +s
@adj ukrainophone +s + +s

@nc agoniste +s @m

@redirect m'sieur monsieur

� c�t�	adv	[pred="� c�t�_____1",cat=adv]	� c�t�_____1	Default
� c�t�s	nc	[pred="� c�t�_____1",cat=nc,@mp]	� c�t�_____1	Default

�t�	v	[pred="�tre_____1<Suj:cln|scompl|sinf|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,cat=v,@K]	�tre_____1	PastParticiple	K %actif

## tmp hack for 'avoir beau' : better to implement it as a concessive construction (il a beau manger, il ne grossit pas)
beau	100	cfi	[pred="beau_____1<Suj:cln|sn,Obl:sinf>",lightverb=avoir]	beau_____1	Default	%default

chance	100	cfi	[pred="chance_____1<Suj:cln|sn,Obj�:(�-sn)>",lightverb=porter]	chance_____1	Default	%default

##attention	100	cfi	[pred="attention_____1<Suj:cln|sn,Obj�:(�-sn|cld|�-sinf)>",lightverb=faire]	attention_____1	Default	%default
    
@nc rappeur +s @m
@nc rappeuse +s @f
@nc greffi�re +s @f

@nc parraineur +s @m

## should be done in lefff to be complete !
@redirect sous-estimer sousestimer
@redirect sous-estime sousestime
@redirect sous-estimes sousestimes
@redirect sous-estiment sousestiment
@redirect sous-estim� sousestim�
@redirect sous-estim�s sousestim�s
@redirect sous-estim�e sousestim�e
@redirect sous-estim�es sousestim�es

@nc semi-remorque +s @m
  
@adj record +s + +s

#voil�	120	prep	[pred="voil�_____1<Obj:(sn)>",pcas = tps]	voil�____1	Default	%default
#voici	120	prep	[pred="voici_____1",pcas= loc, cat= prep]	voici____1	Default	%default    

# Past Conditional seconde form
e�t	600	auxAvoir	[@active,@favoir,@C3s]	avoir_____2	ThirdSing	C3s	%default
e�tes	600	auxAvoir	[@active,@favoir,@C2p]	avoir_____2	Default	C2p	%default

commande	100	cfi	[pred="commande_____1<Suj:cln|sn,Obj�:(�-sn|cld),Objde:(de-sn|clg)>",lightverb=passer]	commande_____1	Default	%default

mati�re	100	cfi	[pred="mati�re_____1<Suj:cln|sn,Obj�:(�-sn|cld)>",lightverb=trouver]	mati�re_____1	Default	%default
pretexte	100	cfi	[pred="pretexte_____1<Suj:cln|sn,Obj�:(�-sn|cld)>",lightverb=trouver]	pretexte_____1	Default	%default
    
## Some acronymes
@nc PIB + @m
@nc TVA + @f
@nc RMI + @m
@nc TGV + @m
@nc PNB + @m
@nc PIB + @m
@nc BTP + @m
@nc RMiste +s @m
@nc TUC + @m
@nc PEL + @m

## irriter +obj

@nc contre-pouvoir +s @m

/	100	ponctw	[]	/_____1	Default	%default

## tmp hack
## du	100	prep	[pred="de_____1",pcas = de]	de_____1	Default	%default

@nc smic + @m
@nc SMIC + @m

@nc pdg + @m

diable	pres	[pred="diable_____1"]	diable_____1	Default

@nc �-pic + @m


conseil	100	cfi	[pred="conseil_____1<Suj:cln|sn>",lightverb=tenir]	conseil_____1	Default	%default
piti�	100	cfi	[pred="piti�_____1<Suj:cln|sn,Objde:(de-sn)>",lightverb=avoir]	piti�_____1	Default	%default

@adj siliceux siliceux siliceuse siliceuses



## corpus Pau
@nc toue +s @f
@nc �ra +s @f
@nc toulouho�re +s @f

## @nc rendez-vous + @m

## Corpus EFL
@adj jurisprudentiel +s +le +les
@adj undecie +s + +s
@adj duodecie +s + +s
@adj terdecie +s + +s
@adj quaterdecie +s + +s
@adj quindecie +s + +s
@adj sexdecie +s + +s
@adj septedecie +s + +s
@adj octodecie +s + +s
@adj novodecie +s + +s

@adj vicie +s + +s
@adj unvicie +s + +s
@adj duovicie +s + +s
@adj tervicie +s + +s
@adj quatervicie +s + +s
@adj quinvicie +s + +s
@adj sexvicie +s + +s
@adj septevicie +s + +s
@adj octovicie +s + +s
@adj novovicie +s + +s

@nc non-d�ductibilit� +s @f
@nc non-lucrativit� +s @f
@adj non-r�sident +s +e +es
@adj non-salari� +s +e +es
@nc assurance-vie assurances-vie @f

## sous-capitaliser
## sous-�valuer
## sous-louer

@adj synallagmatique +s + +s

## titriser v (new)
@adj titris� +s +e +es

@redirect suiv. suivant

@nc irr�vocabilit� +s @f
@adj conchylicole +s + +s
@adj asphaltique +s + +s
@adj notionnel +s +le +les
@nc know-how + @m
@nc inconstructibilit� +s @f
@adj mytilicole +s + +s
@adj franco-belge +s + +s
@adj franco-am�ricain +s +e +es
@nc cubitainer +s @m
@adj laryngectomis� +s +e +es
@nc compte-rendu comptes-rendus @m

@nc memorandum +s @m
@nc titrisassion +s @f

@adj macroprudentiel +s +le +les

@nc irr�sistibilit� +s @f

## franchiser v (new)
@adj franchis� +s +e +es

@adj vosgien +s +ne +nes

@nc fiducie +s @f

## missionner v (lglex)
## conventionner v (lglex)

@adj nosocomial nosocomiaux +le +les
@adj usufructuaire +s + +s
@nc �quivocit� +s @f
@nc r�vocabilit� +s @f
@nc anonymisation +s @f
@nc objectivisation +s @f

@adj liquidatif +s liquidative liquidatives
@adj restituable +s + +s
@adj invocable +s + +s
@nc impatri� +s @m
@nc trustee +s @m
@nc inexploitation +s @f
@nc inali�nabilit� +s @f
@adj r�gularisateur +s r�gularisatrice r�gularisatrices
@nc demanderesse +s @f
@adj confiscatoire +s + +s
@adj individualisable +s + +s
@nc sinistrabilit� +s @f
@nc dirigeable +s @m
@nc cr�matorium +s @m
@adj t�l�chargeable +s + +s
@adj montagnard +s +e +es
@nc lic�it� +s @f
@nc cash + @m
@nc d�r�f�rencement +s @m
@adj exon�ratoire +s + +s
@nc facturette +s @f
@nc pr�empteur +s @m
@nc pr�emptrice +s @f
@adj modificatif +s modificative modificatives
@adj monovalent +s monovalente monovalentes
@adj subrogatif +s subrogative subrogatives
@adj subrogatoire +s + +s
@nc cash-flow cash-flows @m
@adj pond�ral +s pond�rale pond�rales
@nc d�consolidation +s @f
@nc ent�te +s @
@nc affactureur +s @m
@adj qualifiant +s +e +es
@adj tutoral +s +e +es
@nc harceleur +s @m
@nc ressenti +s @m
@adj validable +s + +s
@adj putatif +s putative putatives
@nc sachant +s @m
@nc aidant +s @m
@nc commettant +s @m
@nc promettant +s @m
@nc r�clamant +s @m
@nc patient�le +s @f
@nc financiarisation +s @f
@nc solvabilisation +s @f
@adj mortif�re +s + +s
@nc m�latonine +s @f
@nc professionalit� +s @f
@nc apatride +s @
@nc subventionnement +s @m

appel	100	cfi	[pred="appel_____1<Suj:cln|sn,Objde:(de-sn|en)>",lightverb=interjeter]	appel_____1	Default	%default
acqu�reur	100	cfi	[pred="acqu�reur_____1<Suj:cln|sn,Objde:(de-sn|en)>",lightverb=porter]	acqu�reur_____1	Default	%default

im-_	adjPref	[pred="in-______1<Suj:(sn)>",cat=adj]	in-______1	Default	%default
in-_	adjPref	[pred="in-______1<Suj:(sn)>",cat=adj]	in-______1	Default	%default
d�-_	adjPref	[pred="d�-______1<Suj:(sn)>",cat=adj]	d�-______1	Default	%default
d�s-_	adjPref	[pred="d�-______1<Suj:(sn)>",cat=adj]	d�-______1	Default	%default

@nc labellisation +s @f
@adj substituable +s + +s
@adj r�num�ratoire +s + +s
@adj facturable +s + +s
@adj �valuatif +s �valuative +s
@adj sanctionnable +s + +s
@adj licenciable +s + +s

actuariellement	adv	[pred="actuariellement_____1",cat=adv]	actuariellement_____1	Default

@nc identifiant +s @m
@adj qu�rable +s + +s
@nc externalit� +s @f
@adj d�rogeable +s + +s
@adj curat�laire +s + +s
@nc curat�laire +s @m
@adj dosim�trique +s + +s
@adj illustratif +s illustrative illustratives
@nc sans-papier sans-papiers @
@adj rattachable +s + +s
@nc transf�rabilit� +s @f
@nc d�flocage +s @m
@nc bilat�ralisation +s @f
@nc r��quilibre +s @m
@adj transitionel +s +le +les
@adj topique +s + +s
@adj vaccinal +s +e +es
@nc benfluorex + @m
@nc l�s� +s @m
@nc l�s�e +s @f
@adj incriminateur +s incriminatrice incriminatrices
@nc transsexuel +s @m
@nc parapente +s @m
@adj identifiant +s +e +es
@adj r�g�n�rable +s + +s
@nc biovigilance +s @f
#@adj in_vitro in_vitro in_vitro in_vitro
@adj saisissable +s + +s
@adj sommaire +s + +s
@nc s�ropositivit� +s @f
@nc accipiens + @m
@adj implantable +s + +s
@nc refinancement +s @m
@nc nanoparticule +s @f
@nc repreneur +s @m
@nc repreneuse +s @f
@nc d�compilation +s @f
@adj prorogeable +s + +s
@adj internalisateur +s internalisatrice internalisatrices
@nc commandit� +s @m
@adj v�t�rinaire +s + +s
@nc compte-titres comptes-titres @m

agissant	100	v	[pred="s'agir_____2<Objde:de-scompl|de-sinf|de-sn|en|scompl>",@impers,@pron,cat=v,@G]	agir_____2	Default	G	%actif_impersonnel
r�ception	100	cfi	[pred="r�ception_____1<Suj:cln|sn,Objde:(de-sn|en)>",lightverb=accuser]	r�ception_____1	Default	%default

appert	100	v	[pred="apparoir_____1<Suj:de-sinf|scompl|sn,Obj:(scompl),Obj�:(cld|�-sn)>",@impers,cat=v,@P3s]	apparoir_____1	ThirdSing	P3s	%actif_impersonnel

@nc condensat +s @m
@adj valorisable +s + +s

consid�ration	100	cfi	[pred="consid�ration_____1<Suj:cln|sn>",lightverb=m�riter]	consid�ration_____1	Default	%default

contact	100	cfi	[pred="contact_____1<Suj:cln|sn,Obl:avec-sn>",lightverb=reprendre]	contact_____1	Default	%default

@nc air-bag +s @m
@nc cuisiniste +s @m
@nc recapitalisation +s @f
@nc hyperinflation +s @f
# @adj stockeur
@nc l�nder +s @m
@nc trir�acteur +s @m
#@nc trompe-l'oeil trompes-l'oeil @m
@adj sacro-saint +s +e +es
@adj rhodanien +s +ne +nes
@nc t�l�p�age +s @m
@nc lease-back + @m
@adj interf�rom�trique +s + +s
@nc garde-fous + @m
@adj eurosceptique +s + +s
@nc essuie-tout + @m
@nc conjoncturiste +s @m
#@nc chef-d'oeuvre chefs-d'oeuvre @m
@nc monospace +s @
@nc sweat +s @m
@nc sportswear +s @m
@adj spoliatoire +s + +s
@nc s�raphisme +s @m
@adj rousseauiste +s + +s
@nc roastbeef + @m
@adj r�siduaire +s + +s
@adj prudentiel +s +le +les
@adj recyclable +s + +s
@adj privatisable +s + +s
#@adj prince-de-Galles + + +
@nc mulard +s @m
@adj montable +s + +s
@nc bitioniau +x @m
@nc billetiste +s @
@nc attrape-tout + @m
@nc come-back + @m
@nc be-bop + @m
@nc aromath�rapie +s @f
@nc agiotateur +s @m
@nc a�robic + @
@nc �-coup +s @m
@adj calendaire +s + +s
@nc croissanterie +s @f
@nc entrepeunariat +s @m
@nc laisser-faire + @m
@nc savoir-faire + @m
@nc savoir-vivre + @m

@adj infracommunautaire +s + +s
@adj extracommunautaire +s + +s
@nc mobil-home +s @m

bien	adv	[adv_kind=intensive]	bien____20	Default

## FTB6

@nc stand-by + @m
@nc hoola-hoop + @m
@nc assurance-maladie assurances-maladie @f
@nc scoubidou +s @m
@nc banlieusardisation +s @f

@nc faire-savoir + @m

@nc stockeur +s @m

## faire valoir que
valoir	v	[pred="valoir_____1<Suj:cln|scompl|sinf|sn,Obj:de-sinf|sn|scompl,Obj�:(cld|�-sn)>",@CtrlSujObl,@active,@pers,cat=v,@W]	valoir_____1	Infinitive	W %actif

ordre	100	cfi	[pred="ordre_____1<Suj:cln|sn,Obj�:�-sn|y>",lightverb=mettre]	ordre_____1	Default	%default

las	pres	[pred="las_____1"]	las_____1	Default

## aller bon train
train	100	cfi	[pred="train_____1<Suj:cln|sn,Obl:(sur-sn)>",lightverb=aller]	train_____1	Default	%default

## avoir bon dos
dos	100	cfi	[pred="dos_____1<Suj:cln|sn>",lightverb=avoir]	dos_____1	Default	%default

## avoir cure de
cure	100	cfi	[pred="cure_____1<Suj:cln|sn,Objde:de-sn|en|scompl>",lightverb=avoir]	cure_____1	Default	%default

## trouver preneur
preneur	100	cfi	[pred="preneur_____1<Suj:cln|sn,Obl:(pour-sn)>",lightverb=trouver]	preneur_____1	Default	%default

@nc rural ruraux @m

## From Subtitles 
@nc blaster +s @m
@nc maton +s @m
@nc donut +s @m
@nc spa +s @m
@nc chamallow +s @m
@nc extasie +s @f
@nc klingon +s @m
@nc shooter +s @m
@nc traversier +s @m
@nc tiramisu +s @m
@nc macadamia + @m
@nc distille +s @m
@nc prednisone +s @f
@nc tortilla +s @f
@nc banshee +s @
@nc craquelin +s @m
@nc tapas + @m
@nc pager +s @m
@nc roadie +s @m
@nc bip +s @m
@nc vascularite +s @f
@nc bitosse +s @m
@nc delirium +s @m
@nc chakra +s @m
@nc biffeton +s @m
@nc boxon +s @m
@nc branleur +s @m
@nc branleuse +s @f
@nc sniffeur +s @m
@nc voltigeur +s @m
@nc voltigeuse +s @f
@nc illuminati + @m
@nc crevure +s @f
@nc jumper +s @m
@nc beeper +s @m
@nc playmate +s @f
@nc tantouze +s @f
@nc jacuzzi +s @m
@nc pianola +s @m
@nc gelato +s @m
@nc magnet +s @m
@nc bombasse +s @f
@nc ordonnancier +s @m
@nc croustille +s @f
@nc frittata +s @f
@nc yeshiva +s @f
@nc compadre +s @m
@nc gin-fizz + @m
@nc post-op + @m
@nc machiatto +s @m
@nc practice +s @m
@nc skiffle +s @m
@nc surfer +s @m
@nc cacheton +s @m
@nc hummer +s @m
@nc dramamine +s @f
@nc intercom +s @m
@nc katana +s @m
@nc louloute +s @f
@nc chihuahua +s @m
@nc foufoune +s @f
@nc podcast +s @m
@nc chou�a +s @m
@nc feta +s @
@nc diagnosticien +s @m
@nc nibard +s @m
@nc sambuca +s @f
@nc lambada +s @f
@nc guacamole +s @m
@nc tapineuse +s @f
@nc petit-d�jeuner + @m
@nc croquemitaine +s @m
@nc disserte +s @f
@nc bruschetta +s @f
@nc pepperoni +s @m
@nc comp�te +s @f
@nc tael +s @m
@nc croque-mort +s @m
@nc gangsta +s @m
@nc aspersion +s @f
@nc chimichanga +s @m
@nc ricotta +s @f
@nc nanobot +s @m
@nc profiler +s @m
@nc amuse-gueule +s @m
@nc chaudasse +s @f
@nc tchatcheur +s @m
@nc tchatcheuse +s @f
@nc croque-monsieur +s @m
@nc �clampsie +s @f
@nc couvre-th�iere +s @m
@nc poupoune +s @f
@nc d�monte-pneu +s @m
@nc nuancier +s @m
@nc avorteuse +s @f
@nc leprechaun +s @m
@nc jackass + @m
@nc pare-feu +s @m

@title signor +s @m

@adj d�crypteur +s d�crypteuse +s
@adj balaise +s + +s
@adj cyber + + +
@adj riquiqui + + +
@adj teriyaki + + +
@adj jouasse +s + +s
@adj tamponneur +s tamponneuse +s
@adj mitral mitraux +e +es
## qu�becois: ringard
@adj qu�taine +s + +s

## meths
## ampthets
## zou ?
## basta ?
## pschitt
## craignos
## patati

@redirect ouai oui
@redirect d�gueu d�gueulasse
@redirect d�gueus d�gueulasses
@redirect gastro gastro-ent�rite
@redirect gastros gastro-ent�rites
@redirect ecsta extasie
@redirect exta extasie
@redirect perme permission
@redirect compil compilation
@redirect compils compilations
@redirect jap japonais
@redirect caf�te caf�t�ria
@redirect paras parachutistes
@redirect couv' couverture
@redirect gode godemichet
@redirect godes godemichets
@redirect baballe balle
@redirect thalasso thalassoth�rapie
@redirect amph�te amph�tamine
@redirect amph�tes amph�tamines
@redirect transcript transcription
@redirect transcripts transcriptions
#@redirect m'man maman 
@redirect sc�nar sc�nario
@redirect sc�nars sc�narios
@redirect congel cong�lateur
@redirect chouia chou�a
@redirect petit-d�j petit-d�jeuner
@redirect pourliche pourboire
@redirect pourliches pourboires

#@redirect pouffe pouffiasse
@nc pouffe +s @f

hyper	adv	[pred="hyper_____1",cat=adv]	hyper_____1	Default

## bavasser v
## flipper v
## crasher v
## shooter
## gratouiller
## bipper
## booster
## bidonner
## digresser
## bradycardiser +Obj !
## clamper +Obj
## dealer
## biopser +Obj
## intuber

� part	120	prep	[pred="�_part_____1<Obj:sa|sadv|sinf|sn>",cat=prep]	�_part____1	Default	%default    
� part	120	adj	[pred="�_part_____1<Suj:(cln|sn)>",cat=adj]	�_part____1	Default	%default    

## attendre confirmation
confirmation	100	cfi	[pred="confirmation_____1<Suj:cln|sn,Objde:(de-sn|scompl|de-sinf)>",lightverb=attendre]	confirmation_____1	Default	%default

@nc surqualifi� +s @m
@nc surqualifi�e +s @f

@nc bitexte +s @m
@nc phras�me +s @m
@nc morphe +s @m
@nc metadata + @f
@nc n-gram +s @m
@nc r�analyse +s @f
@nc qualia +s @m

@adj disfluent +s +e +es
@adj voyell� +s +e +es
@adj syntagmatique +s + +s
@adj positionnel +s +le +les 
@adj cooccurrent +s +e +es 
@adj n-aire +s + +s 
@adj n-meilleur +s + +s 

morpho-_	120	advPref	[pred="morpho-______2<Suj:(sn)>",cat=adv]	morpho-______2	Default

@adj sybillin +s +e +es

@nc boson +s @m
@nc flagelle +s @f
@nc dharma + @m
@nc magasine +s @m
@nc ratification +s @f
@nc burka +s @f

@adj multipartite +s + +s
@adj accroupi +s +e +es

## missing passives
## r�ver
## exploser

## pb on agenouiller as participle/passive
## we got a whole set of such verbs (+pron +active)
@adj accroupi +s +e +es
@adj tapi +s +e +es
@adj acoquin� +s +e +es
@adj agenouill� +s +e +es
@adj blotti +s +e +es
@adj mott� +s +e +es
@adj prostern� +s +e +es
@adj recroquevill� +s +e +es
@adj r�fugi� +s +e +es
@adj �croul� +s +e +es
@adj vautr� +s +e +es

## missing @pron on the following verbs
@adj p�m� +s +e +es

@adj d�teint +s +e +es
@adj trembl� +s +e +es

@adj leste +s + +s

@nc cr�mone +s @f
@adj souabe +s + +s
@nc plessis + @m
@nc d�bine +s @f
@nc insens� +s @m
@nc insens�e +s @f
@nc psalmodie +s @f
@nc convict +s @m
@nc pa�s + @m
@nc radier +s @m
@nc palanfrier +s @m
@nc canadair +s @m

@adj vell�itaire +s + +s

mordieu	pres	[pred="mordieu_____1"]	mordieu_____1	Default


mot	100	cfi	[pred="mot_____1<Suj:cln|sn,Objde:(de-sn|scompl|de-sinf)>",lightverb=souffler]	mot_____1	Default	%default
attention	100	cfi	[pred="attention_____1<Suj:cln|sn,Obj�:(�-sn|y|�-sinf)>",lightverb=pr�ter]	attention_____1	Default	%default

## not sure the following line is the best choice
## I believe there is a more general mechanism related to 'mettre' X est hors-jeu => Y met X hors-jeu
hors-jeu	100	cfi	[pred="hors-jeu_____1<Suj:cln|sn,Obj:(sn|cla)>",lightverb=mettre]	hors-jeu_____1	Default	%default

@nc corrigendum corrigenda @m
@nc provisionnement +s @m

@adj mutag�ne +s + +s
@adj hexavalent +s +e +es
@adj myope +s + +s

bon m�nage	100	cfi	[pred="bon_m�nage_____1<Suj:cln|sn>",lightverb=faire]	bon_m�nage_____1	Default	%default

@adj fantasm� +s +e +es

@nc mixte +s @m
@nc exclave +s @f

@adj d�centr� +s +e +es
@adj dialogu� +s +e +es

@nc carcinologiste +s @
@adj trivalent +s +e +es
@nc transposon +s @m
@nc tilapia +s @m
@nc tegula tegulae @f
@nc polytope +s @m
@nc s�quen�age +s @m
@nc �motic�ne +s @f
@nc syst�maticien +s @m

@adj mac�r� +s +e +es
@nc microtubule +s @f
@nc pronostique +s @m
@adj vent� +s +e +es
@nc exhaustion +s @f
@nc bouchain +s @m
@nc cryptanalyste +s @
@adj cadr� +s +e +es
@nc tesseract +s @m
@adj traversier +s traversi�re traversi�res
@nc endormissement +s @m

##@redirect auxquel auxquels
@nc rotavirus + @m
@nc baston +s @f
@adj peureux + peureuse peureuses
@nc cr�nelage +s @m
@adj r�flexif +s r�flexive r�flexives
@nc liquidateur +s @m
@nc e-commerce +s @m

@adj ci-devant + + +
@nc ci-devant + @

@redirect fuss� fusse

@adj jailli +s +e +es

Internet	np	[cat=np]	Internet_____1	Default

## from EMEA error mining
@nc tubule +s @m
@nc virion +s @m
@nc lyophilisat +s @m
@nc folinate +s @m

compte	100	cfi	[pred="compte_____1<Suj:cln|sn,Objde:(de-sn|scompl)>",lightverb=tenir]	compte_____1	Default	%default
parti	100	cfi	[pred="parti_____1<Suj:cln|sn,Objde:(de-sn|scompl)>",lightverb=tirer]	parti_____1	Default	%default

## * faire appel
appel	100	cfi	[pred="appel_____1<Suj:cln|sn,Objde:(de-sn)>",lightverb=faire]	appel_____1	Default

## * faire grief
## * faire peur
peur	100	cfi	[pred="peur_____1<Suj:cln|sn>",lightverb=faire]	peur_____1	Default

## * faire obligation
## * faire long feu
## * faire machine arri�re
## * faire cause
## * faire justice
## * faire jeu �gal

## ** prendre cong� (de lui)
cong�	100	cfi	[pred="cong�_____1<Suj:cln|sn,Objde:(de-sn)>",lightverb=prendre]	cong�_____1	Default	%default

## ** prendre appui (sur lui)
appui	100	cfi	[pred="appui_____1<Suj:cln|sn,Obl:(sur-sn)>",lightverb=prendre]	appui_____1	Default	%default

## ** prendre ombrage
ombrage	100	cfi	[pred="ombrage_____1<Suj:cln|sn,Objde:(de-sn|scompl)>",lightverb=prendre]	ombrage_____1	Default	%default

## ** prendre note (de sa venue)
note	100	cfi	[pred="note_____1<Suj:cln|sn,Objde:(de-sn|scompl)>",lightverb=prendre]	note_____1	Default	%default

## ** tirer avantage
avantage	100	cfi	[pred="avantage_____1<Suj:cln|sn,Objde:(de-sn)>",lightverb=tirer]	avantage_____1	Default	%default

## trouver chaussure � son pied

## ** d�poser plainte (contre lui)
plainte	100	cfi	[pred="plainte_____1<Suj:cln|sn,Obl:(contre-sn)>",lightverb=d�poser]	plainte_____1	Default	%default

## donner suite � X
## suite	100	cfi	[pred="suite_____1<Suj:cln|sn,Obl:(�-sn|y)>",lightverb=donner]	suite_____1	Default	%default



@adj membre +s + +s
@adj sucrier +s sucri�re +s
@adj stockeur +s stockeuse +s
@adj normand +s normande +s
@adj nature +s + +s
@adj junior +s + +s
@adj correcteur +s correctrice +s
@adj voyageur +s voyageuse +s
@adj vid�o +s + +s
@adj v�g�tarien +s v�g�tarienne +s
@adj vainqueur +s + +s
@adj vedette +s + +s

@adj type +s + +s
@adj teigneux +s teigneuse +s
@adj sismique +s + +s
@adj s�culier +s s�culi�re +s
@adj pr�teur +s pr�teuse +s
@adj pol�mique +s + +s
@adj perturbateur +s perturbatrice +s
@adj papetier +s papeti�re +s
@adj menteur +s menteuse +s
@adj l�gumier +s l�gumi�re +s
@adj surnum�raire +s + +s

@adj laitier +s laiti�re +s
@adj multiplicateur +s multiplicatrice +s
@adj diviseur +s diviseuse +s
@adj client +s +e +es

@nc s�rendipit� +s


## Error mining on MERCK 2015/01/29
## autolimit� +�tre

@nc d�trusor +s @m
@nc infiltrat +s @m
@nc pr�charge +s @f
@nc surfactant +s @m
@adj liquidien +s +ne +nes
@nc foramen +s @m
@nc d�congestionnant +s @m
@nc dialysat +s @m
@nc tophi + @m
@nc coagulase +s @f
@nc blaste +s @m
@nc acidurie +s @f
@nc cholestase +s @f
@nc aminoside +s @m
## elle peut passer inaper�ue
@adj tricupside +s + +s
@nc fibrate +s @f
@adj h�matog�ne +s + +s
@nc m�le +s @f
@nc �chodoppler +s @m
@nc hantavirus +s @m
@adj pulsatile +s + +s
@adj carotidien +s +ne +nes
@nc vasopressine +s @f
@nc mucine +s @f
@nc bisphosphonate +s @m
@nc neuroblastome +s @m
@adj inspiratoire +s + +s
@nc scl�rodermie +s @f
@nc thoracotomie +s @f
@nc percentile +s @m
@nc cardiom�galie +s @f
@adj t�tanique +s + +s
@adj m�nopaus� +s +e +es
@nc aspl�nie +s @f
@nc macrolide +s @m
@nc niacine +s @f
@nc bronchiolite +s @f
@nc lymphome +s @m
@nc capsulite +s @f
@adj ad�nomateux + ad�nomateuse ad�nomateuse
@adj m�tastas� +s +e +es
@nc embole +s @f

@adj facteur +s factrice +s
@nc escorteur +s @m
@adj transporteur +s transportrice +s



