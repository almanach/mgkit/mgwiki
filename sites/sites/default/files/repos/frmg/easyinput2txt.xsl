<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:exslt="http://exslt.org/common"
  >

<xsl:output
  method="text"
  indent="yes"
  encoding="ISO-8859-1"/>

<xsl:strip-space elements="*"/>

<xsl:template match="DOCUMENT">
<xsl:text>## FICHIER=</xsl:text>
<xsl:value-of select="@fichier"/>
<xsl:text>
</xsl:text>
<xsl:apply-templates select="*"/>
</xsl:template>

<xsl:template match="E">
<xsl:text>## </xsl:text>
<xsl:value-of select="@ID|@id"/>
<xsl:text>
</xsl:text>
<xsl:if test="F[last()] = '.' and F[last()-1] = 'M'">
<xsl:text>## AGGL
</xsl:text>
</xsl:if>
<xsl:apply-templates select="*"/>
<xsl:text>
</xsl:text>
</xsl:template>

<xsl:template match="F">
  <xsl:variable name="token">
    <xsl:value-of select="translate(.,' ','_')"/>
  </xsl:variable>
  <xsl:text>{{</xsl:text>
  <xsl:value-of select="@ID"/>
  <xsl:text>|</xsl:text>
  <xsl:value-of select="$token"/>
  <xsl:text>}}</xsl:text>
  <xsl:value-of select="$token"/>
  <xsl:text> </xsl:text>
</xsl:template>


</xsl:stylesheet>
