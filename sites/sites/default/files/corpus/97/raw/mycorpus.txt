sauce madère
sauce marchand de vin
sauce mornay
sauce moutarde
sauce nantua
sauce napolitaine
sauce nuoc-mam
sauce pesto
sauce piccalilly
sauce piquante
sauce piquante antillaise
sauce poivre vert
sauce provençale
sauce salade moutarde
sauce tandoori
sauce tartare
sauce tomate
sauce tomate sans sel ajouté
sauce worcestershire
sauce yakitori
saucisse cocktail
saucisse de Francfort
saucisse de Montbéliard
saucisse de Morteau
saucisse de Strasbourg (type Knacki)
saucisse de Toulouse
saucisse de volaille
saucisse sèche
saucisse végétarienne (au tofu)
saucisson de jambon (type Saint Agaûne)
sauge deshydratée
saumon cru / sashimi
saumon fumé
séitan
sel ajouté à table
sel de céleri
sel de préparation
sel de régime (sans sodium) (supprimé)
semoule (graine de couscous)
semoule de mais (polenta)
sirop 0% de sucre à diluer
sirop à 0% de sucre prêt à boire (1+7)
sirop d'agave
sirop de fruit à diluer
sirop de fruit prêt à boire (1 + 7)
sirop de Liège
sirop de sucre de canne
sirop d'érable
soda à l'orange
soda à l'orange light
soda au cola
soda au cola décaféiné
soda au cola light
soda light
soda tonique (type schweppes)
soja, grain cuit
sole
son d'avoine
son de blé
sorbet (glace à l'eau)
sorbet allégé
sorbet au citron
sorgho
steak de cheval
steak de soja au curry
steak de soja au curry
steak de soja aux fines herbes
steak de soja aux fines herbes
steak de soja aux petits légumes
steak de soja aux petits légumes
steak de soja nature
steak de soja tomate-basilic
steak de soja tomate-basilic
steak de soja tomate-poivron
steak de soja tomate-poivron
steak haché de boeuf 10% MG cru
steak haché de boeuf 10% MG cuit
steak haché de boeuf 15% MG cru
steak haché de boeuf 15% MG cuit
steak/bifteck de boeuf grillé
stevia
substitut de repas reconstitué
substitut de viande mycoprotéiné (type Quorn)
substitut de viande mycoprotéiné pané (type Quorn)
sucre blanc
sucre blond, pure canne (type saint louis)
sucre d'orge
sucre glace
sucre roux, cassonade
sucre vanillé
tabasco
tamarin (pulpe sèche)
tapenade
tapioca
tarama
taro
tartare de cheval
tartine craquante (type cracotte)
tartine craquante fourrée chocolat ou fruits (type Craquinette)
tartine craquante fourrée chocolat ou fruits (type Craquinette)
tartine craquante sans gluten
tempeh
téquila
terrine de canard
terrine de poisson
terrine ou mousse de légumes
thé
thé blanc
thé glacé
thé glacé light
thé noir
thé rooibos
thé vert
thon à la catalane
