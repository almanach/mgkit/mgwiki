<?php


/**
 * Build the json tree and the associated decorations and guards
 * @param unknown $xmlNode
 * @param unknown $jsonNode
 */
function buildJSONTree($graph_uid,$xmlNode,&$htmlContent,&$jsonNode,$depth=0,&$debugData=array()){
  static $node_like_types = array(
      'node',
      'sequence',
      'alternative',
      'optional',
      'interleave',
  );
  
  if(isset($debugData[$depth])){
    $debugData[$depth]++;
  }else{
    $debugData[$depth]= 1;
  }
  

  $type = $xmlNode->getName();
  global $debugInfo;
  if(isset($debugInfo['count'])){
    $debugInfo['count']+=1;
  }else{
    $debugInfo['count']=1;
  }

  global $nodeInfo;

  if(in_array($type, $node_like_types)){
    // set uniqid
    $jsonNode['uid'] = uniqid();
    $jsonNode['nodeType'] = $type;
    $jsonNode['hasGuard'] = false;
    $jsonNode['hasFeatures'] = false;
    // copy attributes
    foreach ($xmlNode->attributes() as $key => $value){
      // the id is a property used by d3js, it should not be set
      if("$key" == "id"){
        $jsonNode["name"] = "$value";
        continue;
      }
      $jsonNode["$key"] = "$value";
    }
    // add children
    foreach($xmlNode->children() as $child){
      $child_type = $child->getName();
      if($child_type == 'guards'){
        $jsonNode['hasGuard'] = true;
        processGuards($graph_uid, $child,$htmlContent,$jsonNode['uid']);
      }else if($child_type == 'narg'){
        $jsonNode['hasFeatures'] = true;
        processDecoration($graph_uid, $child,$htmlContent,$jsonNode['uid']);
      }else{
        $jsonChildNode = &$jsonNode['children'][];
        buildJSONTree($graph_uid, $child, $htmlContent, $jsonChildNode, $depth+1, $debugData);
      }
    }
  }
  return $debugData;
}

function processGuards($graph_uid, $guards,&$html_content,$uid){
  $div = $html_content->getElementById("guards-$uid-$graph_uid");
  if($div === NULL){
    $div = $html_content->appendChild(new DOMElement("div"));
    $div->setAttribute("id","guards-$uid-$graph_uid");
    $div->setIdAttribute("id",true);
  }
  if($guards->children()->count()==1){
    $child = $guards->children();
    if($child->children()->count()==0){
      return;
    }
  }
  $table = $div->appendChild(new DOMElement("table"));
  $guard_type = (string) $guards['rel'];
  $table->setAttribute("border","1");
  if($guard_type == '-'){
    $table->setAttribute("class","negative-guard");
  }else if($guard_type == '+'){
    $table->setAttribute("class","positive-guard");
  }
  foreach($guards->children() as $child){
    $cell = $table->appendChild(new DOMElement("tr"));
    processGuardContent($child, $cell);
  }
}

function processGuardContent($guard,&$html){
  $name = $guard->getName();
  if($name == 'or'){
    $td = $html->appendChild(new DOMElement("td"));
    $td->appendChild(new DOMText("OR"));
    $td = $html->appendChild(new DOMElement("td"));
    $td->setAttribute("colspan","2");
    $table = $td->appendChild(new DOMElement("table"));
    $table->setAttribute("class","or");
    foreach($guard->children() as $child){
      $return = $table->appendChild(new DOMElement("tr"));
      processGuardContent($child, $return);
    }
  }else if($name == 'and'){
    $table = $html->appendChild(new DOMElement("td"))->appendChild(new DOMElement("table"));
    $table->setAttribute("class","and");
    foreach($guard->children() as $child){
      $return = $table->appendChild(new DOMElement("tr"));
      processGuardContent($child, $return);
    }
  }
  else if($name == 'guard'){
    $children = $guard->children();
    $td = $html->appendChild(new DOMElement("td"));
    processFeature($children[0],$td);
    $td = $html->appendChild(new DOMElement("td"));
    $td->appendChild(new DOMText("="));
    $td = $html->appendChild(new DOMElement("td"));
    processFeature($children[1],$td);
    return;
  }
}

function processFeature($feature,&$dom){
  $name = $feature->getName();
  $children = $feature->children();
  switch($name){
    case 'var':
      $html_node = $dom->appendChild(new DOMElement("span"));
      $ftable = $html_node->appendChild(new DOMElement("table"));
      $ftable->setAttribute("class","var");
      $html_node = $ftable->appendChild(new DOMElement("tr"));
      $td = $html_node->appendChild(new DOMElement("td"));
      $td->setAttribute("class","varname");
      $td->appendChild(new DOMText($feature['name']));
      if(count($children) == 1){
        $td = $html_node->appendChild(new DOMElement("td"));
        $td->setAttribute("class","fvalue varvalue");
        processFeature($children[0],$td);
      }else{
        //should not happen
      }
      break;
    case 'minus':
      $html_node = $dom->appendChild(new DOMElement("span"));
      $html_node->appendChild(new DOMText("-"));
      break;
    case 'plus':
      $html_node = $dom->appendChild(new DOMElement("span"));
      $html_node->appendChild(new DOMText("+"));
      break;
    case 'symbol':
      $html_node = $dom->appendChild(new DOMElement("span"));
      $html_node->appendChild(new DOMText($feature['value']));
      break;
    case 'vAlt':
      $html_node = $dom->appendChild(new DOMElement("span"));
      for($i=0;$i<count($children);$i++){
        $child = $children[$i];
        processFeature($child,$html_node);
        if($i<count($children)-1){
          $html_node->appendChild(new DOMElement("span","|"))->setAttribute("class","vAlt");
        }
      }
      break;
    case 'not':
      $html_node = $dom->appendChild(new DOMElement("span","~"));
      $html_node->setAttribute("class", "not");
      processFeature($children[0],$html_node);
      break;
    case 'fs':
      $html_node = $dom->appendChild(new DOMElement("span"));
      $ftable = $html_node->appendChild(new DOMElement("table"));
      $ftable->setAttribute("border","1");
      $ftable->setAttribute("class","fs");
      foreach ($children as $child){
        processFeature($child,$ftable);
      }
      break;
    case 'f':
      $html_node = $dom->appendChild(new DOMElement("tr"));
      $html_node->setAttribute("class","fval");
      $td = $html_node->appendChild(new DOMElement("td"));
      $td->setAttribute("class","feature");
      $td->appendChild(new DOMText($feature['name']));
      $td = $html_node->appendChild(new DOMElement("td"));
      $td->setAttribute("class","fvalue");
      processFeature($children[0],$td);
      break;
  }
}
/*
 * var
vAlt
minus
symbol
fs
f
not
plus
*/

function processDecoration($graph_uid, $decoration,&$html_content,$uid){
  $div = $html_content->getElementById("decoration-$uid-$graph_uid");
  if($div === NULL){
    $div = $html_content->appendChild(new DOMElement("div"));
    $div->setAttribute("id","decoration-$uid-$graph_uid");
    $div->setIdAttribute("id",true);
    $table = $div->appendChild(new DOMElement("table"));
    $table->setAttribute("class","decoration");
  }
  $table = $div->getElementsByTagName("table");
  $table = $table->item(0);
  foreach($decoration->children() as $child){
    $tr = $table->appendChild(new DOMElement("tr"));
    $tr->setAttribute("class","fval");
    $cell = $tr->appendChild(new DOMElement("td"));
    $cell->setAttribute("class","feature");
    $cell->appendChild(new DOMText($decoration['type']));
    $cell = $tr->appendChild(new DOMElement("td"));
    $cell->setAttribute("class","fvalue");
    processFeature($child, $cell);
  }
}