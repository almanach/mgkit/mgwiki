<?php

module_load_include("inc", "d3js", "js/depgraph/depgraph_formats");
module_load_include("php", "d3js", "js/depgraph/depgraph");

$targetdir = variable_get('file_public_path').'/graphs_png_export';
if(!file_exists($targetdir)){
  drupal_mkdir($targetdir);
}


$options['title'] = isset($data['title'])?$data['title']:'';

$json_data = $data['data'];
$uid = isset($data['uid']) ? (string) $data['uid'] : uniqid();

$data = json_decode($json_data,true);

global $base_url;

$graph1 = new DepGraph($data,$options);
//$html1 = $graph1->getHTML();
$html1 = $graph1->getHTMLImage($targetdir,$uid,$base_url.'/');
echo $html1;