<?php
$basepath = drupal_get_path('module','d3js') . "/js/";


module_load_include('inc', 'd3js','js/depgraph/depgraph_formats');

$mgwikimodpath = drupal_get_path('module','mgwiki') ;
drupal_add_js($mgwikimodpath."/js/intro.js");
drupal_add_css($mgwikimodpath."/js/introjs.css");

global $base_url;

$ajson = array();
$json_data = $data['data'];


if(isset($options['background-color'])){
  $json = json_decode($json_data,true);
  $json['graph']['#style']['background-color'] = $options['background-color'];
  $json_data = json_encode($json);
}

$format_origin = $data['format'];

$options['exportFormats']=array(
  'json'=>'json',
  'image'=>'png',
  'dep2pict'=>'dep2pict'
);
$options['exportFormats'][$format_origin] = $format_origin;

$uid = $data['uid'];
$sentence = $data['sentence'];
if(!isset($sentence)){
  $sentence = getSentence($json_data);
}
$sentence = preg_replace("/'/","\'",$sentence);
$sentence = str_replace("\n", " ", $sentence);

$options['uid'] = $uid;

//$options['maxwidth']=800;

$options['frmgparserparams']=isset($data['frmgoptions'])?$data['frmgoptions']:'';



$title = isset($data['title'])?preg_replace("/'/","\'",$data['title']):'';
$title = str_replace("\n", " ", $title);
if(trim($title)==""){
  $title = "Graph";
}

$frmgserver_url = $base_url . '/frmgserver_proxy';

$ws_url = $base_url . '/d3js/ws_post';

if(!isset($data['gid'])){
  $data['gid'] = null;
}

if(isset($data['sentence_id'])){
  $sentence_node = node_load($data['sentence_id']);
  $is_primary = d3js_is_primary($data['gid'],$sentence_node);
}

$node = menu_get_object();
$sentence_view = false;
if(isset($node) && $node->type == 'sentence'){
  $sentence_view = true;
}

$options['viewmode']='fit';

if($sentence_view){
  $options['viewmode']='cropped';
  $options['maxwidth']=null;
}

$sentence_link = (isset($data['gid']))?getSentenceLink($data['gid'],$sentence):'#';
$revision_node_link = '';
if(isset($data['gnid'])){
  $revision_node_link = '<div>'.l('Revisions node','node/'.$data['gnid']).'</div>';
}
$refs = '';
if(isset($data['refs'])){
  $refs = '<div><ul style="margin-bottom:0px;">';
  foreach($data['refs'] as $ref){
    $attr = node_get_attribute(array($ref),"title");
    $refs .= '<li>' . l($attr[$ref],'node/'.$ref.'#graph-'.$uid) . '</li>';
  }
  $refs .= '</ul></div>';
}


$json_options = json_encode($options);

if(isset($options['single-edit-mode'])){

  echo '<div onclick="history.back(1);" style="cursor:pointer; text-decoration:underline;">'.t('Return').'</div>';
}


?>



<table style="margin-left:auto;margin-right:auto; text-align:center;">
<tr>
<?php if($sentence_view) {

  echo '
    <td  valign="top" width="40px">
<div id="graph-sidebar-'.$uid.'" class="graph-sidebar">
<ul>
<li>
<div id="graph-star-'.$uid.'" title="'.t("Promote to main representation").'" class="graph-star-'.($is_primary?'on':'off').'"></div>
</li>
<li>
<div id="graph-reload-'.$uid.'" title="'.t("Reload the representation").'" class="graph-reload"></div>
</li>
<li>
<div id="graph-delete-'.$uid.'" title="'.t("Delete this representation").'" class="graph-delete"></div>
</li>

</ul>

</div>
</td>
    ';
}?>
<?php 
if($uid && isset($options['show-votes'])) {
  $totals = d3js_votes($uid);
  $vote = d3js_vote($uid);
  $classUp = "d3js-vote-up";
  $classDown = "d3js-vote-down";
  if($vote != 0){
    if($vote>0){
      $classUp .= " d3js-voted-up";
    }else{
      $classDown .= " d3js-voted-down";
    }
  }

  echo '
    <td  valign="top" width="40px">
<div id="graph-sidebar0-'.$uid.'" class="graph-sidebar">
<ul>
<li>
<div id="graph-upvote-'.$uid.'" title="'.t("Agree with representation").'" class="'.$classUp.'">'.$totals[0].'</div>
</li>
<li>
<div id="graph-downvote-'.$uid.'" title="'.t("Disagree with representation").'" class="'.$classDown.'">'.$totals[1].'</div>
</li>

</ul>';

global $user;

if($user->uid!=0 && isset($options['show-votes'])){
  echo '<script>
jQuery("#graph-downvote-'.$uid.'").on("click",function(){
    var downvote = jQuery("#graph-downvote-'.$uid.'");
    if(!downvote.hasClass("d3js-voted-down")){
      downvote.html(parseInt(downvote.html())+1);
      downvote.addClass("d3js-voted-down");
      var upvote = jQuery("#graph-upvote-'.$uid.'");
      if(upvote.hasClass("d3js-voted-up")){
        upvote.removeClass("d3js-voted-up");
        upvote.html(parseInt(upvote.html())-1);  
      }
      
      depgraphlib.mgwiki_d3js_module_action("downvote","'.$uid.'","'.$ws_url.'");
    }else{
      downvote.html(parseInt(downvote.html())-1);
      downvote.removeClass("d3js-voted-down");
      
      depgraphlib.mgwiki_d3js_module_action("unvote","'.$uid.'","'.$ws_url.'");
    }
  }
);
jQuery("#graph-upvote-'.$uid.'").on("click",function(){
    var upvote = jQuery("#graph-upvote-'.$uid.'");
    if(!upvote.hasClass("d3js-voted-up")){
      upvote.html(parseInt(upvote.html())+1);
      upvote.addClass("d3js-voted-up");
      var downvote = jQuery("#graph-downvote-'.$uid.'");
      if(downvote.hasClass("d3js-voted-down")){
        downvote.removeClass("d3js-voted-down");
        downvote.html(parseInt(upvote.html())-1);  
      }
      
      depgraphlib.mgwiki_d3js_module_action("upvote","'.$uid.'","'.$ws_url.'");
    }else{
      upvote.html(parseInt(upvote.html())-1);
      upvote.removeClass("d3js-voted-up");
      
      depgraphlib.mgwiki_d3js_module_action("unvote","'.$uid.'","'.$ws_url.'");
    }
  }
);
</script>';
}else{
  echo '<script>
jQuery("#graph-upvote-'.$uid.'").on("click",function(){
        alert("Pour voter, connectez vous ou créez un compte mgwiki.");
        }
    );
jQuery("#graph-downvote-'.$uid.'").on("click",function(){
        alert("Pour voter, connectez vous ou créez un compte mgwiki.");
        }
    );
</script>';
}

echo '</div>
</td>
    ';
}
?>

<td style="text-align:center;">
<a id="graph-<?php echo $uid?>" name="graph-<?php echo $uid?>"></a>

<div id="graph-container-<?php echo $uid;?>" 
<?php if($sentence_view){
  echo 'style="width:100%;"';
};
  ?>
  ></div>
<div><?php 
if(!isset($options['wrapped'])) {
  if(isset($options['edit-mode'])){
    echo '<span id="d3js-title-'.$uid.'">';
    echo '
    <script>
      new mgwikilib.ui.MutableField("'.$title.'",jQuery("#d3js-title-'.$uid.'"),
        function(data){
          jQuery.ajax({
            type: "POST", 
            url: "'.$ws_url.'",
            data: {uid:"'.$uid.'",title:data,action:"setTitle"},
            error: function(jqXHR, textStatus, errorThrown) {
              alert(textStatus);
            }
          });
        }
      );
    </script>
    ';
  }else{
    echo $title;
  }
}
      
?>
</div>
<script>
  jQuery('.sentence-example').each(function(elt){
    if(this.innerHTML==""){
      jQuery(this).remove();
      }
    });
  var depGraph = new depgraphlib.DepGraph(jQuery("#graph-container-<?php echo $uid;?>"),<?php echo $json_data; ?>,<?php echo $json_options;?>);
  var viewer = depGraph.viewer;
  depGraph.wsurl = '<?php echo $ws_url ?>';
  <?php
    if(isset($options['edit-mode'])){
      echo "depGraph.sentence = '$sentence';
            depGraph.editObject.mode['default'].save = depgraphlib.EditObject.DefaultModeLib.save;
            depGraph.dataFormat = 'json';
            viewer.setFixedToolbar();
            ";
      if($format_origin == 'depxml'){
        echo "
              frmgEditMode = new depgraphlib.FRMGEditMode('$frmgserver_url');
              depGraph.editObject.addEditMode(frmgEditMode.mode);
              depGraph.editObject.setEditMode('frmg');
              depGraph.editObject.addEditModeSwitcher();
      ";
      }else{
        echo "depGraph.editObject.setEditMode('default');";
      }
    }
  ?>
  if(depGraph.options.viewmode == 'full'){
    viewer.noBorders();
  }  
  
  depGraph.sentenceLink = '<?php echo $sentence_link; ?>';
  depGraph.refs = '<?php echo $refs ?>';
  depGraph.revision_node_link = '<?php echo $revision_node_link ?>';

  if(<?php echo isset($options['wrapped'])?'true':'false';?>){
    var wrapper = viewer.addWrapper('<?php echo $title;?>');
    var div ='<div>'
    + depGraph.refs
    +'</div>';
    div = jQuery(div);
      
    depGraph.viewer.container.append(div);
  }

  
  <?php
      global $user;
      if($user->uid!=0 && !isset($options['edit-mode']) && isset($options['gid'])){
        
        echo "viewer.addToolbarItem({name:'edit',style:'edit',callback:function(){window.location = '" . $base_url . "/d3js/" . $options['gid'] . "/edit';}});";
      }
    ?>


    var promote_button = jQuery('#graph-star-<?php echo $uid?>');
    if(promote_button.hasClass('graph-star-off')){
      promote_button.on('click',function(){
        depgraphlib.promote('<?php echo $data['gid']?>','<?php echo $ws_url?>');
        }
      );
    }

    jQuery('#graph-reload-<?php echo $uid?>').on('click',function(){
        depgraphlib.reload('<?php echo $data['gid']?>','<?php echo $ws_url?>');
        }
    );

    jQuery('#graph-delete-<?php echo $uid?>').on('click',function(){
      var r=confirm("Deletion of the graph is irreversible. Confirm deletion?");
      if (r==true)
        {
        depgraphlib.remove('<?php echo $data['gid']?>','<?php echo $ws_url?>');
        }
    }
    );
    
    
  </script>
</td>
<?php 
if(isset($data['gnid'])){
  global $user;
  
  echo '<script>depGraph.gnid = "'.$data['gnid'].'";';
  if($user->uid != 0){
    echo ' depgraphlib.allowNotes("-'.$uid.'")';
  } 
  
  echo '</script>';
  $comments = d3js_get_notes($data['gnid']);

  
    echo '<td style="text-align:top;"><div id="notes-'.$uid.'"></div>';
    echo '<script>
        depgraphlib.tpl.notesRender('.json_encode($comments).',"'.$uid.'");</script></td>';
  
}

?>
</tr>
</table>

