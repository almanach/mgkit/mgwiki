CKEDITOR.dialog.add( 'editSentenceDialog', function ( editor ) {
    return {
      title: 'Enter new sentence example.',
      minWidth: 400,
      minHeight: 200,

      contents: [
          {
              id: 'tab-basic',
              label: 'Basic Information :',
              elements: [
                {
                  type: 'text',
                  id: 'sentence',
                  label: 'Sentence',
                  validate: CKEDITOR.dialog.validate.notEmpty( "Sentence field cannot be empty!" ),
                  setup: function(element){
                    this.setValue(element.getText());
                  },
                  commit : function(element){
                    element.setText( this.getValue() );
                  }
                },
                {
                  type: 'text',
                  id: 'uid',
                  label: 'UID',
                  onShow: function(){
                    this.disable();
                  },
                  setup: function(element){
                    this.setValue(element.getAttribute('uid'));
                  },
                  commit : function(element){
                    element.setAttribute( 'uid',this.getValue() );
                  }
                },
              ]
          },
          {
            id:'tab-advanced',
            label:'Advanced Settings',
            elements : [
            {
              type:'select',
              id:'format',
              label:'Format/Provider',
              items:[['frmgserver'],['json'],['conll'],['dep2pict'],['raw']],
              'default':'frmgserver',
              onChange:function(api){
                console.log('format/provider changed');
                console.log(this.getDialog());
              },
              setup: function(element){
                var provider = element.getAttribute('provider');
                if(!provider){
                  this.setValue(element.getAttribute('format'));
                }
                else{
                  this.setValue(element.getAttribute('provider'));
                }
              },
              commit : function(element){
                var format = this.getValue();
                if(format == 'frmgserver'){
                  element.setAttribute( 'provider',format );
                  element.removeAttribute( 'format');
                }else{
                  element.setAttribute( 'format', format);
                  element.removeAttribute('provider');
                }
              }
            },
            {
              type:'select',
              id:'viewmode',
              label:'Viewmode',
              items:[['full'],['wrapped']],
              'default':'full',
              setup: function(element){
                var viewmode = element.getAttribute('viewmode');
                if(viewmode){
                  this.setValue(viewmode);
                }
              },
              commit : function(element){
                var viewmode = this.getValue();
                element.setAttribute( 'viewmode', viewmode);
              }
            },
            {
              type:'text',
              id:'title',
              label:'Title',
              setup: function(element){
                var title = element.getAttribute('title');
                if(title){
                  this.setValue(title);
                }
              },
              commit : function(element){
                var title = this.getValue();
                element.setAttribute( 'title', title);
              }
            },
            {
              type:'text',
              id:'options',
              label:'FRMGOptions',
              setup: function(element){
                var options = element.getAttribute('options');
                if(options){
                  this.setValue(options);
                }
              },
              commit : function(element){
                var options = this.getValue();
                element.setAttribute( 'options', options);
              }
            },
            ]
          }
      ],
      
      onShow: function() {
        var selection = editor.getSelection();
        var element = selection.getStartElement();
        if ( element ){
          element = element.getAscendant( 'st', true );
        }

        if ( !element || element.getName() != 'st' ) {
            /*element = editor.document.createElement( 'strong');
            style = editor.document.createElement( 'em');
            sentenceTag = editor.document.createElement( 'st' );
            sentenceTag.setAttribute( 'provider', 'frmgserver' );
            style.appendText('"');
            style.append(sentenceTag);
            style.appendText('"');
            element.append(style);*/
            element = editor.document.createElement( 'st' );
            element.setAttribute( 'provider', 'frmgserver' );
            this.insertMode = true;
        }
        else {
            this.insertMode = false;
        }
        this.element = element;
        if ( !this.insertMode ){
          this.setupContent( element );
        }
      },
      
      onOk: function() {
        var dialog = this,
        sentence = dialog.element;

        dialog.commitContent( sentence );

        if ( dialog.insertMode ){
          var decoration = editor.document.createElement( 'span');
          decoration.setAttribute("class","sentence-example");
          decoration.append(sentence);
          editor.insertElement( decoration );
        }
      }
      
      
      
    };
});