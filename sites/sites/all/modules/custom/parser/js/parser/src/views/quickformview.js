(function(mgwiki,$){
  
  mgwiki.parser.QuickFormView = function(model,$el,options){
    this.model = model;
    this.el = ($el)?$el[0]:$('<div></div>');
    this.$el = $(this.el);
    this.initialized = false;
  };

  mgwiki.parser.QuickFormView.prototype.render = function(){
    var me = this;
    if(this.model){
      if(!this.initialized){
        this.$el.html(mgwiki.parser.QuickFormView.template);
        this.$el.find('.parser-qf-submit').click(function(){
          var sentence = me.$el.find('.parser-qf-st-input').val();
          var throbber = jQuery('.parser-ui-main-throbber',me.$el);
          if(!throbber.is(':hidden')){
            return;  
          }
          throbber.removeClass('parser-ui-throbber-success');
          throbber.removeClass('parser-ui-throbber-error');
          throbber.show();
          throbber.css('display','inline-block');
          me.model.parse(sentence);
        });
        this.initialized = true;
      }
    }
  };

  mgwiki.parser.QuickFormView.template = '<div class="parser-qf-container">'+
    '<div class="parser-qf-header"><b>Analyser une phrase (français) :</b></div>'+
    '<div class="parser-qf-body">'+
      '<textarea class="parser-qf-st-input"></textarea>'+
      '<input class="parser-qf-submit form-submit" type="submit" value="Lancer"><div class="parser-ui-main-throbber parser-ui-throbber" style="display:none;"></div>'+
    '</div>'+
    '<div class="parser-qf-footer"><a href="/frmgwiki/frmg_main/frmg_server">Accéder à l\'interface complète.</a></div>'+
  '</div>';

}(window.mgwiki = window.mgwiki || {},jQuery));
