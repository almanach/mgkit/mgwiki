(function(mgwiki,$){
  
  mgwiki.parser.ResultSourceView = function(model,$el,options){
    this.model = model;
    this.el = ($el)?$el[0]:$('<div></div>');
    this.$el = $(this.el);
    this.initialized = false; 
  };

  mgwiki.parser.ResultSourceView.prototype.render = function(){
    var me = this;
    if(this.model){

      if(this.initialized){
        this.$el.empty();

      }

      this.initialized = true;

      
      this.$el.html(mgwiki.parser.ResultSourceView.template);
      this.$el.find('.parser-result-parse').html("<pre>"+this.model.parseInfo.data+"</pre>");


      this.$el.find('.parser-result-cancel').click(function(){
          if(me.model.options.box){
            me.model.options.box.destroy();  
          }else{
            me.$el.remove();
          }
          
        });


      
    }
  };


  mgwiki.parser.ResultSourceView.template = '<div class="parser-result-container">' +
    '<table><tbody>'+
      '<tr>'+
        '<td class="parser-result-table-container">'+
          '<div class="parser-result-parse"></div>'+
          '<button class="parser-result-cancel" type="button">Cancel</button>'+
        '</td>'+
      '</tr>'+
    '</tbody></table>'+
  '</div>';







}(window.mgwiki = window.mgwiki || {},jQuery));
