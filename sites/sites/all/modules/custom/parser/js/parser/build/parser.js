(function(mgwiki){
  
  /**
   * Parser module.
   * This module is the ui for frmg parser provided to mgwiki.
   * @type {Object}
   */
  mgwiki.parser = {};

}(window.mgwiki = window.mgwiki || {}));
(function(mgwiki,$){
  
  mgwiki.parser.MainForm = function($el,options){
    this.options = options || {};
    if(options.light_ui){
      this.view = new mgwiki.parser.QuickFormView(this,$el,{});
    }else{
      this.view = new mgwiki.parser.MainFormView(this,$el,{});  
    }
    
  };

  mgwiki.parser.MainForm.prototype.parse = function(sentence,options){
    var me = this;
    var params = options || {};
    params.format = params.format || 'depxml';
    var dataType = 'json';
    /*if(params.format == 'passage'){
      dataType = 'text';
    }*/
    jQuery.ajax({
      type: 'POST', 
      url: me.options.parser_url,
      data: {
        sentence: sentence, 
        options: params,
      },
      dataType : dataType,
      success: function(data, textStatus, jqXHR) {
        if(dataType == 'text'){
          me.displayResult(sentence,data,params);
          $('.parser-ui-main-throbber',me.view.$el).addClass('parser-ui-throbber-success');
          $('.parser-ui-main-throbber',me.view.$el).fadeOut(2000);
        }else if(!data.error){
          me.displayResult(sentence,data,params);
          $('.parser-ui-main-throbber',me.view.$el).addClass('parser-ui-throbber-success');
          $('.parser-ui-main-throbber',me.view.$el).fadeOut(2000);
        }else{
          alert("La phrase n'a pas pu être analysée.");
          $('.parser-ui-main-throbber',me.view.$el).addClass('parser-ui-throbber-error');
          $('.parser-ui-main-throbber',me.view.$el).fadeOut(2000);
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        alert(textStatus);
        $('.parser-ui-main-throbber',me.view.$el).addClass('parser-ui-throbber-error');
        $('.parser-ui-main-throbber',me.view.$el).fadeOut(2000);
      }
    });
  };

  mgwiki.parser.MainForm.prototype.displayResult = function(sentence,data,options){
    if(this.options.light_ui){
      this.overlayDisplayResult(sentence,data,options);
    }else{
      this.normalDisplayResult(sentence,data,options);
    }
  };

   mgwiki.parser.MainForm.prototype.normalDisplayResult = function(sentence,data,options){

    var $el = $('<div></div>');
    this.view.$el.find('.parser-mf-results').prepend($el);
    var result = new mgwiki.parser.Result({sentence:sentence,data:data,initialOptions:options.frmg_options,format:options.format},$el,{base_url:this.options.base_url,parser:this,parser_ws:this.options.parser_ws,graph_url:this.options.graph_url});
    result.view.render();

  };

   mgwiki.parser.MainForm.prototype.overlayDisplayResult = function(sentence,data,options){
    var width = $(window).width();
    var height = $(window).height();
    var box = new depgraphlib.Box({closeButton:true,draggable:true,position:{x:100,y:100}});
    box.setMaxSize(width-200,height-200);
    box.open();
    box.setHeader(sentence);

    var resultDiv = $('<div></div>');
    var resultHeaderDiv = $('<div></div>');
    var resultFooterDiv = $('<div></div>');
    var resultHeader = new mgwiki.parser.ResultViewHeader(resultHeaderDiv,this.options);
    resultHeader.render();
    box.setContent(resultHeaderDiv);
    box.setContent(resultDiv);
    box.setContent(resultFooterDiv);

    var $el = resultDiv//$('.depgraphlib-box-content',box.object);
    var result = new mgwiki.parser.Result({sentence:sentence,data:data,initialOptions:options.frmg_options,format:options.format},$el,{base_url:this.options.base_url,box:box,parser:this,parser_ws:this.options.parser_ws,graph_url:this.options.graph_url});
    result.view.render();
    
    box.options.onclose = function(){
      result.destroy();
    };
  };

}(window.mgwiki = window.mgwiki || {},jQuery));
(function(mgwiki,$){
  
  mgwiki.parser.Result = function(parseResult,$el,options){
    this.parseInfo = parseResult;
    this.view = new mgwiki.parser.ResultView(this,$el,{});  
    
    this.options = options || {};
  };

  mgwiki.parser.Result.prototype.destroy = function(){
  };

  mgwiki.parser.Result.prototype.getRelatedPages = function(callbackSuccess){
    var me = this;
    var params = this.parseInfo.initialOptions || '';
    var ddata = me.parseInfo.sentence;
    if(this.view.depGraph){
      if(this.view.depGraph.editObject.editMode == 'frmg'){
        params = depgraphlib.getPreviousParams(this.view.depGraph);
      }
    }
    jQuery.ajax({
      type: 'POST', 
      url: me.options.parser_ws + "/getRelatedPages",
      data: {
        options: params,
        data:ddata,
      },
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        callbackSuccess.call(me,data);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        
      }
    });
  };

  mgwiki.parser.Result.prototype.getClasses = function(data,callbackSuccess){
    var me = this;
    var all_classes = {};
    var all_trees = {};

    for (var i = data.graph.words.length - 1; i >= 0; i--) {
      var word = data.graph.words[i];
      var tree = word['#data'].tree;
      var classes = tree.split(" ");
      classes.shift();
      for (var j = classes.length - 1; j >= 0; j--) {
        var klass = classes[j];
        var items = klass.split(":");
        var klassname = items.pop();
        all_classes[klassname]=klassname;
      };
      var tree = classes.join(" ");
      if(tree){
        all_trees[tree] = tree;  
      }
    };

    jQuery.ajax({
      type: 'POST', 
      url: me.options.parser_ws + "/classBlock",
      data: {
        trees:all_trees,
        classes:all_classes
      },
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        callbackSuccess.call(me,data);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        
      }
    });
  };

  mgwiki.parser.Result.prototype.add2basket = function(){
    var me = this;
    jQuery.ajax({
      type: 'POST', 
      url: me.options.parser_ws + "/add2basket",
      data: {
        uid:me.parseInfo.uid
      },
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        if(!data.error){
          me.added2basket = true;
          me.view.$el.find('.parser-result-add2basket').remove();
        }else{
          alert(data.message);
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        
      }
    });
    
  };

  mgwiki.parser.Result.prototype.proposeSentence = function(){
    var me = this;
    jQuery.ajax({
      type: 'POST', 
      url: me.options.parser_ws + "/proposeSentence",
      data: {
        uid:me.parseInfo.uid
      },
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        window.location = data.url;
      },
      error: function(jqXHR, textStatus, errorThrown) {
        
      }
    });
  };

  mgwiki.parser.Result.prototype.save = function(callbackSuccess,callbackError){
    var me = this;

    var format = this.parseInfo.format; // default result type
    var params = this.parseInfo.initialOptions || '';
    var ddata = me.parseInfo.sentence;
    var highlightInfos = this.view.depGraph.editObject.mode[this.view.depGraph.editObject.editMode].highlightInfos;
    if(this.view.depGraph){
      if(this.view.depGraph.editObject.editMode == 'frmg'){
        params = depgraphlib.getPreviousParams(this.view.depGraph);
      }else{
        format = 'json';
        ddata = this.view.depGraph.cleanData();
      }
    }

    jQuery.ajax({
      type: 'POST', 
      url: me.options.graph_url,
      data: {
        action:'save',
        format:format,
        options: params,
        highlighting:highlightInfos,
        data:ddata,
      },
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        if(data.success){
          me.parseInfo.uid = data.uid;
          me.parseInfo.format = format;
          me.parseInfo.initialOptions = params;
          me.parseInfo.data = me.view.depGraph.data;
          me.view.render();

          callbackSuccess.call();
        }else{
          callbackError.call();
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        callbackError.call();
        alert(textStatus);
      }
    });
  };

}(window.mgwiki = window.mgwiki || {},jQuery));
(function(mgwiki,$){
  
  mgwiki.parser.MainFormView = function(model,$el,options){
    this.model = model;
    this.el = ($el)?$el[0]:$('<div></div>');
    this.$el = $(this.el);
    this.initialized = false;
  };

  mgwiki.parser.MainFormView.prototype.render = function(){
    var me = this;
    if(this.model){
      if(!this.initialized){
        this.$el.html(mgwiki.parser.MainFormView.template);
        var resultHeaderDiv = $('<div></div>');
        var resultHeader = new mgwiki.parser.ResultViewHeader(resultHeaderDiv,this.model.options);
        this.$el.find('.parser-mf-results').prepend(resultHeaderDiv);
        resultHeader.render();
        
        this.$el.find('.parser-mf-submit').click(function(){
          var throbber = jQuery('.parser-ui-main-throbber',me.$el);
          if(!throbber.is(':hidden')){
            return;  
          }
          throbber.removeClass('parser-ui-throbber-success');
          throbber.removeClass('parser-ui-throbber-error');
          throbber.show();
          throbber.css('display','inline-block');

          var sentence = me.$el.find('.parser-mf-st-input').val();
          var format = me.$el.find('.parser-mf-option-format').val();
          var robust = me.$el.find('.parser-mf-option-robust').prop('checked');
          var exotic = me.$el.find('.parser-mf-option-exotic').prop('checked');
          var transform = me.$el.find('.parser-mf-option-transform').prop('checked');
          var rename = me.$el.find('.parser-mf-option-rename').prop('checked');
          var frmg_options = '';
          if(robust){
            frmg_options += 'robust ';
          }
          if(rename){
            frmg_options += 'rename ';
          }
          if(exotic){
            frmg_options += 'exotic ';
          }
          if(transform){
            frmg_options += 'transform ';
          }
          var options = {
            format:format,
            frmg_options:frmg_options
          }
          me.model.parse(sentence,options);
        });
        this.initialized = true;
      }
    }
  };

  mgwiki.parser.MainFormView.template = '<div class="parser-mf-container">'+
    '<label>Analyser une phrase (français) : </label>'+
    '<textarea class="parser-mf-st-input" rows="5" cols="200"></textarea>'+
    '<label>Options : </label>'+
    '<span class="frmg-parser-option">Format : <select class="parser-mf-option-format">'+
    '<option selected="true" value="depxml">depxml</option>'+
    '<option value="depconll">depconll</option>'+
    '<option value="conll">conll</option>'+
    '<option value="passage">passage</option>'+
    '</select></span>'+
    '<span class="frmg-parser-option">Robust :<input class="parser-mf-option-robust" type="checkbox" name="robust"></span>'+
    '<span class="frmg-parser-option">Transform : <input class="parser-mf-option-transform" type="checkbox" name="transform"></span>'+
    '<span class="frmg-parser-option">Rename : <input class="parser-mf-option-rename" type="checkbox" name="rename"></span>'+
    '<span class="frmg-parser-option">Exotic :<input class="parser-mf-option-exotic" type="checkbox" name="exotic"></span>'+
    '<hr>'+
    '<input class="parser-mf-submit" type="button" value="Lancer"><div class="parser-ui-main-throbber parser-ui-throbber" style="display:none;"></div>'+
    '<hr>'+
    '<div class="parser-mf-results"></div>'+
  '</div>';

}(window.mgwiki = window.mgwiki || {},jQuery));

(function(mgwiki,$){
  
  mgwiki.parser.QuickFormView = function(model,$el,options){
    this.model = model;
    this.el = ($el)?$el[0]:$('<div></div>');
    this.$el = $(this.el);
    this.initialized = false;
  };

  mgwiki.parser.QuickFormView.prototype.render = function(){
    var me = this;
    if(this.model){
      if(!this.initialized){
        this.$el.html(mgwiki.parser.QuickFormView.template);
        this.$el.find('.parser-qf-submit').click(function(){
          var sentence = me.$el.find('.parser-qf-st-input').val();
          var throbber = jQuery('.parser-ui-main-throbber',me.$el);
          if(!throbber.is(':hidden')){
            return;  
          }
          throbber.removeClass('parser-ui-throbber-success');
          throbber.removeClass('parser-ui-throbber-error');
          throbber.show();
          throbber.css('display','inline-block');
          me.model.parse(sentence);
        });
        this.initialized = true;
      }
    }
  };

  mgwiki.parser.QuickFormView.template = '<div class="parser-qf-container">'+
    '<div class="parser-qf-header"><b>Analyser une phrase (français) :</b></div>'+
    '<div class="parser-qf-body">'+
      '<textarea class="parser-qf-st-input"></textarea>'+
      '<input class="parser-qf-submit form-submit" type="submit" value="Lancer"><div class="parser-ui-main-throbber parser-ui-throbber" style="display:none;"></div>'+
    '</div>'+
    '<div class="parser-qf-footer"><a href="/frmgwiki/frmg_main/frmg_server">Accéder à l\'interface complète.</a></div>'+
  '</div>';

}(window.mgwiki = window.mgwiki || {},jQuery));

(function(mgwiki,$){
  
  mgwiki.parser.ResultSourceView = function(model,$el,options){
    this.model = model;
    this.el = ($el)?$el[0]:$('<div></div>');
    this.$el = $(this.el);
    this.initialized = false; 
  };

  mgwiki.parser.ResultSourceView.prototype.render = function(){
    var me = this;
    if(this.model){

      if(this.initialized){
        this.$el.empty();

      }

      this.initialized = true;

      
      this.$el.html(mgwiki.parser.ResultSourceView.template);
      this.$el.find('.parser-result-parse').html("<pre>"+this.model.parseInfo.data+"</pre>");


      this.$el.find('.parser-result-cancel').click(function(){
          if(me.model.options.box){
            me.model.options.box.destroy();  
          }else{
            me.$el.remove();
          }
          
        });


      
    }
  };


  mgwiki.parser.ResultSourceView.template = '<div class="parser-result-container">' +
    '<table><tbody>'+
      '<tr>'+
        '<td class="parser-result-table-container">'+
          '<div class="parser-result-parse"></div>'+
          '<button class="parser-result-cancel" type="button">Cancel</button>'+
        '</td>'+
      '</tr>'+
    '</tbody></table>'+
  '</div>';







}(window.mgwiki = window.mgwiki || {},jQuery));

(function(mgwiki,$){
  
  mgwiki.parser.ResultView = function(model,$el,options){
    this.model = model;
    this.el = ($el)?$el[0]:$('<div></div>');
    this.$el = $(this.el);
    this.initialized = false; 
  };

  mgwiki.parser.ResultView.prototype.render = function(){
    var me = this;
    if(this.model){

      if(this.initialized){
        this.$el.empty();

      }

      this.initialized = true;

      this.$el.html(mgwiki.parser.ResultView.template);

      var uniqid;
      var canSave = false;

      if(this.model.parseInfo.uid){
        canSave = true;
        this.$el.find('.parser-result-uid').html(this.model.parseInfo.uid);
        uniqid=this.model.parseInfo.uid;
        this.$el.find('.parser-result-save').remove();
        this.$el.find('.parser-result-cancel').html('Fermer');

        var add2basketbutton = this.$el.find('.parser-result-add2basket');
        if(!this.model.added2basket){
          add2basketbutton.show();
          add2basketbutton.click(function(){
            me.model.add2basket();
          });  
        }

        var proposeSentenceButton = this.$el.find('.parser-propose-sentence');
        proposeSentenceButton.show();
        proposeSentenceButton.click(function(){
          me.model.proposeSentence();
        });
        
      }else{
        uniqid = Date.now();
        this.$el.find('.parser-result-save').click(function(){
          var throbber = jQuery('.parser-ui-throbber',me.$el);
          if(!throbber.is(':hidden')){
            return;  
          }
          throbber.removeClass('parser-ui-throbber-success');
          throbber.removeClass('parser-ui-throbber-error');
          throbber.show();
          throbber.css('display','inline-block');
          me.model.save(function(){me.success();},function(){me.error();});
        });
        
      }

      this.$el.find('.parser-result-cancel').click(function(){
          if(me.model.options.box){
            me.model.options.box.destroy();  
          }else{
            me.$el.remove();
          }
          
        });

      if(this.model.parseInfo.format=='depxml'){
        this.model.getClasses(this.model.parseInfo.data,function(data){
          me.renderInfoBox(data);
        });
         
      }

      this.model.getRelatedPages(function(data){
          me.renderRelatedPages(data);
        }); 

      var formatExport = {
        'json':'json',
        'image':'png',
        'dep2pict':'dep2pict'
      };
      formatExport[this.model.parseInfo.format]=this.model.parseInfo.format;

      // @todo retrieve corresponding tagset
      

      var maxwidth = this.model.options.box?($(window).width()-400):null;
      this.$el.find('.parser-result-parse').html('<div id="graph-container-'+uniqid+'"></div>');
      var depGraph = this.depGraph = new depgraphlib.DepGraph(jQuery("#graph-container-"+uniqid),this.model.parseInfo.data,{"uid":uniqid,viewmode:"cropped"
        ,maxwidth:maxwidth,frmgparserparams:this.model.parseInfo.initialOptions,format:this.model.parseInfo.format,exportFormats:formatExport
      });

      if(this.model.parseInfo.format=='depxml'){
        depGraph.onObjectMouseOver=function(obj,val){me.highlightClasses(obj.__data__,val);};
      }

      depGraph.wsurl = this.model.options.parser.options.graph_url;
      var viewer = depGraph.viewer;
      //depGraph.wsurl = cma.TheApp.ws_url+"graph";
      if(depGraph.options.viewmode == 'cropped'){
        viewer.noBorders();
      }

      depGraph.sentenceLink = '#';

      depGraph.sentence = this.model.parseInfo.sentence;
      depGraph.editObject.mode['default'].save = depgraphlib.EditObject.DefaultModeLib.save;
      depGraph.dataFormat = 'json';

      

      viewer.setFixedToolbar();

      var editMode = 'frmg';
      if(this.model.parseInfo.format == 'frmgserver' || this.model.parseInfo.format == 'depxml'){
        var frmgEditMode = new depgraphlib.FRMGEditMode(this.model.options.parser.options.parser_proxy);
        frmgEditMode.mode.onGetNewResult = function(data){
          console.log("new data");
          console.log(data);
        }
        depGraph.editObject.addEditMode(frmgEditMode.mode);
        
        depGraph.editObject.addEditModeSwitcher();
      }else{
        editMode = 'default';
      }

      depGraph.editObject.setEditMode(editMode,canSave);

      
      


      


      
    }
  };

  mgwiki.parser.ResultView.prototype.highlightClasses = function(d,val){
    var classeslink = this.$el.find('.frmg-class-info');
    if(val){
      var all_classes = {};
      var tree = d['#data'].tree;
      var classes = tree.split(" ");
      classes.shift();
      for (var j = classes.length - 1; j >= 0; j--) {
        var klass = classes[j];
        var items = klass.split(":");
        var klassname = items.pop();
        all_classes[klassname]=klassname;
      };
      classeslink.each(function(i,e){
        for(var k in all_classes){
          if(e.textContent==k){
            $(e).css('text-decoration','underline');
            break;
          }
        }
        
      });

    }else{
      classeslink.each(function(i,e){
        $(e).css('text-decoration','none');
        
      });

    }
    
  };

  mgwiki.parser.ResultView.prototype.renderRelatedPages = function(data){
    var infoBox = this.$el.find('.parser-result-related-pages-box');
    infoBox.html('');
    if(data){
      infoBox.append('<h3>Related Pages</h3>');
      infoBox.append('<ul>');
      for (var i in data) {
        infoBox.append('<li>'+data[i]+'</li>');
      };
    }
    
    
  };

  mgwiki.parser.ResultView.prototype.renderInfoBox = function(data){
    var infoBox = this.$el.find('.parser-result-info-box');
    infoBox.html('');
    infoBox.append('<h3>FrMG Informations</h3>');
    infoBox.append('<h4>FrMG Classes</h4>');
    infoBox.append('<ul>');
    for (var klass in data.classes) {
      infoBox.append('<li class="frmg-class-info">'+data.classes[klass]+'</li>');
    };
    infoBox.append('</ul>');
    infoBox.append('<h4>FrMG Trees</h4>');
    infoBox.append('<ul>');
    for (var tree in data.trees) {
      infoBox.append('<li>'+data.trees[tree]+'</li>');
    };
    infoBox.append('</ul>');
  };


  mgwiki.parser.ResultView.prototype.success = function(){
    $('.parser-ui-throbber',this.$el).addClass('parser-ui-throbber-success');
    $('.parser-ui-throbber',this.$el).fadeOut(2000);
    this.render();
  }

  mgwiki.parser.ResultView.prototype.error = function(){
    $('.parser-ui-throbber',this.$el).addClass('parser-ui-throbber-error');
    $('.parser-ui-throbber',this.$el).fadeOut(2000);
  }

  mgwiki.parser.ResultView.template = '<div class="parser-result-container">' +
    '<table><tbody>'+
      '<tr>'+
        '<td class="parser-result-table-container">'+
          '<div class="parser-result-parse"></div>'+
          '<div class="parser-result-uid"></div><button class="parser-propose-sentence" type="button">Proposer comme phrase du jour</button><button class="parser-result-add2basket" type="button">Ajouter à mon panier</button><button class="parser-result-save" type="button">Save</button><div class="parser-ui-throbber" style="display:none;"></div><button class="parser-result-cancel" type="button">Cancel</button>'+
        '</td>'+
        '<td  class="parser-result-table-container" width="150px">'+
          '<div class="parser-result-related-pages-box"></div>'+
          '<div class="parser-result-info-box"></div>'+
        '</td>'+
      '</tr>'+
    '</tbody></table>'+
  '</div>';

  mgwiki.parser.ResultView.templateError = 'Couldn\'t parse the sentence!';

}(window.mgwiki = window.mgwiki || {},jQuery));
(function(mgwiki,$){
  
  mgwiki.parser.ResultViewHeader = function($el,options){
    this.el = ($el)?$el[0]:$('<div></div>');
    this.$el = $(this.el);
    this.options = options || {};
  };

  mgwiki.parser.ResultViewHeader.prototype.render = function(){
    this.$el.html(mgwiki.parser.ResultViewHeader.template);
    if(!this.options.user){
      this.$el.find('.parser-result-header').prepend(mgwiki.parser.ResultViewHeader.templateAnon);
      this.$el.find('a').attr('href',this.options.url_register);
    }
    this.$el.find('a[name="depgraph"]').attr('href',this.options.url_help_depgraph);
    this.$el.find('a[name="frmg"]').attr('href',this.options.url_help_frmg);
  };

  mgwiki.parser.ResultViewHeader.template = '<div class="parser-result-header">' +
    '<p>Pour plus d\'information sur l\'utilisation de l\'analyseur syntaxique, vous pouvez consulter les pages d\'aides relatives au plugin de <a name="depgraph" href="">visualisation des graphes</a> et/ou aux pages relatives à <a name="frmg" href="">l\'analyseur syntaxique FrMG</a></p>'
  '</div>';

  mgwiki.parser.ResultViewHeader.templateAnon = '<p>Afin de profiter de toutes les fonctionnalité de MGWiki, nous vous invitons à vous <a href="">inscrire</a>.</p>';

}(window.mgwiki = window.mgwiki || {},jQuery));