(function(mgwiki,$){
  
  mgwiki.parser.ResultView = function(model,$el,options){
    this.model = model;
    this.el = ($el)?$el[0]:$('<div></div>');
    this.$el = $(this.el);
    this.initialized = false; 
  };

  mgwiki.parser.ResultView.prototype.render = function(){
    var me = this;
    if(this.model){

      if(this.initialized){
        this.$el.empty();

      }

      this.initialized = true;

      this.$el.html(mgwiki.parser.ResultView.template);

      var uniqid;
      var canSave = false;

      if(this.model.parseInfo.uid){
        canSave = true;
        this.$el.find('.parser-result-uid').html(this.model.parseInfo.uid);
        uniqid=this.model.parseInfo.uid;
        this.$el.find('.parser-result-save').remove();
        this.$el.find('.parser-result-cancel').html('Fermer');

        var add2basketbutton = this.$el.find('.parser-result-add2basket');
        if(!this.model.added2basket){
          add2basketbutton.show();
          add2basketbutton.click(function(){
            me.model.add2basket();
          });  
        }

        var proposeSentenceButton = this.$el.find('.parser-propose-sentence');
        proposeSentenceButton.show();
        proposeSentenceButton.click(function(){
          me.model.proposeSentence();
        });
        
      }else{
        uniqid = Date.now();
        this.$el.find('.parser-result-save').click(function(){
          var throbber = jQuery('.parser-ui-throbber',me.$el);
          if(!throbber.is(':hidden')){
            return;  
          }
          throbber.removeClass('parser-ui-throbber-success');
          throbber.removeClass('parser-ui-throbber-error');
          throbber.show();
          throbber.css('display','inline-block');
          me.model.save(function(){me.success();},function(){me.error();});
        });
        
      }

      this.$el.find('.parser-result-cancel').click(function(){
          if(me.model.options.box){
            me.model.options.box.destroy();  
          }else{
            me.$el.remove();
          }
          
        });

      if(this.model.parseInfo.format=='depxml'){
        this.model.getClasses(this.model.parseInfo.data,function(data){
          me.renderInfoBox(data);
        });
         
      }

      this.model.getRelatedPages(function(data){
          me.renderRelatedPages(data);
        }); 

      var formatExport = {
        'json':'json',
        'image':'png',
        'dep2pict':'dep2pict'
      };
      formatExport[this.model.parseInfo.format]=this.model.parseInfo.format;

      // @todo retrieve corresponding tagset
      

      var maxwidth = this.model.options.box?($(window).width()-400):null;
      this.$el.find('.parser-result-parse').html('<div id="graph-container-'+uniqid+'"></div>');
      var depGraph = this.depGraph = new depgraphlib.DepGraph(jQuery("#graph-container-"+uniqid),this.model.parseInfo.data,{"uid":uniqid,viewmode:"cropped"
        ,maxwidth:maxwidth,frmgparserparams:this.model.parseInfo.initialOptions,format:this.model.parseInfo.format,exportFormats:formatExport
      });

      if(this.model.parseInfo.format=='depxml'){
        depGraph.onObjectMouseOver=function(obj,val){me.highlightClasses(obj.__data__,val);};
      }

      depGraph.wsurl = this.model.options.parser.options.graph_url;
      var viewer = depGraph.viewer;
      //depGraph.wsurl = cma.TheApp.ws_url+"graph";
      if(depGraph.options.viewmode == 'cropped'){
        viewer.noBorders();
      }

      depGraph.sentenceLink = '#';

      depGraph.sentence = this.model.parseInfo.sentence;
      depGraph.editObject.mode['default'].save = depgraphlib.EditObject.DefaultModeLib.save;
      depGraph.dataFormat = 'json';

      

      viewer.setFixedToolbar();

      var editMode = 'frmg';
      if(this.model.parseInfo.format == 'frmgserver' || this.model.parseInfo.format == 'depxml'){
        var frmgEditMode = new depgraphlib.FRMGEditMode(this.model.options.parser.options.parser_proxy);
        frmgEditMode.mode.onGetNewResult = function(data){
          console.log("new data");
          console.log(data);
        }
        depGraph.editObject.addEditMode(frmgEditMode.mode);
        
        depGraph.editObject.addEditModeSwitcher();
      }else{
        editMode = 'default';
      }

      depGraph.editObject.setEditMode(editMode,canSave);

      
      


      


      
    }
  };

  mgwiki.parser.ResultView.prototype.highlightClasses = function(d,val){
    var classeslink = this.$el.find('.frmg-class-info');
    if(val){
      var all_classes = {};
      var tree = d['#data'].tree;
      var classes = tree.split(" ");
      classes.shift();
      for (var j = classes.length - 1; j >= 0; j--) {
        var klass = classes[j];
        var items = klass.split(":");
        var klassname = items.pop();
        all_classes[klassname]=klassname;
      };
      classeslink.each(function(i,e){
        for(var k in all_classes){
          if(e.textContent==k){
            $(e).css('text-decoration','underline');
            break;
          }
        }
        
      });

    }else{
      classeslink.each(function(i,e){
        $(e).css('text-decoration','none');
        
      });

    }
    
  };

  mgwiki.parser.ResultView.prototype.renderRelatedPages = function(data){
    var infoBox = this.$el.find('.parser-result-related-pages-box');
    infoBox.html('');
    if(data){
      infoBox.append('<h3>Related Pages</h3>');
      infoBox.append('<ul>');
      for (var i in data) {
        infoBox.append('<li>'+data[i]+'</li>');
      };
    }
    
    
  };

  mgwiki.parser.ResultView.prototype.renderInfoBox = function(data){
    var infoBox = this.$el.find('.parser-result-info-box');
    infoBox.html('');
    infoBox.append('<h3>FrMG Informations</h3>');
    infoBox.append('<h4>FrMG Classes</h4>');
    infoBox.append('<ul>');
    for (var klass in data.classes) {
      infoBox.append('<li class="frmg-class-info">'+data.classes[klass]+'</li>');
    };
    infoBox.append('</ul>');
    infoBox.append('<h4>FrMG Trees</h4>');
    infoBox.append('<ul>');
    for (var tree in data.trees) {
      infoBox.append('<li>'+data.trees[tree]+'</li>');
    };
    infoBox.append('</ul>');
  };


  mgwiki.parser.ResultView.prototype.success = function(){
    $('.parser-ui-throbber',this.$el).addClass('parser-ui-throbber-success');
    $('.parser-ui-throbber',this.$el).fadeOut(2000);
    this.render();
  }

  mgwiki.parser.ResultView.prototype.error = function(){
    $('.parser-ui-throbber',this.$el).addClass('parser-ui-throbber-error');
    $('.parser-ui-throbber',this.$el).fadeOut(2000);
  }

  mgwiki.parser.ResultView.template = '<div class="parser-result-container">' +
    '<table><tbody>'+
      '<tr>'+
        '<td class="parser-result-table-container">'+
          '<div class="parser-result-parse"></div>'+
          '<div class="parser-result-uid"></div><button class="parser-propose-sentence" type="button">Proposer comme phrase du jour</button><button class="parser-result-add2basket" type="button">Ajouter à mon panier</button><button class="parser-result-save" type="button">Save</button><div class="parser-ui-throbber" style="display:none;"></div><button class="parser-result-cancel" type="button">Cancel</button>'+
        '</td>'+
        '<td  class="parser-result-table-container" width="150px">'+
          '<div class="parser-result-related-pages-box"></div>'+
          '<div class="parser-result-info-box"></div>'+
        '</td>'+
      '</tr>'+
    '</tbody></table>'+
  '</div>';

  mgwiki.parser.ResultView.templateError = 'Couldn\'t parse the sentence!';

}(window.mgwiki = window.mgwiki || {},jQuery));