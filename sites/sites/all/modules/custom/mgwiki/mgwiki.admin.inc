<?php
/**
 * This file contains the definition of the custom administration panel
 */

/**
 * @desc Entry point of the admin mgwiki management page
 * @param unknown $user_id
 * @param unknown $tab
 * @param unknown $function
 * @return Ambigous <The, boolean, string>
 */
function mgwiki_admin_form($tab,$function){
  global $user;

  $default_tab = '';
  if(isset($tab) && !empty($tab)){
    if(isset($function) && !empty($function)){
      $form = mgwiki_admin_function($tab,$function);      
    }else{
      $default_tab = 'edit-admin-'.$tab;
      $form = drupal_get_form('mgwiki_admin_menu',$tab);
    }
  }else{
    $form = drupal_get_form('mgwiki_admin_menu','');
  }
  
  return $form;
}

/**
 * @desc Display the menu of the admin mgwiki management
 * @param unknown $form
 * @param unknown $form_state
 * @param unknown $tab
 * @return unknown
 */
function mgwiki_admin_menu($form,$form_state,$tab){
  global $user;
  
  $form['mgwiki_admin_tabs'] = array(
    '#type' => 'vertical_tabs',
    '#default_tab' =>  $tab,
  );
  
  $items = mgwiki_admin_items(); 
  foreach($items as $name => $item){
    $form['admin_'.$name] = array(
      '#type' => 'fieldset',
      '#title' => $item['title'],
      '#group' => 'mgwiki_admin_tabs',
    );
    $form['admin_'.$name]['menu'] = null;
    $elements = array();
    foreach($item['elements'] as $function => $title){
      if(is_array($title)){
        $form['admin_'.$name][$function] = $title;
      }else{
        $elements[] = l($title,'mgwiki_admin/'.$name.'/'.$function);
      }
    }
    $form['admin_'.$name]['menu']['#markup'] = theme('item_list',array('items'=>$elements));
  }
  
  return $form;
  
}

/**
 * @desc mgwiki admin page function router
 * @param unknown $tab
 * @param unknown $function
 * @return Ambigous <The, boolean, string>
 */
function mgwiki_admin_function($tab,$function){
  global $user;
  $func = 'mgwiki_admin_'.$tab.'_'.$function;
  if(function_exists($func)){
    $form = array();
    $form['home'] = array(
      '#markup' => l('return to menu','mgwiki_admin'),
    );
    $form['content'] = call_user_func($func);
    return $form;
  }else{
    return drupal_get_form('mgwiki_admin_menu','');
  }
}

/**
 * the definition of menus and page callback
 */
function mgwiki_admin_items(){
  $items = array();
  
  $items['frmg'] = array(
    'title' => t('FRMG'),
    'elements' => array(
      'synchro' => t('Synchronisation FRMG'),
    )
  );
  
  $items['parser'] = array(
    'title' => t('Parser FRMG'),
    'elements' => array(
      'failed_graphs' => mgwiki_admin_parser_failed_graph(),
      'log'=>t('FRMG Parser Log'),
      'update_all_graphs'=>t('Force update all graphs'),
      'update_all_graphs_finished'=>t('Graph Updates Summary Logs'),
      'export_sentences'=>t('Export Sentences'),
      'index_sentences'=>t('Index Sentences'),
      //'migrate_sentences'=>t('Migrate Sentences')
    )
  );
  
  $items['users'] = array(
    'title' => t('Users'),
    'elements' => array(
        'ual_log' => t('User Activity Log')
    )
  );
  
  $items['misc'] = array(
    'title' => t('Miscellaneous'),
    'elements' => array(
      //'test'=>t('test'),
      'mgwiki_global_guide'=>t('MGWiki Project Information Guide'),
      'list_highlighted_graphs'=>t('List highlighted graphs'),
      'corpus_reindex'=>t('ReIndex CMA Corpus')
    )
  );
  
  return $items;
  
}

///////////////////////////////////////////////////////////////////
//                  Functions Handlers
///////////////////////////////////////////////////////////////////


/**
 * @desc manage frmg svn synchro
 * @param unknown $form
 * @param unknown $form_state
 * @return unknown
 */
function mgwiki_admin_frmg_synchro(){
  $form = drupal_get_form('mgdata_management_form_wizard');
  return $form;
}

function mgwiki_admin_parser_export_sentences(){
  drupal_goto("parser/sentences/export");
}

function mgwiki_admin_parser_index_sentences(){
  parser_sentences_index();
  drupal_set_message("ok");
};

function mgwiki_admin_parser_migrate_sentences(){
  variable_del("migrate_sentence_status");
  $status = variable_get("migrate_sentence_status",null);
  if(!isset($status)){
    parser_migrate_sentences();  
    drupal_set_message("start processing");
    variable_set("migrate_sentence_status","processing");
  }else if($status=="processing"){
    drupal_set_message("processing");
  }
  
  
}


/**
 * @desc return a sortable table of frmg parser log
 * @return multitype:string multitype:multitype:array   multitype:multitype:string The  multitype:string Ambigous <The, string, A, Optional>
 */
function mgwiki_admin_parser_log(){
  
   $header = array(
    array('data' => t('ID'), 'field' => 'p.sid'),
    array('data' => t('Sentence'), 'field' => 'p.data'),
    array('data' => t('Options'), 'field'=> 'p.options'),
    array('data' => t('Parse Result'), 'field'=> 'p.test'),
    array('data' => t('Count'), 'field'=> 'p.count'),
    array('data' => t('Last time tested'), 'field'=> 'p.lastused'),
    array('data' => t('Creation Date'), 'field' => 'p.created'),
  );
  
  $result = db_select('parser_sentence','p')
    ->extend('TableSort')
    ->extend('PagerDefault')
    ->limit(100)
    ->fields('p',array('sid','data','options','test','count','lastused','created'))
    ->orderByHeader($header)
    ->execute();
  
  
  $rows = array();
  foreach ($result as $row) {
    $row->lastused = date("Y-m-d H:i:s",$row->lastused);
    $row->created = date("Y-m-d H:i:s",$row->created);
    $rows[] = array('data' => (array) $row);
  }
  
  // build the table for the nice output.
  $build['tablesort_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );
  
  $build['tablesort_pager'] = array(
      '#theme'=>'pager',
  );
  
  return $build;
  
}

/**
 * @desc force updating all graphs in mgwiki pages
 */
function mgwiki_admin_parser_update_all_graphs(){
  module_load_include('module', 'parser');
  parser_update_all_graphs_execute();
  batch_process('mgwiki_admin/parser/update_all_graphs_finished');
}

function mgwiki_admin_parser_update_all_graphs_finished(){
  return drupal_get_form('parser_update_all_graphs_finished_form');
}

/**
 * @desc show the parse that failed
 */
function mgwiki_admin_parser_failed_graph(){
  $form = array();
  $graphs = parser_get_all_failed_graph();
  $graphs_sentences = array();
  foreach($graphs as $graph){
    $params = '';
    if(isset($graph['params']) && isset($graph['params']['options'])){
      $params = ' ( options : ' . $graph['params']['options'] . ' ) ';
    }
    $graphs_sentences[] = $graph['content'] . $params;
  }
  $form['title'] = array(
    '#type' => 'item',
    '#title' => 'Failed parses',
  );
  $form['content']['#markup'] = theme('item_list',array('items'=>$graphs_sentences));
  return $form;
}

/**
 * @desc show the log of users activity
 */
function mgwiki_admin_users_ual_log(){
  return ual_log_view();
}

function mgwiki_admin_misc_list_highlighted_graphs(){
  return drupal_get_form('parser_list_highlighted_graphs_form');
}

function mgwiki_admin_misc_test(){
  print standardize_string("http://www.lemonde.fr/coupe-du-monde/article/2014/06/30/qui-soutenir-quand-on-est-orphelin-de-son-equipe_4448016_1616627.html");  
}

function mgwiki_admin_misc_corpus_reindex(){
  module_load_include('module', 'mgwiki_corpus');
  $corpus_db_list = db_select('mgwiki_corpus','mc')->fields('mc',array('cid','owner','permissions'))->execute()->fetchAllAssoc('cid');
  foreach ($corpus_db_list as $cid => $corpus) {
    rest_helper(FRMGCM_HOST. "corpus/".$cid."/_index",array(),'POST','txt');
  }
}

function mgwiki_admin_misc_mgwiki_global_guide(){
  drupal_goto("content/mgwiki-project-information-guide");
}