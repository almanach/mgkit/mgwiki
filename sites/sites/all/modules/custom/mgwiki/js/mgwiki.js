(function(mgwikilib){
  
  jQuery(document).ready(function(){
/*    if(document.URL.indexOf('/edit') == document.URL.length-5){
      window.onbeforeunload = function(e){
        var message = "There is a current edition in process. Are you sure to leave this page?";
        e = e || window.event;
        // For IE and Firefox
        if (e) {
          e.returnValue = message;
        }
        // For Safari
        return message;
      };
    }*/
    jQuery('a').on('mouseover',function(){
      var linkparts = this.href.split('#');
      if(linkparts.length == 2){
        var anchor = linkparts[1];
        var re = /^ref(\d*)/;
        var parts = re.exec(anchor);
        if(parts && parts[1]){
          var reference = jQuery('li#reference'+parts[1]);
          if(reference.length){
            if(!mgwikilib.isInView(reference)){
              mgwikilib.displayReference(reference);
            }
            
          }
        }
      }
    });
    
    jQuery('a').on('mouseout',function(){
      mgwikilib.displayReference();
    });
  });
  
  
  mgwikilib.isInView = function(obj){
    var offset = obj.offset();
    var height = jQuery(window).height();
    var scroll = jQuery(document).scrollTop();
    return scroll + height > offset.top;
  };
  
  mgwikilib.displayReference = function (reference){
    if(reference){
      var div = jQuery('<div id="mgwiki-reference-box"><hr></div>');
      div.append(reference.clone());
      var offset = jQuery('#content').offset();
      var top = jQuery(window).height() - reference.height() - 50;
      div.css('top',top);
      div.css('left',offset.left);
      jQuery('body').append(div);
      console.log(div);
    }else{
      var div = jQuery('div#mgwiki-reference-box');
      div.remove();
    }
    
  };

  mgwikilib.ui = {};

  mgwikilib.ui.MutableField = function(val,$el,submitCallback){
    var me = this;
    this.$el = $el;
    this.submitCallback = submitCallback;

    $el.html('<span class="mgwiki-mutable">'+val+'</span>');

    $el.on("click",function(e){
      var elt = jQuery(this).children().first();
      if(elt[0].nodeName.toLowerCase() == 'input'){
        return;
      }
      var val = elt.text();
      var inputField = '<input type="text" class="mgwiki-mutable" value="'+val+'">';
      elt.replaceWith(inputField);
    });
    
    $el.on('keypress','.mgwiki-mutable',function(e){
      var elt = this;
      if(e.which == 13){
        me.submit();
        e.preventDefault();
      }
    });
    
    jQuery(document).on("click",function(e){
      var elt = jQuery(e.target);
      if(!elt.hasClass('mgwiki-mutable') && elt.attr('id') != $el.attr('id')){
        me.submit();
      }
    });
    
  }

  mgwikilib.ui.MutableField.prototype.submit = function(){
    var me = this;
    var field = me.$el.children().first();
    if(field[0].nodeName.toLowerCase() == 'input'){
      var val = field.attr("value");
      var span = '<span class="mgwiki-mutable">'+val+'</span>';
      this.submitCallback.call(this,val);
      field.replaceWith(span);
    }
  };
  
  
  

}(window.mgwikilib = window.mgwikilib || {}));
