<?php

// add the js application ressources
$basepath = drupal_get_path('module','mgwiki_corpus') ;
drupal_add_js($basepath."/js/cma_reloaded/build/cma_reloaded.js");
drupal_add_css($basepath."/js/cma_reloaded/build/style/cma.css");
drupal_add_js($basepath."/js/cma/lib/jquery-textcomplete-master/jquery.textcomplete.js");
drupal_add_css($basepath."/js/cma/lib/jquery-textcomplete-master/jquery.textcomplete.css");

if(!isset($app)){
  $app = "App";
}

global $user;
$user_light = array(
  'name'=>$user->name,
  'uid'=>$user->uid
);
global $base_url;
$ws_url = $base_url . '/mgwiki_corpus/';

$options = array(
    "proxy_url"=>$base_url.'/mgwiki_cpm_proxy/',
    "ws_url"=>$ws_url,
    "user"=>$user_light,
 );

?>
<div id="cma-reloaded-container"></div>
<script>
  new mgwiki.cma.<?php echo $app; ?>(jQuery('#cma-reloaded-container'),<?php echo json_encode($options);?>);
</script>



