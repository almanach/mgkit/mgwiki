(function(mgwiki,$){
  
  mgwiki.cma.SearchMacroLib = function($el,settings){
    this.settings = settings || {};
    this.view = new mgwiki.cma.SearchMacroLibView(this,$el);
  }
  
}(window.mgwiki = window.mgwiki || {},jQuery));