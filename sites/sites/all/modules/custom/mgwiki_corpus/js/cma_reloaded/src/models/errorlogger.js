(function(mgwiki,$){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.ErrorLogger = function($el,settings){
    this.settings = settings || {};
    this.view = new mgwiki.cma.ErrorLoggerView(this,$el);
    this.init();
  };

  mgwiki.cma.ErrorLogger.prototype.init = function(){
    this.messages = [];
    this.view.render();
  };


  mgwiki.cma.ErrorLogger.prototype.log = function(message){
    this.messages.push(message);
    this.view.render();
  };

  

  
}(window.mgwiki = window.mgwiki || {},jQuery));