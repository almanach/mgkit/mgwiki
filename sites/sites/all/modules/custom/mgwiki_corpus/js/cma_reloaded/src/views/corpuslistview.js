(function(mgwiki,$){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.CorpusListView = function(model,$el,settings){
    this.model = model;
    this.$el = $el || jQuery('<div></div>');
  };

  mgwiki.cma.CorpusListView.prototype.render = function(){
    var me = this;
    this.$el.html(mgwiki.cma.CorpusListView.template);

    var $corpus = this.$el.find('.cma-available-corpus');

    var corpusOption = '<div>Select corpus : <select name="corpus">';
    for(var i=0;i<this.model.list.length;i++){
      var corpusInfo = this.model.list[i];
      var selected = '';
      if(this.model.targetCorpus == corpusInfo.name){
        selected = ' selected'
      }
      corpusOption += '<option value="'+i+'"'+selected+'>'+corpusInfo.name+'</option>';
    }
    corpusOption += '</select></div>';


    $corpus.append(corpusOption);

    $corpus.find('select').on('change',function(e){
      var corpus = e.target.value;
      me.model.app.use(me.model.list[corpus]);
    });

    var filter = $('<div><span style="margin-right:10px;">Filter:</span><span style="margin-right:10px;"><input name="voted" type="checkbox">Hide voted</span><span style="margin-right:10px;"><input name="unvoted" type="checkbox">Hide unvoted</span></div>');
    filter.find('input[name="voted"]').on('change',function(e){
      me.model.app.sentenceCollection.hideVoted = $(this).prop("checked");
      me.model.app.sentenceCollection.goTo(0);
    });
    filter.find('input[name="unvoted"]').on('change',function(e){
      me.model.app.sentenceCollection.hideUnVoted = $(this).prop("checked");
      me.model.app.sentenceCollection.goTo(0);
    });
    $corpus.append(filter);


    if(this.model.list.length>0){
      me.model.app.use(me.model.list[0]);
    }
  };

  mgwiki.cma.CorpusListView.template = '<div class="cma-available-corpus" style="border-bottom:1px solid black; padding-bottom:10px;"></div>';

  
}(window.mgwiki = window.mgwiki || {},jQuery));