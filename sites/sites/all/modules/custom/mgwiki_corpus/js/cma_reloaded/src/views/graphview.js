(function(mgwiki,$){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.GraphView = function(model,$el){
    this.model = model;
    this.$el = $el || $('<div></div>');
  };

  mgwiki.cma.GraphView.prototype.render = function(){
    var me = this;
    this.$el.html('<div class="cma-graph-loading"></div>');
    setTimeout(function(){me.model.fetchData(function(){me.delayedRender();},function(text){alert(text);});},500);
  };

  mgwiki.cma.GraphView.prototype.delayedRender = function(){
    var me = this;
    var mastercontainer = $('<div><div class="cma-graph-info" style="margin-top:15px; margin-bottom:5px;"></div><table style="margin-left:auto;margin-right:auto; text-align:center;"><tr></tr></table></div>');
    mastercontainer.find('tr').append(this.voteView());
    var td = $('<td style="text-align:center;"></td>');
    var graphcontainer = $('<div class="graph-container-'+this.model.settings.id+'"></div>');
    td.append(graphcontainer);
    mastercontainer.find('tr').append(td);
    if(me.model.settings.notes && me.model.settings.notes.length>0){
      mastercontainer.find('tr').append(this.noteView());
    }
    if(this.model.settings.eid){
      mastercontainer.find('.cma-graph-info').append("<span style=\"margin-right:10px\">ID:"+this.model.settings.eid+"</span>");
    }
    if(this.model.settings.sentence){
      mastercontainer.find('.cma-graph-info').append("<span>| "+this.model.settings.sentence+"</span>");
    }
    this.$el.html(mastercontainer);
    var depGraph = this.depgraph =new depgraphlib.DepGraph(graphcontainer,this.model.settings.data,{"uid":this.model.settings.id,viewmode:"fit"});
    depGraph.wsurl = me.model.app.settings.ws_url+"/graph";
    depGraph.sentence = me.model.settings.sentence;
    var viewer = depGraph.viewer;
    viewer.addToolbarItem({name:'add-note',callback:function(){me.addNote(this);},style:'add-note','static':true});
    viewer.getToolbarItem('export').button.unbind('click');
    viewer.getToolbarItem('export').button.click(function(){me.model.export();});
    viewer.noBorders();
  };

  mgwiki.cma.GraphView.prototype.noteView = function(){
    var me = this;
    var uid = me.model.settings.id;
    var notesWidget = '<td style="text-align:top;"><div id="notes-'+me.model.settings.id+'"><div class="graph-note"></div></div></td>';
    var jnotesWidget = $(notesWidget);
    var container = jnotesWidget.find("#notes-"+uid);
    container.on("click",function(event){
      if($(event.target).hasClass("note-delete")){
        return;
      }
      var content = $('.notes-content',this);
      content.toggle();
    });
    var div = '<div class="notes-content hidden"><table title="comments">';
    for(var i=0;i<me.model.settings.notes.length;i++){
      div += "<tr><td>["+me.model.settings.notes[i].user+"] : "+me.model.settings.notes[i].data;
      if(me.model.settings.notes[i].user == me.model.app.settings.user.name){
        div += "<div id=\""+i+"note-"+uid+"\" cid=\""+i+"\" class=\"note-delete\"></div>";
      }
      div += "</td></tr>";
    }
    div += "</tr></table>";
    container.append(div);
    jnotesWidget.find('.note-delete').click(function(event){
      var note = this;
      me.model.removeNote(this);
    });
    return jnotesWidget;
  }


  mgwiki.cma.GraphView.prototype.addNote = function(divbutton){
    var me = this;
    var coords = divbutton.getBoundingClientRect();
    var point = {x:coords.left,y:coords.top + coords.height + 2};
    var form = '<form id="note-form-'+me.depgraph.options.uid+'">';
    form += '<textarea id="note-text-'+me.depgraph.options.uid+'" name="note" rows="4" cols="50"></textarea><br>';
    form += '<input id="note-submit-'+me.depgraph.options.uid+'" name="note-submit" type="button" value="Add note">';
    form +='</form>';
    var div ='<div></div>';
    div = jQuery(div);
    div.append(form);
    div.find("#note-submit-"+me.depgraph.options.uid).on("click",function(){me.model.submitNote();});
    me.depgraph.viewer.createBox({closeButton:true,position:point,autodestroy:false,forceToolbar:true}).setContent(div).setHeader("Add note").open();
  };

  mgwiki.cma.GraphView.prototype.voteView = function(){
    var me = this;
    var totals = [0,0];
    var classUp = "d3js-vote-up";
    var classDown = "d3js-vote-down";
    for(var i =0;i<this.model.settings.votes.length;i++){
      if(this.model.settings.votes[i].val>0){
        totals[0]++;
      }else if(this.model.settings.votes[i].val<0){
        totals[1]++;
      }
      if(this.model.settings.votes[i].name == this.model.app.settings.user.name){
        if(this.model.settings.votes[i].val>0){
          classUp += " d3js-voted-up";
        }else if(this.model.settings.votes[i].val<0){
          classDown += " d3js-voted-down";
        }
      }
    }

    var uid = this.model.settings.id;
    var td = '<td  valign="top" width="40px">'+
      '<div id="graph-sidebar0-'+uid+'" class="graph-sidebar">'+
        '<ul>'+
          '<li>'+
            '<div id="graph-upvote-'+uid+'" title="Validate" class="'+classUp+'">'+totals[0]+'</div>'+
          '</li>'+
          '<li>'+
            '<div id="graph-downvote-'+uid+'" title="Invalidate" class="'+classDown+'">'+totals[1]+'</div>'+
          '</li>'+
        '</ul>'+
      '</div>' +
    '</td>';
    var jtd = $(td);
    jtd.find('#graph-upvote-'+uid).click(function(){
      var upvote = $(this);
      if(!upvote.hasClass("d3js-voted-up")){
        upvote.html(parseInt(upvote.html())+1);
        upvote.addClass("d3js-voted-up");
        var downvote = $(this).parents('td').find("#graph-downvote-"+uid);
        if(downvote.hasClass("d3js-voted-down")){
          downvote.removeClass("d3js-voted-down");
          downvote.html(parseInt(upvote.html())-1);  
        }
        
        me.model.vote(1);
      }else{
        upvote.html(parseInt(upvote.html())-1);
        upvote.removeClass("d3js-voted-up");
        
        me.model.vote(0);
      }
    });
    jtd.find('#graph-downvote-'+uid).click(function(){
      var downvote = $(this);
      if(!downvote.hasClass("d3js-voted-down")){
        downvote.html(parseInt(downvote.html())+1);
        downvote.addClass("d3js-voted-down");
        var upvote = $(this).parents('td').find("#graph-upvote-"+uid);
        if(upvote.hasClass("d3js-voted-up")){
          upvote.removeClass("d3js-voted-up");
          upvote.html(parseInt(upvote.html())-1);  
        }
        me.model.vote(-1);
      }else{
        downvote.html(parseInt(downvote.html())-1);
        downvote.removeClass("d3js-voted-down");
        me.model.vote(0);
      }
    });
    return jtd;
  }

  mgwiki.cma.GraphView.prototype.sentenceView = function(){
  };

  mgwiki.cma.GraphView.template = '';
  
}(window.mgwiki = window.mgwiki || {},jQuery));
