(function(mgwiki){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.CorpusListItem = function(app,$el,settings){
    this.app = app;
    this.view = new mgwiki.cma.CorpusListItemView(this);
    this.item = settings;
  };

  
}(window.mgwiki = window.mgwiki || {}));