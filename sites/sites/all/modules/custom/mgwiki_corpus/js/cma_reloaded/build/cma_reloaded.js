(function(mgwiki,$){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma = {};

  mgwiki.cma.App = function($el,config){
    this.$el = $el || $('<div></div>');
    this.settings = config || {};
    this.init();
  };

  mgwiki.cma.App.prototype.init = function(){
    var me = this;

    this.$el.html(mgwiki.cma.App.template);

    this.errorLogger = new mgwiki.cma.ErrorLogger(this.$el.find('.cma-error-message'));
    this.searchService = new mgwiki.cma.SearchService(this,this.$el.find('.cma-search-box'),this.settings);


  };

  mgwiki.cma.App.template = 
    '<div class="cma-error-message"></div>'+
    '<div class="cma-search-box"></div>'+
    '<div class="cma-results-container"></div>';


  mgwiki.cma.App2 = function($el,settings){
    this.$el = $el || $('<div></div>');
    this.settings = settings;
    this.init();
  };

  mgwiki.cma.App2.prototype.init = function(){
    var me = this;

    this.$el.html(mgwiki.cma.App2.template);

    this.corpuslist = new mgwiki.cma.CorpusList(this,this.$el.find('.cma-col-left'));
    this.sentenceCollection = new mgwiki.cma.SentenceCollection(this,this.$el.find('.cma-sentencecollection-container'));
  };

  mgwiki.cma.App2.prototype.use = function(item){
    this.sentenceCollection.load(item);
  };

  mgwiki.cma.App2.template = 
    '<div class="cma-col-left"></div>'+
    '<div class="cma-sentencecollection-container"></div>';


}(window.mgwiki = window.mgwiki || {},jQuery));
(function(mgwiki,$){
  
  mgwiki.cma.ui = mgwiki.cma.ui || {};

  mgwiki.cma.ui.CallbackButton = function(name,options){
    var style = "";
    if(options && options.style){
      style = options.style;
    }
    this.view = jQuery('<span><input type="button" '+style+' value="'+name+'"><div class="cma-throbber hidden"></div></span>');
  }



  mgwiki.cma.ui.CallbackButton.prototype.init = function(callback,data){
    var me = this;
    jQuery('input',this.view).click(function(){
      var throbber = jQuery('.cma-throbber',me.view);
      if(!throbber.is(':hidden')){
        return;  
      }
      throbber.removeClass('cma-throbber-success');
      jQuery('.cma-throbber',me.view).removeClass('cma-throbber-error');
      jQuery('.cma-throbber',me.view).show();
      jQuery('.cma-throbber',me.view).css('display','inline-block');
      callback.call(me,data);
    });
  }

  mgwiki.cma.ui.CallbackButton.prototype.success = function(){
    jQuery('.cma-throbber',this.view).addClass('cma-throbber-success');
    jQuery('.cma-throbber',this.view).fadeOut(2000);
  }

  mgwiki.cma.ui.CallbackButton.prototype.error = function(){
    jQuery('.cma-throbber',this.view).addClass('cma-throbber-error');
    jQuery('.cma-throbber',this.view).fadeOut(2000);
  }

  
  
}(window.mgwiki = window.mgwiki || {},jQuery));
(function(cma){

  mgwiki.cma.ui = mgwiki.cma.ui || {};

  mgwiki.cma.ui.FoldableDiv = function(content,startexpanded){
    this.view = jQuery(mgwiki.cma.ui.FoldableDiv.templateMain);
    this.content = jQuery('.cma-foldable-content',this.view);
    this.content.append(content);
    this.expanded = startexpanded;
    if(!this.expanded){
      jQuery('.cma-foldable-head',this.view).addClass('cma-foldable-open');
      this.content.hide();
    }else{
      jQuery('.cma-foldable-head',this.view).addClass('cma-foldable-close');
    }
    this.initView();
  }

  mgwiki.cma.ui.FoldableDiv.prototype.setTitle = function(title){
    jQuery('.cma-foldable-head',this.view).find('div').html(title);
  }

  mgwiki.cma.ui.FoldableDiv.prototype.initView = function(){
    var me = this;
    jQuery('.cma-foldable-head',this.view).click(function(){
      if(me.expanded){
        jQuery(this).addClass('cma-foldable-open');
        jQuery(this).removeClass('cma-foldable-close');
        me.content.hide();
      }else{
        jQuery(this).addClass('cma-foldable-close');
        jQuery(this).removeClass('cma-foldable-open');
        me.content.show();  
      }
      me.expanded = !me.expanded;
    });
  }

  mgwiki.cma.ui.FoldableDiv.templateMain = '<div class="cma-foldable-div"><div class="cma-foldable-head"><div style="border-top:1px solid #bbbbbb; margin-left:20px; margin-top:2px; font-size:10px;"></div></div>'+
    '<div class="cma-foldable-content"></div></div>';
  
}(window.cma = window.cma || {}));
(function(mgwiki,$){

  mgwiki.cma.ui = mgwiki.cma.ui || {};

  mgwiki.cma.ui.Table = function(definition,data){
    this.view = jQuery('<table></table>');
    this.model = definition;
    this.model.bindings = {};
    this.model.style = this.model.style || {};

    this.view.addClass(this.model.style.table);
    this.initView();
    if(data){
      this.update(data);
    }
  }


  mgwiki.cma.ui.Table.prototype.clear = function(){
    this.view.html("");
    this.initView();
  }

  mgwiki.cma.ui.Table.prototype.getData = function(){
    var data = [];
    var rows = jQuery('tr',this.view);
    for(var i =1;i<rows.length;i++){
      var cells = jQuery(rows[i]).children();
      var item = {};
      data.push(item);
      for(var j = 0;j<cells.length;j++){
        item[this.model.headers[j].nameid] = (typeof this.model.headers[j].unformat == 'function')?this.model.headers[j].unformat(jQuery(cells[j]).children()[0]):jQuery(cells[j]).html();
      }
    }
    return data;
  }

  mgwiki.cma.ui.Table.prototype.update = function(data){
    this.clear();
    var styletr0 = (this.model.style.tr0)?' class="'+this.model.style.tr0+'" ':"";
    var styletr1 = (this.model.style.tr1)?' class="'+this.model.style.tr1+'" ':"";
    var styletd = (this.model.style.td)?' class="'+this.model.style.td+'" ':"";
    var ncols = this.model.headers.length;
    this.originalData = data;
    for(var i in data){
      var row = '<tr'+((i%2)?styletr1:styletr0)+'>';
      for(var k=0;k<ncols;k++){
        row += '<td'+styletd+'></td>';
      }
      row += '</tr>';
      row = jQuery(row);
      var cells = row.children();
      for(var name in data[i]){
        if(this.model.bindings[name] !== undefined){
          var content = (typeof this.model.headers[this.model.bindings[name]].format == 'function')?this.model.headers[this.model.bindings[name]].format(name,data[i][name],data[i]):data[i][name];
          jQuery(cells[this.model.bindings[name]]).html(content);
        }
      }
      this.view.append(row);
    }
  }

  mgwiki.cma.ui.Table.prototype.initView = function(){
    var styleth = (this.model.style.th)?' class="'+this.model.style.th+'" ':"";
    var stylethr = (this.model.style.thr)?' class="'+this.model.style.thr+'" ':"";
    var tableheader = '<tr'+stylethr+'>';
    for(var i in this.model.headers){
      var header = this.model.headers[i];
      tableheader += '<th'+styleth+'>'+header.title+'</th>';
      if(header.nameid){
        this.model.bindings[header.nameid] = i;  
      }
    }
    tableheader += '</tr>';

    this.view.append(tableheader);
  }
  
  
}(window.mgwiki = window.mgwiki || {},jQuery));
(function(mgwiki,$){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.CorpusList = function(app,$el,settings){
    this.app = app;
    this.list = [];
    this.view = new mgwiki.cma.CorpusListView(this,$el);
    this.init();
  };

  mgwiki.cma.CorpusList.prototype.init = function(){
    var me = this;
    $.ajax({
      type: 'POST', 
      url: me.app.settings.proxy_url + "sc/_list",
      data: {
        namespace:"passage_group"
      },
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        for(var i=0;i<data.length;i++){
          me.list.push(data[i]);
        }
        me.view.render();
      },
      error: function(jqXHR, textStatus, errorThrown) {
        console.log("error while fetching corpus list");
      }
    });
  };

  
}(window.mgwiki = window.mgwiki || {},jQuery));
(function(mgwiki){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.CorpusListItem = function(app,$el,settings){
    this.app = app;
    this.view = new mgwiki.cma.CorpusListItemView(this);
    this.item = settings;
  };

  
}(window.mgwiki = window.mgwiki || {}));
(function(mgwiki,$){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.ErrorLogger = function($el,settings){
    this.settings = settings || {};
    this.view = new mgwiki.cma.ErrorLoggerView(this,$el);
    this.init();
  };

  mgwiki.cma.ErrorLogger.prototype.init = function(){
    this.messages = [];
    this.view.render();
  };


  mgwiki.cma.ErrorLogger.prototype.log = function(message){
    this.messages.push(message);
    this.view.render();
  };

  

  
}(window.mgwiki = window.mgwiki || {},jQuery));
(function(mgwiki,$){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.Graph = function(app,$el,settings){
    this.app = app;
    this.settings = settings;
    this.view = new mgwiki.cma.GraphView(this,$el);
  };

  mgwiki.cma.Graph.prototype.fetchData = function(callbackSuccess,callbackError){
    var me = this;
    $.ajax({
      type: 'POST', 
      url: me.app.settings.proxy_url + "sc/"+me.settings.sc+"/_graph",
      data: {
        complete:true,
        sid:me.settings.id,
      },
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        me.settings.data = data.data;
        me.settings.notes = data.notes;
        me.settings.votes = data.votes;
        me.settings.eid = data.eid;
        me.settings.sentence = data.sentence;
        callbackSuccess.call(me,data);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        callbackError.call(me,textStatus);
      }
    });
  };

  mgwiki.cma.Graph.prototype.export = function(){
    depgraphlib.windowOpenPost(
      {
        sid:me.settings.id
      },
      me.app.settings.proxy_url + "sc/"+me.settings.sc+"/_graph"
    );
  };


  mgwiki.cma.Graph.prototype.vote = function(val){
    var me = this;
    $.ajax({
      type: 'POST', 
      url: me.app.settings.proxy_url + "sc/"+me.settings.sc+"/_graph/_vote/_put",
      data: {
        sid:me.settings.id,
        val:val
      },
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
      },
      error: function(jqXHR, textStatus, errorThrown) {
      }
    });
  };

  mgwiki.cma.Graph.prototype.removeNote = function(note){
    var index = jQuery(note).attr('cid');
    var me = this;
    $.ajax({
      type: 'POST', 
      url: me.app.settings.proxy_url + "sc/"+me.settings.sc+"/_graph/_note/_remove",
      data: {
        sid:me.settings.id,
        index:index
      },
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        jQuery(note.parentNode.parentNode).remove();
        console.log(data);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  };
  
  mgwiki.cma.Graph.prototype.submitNote = function(){
    var me = this;
    var depgraph = this.view.depgraph;
    var textarea = $("#note-text-"+depgraph.options.uid);
    var data = textarea.val();
    var box = depgraphlib.Box.getBox(textarea[0]);
    $.ajax({
      type: 'POST', 
      url: me.app.settings.proxy_url + "sc/"+me.settings.sc+"/_graph/_note/_add",
      data: {
        sid:me.settings.id,
        data:data
      },
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        me.view.render();
      },
      error: function(jqXHR, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
    box.close(true);
  };

}(window.mgwiki = window.mgwiki || {},jQuery));
(function(mgwiki,$){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.SearchLib = function(settings){
    this.view = jQuery(mgwiki.cma.SearchLib.template);
    this.textarea = settings.textarea;
    this.user = settings.user;
    this.ws_url = settings.proxy_url;
    this.init();
  };


  mgwiki.cma.SearchLib.prototype.initView = function(){
    if(this.currentSelect){
      this.currentSelect.removeClass('cma-searchlib-list-item-selected');
    }
    this.currentHover = null;
    this.currentSelect = null;
    var me = this;
    jQuery('.cma-searchlib-filter-input',this.view).on("keyup",function(e){
      var obj = this;
      if(me.currentSearch != jQuery(obj).val()){
        jQuery.ajax({
          type: 'POST', 
          url: me.ws_url+'dpathlib/_search',
          data:{
            query:jQuery(obj).val()
          },
          dataType : 'json',
          success: function(data, textStatus, jqXHR) {
            me.fillTable(data);
          },
          error: function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
          }
        });
      }
      me.currentSearch = jQuery(obj).val();
    });
    jQuery('.cma-searchlib-list',this.view).on("mouseover",function(e){
      var selection = jQuery(e.target).parents('tr');
      if(me.currentHover){
        me.currentHover.removeClass('cma-searchlib-list-item-hover');
      }
      if(selection.hasClass('cma-searchlib-list-title')){
        me.currentHover = null;
        return;
      }
      me.currentHover = selection;
      selection.addClass('cma-searchlib-list-item-hover');
    });
    jQuery('.cma-searchlib-list',this.view).on("mouseout",function(e){
      if(me.currentHover){
        me.currentHover.removeClass('cma-searchlib-list-item-hover');
        me.currentHover = null;
      }
    });
    jQuery('.cma-searchlib-list',me.view).on("click",function(e){
      var selection = jQuery(e.target).parents('tr');
      if(selection.hasClass('cma-searchlib-list-title')){
        return;
      }
      if(me.currentSelect){
        me.currentSelect.removeClass('cma-searchlib-list-item-selected');
      }
      if(me.currentSelect == selection){
        return;
      }
      me.currentSelect = selection;
      selection.addClass('cma-searchlib-list-item-selected');
    });
    jQuery('.cma-searchlib-insert',me.view).click(function(){
      if(me.currentSelect != null){
        var children = me.currentSelect.children();
        var macro = "$"+children[0].innerHTML;
        var val = me.textarea.html();
        me.textarea.html(val+'<span>'+macro+'</span>');
      }
    });
    this.setTableActions();
  };

  mgwiki.cma.SearchLib.prototype.init = function(){
    var me = this;
    jQuery.ajax({
      type: 'POST', 
      url: me.ws_url+'dpathlib/_all',
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        me.macros = [];
        for (var i = data.length - 1; i >= 0; i--) {
          me.macros.push(data[i].name);
        };
        me.fillTable(data);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        
      }
    });
  };

  mgwiki.cma.SearchLib.prototype.setTableActions = function(){
    var me = this;
    jQuery('.cma-searchlib-action-delete',me.view).click(function(e){
      var selection = jQuery(e.target).parents('tr');
      var children = selection.children();
      var macro = children[0].innerHTML;
      var r = window.confirm("Êtes vous sur de vouloir supprimer cette recherche?");
      if(r){
        me.deleteElement(macro,function(){
          selection.remove();
        });
      }
      
    });
    jQuery('.cma-searchlib-action-edit',me.view).click(function(e){
      var selection = jQuery(e.target).parents('tr');
      var children = selection.children();
      var ispublic = jQuery(children[4]).find('.cma-searchlib-action-public');
      var publicv = false;
      if(ispublic.length){
        publicv = true;
      }
      var macro = {
        name:children[0].innerHTML,
        query:children[1].innerHTML,
        description:children[2].innerHTML,
        publicv:publicv
      };
      me.elementForm(macro);
    });
    jQuery('.cma-searchlib-action-public',me.view).click(function(e){
      var selection = jQuery(e.target).parents('tr');
      var children = selection.children();
      var macro = {
        name:children[0].innerHTML,
        query:children[1].innerHTML,
        description:children[2].innerHTML,
        publicv:false
      };
      me.syncElement(macro.name,macro,function(data){
        if(data.success){
          me.init();
        }else{
          alert(data.message);
        }
      });
    });
    jQuery('.cma-searchlib-action-private',me.view).click(function(e){
      var selection = jQuery(e.target).parents('tr');
      var children = selection.children();
      var macro = {
        name:children[0].innerHTML,
        query:children[1].innerHTML,
        description:children[2].innerHTML,
        publicv:true
      };
      me.syncElement(macro.name,macro,function(data){
        if(data.success){
          me.init();
        }else{
          alert(data.message);
        }
      });
    });
  };

  /**
   * Fill the table with data retrieved by the server
   * @param  {[type]} data [description]
   * @return {[type]}      [description]
   */
  mgwiki.cma.SearchLib.prototype.fillTable = function(data){
    var me = this;
    var tbody = jQuery('.cma-searchlib-list-body',this.view);
    tbody.html("");
    for (var i = data.length - 1; i >= 0; i--) {
      tbody.append('<tr><td>'+data[i].name+'</td><td>'+data[i].query+'</td><td>'+data[i].description+'</td><td>'+data[i].user+'</td><td>'+getActions(data[i],this.user.name)+'</td></tr>');
    };
    me.setTableActions();
  };

  function getActions(macro,user){
    var actions = "";
    if(macro.user == user){
      actions += '<span class="cma-searchlib-action cma-searchlib-action-edit"></span><span class="cma-searchlib-action cma-searchlib-action-delete"></span>';
    }
    if(macro.ispublic){
      actions += '<span class="cma-searchlib-action cma-searchlib-action-public"></span>';
    }else{
      actions += '<span class="cma-searchlib-action cma-searchlib-action-private"></span>';
    }
    return actions;
  }

  /**
   * expand a macro to the query
   * @param  {[type]} name [description]
   * @return {[type]}      [description]
   */
  mgwiki.cma.SearchLib.prototype.expand = function(name){

  };

  /**
   * show the form to add a new macro to the library
   * @param {[type]} name  [description]
   * @param {[type]} query [description]
   */
  mgwiki.cma.SearchLib.prototype.elementForm = function(macroInfo){
    var me = this;
    var width = jQuery(window).width();
    var height = jQuery(window).height();
    var box = new depgraphlib.Box({closeButton:true,draggable:true,position:{x:width/2-200,y:height/2-200}});
    box.setFixedSize(400,400);
    box.open();
    box.setHeader("DPath Query Library");

    var $el = jQuery('.depgraphlib-box-content',box.object);
    $el.append(mgwiki.cma.SearchLib.addNewFormTemplate);

    if(macroInfo.query){
      $el.find('.cma-searchlib-addnewform-query').val(macroInfo.query);  
    }

    if(macroInfo.description){
      $el.find('.cma-searchlib-addnewform-desc').val(macroInfo.description);  
    }

    if(macroInfo.name){
      $el.find('.cma-searchlib-addnewform-name').val(macroInfo.name);  
    }

    if(macroInfo.publicv){
      $el.find('.cma-searchlib-addnewform-public').prop("checked",true);  
    }
    
    $el.find('.cma-searchlib-addnewform-cancel').click(function(){
      box.destroy();
      
    });

    $el.find('.cma-searchlib-addnewform-submit').click(function(){
      var owner = me.user.name || 'unknown';
      var query = $el.find('.cma-searchlib-addnewform-query').val();
      var name = $el.find('.cma-searchlib-addnewform-name').val();
      var desc = $el.find('.cma-searchlib-addnewform-desc').val();

      var publicv = $el.find('.cma-searchlib-addnewform-public')[0].checked;
      
      var macroDefintion = {
        name:name,
        query:query,
        owner:owner,
        description:desc,
        publicv:publicv
      };
      me.syncElement(macroInfo.name,macroDefintion,function(data){
        if(data.success){
          me.init();
          box.destroy();  
        }else{
          alert(data.message);
        }
      });
      
    });
  };

  mgwiki.cma.SearchLib.prototype.deleteElement = function(macroName,callbackResult){
    var proxy_url = "dpathlib/"+macroName+"/_delete";
    var me = this;
    var data = {
      name:macroName,
    };
    jQuery.ajax({
      type: 'POST', 
      url: me.ws_url+proxy_url,
      data: data,
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        callbackResult.call(me,data);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        
      }
    });
  };

  mgwiki.cma.SearchLib.prototype.syncElement = function(macroName,macroDefinition,resultCallback){
    var proxy_url = "dpathlib/_create";
    if(macroName){
      proxy_url = "dpathlib/"+macroName+"/_edit";
    }
    var me = this;
    var data = {
      name:macroDefinition.name,
      query:macroDefinition.query,
      owner:macroDefinition.owner,
      description:macroDefinition.description,
      publicv:macroDefinition.publicv
    };
    jQuery.ajax({
      type: 'POST', 
      url: me.ws_url+proxy_url,
      data: data,
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        resultCallback.call(me,data);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        
      }
    });
  };

  mgwiki.cma.SearchLib.addNewFormTemplate = '<div class="cma-searchlib-addnewform">'+
    '<h2>Save Query macro</h2>'+
    '<fieldset>'+
      '<legend>Required:</legend>'+
      'Name: <input class="cma-searchlib-addnewform-name" type="text"><br>'+
      'Query: <input class="cma-searchlib-addnewform-query" type="text"><br>'+
    '</fieldset>'+
    '<fieldset>'+
      '<legend>Optional:</legend>'+
      'Description: <textarea class="cma-searchlib-addnewform-desc"></textarea><br>'+
      'Public Visibility: <input type="checkbox" class="cma-searchlib-addnewform-public"><br>'+
    '</fieldset>'+
    '<input type="submit" class="cma-searchlib-addnewform-submit" value="Save"><input type="submit" class="cma-searchlib-addnewform-cancel" value="Cancel">'
  '</div>';

  /**
   * delete a macro
   * @param  {[type]} name [description]
   * @return {[type]}      [description]
   */
  mgwiki.cma.SearchLib.prototype.remove = function(name){

  };

  /**
   * show the macro library
   * @return {[type]} [description]
   */
  mgwiki.cma.SearchLib.prototype.show = function(){
    var box = new depgraphlib.Box({closeButton:true,draggable:true,position:{x:50,y:50}});
    box.setMaxSize(null,500);
    box.open();
    box.setHeader("DPath Query Library");

    var $el = jQuery('.depgraphlib-box-content',box.object);
    this.initView();
    $el.append(this.view);
  };

  mgwiki.cma.SearchLib.template = '<div>'+
    '<div><span class="cma-searchlib-filter"><input type="text" class="cma-searchlib-filter-input"></span><span class="cma-searchlib-insert"><button type="submit">Insert Selected Macro</button></span></div>'+
    '<table class="cma-searchlib-list"><thead><tr class="cma-searchlib-list-title"><th>Name</th><th>Query</th><th>Description</th><th>Owner</th><th>Actions</th></tr></thead><tbody class="cma-searchlib-list-body"></tbody></table>'+
  '</div>';


  mgwiki.cma.SearchHistory = function(settings){
    this.view = jQuery('<table class="cma-searchhistory-list"><thead><tr class="cma-searchhistory-list-title"><th>Query</th><th></th></tr></thead><tbody class="cma-searchhistory-list-body"></tbody></table>');
    this.settings = settings;
  };

  mgwiki.cma.SearchHistory.prototype.show = function(){
    var box = new depgraphlib.Box({closeButton:true,draggable:true,position:{x:50,y:50}});
    box.setMaxSize(null,500);
    box.open();
    box.setHeader("DPath Query History");

    var $el = jQuery('.depgraphlib-box-content',box.object);
    this.refreshView();
    $el.append(this.view);
  };

  mgwiki.cma.SearchHistory.prototype.refreshView = function(){
    var me = this;
    var proxy_url="dpathlib/_history"
    jQuery.ajax({
      type: 'POST', 
      url: me.settings.ws_url+proxy_url,
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        me.history = [];
        for (var i = data.length - 1; i >= 0; i--) {
          me.history.push(data[i]);
          var tbody = jQuery('.cma-searchhistory-list-body',me.view);
          tbody.html("");
          for (var i = data.length - 1; i >= 0; i--) {
            tbody.append('<tr><td>'+data[i]+'</td><td><span class="cma-searchhistory-save"></span><span class="cma-searchhistory-insert"></span></td></tr>');
          };
          jQuery('.cma-searchhistory-save',me.view).click(function(e){
            var selection = jQuery(e.target).parents('tr');
            var children = selection.children();
            var macro = {
              query:children[0].innerHTML,
            };
            me.settings.macroLib.elementForm(macro);
          });
          jQuery('.cma-searchhistory-insert',me.view).click(function(e){
            var selection = jQuery(e.target).parents('tr');
            var children = selection.children();
            var query = children[0].innerHTML;
            var val = me.settings.textarea.html();
            me.settings.textarea.html(val+'<span>'+query+'</span>');
          });
        };
      },
      error: function(jqXHR, textStatus, errorThrown) {
        
      }
    });
  };
  
}(window.mgwiki = window.mgwiki || {},jQuery));

  
(function(mgwiki,$){
  
  mgwiki.cma.SearchMacroLib = function($el,settings){
    this.settings = settings || {};
    this.view = new mgwiki.cma.SearchMacroLibView(this,$el);
  }
  
}(window.mgwiki = window.mgwiki || {},jQuery));
(function(mgwiki,$){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.SearchResult = function($el,settings){
    var config = settings || {};
    this.ws_url = config.ws_url;
    this.proxy_url = config.proxy_url;
    this.targetCorpus = config.targetCorpus;
    this.data = config.data || {};
    this.query = config.query || {};
    this.app = config.app;
    this.from = 0;
    this.finished = false;
    this.total = getTotal(this.data);
    this.e_total = 0;
    this.max = mgwiki.cma.SearchService.max;
    this.view = new mgwiki.cma.SearchResultView(this);
    $el.prepend(this.view.$el);
    this.view.render();
    this.fetchResult(0);
  };

  function getTotal(data){
    return data.total;
  }

  mgwiki.cma.SearchResult.prototype.next = function(callbackSuccess,callbackFailure){
    this.goTo(Math.ceil(this.from/this.max)+1,callbackSuccess,callbackFailure);
  };

  mgwiki.cma.SearchResult.prototype.prev = function(callbackSuccess,callbackFailure){
    this.goTo(Math.ceil(this.from/this.max)-1,callbackSuccess,callbackFailure);
  };

  mgwiki.cma.SearchResult.prototype.fetchResult = function(index){
    var me = this;
    var interval = me.fetchInterval = setInterval(function(){fetchResult()}, 2000);
    
    function fetchResult(){
      var searchInfo = me.app.getSearchInfo(me.targetCorpus);
      var data = {
        query:me.query,
        max:me.max
      };
      if(searchInfo.cmaCorpus){
        data['cma-reloaded']=true;
      }
      $.ajax({
        type: 'POST', 
        url: me.proxy_url + searchInfo.url,
        data: data,
        dataType : 'json',
        success: function(data, textStatus, jqXHR) {
          console.log(data);
          if(data.error){
            me.app.errorLogger.log(data.message);
            clearInterval(interval);
          }else{
            if(me.from == 0){
              me.data = data;
            }
            if(data.total != me.total || data.e_total != me.e_total){
              me.total = getTotal(data);
              me.e_total = data.e_total;
              me.view.render();
            }
            if(data.status != 0){
              me.from = me.max*index;
              me.finished = true;
              me.view.render();
              clearInterval(interval);
              delete me.fetchInterval;
            }
          }
        },
        error: function(jqXHR, textStatus, errorThrown) {
          clearInterval(interval);
        }
      });
      
    }
  };

  mgwiki.cma.SearchResult.prototype.abortSearch = function(){
    var me = this;
    var searchInfo = me.app.getSearchInfo(me.targetCorpus);
    var data = {
      query:me.query,
      abort:true
    };
    if(searchInfo.cmaCorpus){
      data['cma-reloaded']=true;
    }
    $.ajax({
      type: 'POST', 
      url: me.proxy_url + searchInfo.url,
      data: data,
    });
  };

  mgwiki.cma.SearchResult.prototype.goTo = function(index,callbackSuccess,callbackFailure){
    var me = this;
    var searchInfo = me.app.getSearchInfo(me.targetCorpus);
    var data = {
      query:me.query,
      max:me.max,
      from:index*me.max
    };
    if(searchInfo.cmaCorpus){
      data['cma-reloaded']=true;
    }
    $.ajax({
      type: 'POST', 
      url: me.proxy_url + searchInfo.url,
      data: data,
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        console.log(data);
        if(data.error){
          me.app.errorLogger.log(data.message);
          callbackFailure.call();
        }else{
          me.from = me.max*index;
          me.data = data;
          me.view.render();
          callbackSuccess.call(me,data);  
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        callbackFailure.call(me,jqXHR);
      }
    });
  };


  mgwiki.cma.SearchResult.prototype.showGraph = function(sid,did,cname){
    var me = this;
    var divloader = '<div style="width:300px; height:150px;"><div class="cma-graph-loading"></div></div>';
    var width = $(window).width()-50;
    var height = $(window).height()-50;
    var box = (new depgraphlib.Box({closeButton:true,draggable:true})).defaultInit().open();
    
    
    box.setHeader(sid+"("+cname+"|"+did+")");
    $.ajax({
      type: 'POST', 
      url: me.proxy_url + "sc/"+cname+"/_graph",
      data: {
        sid:sid,
        depgraphsrc:true
      },
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        
        
        var uniqid = Date.now();
        var div = jQuery('<div><div id="graph-container-'+uniqid+'"></div></div>');
      
        box.resetContent();
        box.setContent(div);
        var depGraph = new depgraphlib.DepGraph(jQuery("#graph-container-"+uniqid),data,{"uid":uniqid,viewmode:"full",maxwidth:width});
        depGraph.wsurl = me.ws_url+"_graph";
        var viewer = depGraph.viewer;
        viewer.noBorders();

        var boxcontent = jQuery('.depgraphlib-box-content',box.object);
        var dgwidth = viewer.mainpanel.width();
        var dgheight = viewer.mainpanel.height();
        boxcontent.width(dgwidth+16);
        boxcontent.height(dgheight+16);
        boxcontent.resizable({
          stop:function(e,data){
            depGraph.setWidthLimitedViewMode(data.size.width-16,true);
            viewer.setHeight(data.size.height-46);
          }
        });

        var point = {x:width/2-dgwidth/2,y:height/2-dgheight/2};
        box.move(point);
      },
      error: function(jqXHR, textStatus, errorThrown) {
      }
    });
  };

  // save the query (call dpathlib forms)
  mgwiki.cma.SearchResult.prototype.save = function(){

  };

  mgwiki.cma.SearchResult.prototype.export = function(){
    var me = this;
    var searchInfo = me.app.getSearchInfo(me.targetCorpus);
    var data = {
      query:me.query,
      export:true
    };
    if(searchInfo.cmaCorpus){
      data['cma-reloaded']=true;
    }
    var postVal = {export:true,query:me.query};
    depgraphlib.windowOpenPost(data,me.proxy_url + searchInfo.url);
  };

  mgwiki.cma.SearchResult.prototype.destroy = function(){
    var me = this;
    if(me.fetchInterval){
      clearInterval(me.fetchInterval);
    }
    this.abortSearch();
    this.app.dismissResult(this);
  };
  
}(window.mgwiki = window.mgwiki || {},jQuery));
(function(mgwiki,$){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.SearchService = function(app,$el,settings){
    this.app = app;
    this.settings = settings;
    this.view = new mgwiki.cma.SearchServiceView(this,$el);
    this.init();
  };

  mgwiki.cma.SearchService.max = 20;

  mgwiki.cma.SearchService.prototype.init = function(){
    var me = this;
    this.corpusList = [];
    this.results = {};
    this.fetchCorpusList(function(){
      me.view.render();
      me.getTagset();
      //me.app.errorLogger.log("Les corpus EP, FrWiki et WkS sont en cours d\'indexation. Les résultats fournis pour les requêtes appliquées à ces corpus seront donc incomplets pour le moment.");
      me.searchlib = new mgwiki.cma.SearchLib({proxy_url:me.settings.proxy_url,user:me.settings.user,textarea:me.view.$el.find('.cma-search-input')});
      me.history = new mgwiki.cma.SearchHistory({ws_url:me.settings.proxy_url,macroLib:me.searchlib,textarea:me.view.$el.find('.cma-search-input')});
    },function(){
      me.app.errorLogger.log("error while loading corpus list");
    });
    window.onbeforeunload = function(e){
      for (var i in me.results) {
        for(var j in me.results[i]){
          if(!me.results[i][j].finished){
            me.results[i][j].abortSearch();
          }
        }
      }
    };
  };

  mgwiki.cma.SearchService.prototype.getFormat =function(){
    var formats = [];
    for(var i=0;i<this.corpusList.length;i++){
      if(this.corpusList[i].name == this.targetCorpus){
        formats = this.corpusList[i].formats;
        break;
      }
    }
    if(!formats){
      return "conll";
    }
    return formats[0];
  }


  mgwiki.cma.SearchService.prototype.getTagset = function(){
    var me = this;
    jQuery.ajax({
      type: 'POST', 
      url: me.settings.proxy_url+'sc/_tagset',
      data:{
        format:me.getFormat()
      },
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        if(data.name){
          me.tagset = data;
          me.props = [];
          for (var i in data.node) {
            me.props.push(i);
          };
          for (var i in data.edge) {
            me.props.push(i);
          };  
        }
        
      },
      error: function(jqXHR, textStatus, errorThrown) {
      }
    });
  };

  mgwiki.cma.SearchService.prototype.getSearchInfo = function(collectionName){
    var searchInfo = {};
    for(var i=0;i<this.corpusList.length;i++){
      if(this.corpusList[i].name == collectionName){
        var corpus = this.corpusList[i];
        if(corpus.cmaCorpus){
          searchInfo.url = "corpus/"+corpus.cid+"/_search";
          searchInfo.cmaCorpus = true;
        }else{
          searchInfo.url = "sc/"+collectionName+"/_search";
        }
      }
    }
    return searchInfo;
  };

  mgwiki.cma.SearchService.prototype.search = function(query,callbackSuccess,callbackFailure){
    var me = this;
    var searchInfo = this.getSearchInfo(me.targetCorpus);
    var data = {
      query:query,
      max:mgwiki.cma.SearchService.max
    };
    if(searchInfo.cmaCorpus){
      data['cma-reloaded']=true;
    }
    $.ajax({
      type: 'POST', 
      url: me.settings.proxy_url + searchInfo.url,
      data: data,
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        console.log(data);
        if(data.error){
          me.app.errorLogger.log(data.message);
          callbackFailure.call();
        }else{
          me.results[me.targetCorpus][query]=new mgwiki.cma.SearchResult(me.app.$el.find('.cma-results-container'),{app:me,query:query,ws_url:me.settings.ws_url,targetCorpus:me.targetCorpus,proxy_url:me.settings.proxy_url,data:data});
          //me.results[me.targetCorpus][query].view.render();
          callbackSuccess.call(me,data);  
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        callbackFailure.call(me,jqXHR);
      }
    });
  }

  mgwiki.cma.SearchService.prototype.dismissResult = function(result){
    delete this.results[result.targetCorpus][result.query];
  };


  mgwiki.cma.SearchService.prototype.fetchCorpusList = function(callbackSuccess,callbackFailure){
    var me = this;
    $.ajax({
      type: 'POST', 
      url: me.settings.proxy_url + "sc/_list",
      data: {
      },
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        me.corpusList = data;
        if(data.length){
          me.targetCorpus = data[0].name;
          for(var i = 0; i<data.length; i++){
            me.results[data[i].name] = me.results[data[i].name] || {};
          }
          callbackSuccess.call(me,data);
        }else{
          me.app.errorLogger.log("no corpus found!");
          callbackFailure.call();
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        callbackFailure.call(me,jqXHR);
      }
    });
  };

  

  
}(window.mgwiki = window.mgwiki || {},jQuery));

(function(mgwiki,$){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.SentenceCollection = function(app,$el,settings){
    this.app = app;
    this.view = new mgwiki.cma.SentenceCollectionView(this,$el);
    this.max = 5;
    this.from = 0;
  };

  mgwiki.cma.SentenceCollection.prototype.load = function(item){
    var me = this;
    this.collection = item;
    this.goTo(0);
  };

  mgwiki.cma.SentenceCollection.prototype.next = function(callbackSuccess,callbackFailure){
    this.goTo(Math.ceil(this.from/this.max)+1,callbackSuccess,callbackFailure);
  };

  mgwiki.cma.SentenceCollection.prototype.prev = function(callbackSuccess,callbackFailure){
    this.goTo(Math.ceil(this.from/this.max)-1,callbackSuccess,callbackFailure);
  };

  mgwiki.cma.SentenceCollection.prototype.goTo = function(index,callbackSuccess,callbackFailure){
    window.scrollTo(0,0);
    var me = this;
    var data = {
      max:me.max,
      from:index*me.max
    };
    if(this.hideVoted){
      data.hideVoted = true;
    }
    if(this.hideUnVoted){
      data.hideUnVoted = true;
    }
    $.ajax({
      type: 'POST', 
      url: me.app.settings.proxy_url + "sc/"+me.collection.name+"/_graph/_all",
      data: data,
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        me.graphs = [];
        for (var i = data.graphs.length - 1; i >= 0; i--) {
          data.graphs[i].sc = me.collection.name;
          me.graphs.push(new mgwiki.cma.Graph(me.app,null,data.graphs[i]));
        };
        me.from = me.max*index;
        me.total = data.total;
        me.view.render();
      },
      error: function(jqXHR, textStatus, errorThrown) {
        console.log("error while fetching corpus list");
      }
    });
  };
  
}(window.mgwiki = window.mgwiki || {},jQuery));
(function(mgwiki,$){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.CorpusListItemView = function(model,$el,settings){
    this.model = model;
    this.$el = $el || $('<div class="cma-corpus-item"></div>');      

  };

  mgwiki.cma.CorpusListItemView.prototype.render = function(){
    var me = this;
    this.$el.html(this.model.item.name);
    this.$el.on("click",function(){
      me.model.app.use(me.model.item);
    });
  }

  
}(window.mgwiki = window.mgwiki || {},jQuery));
(function(mgwiki,$){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.CorpusListView = function(model,$el,settings){
    this.model = model;
    this.$el = $el || jQuery('<div></div>');
  };

  mgwiki.cma.CorpusListView.prototype.render = function(){
    var me = this;
    this.$el.html(mgwiki.cma.CorpusListView.template);

    var $corpus = this.$el.find('.cma-available-corpus');

    var corpusOption = '<div>Select corpus : <select name="corpus">';
    for(var i=0;i<this.model.list.length;i++){
      var corpusInfo = this.model.list[i];
      var selected = '';
      if(this.model.targetCorpus == corpusInfo.name){
        selected = ' selected'
      }
      corpusOption += '<option value="'+i+'"'+selected+'>'+corpusInfo.name+'</option>';
    }
    corpusOption += '</select></div>';


    $corpus.append(corpusOption);

    $corpus.find('select').on('change',function(e){
      var corpus = e.target.value;
      me.model.app.use(me.model.list[corpus]);
    });

    var filter = $('<div><span style="margin-right:10px;">Filter:</span><span style="margin-right:10px;"><input name="voted" type="checkbox">Hide voted</span><span style="margin-right:10px;"><input name="unvoted" type="checkbox">Hide unvoted</span></div>');
    filter.find('input[name="voted"]').on('change',function(e){
      me.model.app.sentenceCollection.hideVoted = $(this).prop("checked");
      me.model.app.sentenceCollection.goTo(0);
    });
    filter.find('input[name="unvoted"]').on('change',function(e){
      me.model.app.sentenceCollection.hideUnVoted = $(this).prop("checked");
      me.model.app.sentenceCollection.goTo(0);
    });
    $corpus.append(filter);


    if(this.model.list.length>0){
      me.model.app.use(me.model.list[0]);
    }
  };

  mgwiki.cma.CorpusListView.template = '<div class="cma-available-corpus" style="border-bottom:1px solid black; padding-bottom:10px;"></div>';

  
}(window.mgwiki = window.mgwiki || {},jQuery));
(function(mgwiki,$){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.ErrorLoggerView = function(model,$el){
    this.model = model;
    this.$el = $el || $('<div></div>');
  };

  mgwiki.cma.ErrorLoggerView.prototype.render = function(){
    if(this.model.messages.length){
      this.$el.prepend(this.model.messages[this.model.messages.length-1]);
    }
  };


  

  
}(window.mgwiki = window.mgwiki || {},jQuery));
(function(mgwiki,$){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.GraphView = function(model,$el){
    this.model = model;
    this.$el = $el || $('<div></div>');
  };

  mgwiki.cma.GraphView.prototype.render = function(){
    var me = this;
    this.$el.html('<div class="cma-graph-loading"></div>');
    setTimeout(function(){me.model.fetchData(function(){me.delayedRender();},function(text){alert(text);});},500);
  };

  mgwiki.cma.GraphView.prototype.delayedRender = function(){
    var me = this;
    var mastercontainer = $('<div><div class="cma-graph-info" style="margin-top:15px; margin-bottom:5px;"></div><table style="margin-left:auto;margin-right:auto; text-align:center;"><tr></tr></table></div>');
    mastercontainer.find('tr').append(this.voteView());
    var td = $('<td style="text-align:center;"></td>');
    var graphcontainer = $('<div class="graph-container-'+this.model.settings.id+'"></div>');
    td.append(graphcontainer);
    mastercontainer.find('tr').append(td);
    if(me.model.settings.notes && me.model.settings.notes.length>0){
      mastercontainer.find('tr').append(this.noteView());
    }
    if(this.model.settings.eid){
      mastercontainer.find('.cma-graph-info').append("<span style=\"margin-right:10px\">ID:"+this.model.settings.eid+"</span>");
    }
    if(this.model.settings.sentence){
      mastercontainer.find('.cma-graph-info').append("<span>| "+this.model.settings.sentence+"</span>");
    }
    this.$el.html(mastercontainer);
    var depGraph = this.depgraph =new depgraphlib.DepGraph(graphcontainer,this.model.settings.data,{"uid":this.model.settings.id,viewmode:"fit"});
    depGraph.wsurl = me.model.app.settings.ws_url+"/graph";
    depGraph.sentence = me.model.settings.sentence;
    var viewer = depGraph.viewer;
    viewer.addToolbarItem({name:'add-note',callback:function(){me.addNote(this);},style:'add-note','static':true});
    viewer.getToolbarItem('export').button.unbind('click');
    viewer.getToolbarItem('export').button.click(function(){me.model.export();});
    viewer.noBorders();
  };

  mgwiki.cma.GraphView.prototype.noteView = function(){
    var me = this;
    var uid = me.model.settings.id;
    var notesWidget = '<td style="text-align:top;"><div id="notes-'+me.model.settings.id+'"><div class="graph-note"></div></div></td>';
    var jnotesWidget = $(notesWidget);
    var container = jnotesWidget.find("#notes-"+uid);
    container.on("click",function(event){
      if($(event.target).hasClass("note-delete")){
        return;
      }
      var content = $('.notes-content',this);
      content.toggle();
    });
    var div = '<div class="notes-content hidden"><table title="comments">';
    for(var i=0;i<me.model.settings.notes.length;i++){
      div += "<tr><td>["+me.model.settings.notes[i].user+"] : "+me.model.settings.notes[i].data;
      if(me.model.settings.notes[i].user == me.model.app.settings.user.name){
        div += "<div id=\""+i+"note-"+uid+"\" cid=\""+i+"\" class=\"note-delete\"></div>";
      }
      div += "</td></tr>";
    }
    div += "</tr></table>";
    container.append(div);
    jnotesWidget.find('.note-delete').click(function(event){
      var note = this;
      me.model.removeNote(this);
    });
    return jnotesWidget;
  }


  mgwiki.cma.GraphView.prototype.addNote = function(divbutton){
    var me = this;
    var coords = divbutton.getBoundingClientRect();
    var point = {x:coords.left,y:coords.top + coords.height + 2};
    var form = '<form id="note-form-'+me.depgraph.options.uid+'">';
    form += '<textarea id="note-text-'+me.depgraph.options.uid+'" name="note" rows="4" cols="50"></textarea><br>';
    form += '<input id="note-submit-'+me.depgraph.options.uid+'" name="note-submit" type="button" value="Add note">';
    form +='</form>';
    var div ='<div></div>';
    div = jQuery(div);
    div.append(form);
    div.find("#note-submit-"+me.depgraph.options.uid).on("click",function(){me.model.submitNote();});
    me.depgraph.viewer.createBox({closeButton:true,position:point,autodestroy:false,forceToolbar:true}).setContent(div).setHeader("Add note").open();
  };

  mgwiki.cma.GraphView.prototype.voteView = function(){
    var me = this;
    var totals = [0,0];
    var classUp = "d3js-vote-up";
    var classDown = "d3js-vote-down";
    for(var i =0;i<this.model.settings.votes.length;i++){
      if(this.model.settings.votes[i].val>0){
        totals[0]++;
      }else if(this.model.settings.votes[i].val<0){
        totals[1]++;
      }
      if(this.model.settings.votes[i].name == this.model.app.settings.user.name){
        if(this.model.settings.votes[i].val>0){
          classUp += " d3js-voted-up";
        }else if(this.model.settings.votes[i].val<0){
          classDown += " d3js-voted-down";
        }
      }
    }

    var uid = this.model.settings.id;
    var td = '<td  valign="top" width="40px">'+
      '<div id="graph-sidebar0-'+uid+'" class="graph-sidebar">'+
        '<ul>'+
          '<li>'+
            '<div id="graph-upvote-'+uid+'" title="Validate" class="'+classUp+'">'+totals[0]+'</div>'+
          '</li>'+
          '<li>'+
            '<div id="graph-downvote-'+uid+'" title="Invalidate" class="'+classDown+'">'+totals[1]+'</div>'+
          '</li>'+
        '</ul>'+
      '</div>' +
    '</td>';
    var jtd = $(td);
    jtd.find('#graph-upvote-'+uid).click(function(){
      var upvote = $(this);
      if(!upvote.hasClass("d3js-voted-up")){
        upvote.html(parseInt(upvote.html())+1);
        upvote.addClass("d3js-voted-up");
        var downvote = $(this).parents('td').find("#graph-downvote-"+uid);
        if(downvote.hasClass("d3js-voted-down")){
          downvote.removeClass("d3js-voted-down");
          downvote.html(parseInt(upvote.html())-1);  
        }
        
        me.model.vote(1);
      }else{
        upvote.html(parseInt(upvote.html())-1);
        upvote.removeClass("d3js-voted-up");
        
        me.model.vote(0);
      }
    });
    jtd.find('#graph-downvote-'+uid).click(function(){
      var downvote = $(this);
      if(!downvote.hasClass("d3js-voted-down")){
        downvote.html(parseInt(downvote.html())+1);
        downvote.addClass("d3js-voted-down");
        var upvote = $(this).parents('td').find("#graph-upvote-"+uid);
        if(upvote.hasClass("d3js-voted-up")){
          upvote.removeClass("d3js-voted-up");
          upvote.html(parseInt(upvote.html())-1);  
        }
        me.model.vote(-1);
      }else{
        downvote.html(parseInt(downvote.html())-1);
        downvote.removeClass("d3js-voted-down");
        me.model.vote(0);
      }
    });
    return jtd;
  }

  mgwiki.cma.GraphView.prototype.sentenceView = function(){
  };

  mgwiki.cma.GraphView.template = '';
  
}(window.mgwiki = window.mgwiki || {},jQuery));

(function(mgwiki,$){
  

  mgwiki.cma.SearchMacroLibView = function(model,$el){
    this.model = model;
    this.$el = $el || $('<div></div>');
    this.initiated = false;
  };


  mgwiki.cma.SearchMacroLibView.template = '<div class="cma-macrolib-container">'+
    '<div class="cma-macrolib-header">'+
      // title
    '</div>'+
    '<div class="cma-macrolib-content">'+
    '</div>'+
    '<div class="cma-macrolib-actions">'+
      '<span class="cma-macrolib-insert"></span>'+
    '</div>'+
  '</div>';
  
}(window.mgwiki = window.mgwiki || {},jQuery));
(function(mgwiki,$){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.SearchResultView = function(model,$el){
    this.model = model;
    this.$el = $el || $('<div class="cma-result-container"></div>');
    this.initiated = false;
  };

  mgwiki.cma.SearchResultView.paginationMax = 10;

  mgwiki.cma.SearchResultView.prototype.render = function(){
    var me = this;

    if(!this.initiated){
      me.$el.hide();  
    }
    
    me.$el.html(mgwiki.cma.SearchResultView.template); // does this practice delete all previous dom bidings / data?

    if(me.model.finished){
      me.$el.find('.cma-result-total').html("<b>Total :</b>"+me.model.total);
      me.$el.find('.cma-result-export').click(function(){
        me.model.export();
      });
    }else{
      me.$el.find('.cma-result-total').html('<b>Total (estimation) : </b>'+me.model.e_total+'<div class="cma-throbber"></div>');
      me.$el.find('.cma-result-export').hide();
    }
    

    this.$el.find('.cma-result-query').html("<b>Query : </b>"+this.model.query);

    this.$el.find('.dismiss-message-icon').click(function(){
      me.model.destroy();
      me.$el.fadeOut(400,function(){
        me.$el.remove();  
      });
    });

    if(this.model.data){
      var table = new mgwiki.cma.ui.Table({
        headers:[
          {title:"ID",nameid:"sid"},
          {title:"Sentence",nameid:"sentence"},
          {title:"Document",nameid:"did"}
        ],
        style:{
          tr0:'cma-table-even cma-table-item',
          tr1:'cma-table-odd cma-table-item'
        }
      });
      me.resultsTable = table;
      me.$el.find('.cma-result-data').append(table.view); // here can go a styling effect of table fading out/in
      table.update(processResults(this.model.data));
      table.view.find('tr').click(function(e){
        var sid = this.cells[0].textContent;
        var did = this.cells[2].textContent;
        if(sid=="ID"){
          return;
        }
        me.model.showGraph(sid,did,me.model.targetCorpus);
      });

      var n = Math.ceil(me.model.total/me.model.max);
      var i = Math.floor(this.model.from / this.model.max);
      

      

      if(i>0){
        var startButton = new mgwiki.cma.ui.CallbackButton("Start",{}); // add styles to callback buttons ...
        me.$el.find('.cma-pagination-start').append(startButton.view); // here if i use .html() are callbacks still preserved?
        startButton.init(function(){
          var button = this;
          me.model.goTo(0,function(){button.success();},function(){button.error();});
        },this);

        var prevButton = new mgwiki.cma.ui.CallbackButton("Prev",{}); // add styles to callback buttons ...
        me.$el.find('.cma-pagination-prev').append(prevButton.view); // here if i use .html() are callbacks still preserved?
        prevButton.init(function(){
          var button = this;
          me.model.prev(function(){button.success();},function(){button.error();});
        },this);  
      }

      if(i<n-1){
        if(me.model.finished){
          var endButton = new mgwiki.cma.ui.CallbackButton("End",{}); // add styles to callback buttons ...
          me.$el.find('.cma-pagination-end').append(endButton.view); // here if i use .html() are callbacks still preserved?
          endButton.init(function(){
            var button = this;
            me.model.goTo(n-1,function(){button.success();},function(){button.error();});
          },this);
        }

        var nextButton = new mgwiki.cma.ui.CallbackButton("Next",{}); // add styles to callback buttons ...
        me.$el.find('.cma-pagination-next').append(nextButton.view); // here if i use .html() are callbacks still preserved?
        nextButton.init(function(){
          var button = this;
          me.model.next(function(){button.success();},function(){button.error();});
        },this);
      }

      var k = mgwiki.cma.SearchResultView.paginationMax/2;
      var kp = 0;
      var kn = 0;
      var a = i-k;
      if(a<0){
        kn = -a;
      }
      var b = i+k;
      if(b>=n){
        kp = n-b;
      }
      var ks = (a+kp>0)?(a+kp):0;
      var ke = (b+kn<n)?(b+kn):(n-1);
      for(var j=ks;j<=ke;j++){
        if(j==i){
          me.$el.find('.cma-pagination-pages').append('<span style="margin-left:10px; margin-right:10px;">'+i+'</span>');
          continue;
        }
        var pagginButton = new mgwiki.cma.ui.CallbackButton(j,{}); // add styles to callback buttons ...
        pagginButton.view.attr('index',j);
        me.$el.find('.cma-pagination-pages').append(pagginButton.view); // here if i use .html() are callbacks still preserved?
        pagginButton.init(function(){
          var button = this;
          me.model.goTo(button.view.attr('index'),function(){button.success();},function(){button.error();});
        },this);  
      }
      
      // callback for save/export/dismiss buttons
      
    }else{
      this.$el.find('cma-result-pagination').hide();
    }

    if(!this.initiated){
      me.$el.slideDown(300);  
      this.initiated = true;
    }

  };



  function processResults(data){
    if(data && data.hits){
      var hits;
      if(data.dpath){
        hits = data.hits;
      }else{
        hits = data.hits.hits;
      }
      for(var i in hits){
        var hit = hits[i];
        hit.sid = hit._source.sid;
        hit.did = hit._source.did;
        if(hit._source.sentence){
          hit.sentence = hit._source.sentence.substr(0,100);  
        }else{
          hit.sentence = "error : could not find sentence content";
        }
        
        delete hit._index; 
        delete hit._type; 
        delete hit._id; 
      }  
    }
    return hits;
  }

  mgwiki.cma.SearchResultView.template = '<div class="cma-result-header"><div class="dismiss-message-icon"></div><div class="cma-result-export"></div><span class="cma-result-save"></span><div class="cma-result-query"></div><div class="cma-result-total"></div></div>'+
    '<div class="cma-result-data"></div>'+
    '<div class="cma-result-pagination"><span class="cma-pagination-start"></span><span class="cma-pagination-prev"></span><span class="cma-pagination-pages"></span><span class="cma-pagination-next"></span><span class="cma-pagination-end"></span></div>';

  
}(window.mgwiki = window.mgwiki || {},jQuery));
(function(mgwiki){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.SearchServiceView = function(model,$el,settings){
    this.model = model;
    this.$el = $el || jQuery('<div></div>');
    this.$el.append('<div style="width:100%; height:150px; text-align:top;">Loading Corpus, please wait.<div class="cma-graph-loading"></div></div>');
  };

  mgwiki.cma.SearchServiceView.prototype.render = function(){
    var me = this;
    this.$el.html(mgwiki.cma.SearchServiceView.template);

    var $options = this.$el.find('.cma-search-options');

    var corpusOption = 'Select corpus : <select name="type">';
    for(var i=0;i<this.model.corpusList.length;i++){
      var corpusInfo = this.model.corpusList[i];
      var selected = '';
      if(this.model.targetCorpus == corpusInfo.name){
        selected = ' selected'
      }
      corpusOption += '<option value="'+corpusInfo.name+'"'+selected+'>'+corpusInfo.name+'</option>';
    }
    corpusOption += '</select>';


    $options.append(corpusOption);

    $options.find('select').on('change',function(e){
      var corpus = e.target.value;
      me.model.targetCorpus = e.target.value;
      me.model.getTagset();
    });

    var helpDiv = 'Le format de requête est de la forme suivante :<br>'+
      'nodes|edges|root ((target|source|next|prev|out|in)* ([@attr="value"])*)+';
    var foldableDiv = new mgwiki.cma.ui.FoldableDiv(helpDiv);
    foldableDiv.setTitle('Help');
    $options.after(foldableDiv.view);

    var strategies = this.autocompleteStrategies();
    var textarea = this.textarea = this.$el.find('.cma-search-input');
    var parent = jQuery(this.$el);
    textarea.width(parent.width()-70);
    textarea.textcomplete(strategies);

    var submitButton = new mgwiki.cma.ui.CallbackButton("Submit search",{});
    $options.append(submitButton.view);
    submitButton.init(function(data){
      var button = this;
      var textarea = data.$el.find('.cma-search-input');
      var select = data.$el.find('.cma-search-options').find('select');
      me.model.targetCorpus = select[0].options[select[0].selectedIndex].value;
      me.model.search(textarea[0].textContent,function(){button.success();},function(){button.error();});
    },this);

    /*this.macroLib = new cma.toolbox.SearchLib(textarea);
    this.history = new cma.toolbox.SearchHistory(this);
*/
    jQuery('.cma-search-insert-macro',this.view).click(function(){
      me.model.searchlib.show();
    });

    jQuery('.cma-search-history',this.view).click(function(){
      me.model.history.show();
    });

    jQuery('.cma-search-visual',this.view).click(function(){
      me.visualSignatureForm();
    });

    jQuery('.cma-search-save-macro',this.view).click(function(){
      var textarea = me.$el.find('.cma-search-input');
      var val = textarea[0].textContent;
      me.model.searchlib.elementForm({query:val});
    });

  };


  mgwiki.cma.SearchServiceView.prototype.autocompleteStrategies = function(){
    var me = this;
    var strategies = [];
    strategies.push({
      match: /@(\w+)\s*=(")?(\w*)$/,
      index:3,
      search: function (term, callback) {
        var prop = this.currentMatch[1];
        var propVal = [];
        if(me.model.tagset && me.model.tagset.edge[prop]){
          propVal = me.model.tagset.edge[prop];
        }else if(me.model.tagset && me.model.tagset.node[prop]){
          propVal = me.model.tagset.node[prop];
        }
        
        callback(jQuery.map(propVal, function (element) {
                return element.indexOf(term) === 0 ? element : null;
            }));
      },
      replace: function (value) {
        return '@$1="' + value +'"';
      },
      cache: true,
      maxCount:99
    });
    strategies.push({
      match: /(\$)(\w*)$/,
      search: function (term, callback) {
        var macros = me.model.searchlib.macros;
        callback(jQuery.map(macros, function (element) {
                return element.indexOf(term) === 0 ? element : null;
            }));
      },
      replace: function (value) {
        return '$1' + value + ' ';
      },
      cache: true,
      maxCount:99
    });
    strategies.push({
      match: /(@)(\w*)$/,
      search: function (term, callback) {
        var props = me.model.props || [];
        callback(jQuery.map(props, function (element) {
                return element.indexOf(term) === 0 ? element : null;
            }));
      },
      replace: function (value) {
        return '$1' + value ;
      },
      cache: true,
      maxCount:99
    });

    return strategies;
  };

  mgwiki.cma.SearchServiceView.prototype.visualSignatureForm = function(){
    var me = this;
    var width = jQuery('body').width();
    var height = jQuery('body').height();
    var offset = jQuery('body').offset();

    var uniqid = Date.now();
    var div = jQuery('<div><div id="graph-container-'+uniqid+'"></div></div>');
  
    // Box || r.window => base sentence
    // data creation (+datamodel)
    
    var sentence = prompt("Please enter a sentence.","");

    if (sentence==null || sentence == ""){
      sentence =".";
    }

    var data = {
      graph:{
        words:[]
      }
    };


    var words = sentence.split(/\s+/);
    for (var i = 0 ; i < words.length ; i++) {
      data.graph.words.push({id:i+1,label:words[i]});
    };
    

    var box = new depgraphlib.Box({closeButton:true,draggable:true}).setContent(div).open();
    box.setHeader("Visual Query (over highlighted graph parts)");
    var depGraph = new depgraphlib.DepGraph(jQuery("#graph-container-"+uniqid),data,{"uid":uniqid,viewmode:"full"});
    var viewer = depGraph.viewer;
    depGraph.wsurl = this.model.settings.ws_url+"graph";
    if(depGraph.options.viewmode == 'full'){
      viewer.noBorders();
    }

    var boxcontent = jQuery('.depgraphlib-box-content',box.object);
    var dgwidth = viewer.mainpanel.width();
    var dgheight = viewer.mainpanel.height();
    boxcontent.width(dgwidth+16);
    boxcontent.height(dgheight+16);
    boxcontent.resizable({
      stop:function(e,data){
        depGraph.setWidthLimitedViewMode(data.size.width-16,true);
        viewer.setHeight(data.size.height-46);
      }
    });

    var point = {x:offset.left+width/2-dgwidth/2,y:50};
    box.move(point);

    depGraph.sentenceLink = '#';

    depGraph.sentence = 'sentence content here';
    depGraph.dataFormat = 'json';

    var pos = me.model.tagset.node["pos"] || me.model.tagset.node["cat"];
    var dataModel = {
      words:[
      {name:"label",view:'label'},
      {name:"lemma",view:"sublabel/0"},
      {name:"pos",values:pos,view:"sublabel/1"}
      ],
      links:[
      {name:"label",values:me.model.tagset.edge["label"],view:"label"},
      ],
    };

    depGraph.editObject.setDataModel(dataModel);
    

    viewer.setFixedToolbar();
    var formatTmp = (me.model.tagset.node["pos"])?"conll":"depxml";
    viewer.addToolbarItem({name:'export_signature',callback:function(){
      var ddata = depGraph.cleanData();
      jQuery.ajax({
        type: 'POST', 
        url: me.model.settings.ws_url+"graph",
        data: {
          action:'_getSig',
          data:ddata,
          format: formatTmp
        },
        dataType : 'text',
        success: function(data, textStatus, jqXHR) {
          console.log(data);
          var prev = me.textarea.html();
          me.textarea.html(prev + data);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          alert(textStatus);
        }
      });
    },tooltip:'Export Signature',style:'mgwiki-export','static':true});

    depGraph.editObject.setEditMode('default',false);
  };

  mgwiki.cma.SearchServiceView.template = '<table><tbody><tr><td><div class="cma-search-input" contenteditable=true></div><div class="cma-search-options"></div></td><td style="vertical-align:top;"><div class="cma-search-tools"><div class="cma-search-insert-macro"></div><div class="cma-search-save-macro"></div><div class="cma-search-visual"></div><div class="cma-search-history"></div></div></td></tr></tbody></table>';
  
}(window.mgwiki = window.mgwiki || {}));
(function(mgwiki,$){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.SentenceCollectionView = function(model,$el){
    this.model = model;
    this.$el = $el || $('<div class="cma-sentencecollection-container"></div>');
    this.initiated = false;
  };

  mgwiki.cma.SentenceCollectionView.paginationMax = 10;

  mgwiki.cma.SentenceCollectionView.prototype.render = function(){
    var me = this;

    this.$el.html(mgwiki.cma.SentenceCollectionView.template);
    var container = this.$el.find('.cma-sentencecollection-items');

    for(var i = 0; i <me.model.graphs.length;i++){
      var graph = me.model.graphs[i];

      container.append(graph.view.$el);
      graph.view.render();

    }

    var n = Math.ceil(me.model.total/me.model.max);
    var i = Math.floor(this.model.from / this.model.max);


    if(i>0){
      var startButton = new mgwiki.cma.ui.CallbackButton("Start",{}); // add styles to callback buttons ...
      me.$el.find('.cma-pagination-start').append(startButton.view); // here if i use .html() are callbacks still preserved?
      startButton.init(function(){
        var button = this;
        me.model.goTo(0,function(){button.success();},function(){button.error();});
      },this);

      var prevButton = new mgwiki.cma.ui.CallbackButton("Prev",{}); // add styles to callback buttons ...
      me.$el.find('.cma-pagination-prev').append(prevButton.view); // here if i use .html() are callbacks still preserved?
      prevButton.init(function(){
        var button = this;
        me.model.prev(function(){button.success();},function(){button.error();});
      },this);  
    }

    if(i<n-1){
      var endButton = new mgwiki.cma.ui.CallbackButton("End",{}); // add styles to callback buttons ...
      me.$el.find('.cma-pagination-end').append(endButton.view); // here if i use .html() are callbacks still preserved?
      endButton.init(function(){
        var button = this;
        me.model.goTo(n-1,function(){button.success();},function(){button.error();});
      },this);

      var nextButton = new mgwiki.cma.ui.CallbackButton("Next",{}); // add styles to callback buttons ...
      me.$el.find('.cma-pagination-next').append(nextButton.view); // here if i use .html() are callbacks still preserved?
      nextButton.init(function(){
        var button = this;
        me.model.next(function(){button.success();},function(){button.error();});
      },this);
    }

    var k = Math.floor(mgwiki.cma.SentenceCollectionView.paginationMax/2);
    var kp = 0;
    var kn = 0;
    var a = i-k;
    if(a<0){
      kn = -a;
    }
    var b = i+k;
    if(b>=n){
      kp = n-b;
    }
    var ks = (a+kp>0)?(a+kp):0;
    var ke = (b+kn<n)?(b+kn):(n-1);
    for(var j=ks;j<=ke;j++){
      if(j==i){
        me.$el.find('.cma-pagination-pages').append('<span style="margin-left:10px; margin-right:10px;">'+i+'</span>');
        continue;
      }
      var pagginButton = new mgwiki.cma.ui.CallbackButton(j,{}); // add styles to callback buttons ...
      pagginButton.view.attr('index',j);
      me.$el.find('.cma-pagination-pages').append(pagginButton.view); // here if i use .html() are callbacks still preserved?
      pagginButton.init(function(){
        var button = this;
        me.model.goTo(button.view.attr('index'),function(){button.success();},function(){button.error();});
      },this);  
    }
    
  };

  mgwiki.cma.SentenceCollectionView.template = '<div class="cma-sentencecollection-items"></div>'+
    '<div class="cma-result-pagination"><span class="cma-pagination-start"></span><span class="cma-pagination-prev"></span><span class="cma-pagination-pages"></span><span class="cma-pagination-next"></span><span class="cma-pagination-end"></span></div>';

  
}(window.mgwiki = window.mgwiki || {},jQuery));
(function(mgwiki){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.templates = {};

  
}(window.mgwiki = window.mgwiki || {}));