(function(mgwiki,$){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.CorpusList = function(app,$el,settings){
    this.app = app;
    this.list = [];
    this.view = new mgwiki.cma.CorpusListView(this,$el);
    this.init();
  };

  mgwiki.cma.CorpusList.prototype.init = function(){
    var me = this;
    $.ajax({
      type: 'POST', 
      url: me.app.settings.proxy_url + "sc/_list",
      data: {
        namespace:"passage_group"
      },
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        for(var i=0;i<data.length;i++){
          me.list.push(data[i]);
        }
        me.view.render();
      },
      error: function(jqXHR, textStatus, errorThrown) {
        console.log("error while fetching corpus list");
      }
    });
  };

  
}(window.mgwiki = window.mgwiki || {},jQuery));