(function(cma){
  
  /*******************************************************************/
  /*                              TODO                               */ 
  /*******************************************************************/
  /*
    
    - add on app close (=route out of app range, page refresh) : unauthenticate user
    - add open mgwiki in new window button (a coté de home)
    






  */
  /*******************************************************************/
  
  cma.App = function(container,options){
    // set the singleton reference
    cma.TheApp = this;

    // options dispatching
    // refactor with no dispatching
    this.ws_url = options.ws_url;
    this.options = options;
    this.options.defaultLang = this.options.defaultLang || "fr";
    this.exit_url = options.exit_url;

    // set up the app layout
    this.view = jQuery(cma.template.main);

    // inserting the view in dom
    if(!container){
      container = jQuery('<div id="cma-overlay"></div>');
      jQuery("body").append(container);
    }
    container.append(this.view);
    
    // logic init
    this.init();

    // modules
    this.mgwiki_sentences = new cma.toolbox.MgwikiSentences();
    
  };
  
  // history navigation
  cma.App.prototype.routeHistory = [];
  
  // logic init
  cma.App.prototype.init = function(){
    var me = this;

    jQuery.ajax({
      type: 'GET', 
      url: cma.TheApp.ws_url+'auth',
      data: {},
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        me.authkey = data.authkey;
        me.user = data.user;
        console.log(me.user);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        alert(textStatus);
      }
    });

    // init view parameters (depends on screen size, etc..)
    this.initView();

    // init web socket / authentication user
   this.websocket_listeners={
      "onopen":{},
      "onmessage":{},
      "onclose":{},
      "onerror":{}
    };
    this.initWebSocket(this.options.websocket_url);
    
    // navigation callback
    jQuery(window).on("hashchange",function(e){
      me.go(location.hash);
    });
    jQuery(window).hashchange();
  };

  cma.App.prototype.initWebSocket = function(url){
    var me = this;
    if(WebSocket == undefined){
      this.websocket_enabled = false;
      return;
    }else{
      this.websocket_enabled = true;
    }

    // request authentication by mgwiki first
    // mgwiki send a secret key to the ws server to accept the ws connexion
    try{
       this.socket = new WebSocket("ws://"+url);
    }catch(err){
      console.log(err);
      return;
    }
   
    jQuery.ajax({
      type: 'GET', 
      url: cma.TheApp.ws_url+'auth',
      data: {},
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        me.authkey = data.authkey;
        me.user = data.user;

        me.websocket_listeners['onopen']['authenticate'] = function(){
          var message = {authkey:cma.TheApp.authkey};
          cma.TheApp.socket.send(JSON.stringify(message));
        };
          

        me.socket.onopen = function() { me.websocketListenersCall("onopen"); };
        me.socket.onmessage = function (e) { 
          me.websocketListenersCall("onmessage",e.data); 
        };
        me.socket.onclose = function() { me.websocketListenersCall("onclose");};
        me.socket.onerror = function() { me.websocketListenersCall("onerror");};

      },
      error: function(jqXHR, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
    

    
  };

  cma.App.prototype.websocketListenersCall = function(theevent,data){
    for(listener in this.websocket_listeners[theevent]){
      if(this.websocket_listeners[theevent][listener] != null){
        this.websocket_listeners[theevent][listener].call(null,data);
      }
    }
  }

  cma.App.prototype.websocketAddListener = function(theevent,name,callback){
    this.websocket_listeners[theevent][name] = callback;
  }
  

  cma.App.prototype.initView = function(){
    var container = jQuery('#cma-app-container');
    var height = jQuery(window).height() - this.view.offset().top; 
    var width = this.view.width();
    var appmainview = cma.ui.get("cma-app");
    var paddingtop = 10;
    var paddingleft = 20;
    appmainview.height(height-130-paddingtop);
    appmainview.width(width-50-paddingleft);
    this.view.height(height);
    container.width(width-50);
    container.height(height-50);
    this.ajaxLoader(true);
    this.initHeaderMenu();

  };

  /**
   * @function help
   * @description launch IntroJS interactive help
   * @memberOf cma.App
   */
  cma.App.prototype.help = function(){
    var me = this;
    var helpType = 0; // default main page help
    var path = location.hash.trim().split("/");
    if(path[0]=="#corpus"){
      if(path[2]=="view"){
        helpType = 2; // document help page
      }else{
        helpType = 1; // corpus help page
      }
    }
    
    var intro = introJs();
    if(helpType==0){
      var uploadForm = jQuery('#corpusUploadForm');
      uploadForm = uploadForm[0];
      intro.setOptions({
        steps:[
        {
          intro:"<h1>Bienvenue!</h1>"
        },
        {
          intro:"Ici vous pouvez uploader votre corpus",
          element:uploadForm
        },
        {
          intro:"Les corpus sont ensuite listés ici",
          element:document.querySelector("#corpus-list-view")
        }
        ]
      });

    }else if(helpType==1){
      intro.setOptions({
        steps:[
        {
          intro:"Vous retrouvez l'ensemble des documents chargés pour ce corpus ici. En cliquant sur un document vous pouvez accéder à une visualisation des résultats par document.",
          element:document.querySelector(".cma-documents-tree")
        },
        {
          intro:"Cet section fournit les actions possible ou l'état du traitement de corpus.",
          element:me.view[0].querySelector('.cma-toolboxitem-container[name="corpus-process"]')
        },
        {
          intro:"Ici vous pouvez effectuer une recherche dans le corpus.",
          element:me.view[0].querySelector('.cma-toolboxitem-container[name="document-search"]')
        },
        {
          intro:"Ici se trouve le rapport du processus de traitement de corpus.",
          element:me.view[0].querySelector('.cma-toolboxitem-container[name="corpus-report"]')
        },
        {
          intro:"Ici vous pouvez modifier les permissions accordées aux utilisateurs.",
          element:me.view[0].querySelector('.cma-toolboxitem-container[name="corpus-permissions"]')
        },
        {
          intro:"Ici se trouve le log des différentes taches constituant le processus de traitement de corpus.",
          element:me.view[0].querySelector('.cma-toolboxitem-container[name="corpus-log"]')
        },
        {
          intro:"Ici se trouve la liste des phrases d'exemples de MGWiki.",
          element:me.view[0].querySelector('.cma-toolboxitem-container[name="mgwiki_sentences"]')
        },
        {
          intro:"Ici se trouvent les fonctions pour télécharger les résultats ou supprimer le corpus.",
          element:me.view[0].querySelector('.cma-toolboxitem-container[name="corpus-misc-actions"]')
        }
        ]
      });
    }else if(helpType==2){
      intro.setOptions({
        steps:[
        {
          intro:"Ici se trouve le texte original. Les phrases analysées (noir et bleu pour une analyse partielle) sont cliquable et ouvre une fenêtre de visualisation du graphe associé à la phrase. Il est aussi possible de sélectionner seulement un fragment de phrase.",
          element:me.view[0].querySelector('.cma-toolboxitem-container[name="corpus-process"]')
        },
        {
          intro:"Ici vous pouvez effectuer une recherche dans ce document.",
          element:me.view[0].querySelector('.cma-toolboxitem-container[name="document-search"]')
        },
        {
          intro:"Vous pouvez ici si vous avez les droits activer le mode édition qui permet d'éditer les sorties d'analyse à partir de l'interface de visualisation des phrases.",
          element:me.view[0].querySelector('.document-edition"]')
        }
        ]
      });
    }
    intro.start();
  };
  
  cma.App.prototype.initHeaderMenu = function(){
    var me = this;
    cma.ui.get("cma-quit").attr("href",this.exit_url);
    cma.ui.get("cma-feedback").click(function(){
      cma.TheApp.feedBackForm();
    });
    cma.ui.get("cma-help").click(function(){
      me.help();
    });
    var menuBox = cma.ui.get("cma-header-menu");
    var menu = '<div id="cma-back" style="display:none;" onclick="cma.TheApp.goBack();"></div><div id="cma-home" onclick="cma.TheApp.goTo(\'\')"></div><div id="cma-mgwiki-link" onclick="window.open(cma.TheApp.options.exit_url);"></div>';
    menuBox.html(menu);
  };
  
  cma.App.prototype.goBack = function(){
    this.routeHistory.pop();
    if(this.routeHistory.length==1){
      cma.ui.get("cma-back").hide();
    }
    this.goTo(this.routeHistory.pop());
  };
  
  cma.App.prototype.goTo = function(route){
    if(location.hash!=route){
      cma.TheApp.ajaxLoader(true);
    }
    location.hash=route;
  };
  
  cma.App.prototype.go = function(route){
    var backbutton = cma.ui.get("cma-back");
    if(backbutton.is(":hidden") && this.routeHistory.length > 0){
      backbutton.show();
    }
    
    if(this.routeHistory[this.routeHistory.length-1] != route){
      this.routeHistory.push(route);
    }

    cma.TheApp.ajaxLoader(true);

        
    var path = route.trim().split("/");
    if(!path[0]){
      cma.AppView.setTemplate(cma.template.upload);
      if(this.options.user.uid == 0){
        jQuery('.corpusUpload').hide();
      }
      cma.AppView.setTitle('<h1>Corpus Management Application</h1>');

      var helpUpload = new cma.ui.FoldableDiv(cma.text.get("uploadHelp"));
      helpUpload.setTitle('Infos');
      cma.ui.get('cma-upload-title').after(helpUpload.view);

      this.corpus_list_view = new cma.CorpusListView('corpus-list-view');
      this.corpus_list_view.refreshData();

      this.corpusUpload = new cma.corpusUpload();
      this.corpusUpload.settings.scriptPath = this.ws_url+"upload";
      this.corpusUpload.settings.corpusListView = this.corpus_list_view; 

      cma.ui.get('cma-app').removeClass('hidden');

      this.corpus = null;

    }
    else if(path[0]=="#corpus"){
      var cid = path[1];
      var action = path[2];
      if(cid !== null){
        if(action=="delete"){
          this.corpus = new cma.Corpus(cid);
          this.routeHistory.pop();
          this.corpus.callAndDo('delete',{},function(data){
            console.log(data);
          });
          this.goTo("");
          return;
        }
        if(!this.corpus || this.corpus.cid != cid){
          console.log("loading corpus "+cid);
          this.corpus = new cma.Corpus(cid);
          this.corpus.init(function (){cma.TheApp.go(route);});
          return;
        }
      }else{
        this.page404(route);
      }
      
      if(action == "process"){
        this.goBack();
      }else if(action == "delete"){
        this.routeHistory.pop();
        this.corpus.callAndDo('delete',{},function(data){
          console.log(data);
        });
        this.goTo("");
      }else if(action == "download"){
        this.corpus.callAndDo('download',{},function(data){
          window.open(data.url);
        });
        this.goBack();
      }else if(action == "view"){
        var me = this;
        var file = me.corpus.paths[path[3]].filepath;
        me.corpus.displayDocument(path[3],file);
      }else{
        this.corpus.initView();
      }
      
      
    }else{
      this.goTo("");
    }
  
  };

  cma.App.prototype.page404 = function(){

  };

  cma.App.prototype.feedBackForm = function(){
    var width = cma.TheApp.view.width();
    var height = cma.TheApp.view.height();
    var offset = cma.TheApp.view.offset();
    var point = {x:offset.left+width/2,y:offset.top+height/2};
    var div = jQuery('<div><label>'+cma.text.get('feedbackLabel')+'</label><textarea id="cma-feedback-content" name="issue" rows="4" cols="50"></textarea><br><input id="cma-feedback-submit" type="button" value="Send"></div>');
    jQuery("#cma-feedback-submit",div).click(function(){
      var textarea = jQuery('#cma-feedback-content',div);
      var data = textarea.val();
      var box = depgraphlib.Box.getBox(textarea[0]);
      jQuery.ajax({
        type: 'POST', 
        url: cma.TheApp.options.host_url+"/mgwiki/ws/sendMail",
        data: {
          message:data,
        },
        dataType : 'json',
        success: function(data, textStatus, jqXHR) {
          box.setContent(cma.text.get("success-message-feedback"));
          setTimeout(function(){box.close(true);},2000);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          alert(textStatus);
        }
      });
    });
    var box = new depgraphlib.Box({closeButton:true,draggable:true}).setContent(div).setHeader('FeedBack Form').setFixedSize(500).open(point);
  };

  cma.App.prototype.helpPanel = function(){
    var width = cma.TheApp.view.width();
    var height = cma.TheApp.view.height();
    var offset = cma.TheApp.view.offset();
    var point = {x:offset.left+width/2,y:offset.top+height/2};
    var div = jQuery('<div>Add mgwiki cma help content page here!</div>');
    var box = new depgraphlib.Box({closeButton:true,draggable:true}).setContent(div).setFixedSize(500).open(point);
  };
  
  cma.App.prototype.ajaxLoader = function(status){
    var ajaxLoader = jQuery('#ajax-loader');
    if(status){
      var container = jQuery('#cma-app-container');
      var height = jQuery(window).height() - this.view.offset().top; 
      var width = this.view.width();
      ajaxLoader.height(height-130);
      ajaxLoader.width(width-50);
      ajaxLoader.css("top",54);
      ajaxLoader.removeClass("hidden");
    }else{
      ajaxLoader.addClass("hidden");
    }
  };

  cma.App.prototype.callProxy = function(path,data,callback,errorCallback,dataType){
    var me = this;
    data = data || {};
    data.proxy_url = path;
    dataType = dataType || 'json';
    jQuery.ajax({
      type: 'POST', 
      url: cma.TheApp.ws_url+'frmgcm_proxy',
      data: data,
      dataType : dataType,
      success: function(data, textStatus, jqXHR) {
        callback(data,textStatus,jqXHR);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        if(errorCallback){
          errorCallback(jqXHR, textStatus, errorThrown);
        }else{
          alert(textStatus);
        }
      }
    });
  };

  
  
  
}(window.cma = window.cma || {}));

(function(cma){
  
  cma.Corpus = function(cid){
    this.cid = cid;
    this.paths = {};
    this.doc = null;
    this.docsTree = null;
    this.view = jQuery(cma.template.corpusViewMain);
    cma.AppView.setTemplate(this.view);
    cma.Log("corpus loaded");
  };




 cma.Corpus.prototype.init = function(doAfterInit){
  var me = this;
  this.permissions = new cma.toolbox.Permissions();
  this.getAndDo('info',{},function(data){
    me.name = data.name;
    me.status = data.status;    
    if(data.status == "available" || data.status == "processing"){
      me.initialized = true;
      me.getAndDo('documents',{},function(documentInfo){
        me.docsTree = documentInfo;
        me.getDocumentsInfo(documentInfo);
        doAfterInit.call();
      });
    }else{
      doAfterInit.call();
    }
  });

 };

 
  cma.Corpus.prototype.initView = function(){
    var me = this;

    me.prepareView(this.status);
    //me.initContactForm();
    me.updateHeader();

    if(this.initialized){
      var documents = jQuery('<div style="display:inline-block;"><h2 style="margin-top:0;">Corpus Files</h2></div>');
      var documentsTree = jQuery('<div class="cma-documents-tree"></div>');
      documents.append(documentsTree);
      cma.AppView.getMainLayoutCell(0,0).append(documents);
      me.displayDocuments(this.docsTree,documentsTree);
      jQuery(".cma-folder-toggle",document).click(function(event){
        var togglerIcon = jQuery(event.target);
        if(togglerIcon.hasClass("cma-folder-collapse")){
          togglerIcon.addClass("cma-folder-expand");
          togglerIcon.removeClass("cma-folder-collapse");
        }else{
          togglerIcon.addClass("cma-folder-collapse");
          togglerIcon.removeClass("cma-folder-expand");
        }
        jQuery(".cma-document-tree",jQuery(event.target).parent()).toggle();
        event.stopPropagation();
      });  
    }else{
      cma.AppView.getMainLayoutCell(0,0).append("not ready");
    }
    

  };
  
  cma.Corpus.prototype.prepareView = function(status){
    var me = this;

    cma.AppView.setTitle(jQuery(cma.template.corpusViewHeader));
    cma.ui.initMutable();
    
    var cmaContentContainer = cma.ui.get('cma-content');
    var height = cma.ui.get('cma-app-container').height();
    //cmaContentContainer.height(height-100);


    var layout = [];
    var row = [{name:"firstCol",width:30},{name:"secondCol",width:65}];
    layout.push(row);
    cma.AppView.setMainLayout(layout);

    
    
    this.initCorpusToolBox();
    
    
  };
  
  cma.Corpus.prototype.load = function(cid){
    
  };
  
  cma.Corpus.prototype.show = function(documentInfo){
    
  };
  
  cma.Corpus.prototype.showStatus = function(){
    
  };
  
  cma.Corpus.prototype.initCorpusToolBox = function(){
    var me = this; 
    var cell = cma.AppView.getMainLayoutCell(0,1);

    var toolboxobj = new cma.ui.ToolBox();
    cell.html(toolboxobj.view);

    var permissions = this.permissions;
    var miscActions = new cma.toolbox.Misc();

   permissions.haveRights('process',function(){
      var process = me.process = new cma.toolbox.Process();
      toolboxobj.addBoxItem('corpus-process',process.view,'Process'); 
   });
    
    var search = new cma.toolbox.Search();
    toolboxobj.addBoxItem('document-search',search.view,'Search',{startOpen:true});
    search.init();
    
    
    
    toolboxobj.addBoxItem('corpus-report','<div id="cma-corpus-status"></div>','Corpus Analysis Report',{startOpen:true});
    toolboxobj.addBoxItem('corpus-permissions',permissions.view,'Permissions',{startOpen:true});
    toolboxobj.addBoxItem('corpus-log','<div id="cma-corpus-log"></div>','Corpus Log');
    toolboxobj.addBoxItem('mgwiki_sentences',cma.TheApp.mgwiki_sentences.view,'MGWiki Sentences');
    toolboxobj.addBoxItem('corpus-misc-actions',miscActions.view,'Misc');

    cma.TheApp.mgwiki_sentences.init();
    
    

    permissions.init();

    this.getAndDo('tasks',{},function(data){
      console.log(data);
      var tasks = me.setTasks(data);
      cma.ui.get('cma-corpus-log').html('');
      cma.ui.get('cma-corpus-log').append(tasks);
    });

    this.getAndDo("report",{},function(data){

      cma.ui.get('cma-corpus-status').html(data);
    },null,'html');

    
  }

  
  
  cma.Corpus.prototype.updateHeader = function(){
    var me = this;
    this.getAndDo('info',{},function(data){
      me.name = data.name;
      cma.ui.get('cma-title').html(data.name);
      var format = "";
      if(data.format){
        format = " ("+data.format+")";
      }
      cma.ui.get('cma-status').html(data.status+format);
      
      me.setActions(data);
      cma.ui.get('cma-app').removeClass('hidden');
      cma.TheApp.ajaxLoader(false);
    });
  };
  

  cma.Corpus.prototype.setActions = function(data){
    var ui = cma.ui.get('cma-actions');
    var html = '';
    for(var i=0;i<data.actions.length;++i){
      html+= '<div class="cma-app-button"><a href="#corpus/'+this.cid+'/'+data.actions[i]+'" class="icon-action-'+data.actions[i]+'"></a></div>';
    }
    ui.html(html);
  };

  cma.Corpus.prototype.getDocumentsInfo = function(data){
    if(data.file){
      this.paths[data.did] = {filepath:data.fullpath,scid:data.scid,name:data.path};
    }else{
      for(i in data.children){
        this.getDocumentsInfo(data.children[i]);
      }
    }
  }
  
  cma.Corpus.prototype.displayDocuments = function(data,container){
    if(data.file){
      this.paths[data.did] = {filepath:data.fullpath,name:data.path};
      container.append('<a href="#corpus/'+this.cid+'/view/'+data.did+'" class="cma-document-file">'+data.path+'</a>');
    }else{
      var folder = jQuery('<div class="cma-document-folder"><div class="cma-folder-toggle cma-folder-collapse"></div><div class="cma-document-folder-file">'+data.path+'</div></div>');
      var newcontainer = jQuery('<div class="cma-document-tree"></div>');
      container.append(folder);
      folder.append(newcontainer);
      for(i in data.children){
        this.displayDocuments(data.children[i],newcontainer);
      }
    }
  };
  
  cma.Corpus.prototype.displayDocument = function(did,filename){
    var me = this;
    this.doc = new cma.Document(did);
    jQuery.ajax({
      type: 'POST', 
      url:  cma.TheApp.ws_url+'frmgcm_proxy',
      data : {proxy_url: 'corpus/'+me.cid+'/document/'+did+'/annotated'},
      dataType : 'html',
      success: function(data, textStatus, jqXHR) {
        me.doc.initView(filename);
        
        me.doc.init(data);

        cma.TheApp.ajaxLoader(false);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        if(errorCallback){
          errorCallback(jqXHR, textStatus, errorThrown);
        }else{
          alert(textStatus);
        }
        cma.TheApp.ajaxLoader(false);
      }
    });
    
  };






  
  cma.Corpus.prototype.setTasks = function(data){
    var me = this;
    this.taskstatus = data;
    var tasksui = jQuery(cma.template.tasks);
    var table = jQuery("table",tasksui);
    for(var i=0; i<data.length; i++){
      var task = data[i];
      for(var j=0;j<task.length;j++){
        var taskunit = task[j];
        var tr = '<tr><td class="cma-app">'+taskunit.name
          +'</td><td class="cma-app">'+taskunit.status
          +'</td><td class="cma-app"><a style="text-decoration: underline; cursor:pointer;" ref="stdout-'+taskunit.stdout+'">view</a>'
          +'</td><td class="cma-app"><a style="text-decoration: underline; cursor:pointer;" ref="stderr-'+taskunit.stderr+'">view</a>'
          +'</td></tr>';
        table.append(tr);
      }
    }
    jQuery('a',table).click(function(){
      me.showTask(jQuery(this).attr('ref'));
    });
    return tasksui;
  };

  cma.Corpus.prototype.showTask = function(task){
    this.getAndDo('output',{resource:task},function(data){
      var width = cma.TheApp.view.width();
      var height = cma.TheApp.view.height();
      var offset = cma.TheApp.view.offset();
      var point = {x:offset.left+width/2,y:offset.top+height/2};
      var div = jQuery('<div>'+data+'</div>');
      var box = new depgraphlib.Box({closeButton:true,draggable:true}).setContent(div).setFixedSize(500).open(point);
    },null,'html');
  }

  cma.Corpus.prototype.initContactForm = function(){
    var me = this;
    var contactForm = cma.ui.get("cma-corpus-contact-form");
    contactForm.html(cma.template.contactForm);

    jQuery("#cma-contact-form-submit").click(function(event){
      var form = jQuery(jQuery(event.target).parent());
      var subject = jQuery("#cma-contact-form-subject",form).val();
      var body = jQuery("#cma-contact-form-body",form).val();
      me.callAndDo("sendMail",{subject:subject,body:body},function(data){
        console.log(data);
      });
    });
  }
  

  
  cma.Corpus.prototype.callAndDo = function(action,data,callback,errorCallback,dataType){
    var me = this;
    data = data || {};
    data.cid = me.cid;
    dataType = dataType || 'json';
    jQuery.ajax({
      type: 'POST', 
      url: cma.TheApp.ws_url+action,
      data: data,
      dataType : dataType,
      success: function(data, textStatus, jqXHR) {
        callback(data,textStatus,jqXHR);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        if(errorCallback){
          errorCallback(jqXHR, textStatus, errorThrown);
        }else{
          alert(textStatus);
        }
      }
    });
  };

  
  cma.Corpus.prototype.getAndDo = function(action,data,callback,errorCallback,dataType){
    var me = this;
    data = data || {};
    data.cid = me.cid;
    data.action = action;
    dataType = dataType || 'json';
    jQuery.ajax({
      type: 'POST', 
      url: cma.TheApp.ws_url+'get',
      data: data,
      dataType : dataType,
      success: function(data, textStatus, jqXHR) {
        callback(data,textStatus,jqXHR);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        if(errorCallback){
          errorCallback(jqXHR, textStatus, errorThrown);
        }else{
          alert(textStatus);
        }
      }
    });
  };
  

  
  
  
  
  
}(window.cma = window.cma || {}));

(function(cma){

  
  /**
   * @class {object} CorpusListView
   * @alias CorpusListView
   * 
   * @desc View the list of the corpus for a user
   * 
   * @param container the id string of the div container that will host the view
   * @returns {cma.corpusListView}
   */
  cma.CorpusListView = function(container){
    
    var me = this;
    
    this.settings = {
      'ws_url' : cma.TheApp.ws_url,
      'container' : container,
    };
    
    
    this.data = null;
    
    this.refreshData = function(){
      jQuery.ajax({
        type: 'POST', 
        url: me.settings.ws_url + 'corpus_list',
        dataType : 'json',
        success: function(data, textStatus, jqXHR) {
          if(jqXHR.status == 200){
            me.data = data;
            me.render();
            cma.TheApp.ajaxLoader(false);
          }else{
            console.log('error when fetching data');
          }
        },
        error: function(jqXHR, textStatus, errorThrown) {
        }
      });
    };
    
    this.render = function(){
      var html = '<table id="corpus-list" style="margin-top:10px;"><tr><th>Corpus Name</th><th>Owner</th><th>Status</th><th>Actions</th></tr>';
      html += '<tr class="alt"><td colspan="4">My Corpus</td></tr>';
      for(i in this.data.private){
        var corpus = this.data.private[i];
        html += this.render_corpus(corpus);
      }
      if(this.data.other.length>0){
        html += '<tr class="alt"><td colspan="4">Other Corpus</td></tr>';
        for(i in this.data.other){
          var corpus = this.data.other[i];
          html += this.render_corpus(corpus);
        }  
      }
      if(this.data.public.length>0){
        html += '<tr class="alt"><td colspan="4">Public Corpus</td></tr>';
        for(i in this.data.public){
          var corpus = this.data.public[i];
          html += this.render_corpus(corpus);
        }
      }
      html += '</table>';
      jQuery('#'+this.settings.container).html(html);
      
    };
    
    this.render_corpus = function(corpus){
      var actions = corpus.actions;
      var format = "";
      if(corpus['format']){
        format = " ("+corpus['format']+")";
      }
      var html = '<tr class="corpus-item">'
        +'<td class="corpus-name">'
        +'<a href="#corpus/'+corpus['cid']+'">'+corpus['name']+'</a>'
        +'</td>'
        +'<td class="corpus-owner">'
        +'<a href="user/'+corpus['dbobject']['owner']+'">'+corpus['ownerName']+'</a>'
        +'</td>'
        +'<td width="100px;" class="corpus-status">'
        + corpus.status + format
        +'</td>'
        +'<td class="corpus-action">';
      for(i in actions){
        var action = actions[i];
        html += '<a class="cma-app-button icon-action-'+action+'" href="#corpus/'+corpus['cid']+'/'+action+'" title="'+action+'"></a>';
      }
      html += '</td>';
      html += '</tr>';
      return html;
    };
        
  };
  
  cma.corpusUpload = function() {

    //These are the main config variables and should be able to take care of most of the customization
    this.settings = {
      //The id of the file input
      'inputField': 'corpusUploadFile',
      'altInputField':'corpusUploadUrl',

      //The id of the form with the file upload.
      //This should be a valid html form (see index.html) so there is a fallback for unsupported browsers
      'formId': 'corpusUploadForm',

      //The id of the progress bar
      //Width of this element will change based on progress
      //Content of this element will display a percentage
      //See corpusUpload.progressUpdate() to change this code
      'progressBarField': 'corpusUploadProgressBarFilled',

      //The id of the time remaining field
      //Content of this element will display the estimated time remaining for the upload
      //See corpusUpload.progressUpdate() to change this code
      'timeRemainingField': 'corpusUploadTimeRemaining',

      //The id of the text response field
      //Content of this element will display the response from the server on success or error
      'responseField': 'corpusUploadResponse',

      //The id of the submit button
      //This is then changed to become the pause/resume button based on the status of the upload
      'submitButton': 'corpusUploadSubmit',

      //Color of the background of the progress bar
      //This must also be defined in the progressBarField css, but it's used here to reset the color after an error
      //Default: green
      'progressBarColor': '#5bb75b',

      //Color of the background of the progress bar when an error is triggered
      //Default: red
      'progressBarColorError': '#da4f49',

      //Path to the php script for handling the uploads
      'scriptPath': '',

      //Additional URL variables to be passed to the script path
      //ex: &foo=bar
      'scriptPathParams': '',

      //Size of chunks to upload (in bytes)
      //Default: 1MB
      'chunkSize': 1000000,

      //Max file size allowed
      //Default: 2GB
      'maxFileSize': 21474836,
      
      //CorpusListView
      //Must be set
      'corpusListView' : null,
    };

    //Upload specific variables
    this.uploadData = {
      'uploadStarted': false,
      'file': false,
      'numberOfChunks': 0,
      'aborted': false,
      'paused': false,
      'pauseChunk': 0,
      'key': 0,
      'timeStart': 0,
      'totalTime': 0
    };

    //Success callback
    this.success = function(response) {

    };

    parent = this;

    //Quick function for accessing objects
    this.$ = function(id) {
      return document.getElementById(id);
    };

    //Resets all the upload specific data before a new upload
    this.resetKey = function() {
        this.uploadData = {
          'uploadStarted': false,
          'file': false,
          'numberOfChunks': 0,
          'aborted': false,
          'paused': false,
          'pauseChunk': 0,
          'key': 0,
          'timeStart': 0,
          'totalTime': 0
        };
      };

    //Inital method called
    //Determines whether to begin/pause/resume an upload based on whether or not one is already in progress
    this.fire = function() {
      if(cma.TheApp.options.user.uid == 0){
        alert("You don't have the right to upload corpus");
        return;
      }
      if(this.uploadData.uploadStarted === true && this.uploadData.paused === false) {
        this.pauseUpload();
      }
      else if(this.uploadData.uploadStarted === true && this.uploadData.paused === true) {
        this.resumeUpload();
      }
      else {
        this.processFiles();
      }

    };

    //Initial upload method
    //Pulls the size of the file being uploaded and calculated the number of chunks, then calls the recursive upload method
    this.processFiles = function() {

      //If the user is using an unsupported browser, the form just submits as a regular form
      if (!window.File || !window.FileReader || !window.FileList || !window.Blob) {
        this.$(this.settings.formId).submit();
        return;
      }

      //Reset the upload-specific variables
      this.resetKey();
      this.uploadData.uploadStarted = true;

      //Some HTML tidying
      //Reset the background color of the progress bar in case it was changed by any earlier errors
      //Change the Upload button to a Pause button
      this.$(this.settings.progressBarField).style.backgroundColor = this.settings.progressBarColor;
      this.$(this.settings.responseField).textContent = 'Uploading...';
      this.$(this.settings.submitButton).value = 'Pause';

      //Alias the file input object to this.uploadData
      this.uploadData.file = this.$(this.settings.inputField).files[0];

      if(!this.uploadData.file){
        var url = this.$(this.settings.altInputField).value;
        if(url){
          this.sendUploadUrl(url);
          return;
        }else{
          this.printResponse('Please enter either a local file or url.', true);
          return;
        }
      }

      //Check the filesize. Obviously this is not very secure, so it has another check in inc/corpusUpload.php
      //But this should be good enough to catch any immediate errors
      var fileSize = this.uploadData.file.size;
      if(fileSize > this.settings.maxFileSize) {
        this.printResponse('The file you have chosen is too large.', true);
        return;
      }

      //Calculate the total number of file chunks
      this.uploadData.numberOfChunks = Math.ceil(fileSize / this.settings.chunkSize);

      //Start the upload
      this.sendFile(0);
    };

    //Main upload method
    this.sendFile = function (chunk) {

      //Set the time for the beginning of the upload, used for calculating time remaining
      this.uploadData.timeStart = new Date().getTime();

      //Check if the upload has been cancelled by the user
      if(this.uploadData.aborted === true) {
        return;
      }

      //Check if the upload has been paused by the user
      if(this.uploadData.paused === true) {
        this.uploadData.pauseChunk = chunk;
        this.printResponse('Upload paused.', false);
        return;
      }

      //Set the byte to start uploading from and the byte to end uploading at
      var start = chunk * this.settings.chunkSize;
      var stop = start + this.settings.chunkSize;

      //Initialize a new FileReader object
      var reader = new FileReader();

      reader.onloadend = function(evt) {

        //Build the AJAX request
        //
        //this.uploadData.key is the temporary filename
        //If the server sees it as 0 it will generate a new filename and pass it back in the JSON object
        //this.uploadData.key is then populated with the filename to use for subsequent requests
        //When this method sends a valid filename (i.e. key != 0), the server will just append the data being sent to that file.
        xhr = new XMLHttpRequest();
        xhr.open("POST", parent.settings.scriptPath + '/upload/' + parent.uploadData.key + parent.settings.scriptPathParams, true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        xhr.onreadystatechange = function() {
          if(xhr.readyState == 4) {
            var response = JSON.parse(xhr.response);

            //If there's an error, call the error method and break the loop
            if(response.errorStatus !== 0 || xhr.status != 200) {
              parent.printResponse(response.errorText, true);
              return;
            }

            //If it's the first chunk, set this.uploadData.key to the server response (see above)
            if(chunk === 0 || parent.uploadData.key === 0) {
              parent.uploadData.key = response.key;
            }

            //If the file isn't done uploading, update the progress bar and run this.sendFile again for the next chunk
            if(chunk < parent.uploadData.numberOfChunks) {
              parent.progressUpdate(chunk + 1);
              var start = new Date().getTime();
              for (var i = 0; i < 1e7; i++) {
                if ((new Date().getTime() - start) > 1000){
                  break;
                }
              }
              parent.sendFile(chunk + 1);
            }
            //If the file is complete uploaded, instantiate the finalizing method
            else {
              parent.sendFileData();
            }

          }

        };

        //Send the file chunk
        xhr.send(blob);
      };

      //Slice the file into the desired chunk
      //This is the core of the script. Everything else is just fluff.
      var blob = this.uploadData.file.slice(start, stop);
      reader.readAsBinaryString(blob);
    };

    this.sendUploadUrl = function(url){
      jQuery.ajax({
        type: 'POST', 
        url: cma.TheApp.ws_url+'uploadUrl',
        data: {
          url:url
        },
        success: function(data, textStatus, jqXHR) {
          parent.resetKey();

          //Change the submit button text so it's ready for another upload and spit out a sucess message
          parent.$(parent.settings.submitButton).value = 'Start Upload';
          parent.printResponse('File uploaded successfully.', false);

          parent.success(data);
          parent.settings.corpusListView.refreshData();
        },
        error: function(jqXHR, textStatus, errorThrown) {
          alert(textStatus);
        }
      });
    }

    //This method is for whatever housekeeping work needs to be completed after the file is finished uploading.
    //As it's setup now, it passes along the original filename to the server and the server renames the file and removes it form the temp directory.
    //This function could also pass things like this.uploadData.file.type for the mime-type (although it would be more accurate to use php for that)
    //Or it could pass along user information or something like that, depending on the context of the application.
    this.sendFileData = function() {
      var data = 'key=' + this.uploadData.key + '&name=' + this.uploadData.file.name;
      xhr = new XMLHttpRequest();
      xhr.open("POST", parent.settings.scriptPath + '/finish', true);
      xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

      xhr.onreadystatechange = function() {
          if(xhr.readyState == 4) {
            var response = JSON.parse(xhr.response);

            //If there's an error, call the error method
            if(response.errorStatus !== 0 || xhr.status != 200) {
              parent.printResponse(response.errorText, true);
              return;
            }

            //Reset the upload-specific data so we can process another upload
            parent.resetKey();

            //Change the submit button text so it's ready for another upload and spit out a sucess message
            parent.$(parent.settings.submitButton).value = 'Start Upload';
            parent.printResponse('File uploaded successfully.', false);

            parent.success(response);
            parent.settings.corpusListView.refreshData();
          }
        };

      //Send the reques
      xhr.send(data);
    };

    //This method cancels the upload of a file.
    //It sets this.uploadData.aborted to true, which stops the recursive upload script.
    //The server then removes the incomplete file from the temp directory, and the html displays an error message.
    this.abortFileUpload = function() {
      this.uploadData.aborted = true;
      var data = 'key=' + this.uploadData.key;
      xhr = new XMLHttpRequest();
      xhr.open("POST", this.settings.scriptPath + '/abort', true);
      xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

      xhr.onreadystatechange = function() {
          if(xhr.readyState == 4) {
            var response = JSON.parse(xhr.response);

            //If there's an error, call the error method.
            if(response.errorStatus !== 0 || xhr.status != 200) {
              parent.printResponse(response.errorText, true);
              return;
            }
            parent.printResponse('File upload was cancelled.', true);
          }

        };

      //Send the request
      xhr.send(data);
    };

    //Pause the upload
    //Sets this.uploadData.paused to true, which breaks the upload loop.
    //The current chunk is still stored in this.uploadData.pauseChunk, so the upload can later be resumed.
    //In a production environment, you might want to have a cron job to clean up files that have been paused and never resumed,
    //because this method won't delete the file from the temp directory if the user pauses and then leaves the page.
    this.pauseUpload = function() {
      this.uploadData.paused = true;
      this.printResponse('', false);
      this.$(this.settings.submitButton).value = 'Resume';
    };

    //Resume the upload
    //Undoes the doings of this.pauseUpload and then re-enters the loop at the last chunk uploaded
    this.resumeUpload = function() {
      this.uploadData.paused = false;
      this.$(this.settings.submitButton).value = 'Pause';
      this.sendFile(this.uploadData.pauseChunk);
    };

    //This method updates a simple progress bar by calculating the percentage of chunks uploaded.
    //Also includes a method to calculate the time remaining by taking the average time to upload individual chunks
    //and multiplying it by the number of chunks remaining.
    this.progressUpdate = function(progress) {

      var percent = Math.ceil((progress / this.uploadData.numberOfChunks) * 100);
      this.$(this.settings.progressBarField).style.width = percent + '%';
      this.$(this.settings.progressBarField).textContent = percent + '%';

      //Calculate the estimated time remaining
      //Only run this every five chunks, otherwise the time remaining jumps all over the place (see: http://xkcd.com/612/)
      if(progress % 5 === 0) {

        //Calculate the total time for all of the chunks uploaded so far
        this.uploadData.totalTime += (new Date().getTime() - this.uploadData.timeStart);
        console.log(this.uploadData.totalTime);

        //Estimate the time remaining by finding the average time per chunk upload and
        //multiplying it by the number of chunks remaining, then convert into seconds
        var timeLeft = Math.ceil((this.uploadData.totalTime / progress) * (this.uploadData.numberOfChunks - progress) / 100);
        console.log(Math.ceil(((this.uploadData.totalTime / progress) * this.settings.chunkSize) / 1024) + 'kb/s');

        //Update this.settings.timeRemainingField with the estimated time remaining
        this.$(this.settings.timeRemainingField).textContent = timeLeft + ' seconds remaining';
      }
    };

    //Simple response/error handler
    this.printResponse = function(responseText, error) {
      this.$(this.settings.responseField).textContent = responseText;
      this.$(this.settings.timeRemainingField).textContent = '';
      if(error === true) {
        this.$(this.settings.progressBarField).style.backgroundColor = this.settings.progressBarColorError;
      }
    };
  };  
  
}(window.cma = window.cma || {}));


(function(cma){
  
  cma.Document = function(did){
    this.did = did;
    this.editMode = false;
    this.openedGraphs = {};
  }

  cma.Document.prototype.initView = function(filename){
    var cmaContentContainer = cma.ui.get('cma-content');
    var height = cma.ui.get('cma-app-container').height();
    //cmaContentContainer.height(height-100);

    // set the title
    cma.AppView.setTitle('<h1>'+cma.TheApp.corpus.name+' | '+filename+'</h1>');
    
    var layout = [];
    var row = [{name:"firstCol",width:80},{name:"secondCol",width:15}];
    layout.push(row);
    cma.AppView.setMainLayout(layout);

    this.document = jQuery('<div id="cma-document"></div>');
    cma.AppView.getMainLayoutCell(0,0).html(this.document);
    this.document.after(cma.Document.templateLegend);

    this.initToolBox();

    

  }

  cma.Document.prototype.init = function(data){
    var me = this;
    var height = cma.TheApp.view.height();
    this.document.height(height - 220);
    this.document.html(data);

    jQuery(".cma-sentence",this.document).on("click",function(){
      if(!me.preventShowGraph){
        me.showGraph(me.did,jQuery(this).attr("id"));  
      }
    });
    jQuery(this.document).ready(function(){
      jQuery(me.document).bind("mouseup", function(event){
        
        var text = me.getSelectionText();
        if(text != ''){
          me.showFragment(event,text,me.did);
          me.preventShowGraph = true;
        }else{
          me.preventShowGraph = false;
        }
      });
    });
    
    var def = {
      "infos":{
        menu:"Infos",
        click:function(element){
          var infos = jQuery('<div />').text(element.attr('infos')).html();
          var width = cma.TheApp.view.width();
          var height = cma.TheApp.view.height();
          var offset = cma.TheApp.view.offset();
          var point = {x:offset.left+width/2-400,y:offset.top+height/2-200};
          var box = new depgraphlib.Box({closeButton:true,draggable:true}).setContent(infos).open(point);
        }
      },
      "revisions":{
        menu:"Revisions",
        click:function(element){
          me.showGraphRevisions(element.attr('id'));
          console.log(element);
          //alert('show panel with revisions list for this sentence');
        }
      },
    };
    /*for(menu in this.mode[this.editMode].onLinkContext){
      def[menu] = {
        menu : menu,
        click: function(element) {  // element is the jquery obj clicked on when context menu launched
          onContextClick('onLinkContext',this,element);
        },
        klass: "menu-item-1" // a custom css class for this menu item (usable for styling)
      };
    }*/
    jQuery(".cma-sentence",this.document).contextMenu('sentence-context-menu', def);

    
  }

  cma.Document.prototype.initToolBox = function(){

    var me = this; 
    var cell = cma.AppView.getMainLayoutCell(0,1);

    var toolboxobj = new cma.ui.ToolBox();
    cell.html(toolboxobj.view);

    var search = new cma.toolbox.Search();
    var edition = this.edition = new cma.toolbox.Edition();

    toolboxobj.addBoxItem('document-search',search.view,'Search',{startOpen:true});
    toolboxobj.addBoxItem('document-edition',edition.view,'Edition',{startOpen:true});
    
    search.init(this.did);
    edition.init();

    

  };

  cma.Document.prototype.showGraphRevisions = function(id){
    var me = this;
    var html = "<ul>";
    html += '<li ref="'+id+'">0</li>';
    var revisions = this.edition.revisionList.originalData;
    for(var i in revisions){
      var rev = revisions[i];
      for(var change in rev.state){
        if(change==id){
          html += '<li ref="'+change+'">'+rev.state[id]+'</li>';
        }
      }
    }
    html += "</ul>";

    var div = jQuery(html);
    jQuery('li',div).click(function(element){
      if(!me.preventShowGraph){
        me.showGraph(me.did,jQuery(this).attr("ref"),jQuery(this).html());  
      }
    })

    var width = cma.TheApp.view.width();
    var height = cma.TheApp.view.height();
    var offset = cma.TheApp.view.offset();
    var point = {x:offset.left+width/2-400,y:offset.top+height/2-200};
    var box = new depgraphlib.Box({closeButton:true,draggable:true}).setContent(div).open(point);
  }


  cma.Document.prototype.displayGraph = function(data,id,box){
    var me = this;
    var width = cma.TheApp.view.width();
    var height = cma.TheApp.view.height();
    var offset = cma.TheApp.view.offset();
    
    var uniqid = Date.now();
    var div = jQuery('<div><div id="graph-container-'+uniqid+'"></div></div>');
  
    if(!box){
      box = new depgraphlib.Box({closeButton:true,draggable:true}).setContent(div).open();
    }else{
      box.resetContent().setContent(div);
    }
    box.setHeader(id);
    var depGraph = new depgraphlib.DepGraph(jQuery("#graph-container-"+uniqid),data,{"uid":uniqid,viewmode:"full",maxwidth:cma.TheApp.view.width()-100});
    var viewer = depGraph.viewer;
    depGraph.wsurl = cma.TheApp.ws_url+"graph";
    if(depGraph.options.viewmode == 'full'){
      viewer.noBorders();
    }

    var boxcontent = jQuery('.depgraphlib-box-content',box.object);
    var dgwidth = viewer.mainpanel.width();
    var dgheight = viewer.mainpanel.height();
    boxcontent.width(dgwidth+16);
    boxcontent.height(dgheight+16);
    boxcontent.resizable({
      stop:function(e,data){
        depGraph.setWidthLimitedViewMode(data.size.width-16,true);
        viewer.setHeight(data.size.height-46);
      }
    });

    var point = {x:width/2-dgwidth/2,y:height/2-dgheight/2};
    box.move(point);



    viewer.addToolbarItem({name:'saveInMgwiki',callback:function(){
        var ddata = depGraph.cleanData();
        jQuery.ajax({
          type: 'POST', 
          url: cma.TheApp.ws_url+"graph",
          data: {
            format:'json',
            action:'mgwiki-import',
            options: '',
            data:ddata,
          },
          dataType : 'json',
          success: function(data, textStatus, jqXHR) {
            alert("Successfully imported to mgwiki.")
            console.log(data);
          },
          error: function(jqXHR, textStatus, errorThrown) {
            alert(textStatus);
          }
        });
      },style:'mgwiki-export','static':true});
    

    if(this.editMode){
      depGraph.sentenceLink = '#';

      depGraph.editObject.mode['default'].save = depgraphlib.EditObject.DefaultModeLib.save;
      depGraph.dataFormat = 'json';
      viewer.setFixedToolbar();
      if(cma.TheApp.corpus.graphFormat == 'depxml'){
        frmgEditMode = new depgraphlib.FRMGEditMode('frmgserver_url_here');
        depGraph.editObject.addEditMode(frmgEditMode.mode);
        depGraph.editObject.setEditMode('frmg');
        depGraph.editObject.addEditModeSwitcher();
      }else{
        depGraph.editObject.setEditMode('default');
      }



      depGraph.editObject.mode[depGraph.editObject.editMode].broadcast = function(action){
        var message = {type:"depgraphbroadcast",id:id,action:action};
        cma.TheApp.socket.send(JSON.stringify(message));
      };

      // @todo : pb server side => convert to conll everytime
      depGraph.editObject.mode[depGraph.editObject.editMode].save = function(depgraph){
        var ddata = depgraph.cleanData();
        jQuery.ajax({
          type: 'POST', 
          url: cma.TheApp.ws_url+"graph",
          data: {
            action:'save',
            format:depgraph.dataFormat,
            options: '',
            gid:id,
            cid:cma.TheApp.corpus.cid,
            did:cma.TheApp.corpus.doc.did,
            data:ddata,
            uid:depgraph.options.uid
          },
          dataType : 'json',
          success: function(data, textStatus, jqXHR) {
            depgraph.editObject.lastSavedPtr = depgraph.editObject.currentPtr;
            depgraph.editObject.needToSaveState = false;
            depgraph.editObject.updateSaveState();
          },
          error: function(jqXHR, textStatus, errorThrown) {
            alert(textStatus);
          }
        });
      }



      point.x = offset.left+width/2-viewer._width/2;
      point.y = offset.top+height/2-viewer._height/2;
      box.move(point); 

           
    }

    box.options.onClose = function(){
        me.openedGraphs[id] = null;
      }

    return depGraph;
  };

  cma.Document.prototype.showGraph = function(did,id,vid){
    var me = this;
    var box = new depgraphlib.Box({closeButton:true,draggable:true}).defaultInit().open();
    

    cma.TheApp.callProxy('sc/'+did+'/_graph',{sid:id,depgraphsrc:true},function(data){
      var depGraph = me.displayGraph(data,id,box);
      me.openedGraphs[id] = depGraph;
    });
  };


  cma.Document.prototype.showFragment = function(event,textSelection,did){
    var me = this;

    var tsInfo;
    var teInfo;
    var slist = [];

    var aTokens = getToken(textSelection.anchorNode,textSelection.anchorOffset);
    var fTokens = getToken(textSelection.focusNode,textSelection.focusOffset);


    var relativePosition = aTokens.prevToken.compareDocumentPosition( fTokens.prevToken );
    if(relativePosition == 2){
      tsInfo = {tid:jQuery(fTokens.nextToken).attr('ref'),sid:fTokens.nextToken.parentNode.id,token:fTokens.nextToken};
      teInfo = {tid:jQuery(aTokens.prevToken).attr('ref'),sid:aTokens.prevToken.parentNode.id,token:aTokens.prevToken};
    }else if(relativePosition == 4){
      tsInfo = {tid:jQuery(aTokens.nextToken).attr('ref'),sid:aTokens.nextToken.parentNode.id,token:aTokens.nextToken};
      teInfo = {tid:jQuery(fTokens.prevToken).attr('ref'),sid:fTokens.prevToken.parentNode.id,token:fTokens.prevToken};
    }else{
      alert("error");
      return;
    }

    var sStart = tsInfo.token.parentNode;
    var sEnd = teInfo.token.parentNode;
    var sentence = sStart;
    slist.push(sentence.id);
    while(sentence.id!=sEnd.id){
      sentence = sentence.nextSibling;
      while(!jQuery(sentence).hasClass("cma-sentence")){
        sentence = sentence.nextSibling;
      }
      slist.push(sentence.id);
    }
    

    console.log(tsInfo);
    console.log(teInfo);
    console.log(slist);
    var box = new depgraphlib.Box({closeButton:true,draggable:true}).defaultInit().open();
    cma.TheApp.corpus.getAndDo("fragment",{ts:tsInfo.tid,te:teInfo.tid,slist:slist,did:did},function(data){
        me.displayGraph(data,slist,box);

      },null);


    function getToken(node,offset){
      if(node.id == "cma-document"){
        node = node.childNodes[offset];
      }
      if(node.parentNode.id == "cma-document"){ // case outside a sentence
        var nextNode = node;
        while(!jQuery(nextNode).hasClass("cma-sentence")){
          nextNode = nextNode.nextSibling;
        }
        var nextToken;
        for(var i=0;i<nextNode.childNodes.length;i++){
          if(nextNode.childNodes[i].nodeName.toUpperCase()=="TOKEN"){
            nextToken = nextNode.childNodes[i];
            break;
          }
        }

        var prevNode = node;
        while(!jQuery(prevNode).hasClass("cma-sentence")){
          prevNode = prevNode.previousSibling;
        }
        var prevToken;
        for(var i=prevNode.childNodes.length-1;i>=0;i--){
          if(prevNode.childNodes[i].nodeName.toUpperCase()=="TOKEN"){
            prevToken = prevNode.childNodes[i];
            break;
          }
        }
        return {prevToken:prevToken,nextToken:nextToken};
      }else if(jQuery(node.parentNode).hasClass("cma-sentence")){ // case token or random node (br txtnode etc..)
        var nextToken = node;
        while(nextToken.nodeName.toUpperCase()!="TOKEN"){
          nextToken = nextToken.nextSibling;
        }
        var prevToken = node;
        while(prevToken.nodeName.toUpperCase()!="TOKEN"){
          prevToken = prevToken.nextSibling;
        }
        return {prevToken:prevToken,nextToken:nextToken};
      }else{ // case txtnode inside token
        return {prevToken:node.parentNode,nextToken:node.parentNode};
      }
    }
              
  }

  cma.Document.prototype.getSelectionText = function() {
    var t = '';
    if(window.getSelection){
      t = window.getSelection();
    }else if(document.getSelection){
      t = document.getSelection();
    }else if(document.selection){
      t = document.selection.createRange().text;
    }
    return t;
 };


  cma.Document.prototype.startEditMode = function(){
    if(!cma.TheApp.websocket_enabled){
      alert('web socket are not enabled. please use a more recent browser version');
      return;
    }

    

    this.editMode = true;
    return true;
  };

  cma.Document.prototype.stopEditMode = function(){
    if(depgraphlib.Box.instances.length>0){
      alert("close all graph windows before");
      return false;
    }
    /*for(var i=0;i< depgraphlib.Box.instances.length;i++){
      depgraphlib.Box.instances[i].close(true);
    }
    var r=confirm("Press a button");
        if (r==true)
          {
          x="You pressed OK!";
          }
        else
          {
          x="You pressed Cancel!";
          }*/

    this.editMode = false;
    return true;
  };

  cma.Document.templateLegend = '<div id="cma-document-legend">Legend : '+
    '<span class="cma-document-legend-item"><span style="color:red;">red</span> : failed parse</span>'+
    '<span class="cma-document-legend-item"><span style="color:blue;">blue</span> : partial parse</span>'+
    '<span class="cma-document-legend-item"><span style="border-bottom:1px dotted black;">underlined</span> : edited sentence</span>'+
    '</div>';
  
}(window.cma = window.cma || {}));

/*
 * jQuery hashchange event - v1.3 - 7/21/2010
 * http://benalman.com/projects/jquery-hashchange-plugin/
 * 
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
(function($,e,b){var c="hashchange",h=document,f,g=$.event.special,i=h.documentMode,d="on"+c in e&&(i===b||i>7);function a(j){j=j||location.href;return"#"+j.replace(/^[^#]*#?(.*)$/,"$1")}$.fn[c]=function(j){return j?this.bind(c,j):this.trigger(c)};$.fn[c].delay=50;g[c]=$.extend(g[c],{setup:function(){if(d){return false}$(f.start)},teardown:function(){if(d){return false}$(f.stop)}});f=(function(){var j={},p,m=a(),k=function(q){return q},l=k,o=k;j.start=function(){p||n()};j.stop=function(){p&&clearTimeout(p);p=b};function n(){var r=a(),q=o(m);if(r!==m){l(m=r,q);$(e).trigger(c)}else{if(q!==m){location.href=location.href.replace(/#.*/,"")+q}}p=setTimeout(n,$.fn[c].delay)}$.browser.msie&&!d&&(function(){var q,r;j.start=function(){if(!q){r=$.fn[c].src;r=r&&r+a();q=$('<iframe tabindex="-1" title="empty"/>').hide().one("load",function(){r||l(a());n()}).attr("src",r||"javascript:0").insertAfter("body")[0].contentWindow;h.onpropertychange=function(){try{if(event.propertyName==="title"){q.document.title=h.title}}catch(s){}}}};j.stop=k;o=function(){return a(q.location.href)};l=function(v,s){var u=q.document,t=$.fn[c].domain;if(v!==s){u.title=h.title;u.open();t&&u.write('<script>document.domain="'+t+'"<\/script>');u.close();q.location.hash=v}}})();return j})()})(jQuery,this);
(function(cma){
  
  cma.text = {};

  cma.text.fr = {

    "uploadHelp":"<p>Le service accepte les formats de fichiers suivants : pdf, doc, docx, rdf, txt. Les formats html et xml seront bientôt pris également en charge. :) A noter que pour de meilleurs résultats il est conseillé d'utiliser un format de texte brut, c'est à dire 'txt'.</p><p> Dans le cas où votre corpus est composé de plusieurs fichiers séparés (préférable à un seul gros fichier), le service accepte les archives zip et tar.gz.</p><p>Nous imposons pour ce service une limite pour la taille totale des fichiers (10M). Si vous souhaitez traiter un plus large volume de donnée, vous pouvez nous contacter via le formulaire de contact du site principal (Navigation->Contact).</p><p> Merci de ne pas utiliser abusivement ce service.</p>",

    "feedbackLabel":"Entrez votre message : ",
    "success-message-feedback":"Votre message de feedback a bien été envoyé. Merci.",
    "delete-corpus-confirm":"Etes vous sûr de vouloir supprimer ce corpus?",
    "delete-macrodef-confirm":"Etes vous sûr de vouloir supprimer cette macro?"
  };

  cma.text.en = {

    "uploadHelp":"to do",

  };

  cma.text.get = function(item,lang){
    if(!lang){
      lang = cma.TheApp.options.defaultLang;
    }

    return cma.text[lang][item];
  }

  
}(window.cma = window.cma || {}));

(function(cma){
  
  cma.toolbox = cma.toolbox || {};

  cma.toolbox.Edition = function(){
    var me = this;
    this.initView();
    cma.TheApp.websocketAddListener(
      "onmessage",
      "editBroadcast",
      function(data){
        var json = JSON.parse(data);
        if(json.type == "depgraphbroadcast"){
          if(cma.TheApp.corpus.doc.openedGraphs[json.id]){
            cma.TheApp.corpus.doc.openedGraphs[json.id].editObject.pushAction(json.action,true);
          }
        }else if(json.type == "saveNewRevisionsResult"){

        }
      }
    );
  }

  cma.toolbox.Edition.prototype.updateRevisionList = function(){
    var me = this;
    cma.TheApp.corpus.getAndDo('docRevs',{did:cma.TheApp.corpus.doc.did},function(data){
      console.log(data);
      me.revisionList.update(data);
    },function(){alert('error while fetching doc revs')},'json');
  }
  
  cma.toolbox.Edition.prototype.initView = function(){
    var me = this;
    this.view = jQuery(cma.toolbox.Edition.templateMain);

    this.revisionList = new cma.ui.Table({headers:[
      //{title:"rID",nameid:"_id",format:formatRID,unformat:unformatRID},
      {title:"date",nameid:"timedate",format:formatDate,unformat:unformatDate},
      {title:"name",nameid:"title"},
      {title:"user",nameid:"author"},
      {title:"tags",nameid:"tags",format:formatTAGs,unformat:unformatTAGs},
    ]});

    cma.ui.get('cma-edit-revisions',this.view).html(this.revisionList.view);

    this.editSaveButton = new cma.ui.CallbackButton("Save New Revision");
    cma.ui.get('cma-edit-save-submit',this.view).append(this.editSaveButton.view);
  }
  
  cma.toolbox.Edition.prototype.init = function(){
    var me = this;

    this.updateRevisionList();


    cma.ui.get('cma-edit-submit').click(function(){
      var val = cma.ui.get('cma-edit-input').val();
      var message = {data:val,authkey:cma.TheApp.authkey};
      cma.TheApp.socket.send(JSON.stringify(message));
    });
    cma.TheApp.corpus.permissions.haveRights('edit',function(){
    },function(){
      cma.ui.get('cma-edit-mode-switch').hide();
      cma.ui.get('cma-edit-input').hide();
      me.editSaveButton.view.hide();
    });
    cma.ui.get('cma-edit-mode-switch').click(function(){
      var button  = jQuery(this);
      cma.TheApp.corpus.permissions.haveRights('edit',function(){
        if(cma.TheApp.corpus.doc.editMode){
          if(cma.TheApp.corpus.doc.stopEditMode()){
            button.removeClass("cma-edit-mode-on").addClass("cma-edit-mode-off")
            button.html('<div style="padding-top:7px">off</div>');  
          }
        }else{
          if(cma.TheApp.corpus.doc.startEditMode()){
            var message = {authkey:cma.TheApp.authkey};
            cma.TheApp.socket.send(JSON.stringify(message));
            button.removeClass("cma-edit-mode-off").addClass("cma-edit-mode-on");
            button.html('<div style="padding-top:7px">on</div>'); 
          }
        }
      },function(){
        alert("you don't have the rights to edit documents in this corpus!");
      });
    });

    this.editSaveButton.init(function(button,caller){
      var name = cma.ui.get('cma-edit-input',caller.view).val();
      cma.TheApp.callProxy('corpus/'+cma.TheApp.corpus.cid+'/document/'+cma.TheApp.corpus.doc.did+'/_save',{name:name},function(data){
        if(data.success){
          cma.ui.get('cma-edit-input',caller.view).val("");
          button.success();
          caller.updateRevisionList();
        }else{
          button.error();
        }
      },function(){
        button.error();
      },'json');
    },this);
  }

  cma.toolbox.Edition.templateMain = '<div id="cma-tmp">'+
    '<div id="cma-edit-mode-switch" class="cma-edit-mode-off"><div style="padding-top:7px">off</div></div>'+
    '<div id="cma-edit-revisions"></div>'+
    '<input id="cma-edit-input" type="text">'+
    '<span id="cma-edit-save-submit" ></span>'+
    '</div>';

  function formatDate(name,value,data){
    return (new Date(value)).toString();
  }

  function unformatDate(value){
    return (new Date(value)).getTime();
  }

  function formatTAGs(name,value,data){
    if(value)
      return value.join(' | ');
    return "";
  }

  function unformatTAGs(value){
    if(value)
      return value.split('|');
    return null;
  }

}(window.cma = window.cma || {}));
(function(cma){


  cma.toolbox = cma.toolbox || {};


  cma.toolbox.MgwikiSentences = function(){
    this.initView();
  };

  cma.toolbox.MgwikiSentences.prototype.initView = function(){
    var me = this;
    this.view = jQuery(cma.toolbox.MgwikiSentences.templateMain);
    var table = new cma.ui.Table({headers:[
      {title:"Sentence",nameid:"title",format:formatTitle},
      {title:"Graphs",nameid:"graphs",format:formatGraphs},
    ]});
    me.listSentences = table;
    
    
  };
  
  cma.toolbox.MgwikiSentences.prototype.init = function(){
    var me = this;
    this.view.html(me.listSentences.view);
    data = {};
    data.func = "listSentences";
    dataType = 'json';
    jQuery.ajax({
      type: 'POST', 
      url: cma.TheApp.ws_url+'func',
      data: data,
      dataType : dataType,
      success: function(data, textStatus, jqXHR) {
        me.listSentences.update(data);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  };


  cma.toolbox.MgwikiSentences.templateMain = '<div id="cma-mgwiki-sentences"></div>';

  cma.toolbox.MgwikiSentences.showGraph = function(data){
    var width = cma.TheApp.view.width();
    var height = cma.TheApp.view.height();
    var offset = cma.TheApp.view.offset();
    var point = {x:offset.left+width/2-400,y:offset.top+height/2-200};
    var uniqid = Date.now();
    var div = jQuery('<div><div id="graph-container-'+uniqid+'"></div></div>');
  
    var box = new depgraphlib.Box({closeButton:true,draggable:true}).setContent(div).open(point);
    var depGraph = new depgraphlib.DepGraph(jQuery("#graph-container-"+uniqid),data,{"uid":uniqid,"maxwidth":800});
    var viewer = depGraph.viewer;
    depGraph.wsurl = cma.TheApp.ws_url+"graph";
    if(depGraph.options.viewmode == 'full'){
      viewer.noBorders();
    }
    point.x = offset.left+width/2-viewer._width/2;
    point.y = offset.top+height/2-viewer._height/2;
    box.move(point);
  }
 
  function formatTitle(name,value,data){
    var html = jQuery(value).attr("target","_blank");
    return html;
  }
  

  function formatGraphs(name,value,data){
    var html = "<ul>";
    for(i in value){
      var graph = value[i];
      html += '<li gid="'+graph['gid']+'">graph '+graph['title']+"</li>";
    }
    html += "</ul>";
    html = jQuery(html);
    jQuery('li',html).click(function(element){
      jQuery.ajax({
        type: 'GET', 
        url: cma.TheApp.options.host_url+'/d3js/'+jQuery(this).attr('gid')+'/edit/export/json',
        dataType : 'json',
        success: function(data, textStatus, jqXHR) {
          cma.toolbox.MgwikiSentences.showGraph(data);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          alert(textStatus);
        }
      });
    });
    return html;
  }

}(window.cma = window.cma || {}));
(function(cma){
  
  cma.toolbox = cma.toolbox || {};

  cma.toolbox.Misc = function(){
    this.initView();
  }

  cma.toolbox.Misc.prototype.initView = function(){
    var me = this;
    this.view = jQuery(cma.toolbox.Misc.templateMain);

    jQuery('#cma-misc-download-results',this.view).click(function(){
      cma.TheApp.corpus.callAndDo('download',{},function(data){
          window.open(data.url);
      });
    });

    jQuery('#cma-misc-delete',this.view).click(function(){
      var r = window.confirm(cma.text.get("delete-corpus-confirm"));
      if(r){
        cma.TheApp.corpus.callAndDo('delete',{},function(data){
          console.log(data);
          cma.TheApp.goTo("");
        });
      }
      
    });

    /*if(cma.TheApp.corpus.status != "waiting"){
      jQuery('#cma-process-default-analysis',this.view).hide();
    }*/
    
  };

  cma.toolbox.Misc.templateMain = '<div><h3>Misc</h3>'+
    '<h4>Download</h4>'+
    '<input type="button" id="cma-misc-download-results" value="Download Analysis Results">'+
    '<h4>Other</h4>'+
    '<input type="button" id="cma-misc-delete" value="Delete this corpus."><br>Warning this can\'t be undone!'+
    '</div>';
  
}(window.cma = window.cma || {}));
(function(cma){
  
  cma.toolbox = cma.toolbox || {};

  cma.toolbox.Permissions = function(){
    this.initView();
  }

  cma.toolbox.Permissions.prototype.initView = function(){
    var me = this;
    this.view = jQuery(cma.toolbox.Permissions.templateMain);

    this.autocomplete = new cma.ui.AutoComplete(cma.ui.get("cma-permissions-add-user-input",this.view),cma.TheApp.options.ws.userlist);
    
    this.table = new cma.ui.Table({headers:[{title:"User",nameid:"user"},
      {title:"Read",nameid:"read",format:function(name,value,data){return formatPermission(name,value,data);},unformat:function(value){return unFormatPermission(value);}},
      {title:"Write",nameid:"write",format:function(name,value,data){return formatPermission(name,value,data);},unformat:function(value){return unFormatPermission(value);}},
      {title:"Admin",nameid:"admin",format:function(name,value,data){return formatPermission(name,value,data);},unformat:function(value){return unFormatPermission(value);}}]}
      );

    cma.ui.get('cma-permissions-table',this.view).html(this.table.view);

    this.addUserPermissionsbutton = new cma.ui.CallbackButton("Add User Permissions");
    cma.ui.get('cma-permissions-add-user-submit',this.view).append(this.addUserPermissionsbutton.view);
    
    this.savebutton = new cma.ui.CallbackButton("Update Permissions");
    cma.ui.get('cma-permissions-update-button',this.view).append(this.savebutton.view);

    this.update();
  };

  cma.toolbox.Permissions.prototype.update = function(doAfter){
    var me = this;
    cma.TheApp.corpus.getAndDo("permissions",{},function(data){
      
      me.table.update(data);

      jQuery('input[type="checkbox"]',me.view).change(function() {
        regex = /cma-permissions-(\w+)-input-(.*)/;
        var match = regex.exec(this.id);
        if (match != null) {
          permissionsInputsClean(match[1],match[2],this.checked);
        }

      });

      if(doAfter != null){
        doAfter.call();
      }
    });
  };

  cma.toolbox.Permissions.prototype.haveRights = function(activity,okcallback,notokcallback){
    var me = this;
    if(!this.table.originalData){
      this.update(function(){me.haveRights(activity,okcallback,notokcallback);})
      return;
    }

    var username = cma.TheApp.options.user.name;
    // theses permissions are only ui level. server check of identity and permissions is done for
    // any actions.
    if(username == 'admin'){
      okcallback.call();
      return true; 
    }
    var rights = null;
    for(var i in this.table.originalData){
      if(this.table.originalData[i].user == username){
        rights = this.table.originalData[i];
        break;
      }
    }
    if(!rights){
      rights = this.table.originalData[0];
    }

    if(activity == 'process'){
      if(rights.admin.value){
        okcallback.call();
        return true;
      }
    }else if(activity == 'edit'){
      if(rights.write.value){
        okcallback.call();
        return true;
      }
    }else if(activity == 'updatePermissions'){
      if(rights.admin.value){
        okcallback.call();
        return true;
      }
    }else{
      alert('error : unknown activity');  
    }
    
    if(notokcallback!=null){
      notokcallback.call();
    }
    return false;
  }

  cma.toolbox.Permissions.prototype.submit = function(button){
    var me = this;
    var data = this.table.getData();
    for(var i in data){
      data[i].cid = cma.TheApp.corpus.cid;
      data[i].rights = data[i].admin ? 9 : (data[i].write ? 5 : (data[i].read ? 2 : 0));
    }
    cma.TheApp.corpus.callAndDo('func',{func:"updatePermissions",permissions:data},function(data){
        console.log(data);
        button.success();
        me.update();
      },function(data){
        console.log(data);
        button.error();
      });
  };
  
  cma.toolbox.Permissions.prototype.init = function(){
    var me = this;

    this.haveRights('updatePermissions',function(){},function(){
      cma.ui.get('cma-permissions-add-user',me.view).hide();
      cma.ui.get('cma-permissions-update-button',me.view).hide();
      jQuery('input',me.view).prop('disabled', true);
    });


    this.addUserPermissionsbutton.init(function(button,caller){
      var read = cma.ui.get('cma-permissions-add-user-read',caller.view)[0].checked;
      var write = cma.ui.get('cma-permissions-add-user-write',caller.view)[0].checked;
      var admin = cma.ui.get('cma-permissions-add-user-admin',caller.view)[0].checked;
      var rights = admin ? 9 : (write ? 5 : (read ? 2 : 0));
      var permission = {
        user:cma.ui.get('cma-permissions-add-user-input').val(),
        rights:rights
      }
      cma.TheApp.corpus.callAndDo('func',{func:"updatePermissions",permissions:[permission]},function(data){
        if(data.error){
          alert(data.error);
          button.error();
        }else{
          caller.update();
          button.success();  
        }
      });
    },this);

    this.savebutton.init(function(button,caller){ 
      me.submit(button,caller);
    },this);

    this.autocomplete.init();
  };

  cma.toolbox.Permissions.templateMain = '<div id="cma-tb-permission">'+
    '<div id="cma-permissions-table"></div>'+
    '<div id="cma-permissions-update-button"></div>'+
    '<div id="cma-permissions-add-user">'+
    '<input id="cma-permissions-add-user-input" type="text">'+
    '<input id="cma-permissions-add-user-read" checked name="read" value="r" type="checkbox">read'+
    '<input id="cma-permissions-add-user-write" name="write" value="w" type="checkbox">write'+
    '<input id="cma-permissions-add-user-admin" name="admin" value="a" type="checkbox">admin'+
    '<span id="cma-permissions-add-user-submit"></span>'+
    '</div></div>';

  function formatPermission(name,item,data){
    var lock = (name == "write" && data.admin.value) || (name == "read" && data.write.value);
    return '<input id="cma-permissions-'+name+'-input-'+data.user+'" '+(item.value?' checked ':'')+((!item.editable || lock)?' disabled ':'')+' type="checkbox">';
  }

  function unFormatPermission(value){
    return jQuery(value)[0].checked;
  }

  function permissionsInputsClean(name,user,value){
    var read = jQuery('input[id="cma-permissions-read-input-'+user+'"]');
    var write = jQuery('input[id="cma-permissions-write-input-'+user+'"]');
    if(name == "admin"){
      if(value){
        write.prop("checked",true);
        read.prop("checked",true);
      }
      write.prop("disabled",value);
      read.prop("disabled",value);
    }else if(name == "write"){
      if(value){
        read.prop("checked",true);
      }
      read.prop("disabled",value);
    }
  }

}(window.cma = window.cma || {}));

(function(cma){
  
  cma.toolbox = cma.toolbox || {};

  cma.toolbox.Process = function(){
    this.initView();
  }

  cma.toolbox.Process.prototype.initView = function(){
    var me = this;
    if(this.view){
      this.view.html(cma.toolbox.Process.templateMain)
    }else{
      this.view = jQuery(cma.toolbox.Process.templateMain);  
    }
    


    
    
    cma.TheApp.corpus.getAndDo('tasks',{},function(data){
      console.log(data);
      var tasks = cma.TheApp.corpus.setTasks(data);
      cma.ui.get('cma-corpus-log').html('');
      cma.ui.get('cma-corpus-log').append(tasks);
      cma.utils.promise(function(){
        me.defaultAnalysis();
        //me.indexation();
        //me.termExtraction();
      },
        [{object:cma.TheApp.corpus,field:"taskstatus"}]
      );
    });

    

    /*if(cma.TheApp.corpus.status != "waiting"){
      jQuery('#cma-process-default-analysis',this.view).hide();
    }*/
    
  };

  cma.toolbox.Process.prototype.defaultAnalysis = function(){
    var me = this;
    var canStartDefaultProcess = false;
    var finished = false;
    for(var i=0;i<cma.TheApp.corpus.taskstatus.length;++i){
      for(var j=0;j<cma.TheApp.corpus.taskstatus[i].length;j++){
        var task = cma.TheApp.corpus.taskstatus[i][j];
        if(task.name == "raw2dag" && task.status == "waiting"){
          canStartDefaultProcess = true;
          break;
        }
        if(task.name == "retrieveResults" && task.status != "waiting"){
          finished = true;
          break;
        }
      }
    }

    this.view.append('<h3 class="default-analysis">Default Analysis</h3>');

    if(!canStartDefaultProcess){
      if(!finished){
        this.view.append('processing');
      }else{
        this.view.append('finished');
      }
      return;
    }

    this.view.append('<input type="button" id="cma-process-default-analysis" value="Launch process">');

    var options = '<label style="display:inline-block;">Parse format output :</label>'+
      '<select id="cma-process-output-format-select" style="margin-left:10px;" name="format-type">'+
      '<option selected="true" value="conll">conll</option>'+
      '<option value="dis_xmldep">depxml</option>'+
      '<option value="depconll">depconll</option>'+
      '<option value="passage">passage</option>'+
      '</select>'+
      '<br>'+
      '<label style="display:inline-block;">Term Extraction & Semantic Distribution :</label>'+
      '<input id="cma-process-termextract" type="checkbox" style="margin-left:10px;" name="termextract">';
    var foldableDiv = new cma.ui.FoldableDiv(options);
    foldableDiv.setTitle('Options');
    jQuery('h3.default-analysis',this.view).after(foldableDiv.view);

    jQuery('#cma-process-termextract',me.view).change(function(){
      var select = jQuery('#cma-process-output-format-select',me.view);
      select[0].value = "passage";
      if(this.checked){
        select[0].disabled = true;
      }else{
        select[0].disabled = false;
      }
      
    });

    jQuery('#cma-process-default-analysis',this.view).click(function(){
      var select = jQuery('#cma-process-output-format-select',me.view);
      var format = select[0].options[select[0].selectedIndex].value;
      var settings = {pid:1,reset:true,format:format};
      var termextract = jQuery('#cma-process-termextract',me.view);
      if(termextract[0].checked){
        settings.termextract = true;
      }
      cma.TheApp.callProxy('corpus/'+cma.TheApp.corpus.cid+'/_process',settings,function(data){
        me.initView();
      });
    });
  };

  cma.toolbox.Process.prototype.indexation = function(){
    this.view.append('<h3>Indexation</h3><input type="button" id="cma-process-index" value="Index results">');
    jQuery('#cma-process-index',this.view).click(function(){
      
      cma.TheApp.callProxy('corpus/'+cma.TheApp.corpus.cid+'/_index',{pid:2,reset:true},function(data){
        console.log(data);
     });
    });
  };

  cma.toolbox.Process.prototype.termExtraction = function(){
    var me = this;
    var canStartTermProcess = false;
    for(var i=0;i<cma.TheApp.corpus.taskstatus.length;++i){
      for(var j=0;j<cma.TheApp.corpus.taskstatus[i].length;j++){
        var task = cma.TheApp.corpus.taskstatus[i];
        if(true || task.name == "retrieveResults" && task.status == 0){
          canStartTermProcess = true;
          break;
        }
      }
    }

    if(!canStartTermProcess){
      return;
    }
    this.view.append('<h3>Term Extraction</h3><input type="button" id="cma-process-term-extraction" value="Launch term extraction.">');
    jQuery('#cma-process-term-extraction',this.view).click(function(){
      cma.TheApp.callProxy('corpus/'+cma.TheApp.corpus.cid+'/_process',{pid:3,reset:true},function(data){
        me.initView();
      });
    });
  };

  cma.toolbox.Process.templateMain = '<div>'+
    '</div>';
  
}(window.cma = window.cma || {}));

(function(cma){


  cma.toolbox = cma.toolbox || {};

  /**
  * 
  * Search features:
  *   - sort results
      - filter results
      - limit result to 100 (performance) => mode query all après
      - go to sentence
      - export result
      - export query to a webservice (return url) => create in db url for query handler (push new changes, get query result)
  *
  *
  *
  *
  *
  **/

  cma.toolbox.Search = function(){
    this.max = 20;
    this.from = 0;
    this.getTagSet();
    this.initView();
  };

  cma.toolbox.Search.prototype.initView = function(){
    var me = this;
    this.view = jQuery(cma.toolbox.Search.templateMain);
    var table = new cma.ui.Table({headers:[{title:"ID",nameid:"sid",format:function(name,value){return formatSID(name,value);},unformat:function(value){return unFormatSID(value);}},
      {title:"Sentence",nameid:"sentence",format:function(name,value,data){return formatSentence(name,value,data);},unformat:function(value){return unFormatSentence(value);}},
      {title:"Score",nameid:"_score"},
      {title:"Document",nameid:"did",format:function(name,value){return formatDocument(name,value);},unformat:function(value){return unFormatDocument(value);}}]
    });
    me.resultsTable = table;

    this.searchbutton = new cma.ui.CallbackButton("Search");
    cma.ui.get('cma-search-submit-div',this.view).append(this.searchbutton.view);

    var searchTypes = '<select name="type">'
        //+'<option value="id">id</option>'
        +'<option value="dpath" selected>dpath</option>'
        +'<option value="sentence">sentence keywords</option>'
        +'</select>';

    cma.ui.get('cma-search-type',this.view).html(searchTypes);

    var textarea = cma.ui.get('cma-search-input',this.view);
    this.textarea = textarea;

    var infoDpath = 'Description of dpath language here..';
    var foldableDiv = new cma.ui.FoldableDiv(infoDpath);
    foldableDiv.setTitle('Help');
    textarea.after(foldableDiv.view);


    var strategies = this.autocompleteStrategies();
    textarea.textcomplete(strategies);

    textarea.on("keypress",function(e){
      if(e.keyCode == 9){
        e.preventDefault();
        console.log("autocomplete");

      }
      cma.utils.getTextareaCarretPosition(this);
    });

    this.macroLib = new cma.toolbox.SearchLib(textarea);
    this.history = new cma.toolbox.SearchHistory(this);

    jQuery('.cma-search-insert-macro',this.view).click(function(){
      me.macroLib.show();
    });

    jQuery('.cma-search-history',this.view).click(function(){
      me.history.show();
    });

    jQuery('.cma-search-visual',this.view).click(function(){
      me.visualSignatureForm();
    });

    jQuery('.cma-search-save-macro',this.view).click(function(){
      var val = textarea[0].textContent;
      me.macroLib.elementForm({query:val});
    });

  };

  cma.toolbox.Search.prototype.getTagSet = function(){
    var me = this;
    var data = {
      proxy_url:"corpus/"+cma.TheApp.corpus.cid+"/_tagset"
    };
    jQuery.ajax({
      type: 'POST', 
      url: cma.TheApp.ws_url+'frmgcm_proxy',
      data: data,
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        if(data.name){
          me.tagset = data;
          me.props = [];
          for (var i in data.node) {
            me.props.push(i);
          };
          for (var i in data.edge) {
            me.props.push(i);
          };  
        }
        
      },
      error: function(jqXHR, textStatus, errorThrown) {
      }
    });
  };

  cma.toolbox.Search.prototype.autocompleteStrategies = function(){
    var me = this;
    var strategies = [];
    strategies.push({
      match: /@(\w+)\s*=(")?(\w*)$/,
      index:3,
      search: function (term, callback) {
        var prop = this.currentMatch[1];
        var propVal = [];
        if(me.tagset && me.tagset.edge[prop]){
          propVal = me.tagset.edge[prop];
        }else if(me.tagset && me.tagset.node[prop]){
          propVal = me.tagset.node[prop];
        }
        
        callback(jQuery.map(propVal, function (element) {
                return element.indexOf(term) === 0 ? element : null;
            }));
      },
      replace: function (value) {
        return '@$1="' + value +'"';
      },
      cache: true,
      maxCount:99
    });
    strategies.push({
      match: /(\$)(\w*)$/,
      search: function (term, callback) {
        var macros = me.macroLib.macros;
        callback(jQuery.map(macros, function (element) {
                return element.indexOf(term) === 0 ? element : null;
            }));
      },
      replace: function (value) {
        return '$1' + value + ' ';
      },
      cache: true,
      maxCount:99
    });
    strategies.push({
      match: /(@)(\w*)$/,
      search: function (term, callback) {
        var props = me.props || [];
        callback(jQuery.map(props, function (element) {
                return element.indexOf(term) === 0 ? element : null;
            }));
      },
      replace: function (value) {
        return '$1' + value ;
      },
      cache: true,
      maxCount:99
    });

    return strategies;
  };


  cma.toolbox.Search.prototype.init = function(did){
    var me = this;
    this.did = did;

    this.searchbutton.init(function(button,caller){ 
      var textarea = cma.ui.get('cma-search-input',me.view);
      var val = textarea[0].textContent;
      me.from = 0;
      var select = jQuery('select',cma.ui.get('cma-search-type',me.view));
      var type = select[0].options[select[0].selectedIndex].value;
      me.search(val,type,me.from,me.max,function(){button.success();},function(){button.error();});
    },this);

    
  };

  cma.toolbox.Search.prototype.search = function(query,searchType,from,max,callbacksuccess,callbackerror){
    var me = this;
    cma.TheApp.corpus.callAndDo('_search',{query:query,searchType:searchType,from:from,max:max,did:this.did},function(data){
      me.processResults(data);

      var total = data.output.total || ((data.output.hits)?data.output.hits.total:0);
      var header = "Total : "+total+'<input id="cma-search-export" style="display:inline-block; margin-left:15px;" type="button" value="Export">';

      var resulthtml = '<div><input id="cma-search-result-prev" type="button" value="prev"><input id="cma-search-result-next" type="button" value="next"></div>';
      var resultObj = jQuery(resulthtml).prepend(me.resultsTable.view).prepend(header);
      cma.ui.get('cma-search-result',me.view).html(resultObj);
      cma.ui.get('cma-search-export',me.view).click(function(){
        var postVal = {cid:cma.TheApp.corpus.cid,export:true,query:query,searchType:searchType,from:from};
        if(me.did){
          postVal.did = me.did;
        }
        depgraphlib.windowOpenPost(postVal,cma.TheApp.ws_url+"_search");
      },null,'html');
      cma.ui.get('cma-search-result-next',me.view).click(function(){
        me.from+=me.max;
        me.search(query,searchType,me.from,me.max);
      });
      cma.ui.get('cma-search-result-prev',me.view).click(function(){
        me.from-=me.max;
        me.search(query,searchType,me.from,me.max);
      });
      
      if((me.from+me.max)>me.total){
        cma.ui.get('cma-search-result-next',me.view).hide();  
      }
      if(me.from==0){
        cma.ui.get('cma-search-result-prev',me.view).hide();  
      }
      if(callbacksuccess){
        callbacksuccess.call();  
      }
      
      
    },callbackerror);
  };

  cma.toolbox.Search.prototype.processResults = function(data){
    if(data.output && data.output.hits){
      var hits;
      if(data.output.dpath){
        hits = data.output.hits;
        this.total = data.output.total;
      }else{
        hits = data.output.hits.hits;
        this.total = data.output.hits.total;
      }
      for(var i in hits){
        var hit = hits[i];
        hit.sid = hit._source.sid;
        hit.did = hit._source.did;
        if(hit._source.sentence){
          hit.sentence = hit._source.sentence.substr(0,200);  
        }else{
          hit.sentence = "error : could not find sentence content";
        }
        
        delete hit._index; 
        delete hit._type; 
        delete hit._id; 
      }  
      this.resultsTable.update(hits);
    }
    console.log(data);
  };

  cma.toolbox.Search.templateMain = '<div><div id="cma-search-box">'+
    '<div id="cma-search-type"></div>'+
    '<div class="cma-search-utils"><div class="cma-search-insert-macro"></div><div class="cma-search-save-macro"></div><div class="cma-search-visual"></div><div class="cma-search-history"></div></div>'+
    '<div id="cma-search-input" contenteditable=true></div> '+
    //'<textarea id="cma-search-input" rows="4" cols="45"></textarea>'+
    '<div id="cma-search-submit-div"></div>'+
    '</div>'+
    '<div id="cma-search-result"></div></div>';

  function formatDocument(name,value){
    for (var did in cma.TheApp.corpus.paths) {
      if(cma.TheApp.corpus.paths[did].scid == value){
        return '<a did="'+value+'" href="#corpus/'+cma.TheApp.corpus.cid+'/view/'+did+'">'+cma.TheApp.corpus.paths[did].name+'</a>';  
      }
    };
    return value;
  }

  function unFormatDocument(value){
    return jQuery(value).attr('did');
  }

  function formatSID(name,value){
    return value;
    //return (value.indexOf('E') == 0)?value:'E'+String(value);
  }

  function unFormatSID(value){
    return value;
  }

  function formatSentence(name,value,data){
    var sid = data['sid'];//(data['sid'].indexOf('E') == 0)?data['sid']:'E'+String(data['sid']);
    var html =  'Sentence : <a style="cursor:pointer;">'+value+'</a>';
    var obj = jQuery(html).click(function(){
      cma.TheApp.corpus.doc.showGraph("frmgwiki",sid);
    });
    return obj;
  }

  function unFormatSentence(value){
    return jQuery(value).html();
  }

  cma.toolbox.Search.prototype.visualSignatureForm = function(){
    var me = this;
    var width = cma.TheApp.view.width();
    var height = cma.TheApp.view.height();
    var offset = cma.TheApp.view.offset();
    
    var uniqid = Date.now();
    var div = jQuery('<div><div id="graph-container-'+uniqid+'"></div></div>');
  
    // Box || r.window => base sentence
    // data creation (+datamodel)
    
    var sentence = prompt("Please enter a sentence.","");

    if (sentence==null || sentence == ""){
      sentence =".";
    }

    var data = {
      graph:{
        words:[]
      }
    };


    var words = sentence.split(/\s+/);
    for (var i = 0 ; i < words.length ; i++) {
      data.graph.words.push({id:i+1,label:words[i]});
    };
    

    var box = new depgraphlib.Box({closeButton:true,draggable:true}).setContent(div).open();
    box.setHeader("test");
    var depGraph = new depgraphlib.DepGraph(jQuery("#graph-container-"+uniqid),data,{"uid":uniqid,viewmode:"full",maxwidth:cma.TheApp.view.width()-100});
    var viewer = depGraph.viewer;
    depGraph.wsurl = cma.TheApp.ws_url+"graph";
    if(depGraph.options.viewmode == 'full'){
      viewer.noBorders();
    }

    var boxcontent = jQuery('.depgraphlib-box-content',box.object);
    var dgwidth = viewer.mainpanel.width();
    var dgheight = viewer.mainpanel.height();
    boxcontent.width(dgwidth+16);
    boxcontent.height(dgheight+16);
    boxcontent.resizable({
      stop:function(e,data){
        depGraph.setWidthLimitedViewMode(data.size.width-16,true);
        viewer.setHeight(data.size.height-46);
      }
    });

    var point = {x:offset.left+width/2-dgwidth/2,y:offset.top+height/2-dgheight/2};
    box.move(point);

    depGraph.sentenceLink = '#';

    depGraph.sentence = 'sentence content here';
    depGraph.editObject.mode['default'].save = depgraphlib.EditObject.DefaultModeLib.save;
    depGraph.dataFormat = 'json';

    var dataModel = {
      words:[
      {name:"label",view:'label'},
      {name:"lemma",view:"sublabel/0"},
      {name:"pos",values:me.tagset.node["pos"],view:"sublabel/1"}
      ],
      links:[
      {name:"label",values:me.tagset.edge["label"],view:"label"},
      ],
    };

    depGraph.editObject.setDataModel(dataModel);
    

    viewer.setFixedToolbar();
    var formatTmp = (me.model.tagset.node["pos"])?"conll":"depxml";
    viewer.addToolbarItem({name:'export_signature',callback:function(){
      var ddata = depGraph.cleanData();
      jQuery.ajax({
        type: 'POST', 
        url: cma.TheApp.ws_url+"graph",
        data: {
          action:'_getSig',
          data:ddata,
          format:formatTmp
        },
        dataType : 'text',
        success: function(data, textStatus, jqXHR) {
          console.log(data);
          var prev = me.textarea.html();
          me.textarea.html(prev + data);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          alert(textStatus);
        }
      });
    },tooltip:'Export Signature',style:'mgwiki-export','static':true});

    depGraph.editObject.setEditMode('default',false);
    /*depGraph.editObject.mode[depGraph.editObject.editMode].save = function(depgraph){
    }*/
  };

  /*********************************************************************/
  /*                 DPath Search Library                              */
  /*********************************************************************/

  cma.toolbox.SearchLib = function(textarea){
    this.view = jQuery(cma.toolbox.SearchLib.template);
    this.init();
    this.textarea = textarea;
  };

  cma.toolbox.SearchLib.prototype.initView = function(){
    if(this.currentSelect){
      this.currentSelect.removeClass('cma-searchlib-list-item-selected');
    }
    this.currentHover = null;
    this.currentSelect = null;
    var me = this;
    jQuery('.cma-searchlib-list',this.view).on("mouseover",function(e){
      var selection = jQuery(e.target).parents('tr');
      if(me.currentHover){
        me.currentHover.removeClass('cma-searchlib-list-item-hover');
      }
      if(selection.hasClass('cma-searchlib-list-title')){
        me.currentHover = null;
        return;
      }
      me.currentHover = selection;
      selection.addClass('cma-searchlib-list-item-hover');
    });
    jQuery('.cma-searchlib-list',this.view).on("mouseout",function(e){
      if(me.currentHover){
        me.currentHover.removeClass('cma-searchlib-list-item-hover');
        me.currentHover = null;
      }
    });
    jQuery('.cma-searchlib-list',me.view).on("click",function(e){
      var selection = jQuery(e.target).parents('tr');
      if(selection.hasClass('cma-searchlib-list-title')){
        return;
      }
      if(me.currentSelect){
        me.currentSelect.removeClass('cma-searchlib-list-item-selected');
      }
      if(me.currentSelect == selection){
        return;
      }
      me.currentSelect = selection;
      selection.addClass('cma-searchlib-list-item-selected');
    });
    jQuery('.cma-searchlib-insert',me.view).click(function(){
      if(me.currentSelect != null){
        var children = me.currentSelect.children();
        var macro = "$"+children[0].innerHTML;
        var val = me.textarea.html();
        me.textarea.html(val+'<span>'+macro+'</span>');
      }
    });
  };

  cma.toolbox.SearchLib.prototype.init = function(){
    var me = this;
    var data = {
      proxy_url:"dpathlib/_all"
    };
    jQuery.ajax({
      type: 'POST', 
      url: cma.TheApp.ws_url+'frmgcm_proxy',
      data: data,
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        me.macros = [];
        for (var i = data.length - 1; i >= 0; i--) {
          me.macros.push(data[i].name);
        };
        me.fillTable(data);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        
      }
    });
  };

  /**
   * Fill the table with data retrieved by the server
   * @param  {[type]} data [description]
   * @return {[type]}      [description]
   */
  cma.toolbox.SearchLib.prototype.fillTable = function(data){
    var me = this;
    var tbody = jQuery('.cma-searchlib-list-body',this.view);
    tbody.html("");
    for (var i = data.length - 1; i >= 0; i--) {
      tbody.append('<tr><td>'+data[i].name+'</td><td>'+data[i].query+'</td><td>'+data[i].description+'</td><td>'+data[i].user+'</td><td>'+getActions(data[i])+'</td></tr>');
    };
    jQuery('.cma-searchlib-action-delete',me.view).click(function(e){
      var selection = jQuery(e.target).parents('tr');
      var children = selection.children();
      var macro = children[0].innerHTML;
      var r = window.confirm(cma.text.get("delete-macrodef-confirm"));
      if(r){
        me.deleteElement(macro,function(){
          selection.remove();
        });
      }
      
    });
    jQuery('.cma-searchlib-action-edit',me.view).click(function(e){
      var selection = jQuery(e.target).parents('tr');
      var children = selection.children();
      var ispublic = jQuery(children[4]).find('.cma-searchlib-action-public');
      var publicv = false;
      if(ispublic.length){
        publicv = true;
      }
      var macro = {
        name:children[0].innerHTML,
        query:children[1].innerHTML,
        description:children[2].innerHTML,
        publicv:publicv
      };
      me.elementForm(macro);
    });
    jQuery('.cma-searchlib-action-public',me.view).click(function(e){
      var selection = jQuery(e.target).parents('tr');
      var children = selection.children();
      var macro = {
        name:children[0].innerHTML,
        query:children[1].innerHTML,
        description:children[2].innerHTML,
        publicv:false
      };
      me.syncElement(macro.name,macro,function(data){
        if(data.success){
          me.init();
        }else{
          alert(data.message);
        }
      });
    });
    jQuery('.cma-searchlib-action-private',me.view).click(function(e){
      var selection = jQuery(e.target).parents('tr');
      var children = selection.children();
      var macro = {
        name:children[0].innerHTML,
        query:children[1].innerHTML,
        description:children[2].innerHTML,
        publicv:true
      };
      me.syncElement(macro.name,macro,function(data){
        if(data.success){
          me.init();
        }else{
          alert(data.message);
        }
      });
    });
  };

  function getActions(macro){
    var actions = "";
    if(macro.user == cma.TheApp.user){
      actions += '<span class="cma-searchlib-action cma-searchlib-action-edit"></span><span class="cma-searchlib-action cma-searchlib-action-delete"></span>';
    }
    if(macro.ispublic){
      actions += '<span class="cma-searchlib-action cma-searchlib-action-public"></span>';
    }else{
      actions += '<span class="cma-searchlib-action cma-searchlib-action-private"></span>';
    }
    return actions;
  }

  /**
   * expand a macro to the query
   * @param  {[type]} name [description]
   * @return {[type]}      [description]
   */
  cma.toolbox.SearchLib.prototype.expand = function(name){

  };

  /**
   * show the form to add a new macro to the library
   * @param {[type]} name  [description]
   * @param {[type]} query [description]
   */
  cma.toolbox.SearchLib.prototype.elementForm = function(macroInfo){
    var me = this;
    var width = jQuery(window).width();
    var height = jQuery(window).height();
    var box = new depgraphlib.Box({closeButton:true,draggable:true,position:{x:width/2-200,y:height/2-200}});
    box.setFixedSize(400,400);
    box.open();
    box.setHeader("DPath Query Library");

    var $el = jQuery('.depgraphlib-box-content',box.object);
    $el.append(cma.toolbox.SearchLib.addNewFormTemplate);

    if(macroInfo.query){
      $el.find('.cma-searchlib-addnewform-query').val(macroInfo.query);  
    }

    if(macroInfo.description){
      $el.find('.cma-searchlib-addnewform-desc').val(macroInfo.description);  
    }

    if(macroInfo.name){
      $el.find('.cma-searchlib-addnewform-name').val(macroInfo.name);  
    }

    if(macroInfo.publicv){
      $el.find('.cma-searchlib-addnewform-public').prop("checked",true);  
    }
    
    $el.find('.cma-searchlib-addnewform-cancel').click(function(){
      box.destroy();
      
    });

    $el.find('.cma-searchlib-addnewform-submit').click(function(){
      var owner = cma.TheApp.user || 'unknown';
      var query = $el.find('.cma-searchlib-addnewform-query').val();
      var name = $el.find('.cma-searchlib-addnewform-name').val();
      var desc = $el.find('.cma-searchlib-addnewform-desc').val();

      var publicv = $el.find('.cma-searchlib-addnewform-public')[0].checked;
      
      var macroDefintion = {
        name:name,
        query:query,
        owner:owner,
        description:desc,
        publicv:publicv
      };
      me.syncElement(macroInfo.name,macroDefintion,function(data){
        if(data.success){
          me.init();
          box.destroy();  
        }else{
          alert(data.message);
        }
      });
      
    });
  };

  cma.toolbox.SearchLib.prototype.deleteElement = function(macroName,callbackResult){
    var proxy_url = "dpathlib/"+macroName+"/_delete";
    var me = this;
    var data = {
      proxy_url:proxy_url,
      name:macroName,
    };
    jQuery.ajax({
      type: 'POST', 
      url: cma.TheApp.ws_url+'frmgcm_proxy',
      data: data,
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        callbackResult.call(me,data);
        console.log(data);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        
      }
    });
  };

  cma.toolbox.SearchLib.prototype.syncElement = function(macroName,macroDefinition,resultCallback){
    var proxy_url = "dpathlib/_create";
    if(macroName){
      proxy_url = "dpathlib/"+macroName+"/_edit";
    }
    var me = this;
    var data = {
      proxy_url:proxy_url,
      name:macroDefinition.name,
      query:macroDefinition.query,
      owner:macroDefinition.owner,
      description:macroDefinition.description,
      publicv:macroDefinition.publicv
    };
    console.log(data);
    jQuery.ajax({
      type: 'POST', 
      url: cma.TheApp.ws_url+'frmgcm_proxy',
      data: data,
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        resultCallback.call(me,data);
        console.log(data);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        
      }
    });
  };

  cma.toolbox.SearchLib.addNewFormTemplate = '<div class="cma-searchlib-addnewform">'+
    '<h2>Save Query macro</h2>'+
    '<fieldset>'+
      '<legend>Required:</legend>'+
      'Name: <input class="cma-searchlib-addnewform-name" type="text"><br>'+
      'Query: <input class="cma-searchlib-addnewform-query" type="text"><br>'+
    '</fieldset>'+
    '<fieldset>'+
      '<legend>Optional:</legend>'+
      'Description: <textarea class="cma-searchlib-addnewform-desc"></textarea><br>'+
      'Public Visibility: <input type="checkbox" class="cma-searchlib-addnewform-public"><br>'+
    '</fieldset>'+
    '<input type="submit" class="cma-searchlib-addnewform-submit" value="Save"><input type="submit" class="cma-searchlib-addnewform-cancel" value="Cancel">'
  '</div>';

  /**
   * delete a macro
   * @param  {[type]} name [description]
   * @return {[type]}      [description]
   */
  cma.toolbox.SearchLib.prototype.remove = function(name){

  };

  /**
   * show the macro library
   * @return {[type]} [description]
   */
  cma.toolbox.SearchLib.prototype.show = function(){
    var box = new depgraphlib.Box({closeButton:true,draggable:true,position:{x:50,y:50}});
    box.setMaxSize(null,500);
    box.open();
    box.setHeader("DPath Query Library");

    var $el = jQuery('.depgraphlib-box-content',box.object);
    this.initView();
    $el.append(this.view);
  };

  /**
   * hide the macro library
   * @return {[type]} [description]
   */
  cma.toolbox.SearchLib.prototype.hide = function(){

  };
  
  cma.toolbox.SearchLib.template = '<div>'+
    //'<div class="cma-searchlib-filter"><input type="text" class="cma-searchlib-filter-input"></div>'+
    '<table class="cma-searchlib-list"><thead><tr class="cma-searchlib-list-title"><th>Name</th><th>Query</th><th>Description</th><th>Owner</th><th>Actions</th></tr></thead><tbody class="cma-searchlib-list-body"></tbody></table>'+
    '<div class="cma-searchlib-insert"><button type="submit">Insert Selected Macro</button></div>'+
  '</div>';


  /*********************************************************************/
  /*                 DPath Search History                              */
  /*********************************************************************/

  cma.toolbox.SearchHistory = function(searchmodule){
    this.view = jQuery('<table class="cma-searchhistory-list"><thead><tr class="cma-searchhistory-list-title"><th>Query</th><th></th></tr></thead><tbody class="cma-searchhistory-list-body"></tbody></table>');
    this.searchmodule = searchmodule;
  };

  cma.toolbox.SearchHistory.prototype.show = function(){
    var box = new depgraphlib.Box({closeButton:true,draggable:true,position:{x:50,y:50}});
    box.setMaxSize(null,500);
    box.open();
    box.setHeader("DPath Query History");

    var $el = jQuery('.depgraphlib-box-content',box.object);
    this.refreshView();
    $el.append(this.view);
  };

  cma.toolbox.SearchHistory.prototype.refreshView = function(){
    var me = this;
    var data = {
      proxy_url:"dpathlib/_history"
    };
    jQuery.ajax({
      type: 'POST', 
      url: cma.TheApp.ws_url+'frmgcm_proxy',
      data: data,
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        me.history = [];
        for (var i = data.length - 1; i >= 0; i--) {
          me.history.push(data[i]);
          var tbody = jQuery('.cma-searchhistory-list-body',me.view);
          tbody.html("");
          for (var i = data.length - 1; i >= 0; i--) {
            tbody.append('<tr><td>'+data[i]+'</td><td><span class="cma-searchhistory-save"></span><span class="cma-searchhistory-insert"></span></td></tr>');
          };
          jQuery('.cma-searchhistory-save',me.view).click(function(e){
            var selection = jQuery(e.target).parents('tr');
            var children = selection.children();
            var macro = {
              query:children[0].innerHTML,
            };
            me.searchmodule.macroLib.elementForm(macro);
          });
          jQuery('.cma-searchhistory-insert',me.view).click(function(e){
            var selection = jQuery(e.target).parents('tr');
            var children = selection.children();
            var query = children[0].innerHTML;
            var val = me.searchmodule.textarea.html();
            me.searchmodule.textarea.html(val+'<span>'+query+'</span>');
          });
        };
      },
      error: function(jqXHR, textStatus, errorThrown) {
        
      }
    });
  };


}(window.cma = window.cma || {}));

(function(cma){
  
  cma.AppView = {


    setTitle : function(content){
      var app = cma.TheApp;
      jQuery("#cma-header-title",app.view).html(content);
    },
   

    setTemplate : function(template){
      var app = cma.TheApp;
      jQuery("#cma-app",app.view).html(template);
    },

    setMainLayout : function(layoutDefinition){
      var app = cma.TheApp;

      var layout = '';
      for(var i=0;i<layoutDefinition.length;++i){
        var row = layoutDefinition[i];
        layout += '<div id="cma-main-layout-row-'+i+'" class="cma-layout-row" style="height:'+(100/layoutDefinition.length)+'%">';
        for(var j=0; j<layoutDefinition[i].length;++j){
          var col = layoutDefinition[i][j];
          layout += '<div id="cma-main-layout-row-'+i+'-'+j+'" class="cma-layout-cell" style="width:'+layoutDefinition[i][j].width+'%"></div>';
          if(j<layoutDefinition[i].length-1){
            layout += '<div class="cma-layout-hresize-bar"></div>';
          }
        }
        layout += '</div>';
        if(i<layoutDefinition.length -1){
          layout += '<div class="cma-layout-vresize-bar"></div>';
        }
      }

      jQuery('#cma-content',app.view).html(layout);

      /*
      jQuery('.cma-layout-hresize-bar').on("mousedown",function(e){
        cma.resizing = this;
        cma.resizingVal = e.clientX;
      });
      jQuery(cma.TheApp.view).on("mouseup",function(e){
        delete cma.resizing;
        delete cma.resizingVal;
      });
      jQuery(cma.TheApp.view).on("mousemove",function(e){
        if(cma.resizing){
          
          var offset = e.clientX - cma.resizingVal;
          var prevWidth = jQuery(cma.resizing.previousSibling).width();
          jQuery(cma.resizing.previousSibling).width(prevWidth+offset);
          var nextWidth = jQuery(cma.resizing.nextSibling).width();
          jQuery(cma.resizing.nextSibling).width(nextWidth-offset);
          cma.resizingVal = e.clientX;
        }
      });*/
    },

    getMainLayoutCell : function(i,j){
      var app = cma.TheApp;
      return jQuery('#cma-main-layout-row-'+i+'-'+j,app.view);
    },


  };



  
  
  
}(window.cma = window.cma || {}));
(function(cma){
  
  cma.ui = cma.ui || {};

  cma.ui.AutoComplete = function(element,uri,restrict){
    this.view = element;
    this.initView();
    this.uri = uri;
    this.update();
  }



  cma.ui.AutoComplete.prototype.initView = function(){
  }

  cma.ui.AutoComplete.prototype.init = function(){
/*    this.view.keyup(function(){
      console.log(jQuery(this).val());
    });*/
  }

  cma.ui.AutoComplete.prototype.update = function(){
    if(!this.uri){
      this.entries = [];
      return;
    }
      

    var me = this;
    jQuery.ajax({
      type: 'GET', 
      url: this.uri,
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        me.entries = data;
        if(me.view.autocomplete){
          me.view.autocomplete({
            source: data,
          });
          me.view.data("uiAutocomplete")._resizeMenu = function () {
            var ul = this.menu.element;
            ul.outerWidth(this.element.outerWidth());
          };
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        
      }
    });
  }

  
  
}(window.cma = window.cma || {}));

(function(cma){
  
  cma.ui = cma.ui || {};

  cma.ui.CallbackButton = function(name,options){
    var style = "";
    if(options && options.style){
      style = options.style;
    }
    this.view = jQuery('<span><input type="button" '+style+' value="'+name+'"><div class="cma-throbber hidden"></div></span>');
  }



  cma.ui.CallbackButton.prototype.init = function(callback,data){
    var me = this;
    jQuery('input',this.view).click(function(){
      var throbber = jQuery('.cma-throbber',me.view);
      if(!throbber.is(':hidden')){
        return;  
      }
      throbber.removeClass('cma-throbber-success');
      jQuery('.cma-throbber',me.view).removeClass('cma-throbber-error');
      jQuery('.cma-throbber',me.view).show();
      jQuery('.cma-throbber',me.view).css('display','inline-block');
      callback.call(null,me,data);
    });
  }

  cma.ui.CallbackButton.prototype.success = function(){
    jQuery('.cma-throbber',this.view).addClass('cma-throbber-success');
    jQuery('.cma-throbber',this.view).fadeOut(2000);
  }

  cma.ui.CallbackButton.prototype.error = function(){
    jQuery('.cma-throbber',this.view).addClass('cma-throbber-error');
    jQuery('.cma-throbber',this.view).fadeOut(2000);
  }

  
  
}(window.cma = window.cma || {}));
(function(cma){
  
  cma.ui = cma.ui || {};
  
  cma.ui.FileViewer = function(){
    
  };
  
  
}(window.cma = window.cma || {}));
(function(cma){

  cma.ui = cma.ui || {};

  cma.ui.FoldableDiv = function(content,startexpanded){
    this.view = jQuery(cma.ui.FoldableDiv.templateMain);
    this.content = jQuery('.cma-foldable-content',this.view);
    this.content.append(content);
    this.expanded = startexpanded;
    if(!this.expanded){
      jQuery('.cma-foldable-head',this.view).addClass('cma-foldable-open');
      this.content.hide();
    }else{
      jQuery('.cma-foldable-head',this.view).addClass('cma-foldable-close');
    }
    this.initView();
  }

  cma.ui.FoldableDiv.prototype.setTitle = function(title){
    jQuery('.cma-foldable-head',this.view).find('div').html(title);
  }

  cma.ui.FoldableDiv.prototype.initView = function(){
    var me = this;
    jQuery('.cma-foldable-head',this.view).click(function(){
      if(me.expanded){
        jQuery(this).addClass('cma-foldable-open');
        jQuery(this).removeClass('cma-foldable-close');
        me.content.hide();
      }else{
        jQuery(this).addClass('cma-foldable-close');
        jQuery(this).removeClass('cma-foldable-open');
        me.content.show();  
      }
      me.expanded = !me.expanded;
    });
  }

  cma.ui.FoldableDiv.templateMain = '<div class="cma-foldable-div"><div class="cma-foldable-head"><div style="border-top:1px solid #bbbbbb; margin-left:20px; margin-top:2px; font-size:10px;"></div></div>'+
    '<div class="cma-foldable-content"></div></div>';
  
}(window.cma = window.cma || {}));
(function(cma){

  cma.ui = cma.ui || {};

  cma.ui.JSONTable = function(data,style){
    this.view = jQuery('<table></table>');
    this.style = style || {};
    this.setData(data);
    return this.view;
  }



  cma.ui.JSONTable.prototype.update = function(data){
    var styleth = (this.style.th)?' class="'+this.style.th+'" ':"";
    var stylethr = (this.style.thr)?' class="'+this.style.thr+'" ':"";
    var styletr0 = (this.style.tr0)?' class="'+this.style.tr0+'" ':"";
    var styletr1 = (this.style.tr1)?' class="'+this.style.tr1+'" ':"";
    var styletd = (this.style.td)?' class="'+this.style.tr1+'" ':"";
    var headers = {};
    var k = 0;
    for(var i =0;i<data.length;i++){
      for(var name in data[i]){
        if(!headers[name]){
          headers[name]=k;
          k++;
        }
      }
    }

    var tableheader = '<tr'+stylethr+'>';
    for(var i in headers){
      tableheader += '<th'+styleth+'></th>';
    }
    tableheader += '</tr>';
    tableheader = jQuery(tableheader);
    var tableheadercells = tableheader.children();
    for(var i in headers){
      var cellIndex = headers[i];
      jQuery(tableheadercells[cellIndex]).html(i);
    }

    this.view.append(tableheader);
    for(var i =0;i<data.length;i++){
      var row = '<tr'+((i%2)?styletr1:styletr0)+'>';
      for(var k=0;k<ncols;k++){
        row += '<td'+styletd+'></td>';
      }
      row += '</tr>';
      row = jQuery(row);
      var cells = row.children();
      for(var name in data[i]){
        var cellIndex = headers[name];
        jQuery(cells[cellIndex]).html(data[i][name]);
      } 
      this.view.append(row);
    }
  }
  
  
}(window.cma = window.cma || {}));
(function(cma){

  cma.ui = cma.ui || {};

  cma.ui.Table = function(definition,data){
    this.view = jQuery('<table></table>');
    this.model = definition;
    this.model.bindings = {};
    this.model.style = this.model.style || {};

    this.view.addClass(this.model.style.table);
    this.initView();
    if(data){
      this.update(data);
    }
  }


  cma.ui.Table.prototype.clear = function(){
    this.view.html("");
    this.initView();
  }

  cma.ui.Table.prototype.getData = function(){
    var data = [];
    var rows = jQuery('tr',this.view);
    for(var i =1;i<rows.length;i++){
      var cells = jQuery(rows[i]).children();
      var item = {};
      data.push(item);
      for(var j = 0;j<cells.length;j++){
        item[this.model.headers[j].nameid] = (typeof this.model.headers[j].unformat == 'function')?this.model.headers[j].unformat(jQuery(cells[j]).children()[0]):jQuery(cells[j]).html();
      }
    }
    return data;
  }

  cma.ui.Table.prototype.update = function(data){
    this.clear();
    var styletr0 = (this.model.style.tr0)?' class="'+this.model.style.tr0+'" ':"";
    var styletr1 = (this.model.style.tr1)?' class="'+this.model.style.tr1+'" ':"";
    var styletd = (this.model.style.td)?' class="'+this.model.style.td+'" ':"";
    var ncols = this.model.headers.length;
    this.originalData = data;
    for(var i in data){
      var row = '<tr'+((i%2)?styletr1:styletr0)+'>';
      for(var k=0;k<ncols;k++){
        row += '<td'+styletd+'></td>';
      }
      row += '</tr>';
      row = jQuery(row);
      var cells = row.children();
      for(var name in data[i]){
        if(this.model.bindings[name] !== undefined){
          var content = (typeof this.model.headers[this.model.bindings[name]].format == 'function')?this.model.headers[this.model.bindings[name]].format(name,data[i][name],data[i]):data[i][name];
          jQuery(cells[this.model.bindings[name]]).html(content);
        }
      }
      this.view.append(row);
    }
  }

  cma.ui.Table.prototype.initView = function(){
    var styleth = (this.model.style.th)?' class="'+this.model.style.th+'" ':"";
    var stylethr = (this.model.style.thr)?' class="'+this.model.style.thr+'" ':"";
    var tableheader = '<tr'+stylethr+'>';
    for(var i in this.model.headers){
      var header = this.model.headers[i];
      tableheader += '<th'+styleth+'>'+header.title+'</th>';
      if(header.nameid){
        this.model.bindings[header.nameid] = i;  
      }
    }
    tableheader += '</tr>';

    this.view.append(tableheader);
  }
  
  
}(window.cma = window.cma || {}));
(function(cma){
  
  cma.template = {};
  
  cma.template.main= '<div style="width:100%; height:800px;"><div id="cma-app-container">'
    + '<div id="ajax-loader"></div>'
    + '<div id="cma-header-bar"><div id="cma-header-menu"></div><div id="cma-header-title"></div><a id="cma-help"></a><a id="cma-quit" class="exit"></a></div>'
    + '<div id="cma-app" class="hidden">'
    + '</div><div id="cma-status-bar"><div id="cma-status-bar-content"></div><a id="cma-feedback"></a></div></div></div>';
  
  
  cma.template.corpusViewHeader = '<div id="cma-header">'
    + '<span id="cma-title" class="mutable"></span>' // on click transform to input for renaming
    + '<span id="cma-status"></span>'
    + '<span id="cma-actions"></span>'
    + '</div>';

  cma.template.corpusViewMain = '<div id="cma-content">'
    + '</div>';

  cma.template.toolboxPermissions = '<h3>Corpus Permissions</h3>'
    + '<h4>Access Corpus</h4>'
    + '<ul></ul>'
    + '<h4>Edit Corpus</h4>';

  cma.template.contactForm = '<form id="cma-contact-form">'
    + '<label>Subject :</label><input id="cma-contact-form-subject" type="textarea"><br>'
    + '<label>Message :</label><textarea id="cma-contact-form-body" name="body" rows="4" cols="50"></textarea><br>'
    + '<input id="cma-contact-form-submit" name="submit" type="button" value="Send">'
    + '</form>';

  cma.template.tasks = '<div><h2 style="margin-top:0px">Log</h2><table class="cma-app"><tr><th class="cma-app">Task</th><th class="cma-app">Status</th><th class="cma-app">StdOut</th><th class="cma-app">StdErr</th></tr>'
    + '</table></div>';
  
  cma.template.upload = '<div class="corpusUpload">'+
    '<div class="corpusUploadContainer cma-container-level-1">'+
    '<h2 id="cma-upload-title" style="margin-top:0px;">Upload new corpus : </h2>'+
    '<form action="corpus_upload/upload" method="post" enctype="multipart/form-data" id="corpusUploadForm">'+
    '<input type="file" id="corpusUploadFile" name="corpusUploadFile" />'+
    '<div>OR</div>'+
    '<label>URL:</label><input type="text" id="corpusUploadUrl" name="corpusUploadUrl" /><br>'+
    '<input type="button" class="corpusUploadButton" value="Start Upload" id="corpusUploadSubmit" onclick="cma.TheApp.corpusUpload.fire();" />'+
    '</form>'+
    '<div id="corpusUploadProgressBarContainer">'+
    '<div id="corpusUploadProgressBarFilled"></div>'+
    '</div>'+
    '<div id="corpusUploadTimeRemaining"></div>'+
    '<div id="corpusUploadResponse"></div>'+
    '</div>'+
    '</div>'+
    '<div class="cma-container-level-1" >'+
    '<h2>Corpus List</h2>'+
    '<div id="corpus-list-view"></div></div>';
  
  cma.template.toolboxitem_inset = '<div class="cma-toolboxitem-container">'+
      '<div class="cma-toolboxitem-header"><div class="cma-icon cma-toolbox-item-detach"></div><div class="cma-icon cma-toolbox-item-expand"></div><div class="cma-toolbox-item-title"></div></div>'+
      '<div class="cma-toolboxitem-content"></div>'+
    '</div>';

  cma.template.toolboxitem_outset = '<div id="cma-toolboxitem_outset"></div>';

  cma.template.toolbox = '<div class="cma-toolbox cma-container-level-1">'+
      '<div class="cma-toolbox-header"><h2 style="margin-top:0px;">Toolbox</h2></div>'+
      '<div class="cma-toolboxitems-container"></div>'+
      '<div class="cma-toolbox-footer"></div>'+
    '</div>';



  
}(window.cma = window.cma || {}));

(function(cma){
  
  cma.ui = cma.ui || {};

  cma.ui.ToolBox = function(){
    this.view = jQuery(cma.template.toolbox);
    this.toolboxitems = {};
    this.detachedBox = jQuery(cma.template.toolboxitem_outset);
    jQuery('body').append(this.detachedBox);
    var width = cma.TheApp.view.width();
    this.detachedBox.css('top',50);
    this.detachedBox.css('left',50);
    this.detachedBox.css('width',width-100);
    this.detachedBoxRef = null;
  };

  cma.ui.ToolBox.prototype.addBoxItem = function(name,content,title,options){
    options = options || {};
    this.toolboxitems[name] = new cma.ui.ToolBoxItem(name,this,options);
    this.toolboxitems[name].setContent(content);
    if(!title){
      title = name;
    }
    this.toolboxitems[name].setTitle(title);
    jQuery('.cma-toolboxitems-container',this.view).append(this.toolboxitems[name].view);

  };

  cma.ui.ToolBox.prototype.detachBoxItem = function(name){
    this.ratachBoxItem();

    var content = this.toolboxitems[name].view.children();
    this.toolboxitems[name].content.show();
    jQuery('.cma-toolbox-item-expand',content).hide();
    content.detach();
    this.detachedBox.append(content);
    this.detachedBoxRef = name;
    this.detachedBox.show();
  };

  cma.ui.ToolBox.prototype.ratachBoxItem = function(){
    if(this.detachedBoxRef){
      var prevContent = this.detachedBox.children();
      jQuery('.cma-toolbox-item-expand',prevContent).show();
      prevContent.detach();
      this.toolboxitems[this.detachedBoxRef].view.append(prevContent);
      if(!this.toolboxitems[this.detachedBoxRef].expanded){
        this.toolboxitems[this.detachedBoxRef].content.hide();
      }
      this.detachedBoxRef = null;
      this.detachedBox.hide();
    }
  }

  cma.ui.ToolBoxItem = function(name,toolboxcontainer,options){
    var me = this;
    this.name = name;
    this.toolboxcontainer = toolboxcontainer;
    this.view = jQuery(cma.template.toolboxitem_inset);
    this.view.attr("name",name);
    this.content = jQuery('.cma-toolboxitem-content',this.view);
    if(options.startOpen){
      this.expanded = true;
    }else{
      this.expanded = false;  
      this.content.hide();  
    }
    this.detached = false;

    jQuery('.cma-toolbox-item-detach',this.view).click(function(){
      if(me.detached){
        me.toolboxcontainer.ratachBoxItem();
      }else{
        me.toolboxcontainer.detachBoxItem(me.name);
      }
      me.detached = !me.detached;
    });
    
    jQuery('.cma-toolbox-item-expand',this.view).click(function(){
      if(me.expanded){
        me.content.hide();
      }else{
        me.content.show();  
      }
      me.expanded = !me.expanded;
    });
  }

  cma.ui.ToolBoxItem.prototype.setContent = function(content){
    this.content.html(content);
  }

  cma.ui.ToolBoxItem.prototype.setTitle = function(title){
    jQuery('.cma-toolbox-item-title',this.view).html(title);
  }
  
  
  
}(window.cma = window.cma || {}));
(function(cma){
  
  cma.ui = cma.ui || {};
  

  
  cma.ui.saveMutable = function(elt){
    var jelt = jQuery(elt);
    if(elt.nodeName.toLowerCase() == 'input'){
      var val = jelt.attr("value");
      var span = '<span id="'+jelt.attr('id')+'" class="mutable">'+val+'</span>';
      cma.TheApp.corpus.callAndDo("set",{"action":"name","val":val},function(data){
        console.log(data);
      });
      jelt.replaceWith(span);
    }
  };
  
  cma.ui.initMutable = function(){
    jQuery(document).on("click",".mutable",function(e){
      var elt = jQuery(this);
      if(this.nodeName.toLowerCase() == 'input'){
        return;
      }
      var val = elt.text();
      var inputField = '<input id="'+elt.attr('id')+'" type="text" class="mutable" value="'+val+'">';
      jQuery(e.target).replaceWith(inputField);
    });
    
    jQuery(document).on('keypress',".mutable",function(e){
      var elt = this;
      if(e.which == 13){
        cma.ui.saveMutable(elt);
        e.preventDefault();
      }
    });
    
    jQuery(document).on("click",function(e){
      var elt = jQuery(e.target);
      jQuery('.mutable').each(function(i,n){
        if(elt.attr('id') != n.id){
          cma.ui.saveMutable(n);
        }
      });
    });
  };
  
  
  
  cma.ui.get = function(id,container){
    if(!container){
      container = cma.TheApp.view;
    }
    return jQuery('#'+id,container);
  };
  
  

  
  
}(window.cma = window.cma || {}));
(function(cma){
  
  
}(window.cma = window.cma || {}));
(function(cma){
  
  cma.Log = function(message){
    cma.ui.get("cma-status-bar-content").html(message);
  }

  cma.utils = {};

  cma.utils.getTextareaCarretPosition = function(textarea){
    
  };

  /**
   * @function promise
   * 
   * @desc call a function when requested data are defined
   * @param callback the function to be called
   * @param requested_data array of requested data definition {object:object,field:string}
   * @param timeout (optional, default = 2000 ms) when to stop trying to wait for requested data to be defined
   * @param interval (optional, default = 200ms) interval whithin trials.
   * @memberof utils#
   */
  cma.utils.promise = function(callback,requested_data,timeout,interval){
    if(!interval){
      interval = 200;
    }
    if(timeout == undefined){
      timeout = 2000;
    }
    if(timeout<=0){
      return;
    }
    for(var i=0;i<requested_data.length;++i){
      if(requested_data[i].object[requested_data[i].field] == undefined){
        setTimeout(function() {cma.utils.promise(callback,requested_data,timeout-interval,interval)}, interval);
        return;
      }
    }
    callback.call();
  }
  
  
  
}(window.cma = window.cma || {}));