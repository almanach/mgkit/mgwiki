(function(cma){
  
  cma.toolbox = cma.toolbox || {};

  cma.toolbox.Process = function(){
    this.initView();
  }

  cma.toolbox.Process.prototype.initView = function(){
    var me = this;
    if(this.view){
      this.view.html(cma.toolbox.Process.templateMain)
    }else{
      this.view = jQuery(cma.toolbox.Process.templateMain);  
    }
    


    
    
    cma.TheApp.corpus.getAndDo('tasks',{},function(data){
      console.log(data);
      var tasks = cma.TheApp.corpus.setTasks(data);
      cma.ui.get('cma-corpus-log').html('');
      cma.ui.get('cma-corpus-log').append(tasks);
      cma.utils.promise(function(){
        me.defaultAnalysis();
        //me.indexation();
        //me.termExtraction();
      },
        [{object:cma.TheApp.corpus,field:"taskstatus"}]
      );
    });

    

    /*if(cma.TheApp.corpus.status != "waiting"){
      jQuery('#cma-process-default-analysis',this.view).hide();
    }*/
    
  };

  cma.toolbox.Process.prototype.defaultAnalysis = function(){
    var me = this;
    var canStartDefaultProcess = false;
    var finished = false;
    for(var i=0;i<cma.TheApp.corpus.taskstatus.length;++i){
      for(var j=0;j<cma.TheApp.corpus.taskstatus[i].length;j++){
        var task = cma.TheApp.corpus.taskstatus[i][j];
        if(task.name == "raw2dag" && task.status == "waiting"){
          canStartDefaultProcess = true;
          break;
        }
        if(task.name == "retrieveResults" && task.status != "waiting"){
          finished = true;
          break;
        }
      }
    }

    this.view.append('<h3 class="default-analysis">Default Analysis</h3>');

    if(!canStartDefaultProcess){
      if(!finished){
        this.view.append('processing');
      }else{
        this.view.append('finished');
      }
      return;
    }

    this.view.append('<input type="button" id="cma-process-default-analysis" value="Launch process">');

    var options = '<label style="display:inline-block;">Parse format output :</label>'+
      '<select id="cma-process-output-format-select" style="margin-left:10px;" name="format-type">'+
      '<option selected="true" value="conll">conll</option>'+
      '<option value="dis_xmldep">depxml</option>'+
      '<option value="depconll">depconll</option>'+
      '<option value="passage">passage</option>'+
      '</select>'+
      '<br>'+
      '<label style="display:inline-block;">Term Extraction & Semantic Distribution :</label>'+
      '<input id="cma-process-termextract" type="checkbox" style="margin-left:10px;" name="termextract">';
    var foldableDiv = new cma.ui.FoldableDiv(options);
    foldableDiv.setTitle('Options');
    jQuery('h3.default-analysis',this.view).after(foldableDiv.view);

    jQuery('#cma-process-termextract',me.view).change(function(){
      var select = jQuery('#cma-process-output-format-select',me.view);
      select[0].value = "passage";
      if(this.checked){
        select[0].disabled = true;
      }else{
        select[0].disabled = false;
      }
      
    });

    jQuery('#cma-process-default-analysis',this.view).click(function(){
      var select = jQuery('#cma-process-output-format-select',me.view);
      var format = select[0].options[select[0].selectedIndex].value;
      var settings = {pid:1,reset:true,format:format};
      var termextract = jQuery('#cma-process-termextract',me.view);
      if(termextract[0].checked){
        settings.termextract = true;
      }
      cma.TheApp.callProxy('corpus/'+cma.TheApp.corpus.cid+'/_process',settings,function(data){
        me.initView();
      });
    });
  };

  cma.toolbox.Process.prototype.indexation = function(){
    this.view.append('<h3>Indexation</h3><input type="button" id="cma-process-index" value="Index results">');
    jQuery('#cma-process-index',this.view).click(function(){
      
      cma.TheApp.callProxy('corpus/'+cma.TheApp.corpus.cid+'/_index',{pid:2,reset:true},function(data){
        console.log(data);
     });
    });
  };

  cma.toolbox.Process.prototype.termExtraction = function(){
    var me = this;
    var canStartTermProcess = false;
    for(var i=0;i<cma.TheApp.corpus.taskstatus.length;++i){
      for(var j=0;j<cma.TheApp.corpus.taskstatus[i].length;j++){
        var task = cma.TheApp.corpus.taskstatus[i];
        if(true || task.name == "retrieveResults" && task.status == 0){
          canStartTermProcess = true;
          break;
        }
      }
    }

    if(!canStartTermProcess){
      return;
    }
    this.view.append('<h3>Term Extraction</h3><input type="button" id="cma-process-term-extraction" value="Launch term extraction.">');
    jQuery('#cma-process-term-extraction',this.view).click(function(){
      cma.TheApp.callProxy('corpus/'+cma.TheApp.corpus.cid+'/_process',{pid:3,reset:true},function(data){
        me.initView();
      });
    });
  };

  cma.toolbox.Process.templateMain = '<div>'+
    '</div>';
  
}(window.cma = window.cma || {}));
