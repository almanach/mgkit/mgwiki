(function(cma){
  
  cma.AppView = {


    setTitle : function(content){
      var app = cma.TheApp;
      jQuery("#cma-header-title",app.view).html(content);
    },
   

    setTemplate : function(template){
      var app = cma.TheApp;
      jQuery("#cma-app",app.view).html(template);
    },

    setMainLayout : function(layoutDefinition){
      var app = cma.TheApp;

      var layout = '';
      for(var i=0;i<layoutDefinition.length;++i){
        var row = layoutDefinition[i];
        layout += '<div id="cma-main-layout-row-'+i+'" class="cma-layout-row" style="height:'+(100/layoutDefinition.length)+'%">';
        for(var j=0; j<layoutDefinition[i].length;++j){
          var col = layoutDefinition[i][j];
          layout += '<div id="cma-main-layout-row-'+i+'-'+j+'" class="cma-layout-cell" style="width:'+layoutDefinition[i][j].width+'%"></div>';
          if(j<layoutDefinition[i].length-1){
            layout += '<div class="cma-layout-hresize-bar"></div>';
          }
        }
        layout += '</div>';
        if(i<layoutDefinition.length -1){
          layout += '<div class="cma-layout-vresize-bar"></div>';
        }
      }

      jQuery('#cma-content',app.view).html(layout);

      /*
      jQuery('.cma-layout-hresize-bar').on("mousedown",function(e){
        cma.resizing = this;
        cma.resizingVal = e.clientX;
      });
      jQuery(cma.TheApp.view).on("mouseup",function(e){
        delete cma.resizing;
        delete cma.resizingVal;
      });
      jQuery(cma.TheApp.view).on("mousemove",function(e){
        if(cma.resizing){
          
          var offset = e.clientX - cma.resizingVal;
          var prevWidth = jQuery(cma.resizing.previousSibling).width();
          jQuery(cma.resizing.previousSibling).width(prevWidth+offset);
          var nextWidth = jQuery(cma.resizing.nextSibling).width();
          jQuery(cma.resizing.nextSibling).width(nextWidth-offset);
          cma.resizingVal = e.clientX;
        }
      });*/
    },

    getMainLayoutCell : function(i,j){
      var app = cma.TheApp;
      return jQuery('#cma-main-layout-row-'+i+'-'+j,app.view);
    },


  };



  
  
  
}(window.cma = window.cma || {}));