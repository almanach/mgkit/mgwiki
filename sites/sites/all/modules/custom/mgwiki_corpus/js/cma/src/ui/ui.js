(function(cma){
  
  cma.ui = cma.ui || {};
  

  
  cma.ui.saveMutable = function(elt){
    var jelt = jQuery(elt);
    if(elt.nodeName.toLowerCase() == 'input'){
      var val = jelt.attr("value");
      var span = '<span id="'+jelt.attr('id')+'" class="mutable">'+val+'</span>';
      cma.TheApp.corpus.callAndDo("set",{"action":"name","val":val},function(data){
        console.log(data);
      });
      jelt.replaceWith(span);
    }
  };
  
  cma.ui.initMutable = function(){
    jQuery(document).on("click",".mutable",function(e){
      var elt = jQuery(this);
      if(this.nodeName.toLowerCase() == 'input'){
        return;
      }
      var val = elt.text();
      var inputField = '<input id="'+elt.attr('id')+'" type="text" class="mutable" value="'+val+'">';
      jQuery(e.target).replaceWith(inputField);
    });
    
    jQuery(document).on('keypress',".mutable",function(e){
      var elt = this;
      if(e.which == 13){
        cma.ui.saveMutable(elt);
        e.preventDefault();
      }
    });
    
    jQuery(document).on("click",function(e){
      var elt = jQuery(e.target);
      jQuery('.mutable').each(function(i,n){
        if(elt.attr('id') != n.id){
          cma.ui.saveMutable(n);
        }
      });
    });
  };
  
  
  
  cma.ui.get = function(id,container){
    if(!container){
      container = cma.TheApp.view;
    }
    return jQuery('#'+id,container);
  };
  
  

  
  
}(window.cma = window.cma || {}));