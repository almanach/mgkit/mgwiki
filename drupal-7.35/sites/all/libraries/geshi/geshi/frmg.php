<?php 
/*************************************************************************************
 * frmg.php
 * --------------
 * Author: Paul Bui-Quang (paul.bui-quang@inria.fr)
 * Copyright: (c) 2013 Inria (www.inria.fr)
 * Release Version: 1.0.8.11
 * Date Started: 2013/01/02
 *
 * FRMG language file for GeSHi.
 *
 * CHANGES
 * -------
 * 2012/01/02 (1.0.8.6)
 *  -  First Release
 *
 * -------------------------
 *
 *************************************************************************************
 *
 *     This file is part of GeSHi.
 *
 *   GeSHi is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   GeSHi is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with GeSHi; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 ************************************************************************************/
global $base_url;

$language_data = array (
    'LANG_NAME' => 'FRMG',
    'COMMENT_SINGLE' => array(1 => '%'),
    'COMMENT_MULTI' => array('/*' => '*/'),
    'CASE_KEYWORDS' => GESHI_CAPS_NO_CHANGE,
    'QUOTEMARKS' => array("'",'"'),
    'ESCAPE_CHAR' => '',
    'KEYWORDS' => array(
        1 => array(
           'class', 'node', 'desc', 'value', 'father', 'path', 'template', '&#36;',
            ),
        ),
    'SYMBOLS' => array(
        '*', '/', '%', '<:', '>>+',
        '!', '.', '&', '|', '^',
        '<', '>', '=', '~',
        ),
    'CASE_SENSITIVE' => array(
        GESHI_COMMENTS => false,
        1 => true,
        2 => true,
        3 => true,
        4 => true,
        5 => true,
        6 => true,
        7 => true,
        8 => true
        ),
    'STYLES' => array(
        'KEYWORDS' => array(
            1 => 'color: #a020f0;',
            ),
        'COMMENTS' => array(
            1 => 'color: #b52c2c; font-style:italic;',
            'MULTI' => 'color: #b52c2c; font-style:italic;'
            ),
        'ESCAPE_CHAR' => array(
            0 => 'color: #3366CC;',
            1 => 'color: #3366CC;',
            2 => 'color: #3366CC;',
            3 => 'color: #3366CC;',
            4 => 'color: #3366CC;',
            5 => 'color: #3366CC;',
            'HARD' => '',
            ),
        'BRACKETS' => array(
            0 => 'color: #008800;'
            ),
        'STRINGS' => array(
            0 => 'color: #bc8f8f;'
            ),
        'NUMBERS' => array(
            0 => 'color: #FF00FF;',
            GESHI_NUMBER_BIN_PREFIX_0B => 'color: #FF00FF;',
            GESHI_NUMBER_OCT_PREFIX => 'color: #FF00FF;',
            GESHI_NUMBER_HEX_PREFIX => 'color: #FF00FF;',
            GESHI_NUMBER_FLT_SCI_SHORT => 'color: #FF00FF;',
            GESHI_NUMBER_FLT_SCI_ZERO => 'color: #FF00FF;',
            GESHI_NUMBER_FLT_NONSCI_F => 'color: #FF00FF;',
            GESHI_NUMBER_FLT_NONSCI => 'color: #FF00FF;'
            ),
        'METHODS' => array(
            1 => 'color: #660066;'
            ),
        'SYMBOLS' => array(
            0 => 'color: #de7fda;'
            ),
        'REGEXPS' => array(
            0 => 'color: #ffa500;',
            1 => 'color: #ffa500;',
            2 => 'color: #82C30C;',
            3 => 'color: #82C30C;',
            4 => 'color: #82C30C;',
            5 => 'color: #b8860b;',
            6 => 'color: #0202ff;',
            7 => 'color: #ff0707;',
            8 => 'color: #5f9ea0;',
            9 => 'color: #de7fda;',
            10 => 'color: #a020f0;',
        ),
        'SCRIPT' => array(
            0 => '',
            1 => '',
            2 => '',
            3 => ''
            )
        ),
    'URLS' => array(
        1 => '',
        ),
    'OOLANG' => FALSE,
    'OBJECT_SPLITTERS' => array(
        ),
    'REGEXPS' => array(
        0 => array( // attributes
            GESHI_SEARCH => '(\.)(\w+)',
            GESHI_REPLACE => '\\2',
            GESHI_MODIFIERS => '',
            GESHI_BEFORE => '\\1',
            GESHI_AFTER => ''
            ),
        1 => array( // attributes
            GESHI_SEARCH => '([\[|,]\s*)(\w+)(\s*:)',
            GESHI_REPLACE => '\\2',
            GESHI_MODIFIERS => '',
            GESHI_BEFORE => '\\1',
            GESHI_AFTER => '\\3'
            ),
        2 => array( // variables
            GESHI_SEARCH => "(=|&gt;&gt;|&gt;&gt;\+|&lt;)(\s*)([\$]*\w+::)*(\w+)(<SEMI>)",
            GESHI_REPLACE => '\\4',
            GESHI_MODIFIERS => '',
            GESHI_BEFORE => '\\1\\2\\3',
            GESHI_AFTER => '\\5'
            ),
        3 => array( // variables
            GESHI_SEARCH => '([\s|\.]+)([\$]*\w+::)*(\w+)(\s*)(\+|=|=&gt;|&gt;&gt;\+|&gt;&gt;|&lt;)',
            GESHI_REPLACE => '\\3',
            GESHI_MODIFIERS => '',
            GESHI_BEFORE => '\\1\\2',
            GESHI_AFTER => '\\4\\5'
            ),
        4 => array( // variables
            GESHI_SEARCH => '(<\|\/\d*\/>node\|>)(\s*|\()(\w+::)*(\w+)',
            GESHI_REPLACE => '\\4',
            GESHI_MODIFIERS => '',
            GESHI_BEFORE => '\\1\\2\\3',
            GESHI_AFTER => ''
            ),
        5 => array( // ??
            GESHI_SEARCH => '([\$]*\w+::)*(\w+)(\.)',
            GESHI_REPLACE => '\\2',
            GESHI_MODIFIERS => '',
            GESHI_BEFORE => '\\1',
            GESHI_AFTER => '\\3'
        ),
        6 => array( // classnames
            GESHI_SEARCH => '(&lt;:|<\|\/\d*\/>class\|>)(\s+)(\w+)',
            GESHI_REPLACE => '<a href="' . $base_url . '/mgdata/class/\\3" style="color:inherit">\\3</a>',
            GESHI_MODIFIERS => '',
            GESHI_BEFORE => '\\1\\2',
            GESHI_AFTER => ''
            ),
        7 => array( // resources
            GESHI_SEARCH => '(^|\s+)(\+|-)(\s*)(\w+::)*(\w+)',
            GESHI_REPLACE => '<a href="' . $base_url . '/mgdata/resource/\\5" style="color:inherit">\\5</a>',
            GESHI_MODIFIERS => '',
            GESHI_BEFORE => '\\1\\2\\3\\4',
            GESHI_AFTER => ''
            ),
        8 => array( // namespaces
            GESHI_SEARCH => '(\w+)(::)',
            GESHI_REPLACE => '\\1',
            GESHI_MODIFIERS => '',
            GESHI_BEFORE => '',
            GESHI_AFTER => '\\2'
            ),
        9 => array( // special operator +|-
            GESHI_SEARCH => '(\n|^)(\s+)(\+|-)',
            GESHI_REPLACE => '\\3',
            GESHI_MODIFIERS => '',
            GESHI_BEFORE => '\\1\\2',
            GESHI_AFTER => ''
            ),
        10 => array( // special operator +|-
            GESHI_SEARCH => '(<\|!REG3XP\d*\!>)*(\w+)(\|>)*(\s+)(\+|-)',
            GESHI_REPLACE => '\\5',
            GESHI_MODIFIERS => '',
            GESHI_BEFORE => '\\1\\2\\3\\4',
            GESHI_AFTER => ''
        ),
    ),
    'STRICT_MODE_APPLIES' => GESHI_NEVER,
    'SCRIPT_DELIMITERS' => array(
        ),
    'HIGHLIGHT_STRICT_BLOCK' => array(
        ),
    'TAB_WIDTH' => 2
);