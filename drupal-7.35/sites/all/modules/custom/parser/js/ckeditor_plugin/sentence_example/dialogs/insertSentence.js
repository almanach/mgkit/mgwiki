CKEDITOR.dialog.add( 'insertSentenceDialog', function ( editor ) {
    return {
      title: 'Enter new sentence example.',
      minWidth: 400,
      minHeight: 200,

      contents: [
          {
              id: 'tab-basic',
              label: 'Enter your sentence :',
              elements: [
                {
                  type: 'text',
                  id: 'sentence',
                  label: 'Sentence',
                  setup: function(element){
                    this.setValue(element.getText());
                  },
                  commit : function(element){
                    element.setText( this.getValue() );
                  }
                },
                {
                  type: 'html',
                  html: '<div>OU</div><hr>',
                },
                {
                  type: 'text',
                  id: 'uid',
                  label: 'UID',
                  onShow: function(){
                    if(this._.dialog.element.getAttribute('uid')!=null){
                      this.disable();
                    }else{
                      this.enable();
                    }
                  },
                  setup: function(element){
                    this.setValue(element.getAttribute('uid'));
                  },
                  commit : function(element){
                    element.setAttribute( 'uid',this.getValue() );
                  }
                },
                {
                  type: 'html',
                  html: '<div>OU</div><hr>',
                },
                {
                  type:'select',
                  id:'format',
                  label:'Format/Provider',
                  items:[['--'],['json'],['conll'],['dep2pict']],
                  'default':'--',
                  /*onChange:function(api){
                    console.log('format/provider changed');
                    console.log(this.getDialog());
                  },*/
                  setup: function(element){
                    /* should not happend since this dialog is for sentence creation*/
                  },
                  commit : function(element){
                    var format = this.getValue();
                    if(format != '--'){
                      element.setAttribute( 'format',format );
                    }
                  }
                },
                {
                  type: 'textarea',
                  id: 'rawdata',
                  label: 'Graph Definition',
                  onShow: function(){
                    if(this._.dialog.element.getAttribute('rawdata')!=null){
                      this.disable();
                    }else{
                      this.enable();
                    }
                  },
                  setup: function(element){
                    this.setValue(element.getAttribute('rawdata'));
                  },
                  commit : function(element){
                    element.setAttribute( 'rawdata',this.getValue() );
                  }
                },
              ]
          }
      ],
      
      onShow: function() {
        var selection = editor.getSelection();
        var element = selection.getStartElement();
        if ( element ){
          element = element.getAscendant( 'st', true );
        }

        if ( !element || element.getName() != 'st' ) {
            /*element = editor.document.createElement( 'strong');
            style = editor.document.createElement( 'em');
            sentenceTag = editor.document.createElement( 'st' );
            sentenceTag.setAttribute( 'provider', 'frmgserver' );
            style.appendText('"');
            style.append(sentenceTag);
            style.appendText('"');
            element.append(style);*/
            element = editor.document.createElement( 'st' );
            this.insertMode = true;
        }
        else {
            this.insertMode = false;
        }
        this.element = element;
        if ( !this.insertMode ){
          this.setupContent( element );
        }
      },
      
      onOk: function() {
        var dialog = this,
        sentence = dialog.element;
        
        var uid = dialog.getContentElement( 'tab-basic', 'uid' ).getValue();
        var data = dialog.getContentElement( 'tab-basic', 'sentence' ).getValue();
        var rawdata = dialog.getContentElement( 'tab-basic', 'rawdata' ).getValue();
        var format = dialog.getContentElement('tab-basic', 'format').getValue();

        if(uid!=""){
          jQuery.ajax({
            type: 'POST', 
            url: Drupal.settings.basePath+'d3js/ws_post',
            data: {
              uid:uid,
              action:'get tag',
            },
            dataType : 'json',
            success: function(data, textStatus, jqXHR) {
              if(data.error){
                alert(data.error);
                return;
              }else{
                for(var key in data){
                  if(key == 'content'){
                    var text = data[key];
                    var decoded = jQuery('<div/>').html(text).text();
                    sentence.setText(decoded);
                  }else{
                    sentence.setAttribute(key,data[key]);
                  }
                }
              }
            },
            error: function(jqXHR, textStatus, errorThrown) {
              alert(textStatus);
            }
          });
        }else{
          
          if(format!='--'){
            sentence.setAttribute( 'format', format );  
            sentence.setText(rawdata);
          }else{
            sentence.setAttribute('provider','frmgserver');
            sentence.setText(data);
          }
        }
        
        
        //dialog.commitContent( sentence );

        if ( dialog.insertMode ){
          var decoration = editor.document.createElement( 'span');
          decoration.setAttribute("class","sentence-example");
          decoration.append(sentence);
          editor.insertElement( decoration );
        }
      }
      
      
      
    };
});