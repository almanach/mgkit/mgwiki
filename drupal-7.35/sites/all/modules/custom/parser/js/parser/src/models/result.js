(function(mgwiki,$){
  
  mgwiki.parser.Result = function(parseResult,$el,options){
    this.parseInfo = parseResult;
    this.view = new mgwiki.parser.ResultView(this,$el,{});  
    
    this.options = options || {};
  };

  mgwiki.parser.Result.prototype.destroy = function(){
  };

  mgwiki.parser.Result.prototype.getRelatedPages = function(callbackSuccess){
    var me = this;
    var params = this.parseInfo.initialOptions || '';
    var ddata = me.parseInfo.sentence;
    if(this.view.depGraph){
      if(this.view.depGraph.editObject.editMode == 'frmg'){
        params = depgraphlib.getPreviousParams(this.view.depGraph);
      }
    }
    jQuery.ajax({
      type: 'POST', 
      url: me.options.parser_ws + "/getRelatedPages",
      data: {
        options: params,
        data:ddata,
      },
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        callbackSuccess.call(me,data);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        
      }
    });
  };

  mgwiki.parser.Result.prototype.getClasses = function(data,callbackSuccess){
    var me = this;
    var all_classes = {};
    var all_trees = {};

    for (var i = data.graph.words.length - 1; i >= 0; i--) {
      var word = data.graph.words[i];
      var tree = word['#data'].tree;
      var classes = tree.split(" ");
      classes.shift();
      for (var j = classes.length - 1; j >= 0; j--) {
        var klass = classes[j];
        var items = klass.split(":");
        var klassname = items.pop();
        all_classes[klassname]=klassname;
      };
      var tree = classes.join(" ");
      if(tree){
        all_trees[tree] = tree;  
      }
    };

    jQuery.ajax({
      type: 'POST', 
      url: me.options.parser_ws + "/classBlock",
      data: {
        trees:all_trees,
        classes:all_classes
      },
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        callbackSuccess.call(me,data);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        
      }
    });
  };

  mgwiki.parser.Result.prototype.add2basket = function(){
    var me = this;
    jQuery.ajax({
      type: 'POST', 
      url: me.options.parser_ws + "/add2basket",
      data: {
        uid:me.parseInfo.uid
      },
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        if(!data.error){
          me.added2basket = true;
          me.view.$el.find('.parser-result-add2basket').remove();
        }else{
          alert(data.message);
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        
      }
    });
    
  };

  mgwiki.parser.Result.prototype.proposeSentence = function(){
    var me = this;
    jQuery.ajax({
      type: 'POST', 
      url: me.options.parser_ws + "/proposeSentence",
      data: {
        uid:me.parseInfo.uid
      },
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        window.location = data.url;
      },
      error: function(jqXHR, textStatus, errorThrown) {
        
      }
    });
  };

  mgwiki.parser.Result.prototype.save = function(callbackSuccess,callbackError){
    var me = this;

    var format = this.parseInfo.format; // default result type
    var params = this.parseInfo.initialOptions || '';
    var ddata = me.parseInfo.sentence;
    var highlightInfos = this.view.depGraph.editObject.mode[this.view.depGraph.editObject.editMode].highlightInfos;
    if(this.view.depGraph){
      if(this.view.depGraph.editObject.editMode == 'frmg'){
        params = depgraphlib.getPreviousParams(this.view.depGraph);
      }else{
        format = 'json';
        ddata = this.view.depGraph.cleanData();
      }
    }

    jQuery.ajax({
      type: 'POST', 
      url: me.options.graph_url,
      data: {
        action:'save',
        format:format,
        options: params,
        highlighting:highlightInfos,
        data:ddata,
      },
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        if(data.success){
          me.parseInfo.uid = data.uid;
          me.parseInfo.format = format;
          me.parseInfo.initialOptions = params;
          me.parseInfo.data = me.view.depGraph.data;
          me.view.render();

          callbackSuccess.call();
        }else{
          callbackError.call();
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        callbackError.call();
        alert(textStatus);
      }
    });
  };

}(window.mgwiki = window.mgwiki || {},jQuery));