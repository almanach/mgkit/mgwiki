(function(mgwiki,$){
  
  mgwiki.parser.ResultViewHeader = function($el,options){
    this.el = ($el)?$el[0]:$('<div></div>');
    this.$el = $(this.el);
    this.options = options || {};
  };

  mgwiki.parser.ResultViewHeader.prototype.render = function(){
    this.$el.html(mgwiki.parser.ResultViewHeader.template);
    if(!this.options.user){
      this.$el.find('.parser-result-header').prepend(mgwiki.parser.ResultViewHeader.templateAnon);
      this.$el.find('a').attr('href',this.options.url_register);
    }
    this.$el.find('a[name="depgraph"]').attr('href',this.options.url_help_depgraph);
    this.$el.find('a[name="frmg"]').attr('href',this.options.url_help_frmg);
  };

  mgwiki.parser.ResultViewHeader.template = '<div class="parser-result-header">' +
    '<p>Pour plus d\'information sur l\'utilisation de l\'analyseur syntaxique, vous pouvez consulter les pages d\'aides relatives au plugin de <a name="depgraph" href="">visualisation des graphes</a> et/ou aux pages relatives à <a name="frmg" href="">l\'analyseur syntaxique FrMG</a></p>'
  '</div>';

  mgwiki.parser.ResultViewHeader.templateAnon = '<p>Afin de profiter de toutes les fonctionnalité de MGWiki, nous vous invitons à vous <a href="">inscrire</a>.</p>';

}(window.mgwiki = window.mgwiki || {},jQuery));