<?php 


function standardize_string($string){
  return preg_replace("/([^a-zA-Z0-9\.])/", "_", $string);
}


function DEBUG_TRACK_TIME_START($func){
  global $time_tracking;
  if(!isset($time_tracking)){
    $time_tracking = array();
  }
  
  $time_tracking[$func] = microtime(true);
}

function DEBUG_TRACK_TIME_END($func){
  global $time_tracking;
  Logger::Log(microtime(true)-$time_tracking[$func],$func);
}


function rest_helper_new($url,$query_params = null,$query_body = null,$verb = 'GET'){
  $cparams = array(
      'http' => array(
          'method' => $verb,
          'ignore_errors' => true,
      )
  );
  if ($query_params !== null) {
    $query_params = http_build_query($query_params);
    $url .= '?' . $query_params;
  }

  if($query_body !== null){
    if(is_array($query_body)){
      $query_body = json_encode($query_body);
    }
    $cparams['http']['content'] = $query_body;
    $cparams['http']['header'] = "Content-type: application/json\r\n" .
        "Content-Length: " . strlen ( $query_body ) . "\r\n";
  }

  $context = stream_context_create($cparams);
  $fp = fopen($url, 'rb', false, $context);
  if (!$fp) {
    $res = false;
  } else {
    // If you're trying to troubleshoot problems, try uncommenting the
    // next two lines; it will show you the HTTP response headers across
    // all the redirects:
    // $meta = stream_get_meta_data($fp);
    // var_dump($meta['wrapper_data']);
    
    $res = stream_get_contents($fp);
  }
  
  if ($res === false) {
    throw new Exception("$verb $url failed: $php_errormsg");
  }

  return $res;
}

function rest_helper($url, $params = null, $verb = 'GET', $format = 'json')
{
  Logger::Log($params);
  $cparams = array(
      'http' => array(
          'method' => $verb,
          'ignore_errors' => true,
      )
  );
  if ($params !== null ) {
    if ($verb == 'POST') {
      if(is_array($params) ){
        if(count($params)>0){
          $params = json_encode($params);
          $cparams['http']['content'] = $params;
          $cparams['http']['header'] = "Content-type: text/json\r\n" .
             "Content-Length: " . strlen ( $params ) . "\r\n";  
        }
     }else{
        $cparams['http']['content'] = $params;
        $cparams['http']['header'] = "Content-type: text/xml\r\n" .
            "Content-Length: " . strlen ( $params ) . "\r\n";
      }
      
      
    } else {
      $params = http_build_query($params);
      $url .= '?' . $params;
    }
  }

  $context = stream_context_create($cparams);
  $fp = fopen($url, 'rb', false, $context);
  if (!$fp) {
    $res = false;
  } else {
    // If you're trying to troubleshoot problems, try uncommenting the
    // next two lines; it will show you the HTTP response headers across
    // all the redirects:
    // $meta = stream_get_meta_data($fp);
    // var_dump($meta['wrapper_data']);
    
    $res = stream_get_contents($fp);
  }
  
  if ($res === false) {
    throw new Exception("$verb $url failed");
  }

  switch ($format) {
    case 'json':
      $r = json_decode($res);
      if ($r === null) {
        throw new Exception("failed to decode $res as json");
      }
      return $r;

    case 'xml':
      $r = simplexml_load_string($res);
      if ($r === null) {
        throw new Exception("failed to decode $res as xml");
      }
      return $r;
  }
  return $res;
}


function getTheOnlyEltInArrayOrReturnFalse($array){
  if(empty($array)){
    Logger::Log("error no element in this array");
    return false;
  }else if(count($array)>1){
    Logger::Log("error more than 1 element in this array");
    return false;
  }else{
    return $array[0];
  }
}

function isAdmin($theuser=null){
  if(!isset($theuser)){
    global $user;
    $theuser = $user;
  }
  foreach($theuser->roles as $role){
    if($role == 'administrator' || $role == 'moderator'){
      return true;
    }
  }
  return false;
}


function file_get_contents_utf8($fn) {
  $content = file_get_contents($fn);
  return mb_convert_encoding($content, 'UTF-8',
      mb_detect_encoding($content, 'UTF-8, ISO-8859-1', true));
}

function startsWith($haystack, $needle)
{
  return $needle === "" || strpos($haystack, $needle) === 0;
}

function endsWith($haystack, $needle)
{
  return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
}

function deleteDir($dir) {
  $files = array_diff(scandir($dir), array('.','..'));
  foreach ($files as $file) {
    (is_dir("$dir/$file")) ? deleteDir("$dir/$file") : unlink("$dir/$file");
  }
  return rmdir($dir);
} 

function compare_array_values($array1,$array2,$keys=array()){
  if(!count($keys)){
    if(count($array1) == count($array2)){
      $same = false;
      foreach($array1 as $key => $value){
        $same = ($array1[$key] === $array2[$key]);
        if(!$same){
          return false;
        }
      }
    }
    else{
      return false;
    }
  }else{
    $same = false;
    foreach($keys as $key){
      $notnull1 = isset($array1[$key]) && !empty($array1[$key]); 
      $notnull2 = isset($array2[$key]) && !empty($array2[$key]);
      $samestate = $notnull1 == $notnull2; 
      $same = $samestate && (($notnull1 && ($array1[$key] == $array2[$key])) || !$notnull1);
      if(!$same){
        return false;
      }
    }
  }
  return true;
}

class FlexibleCallback{

  public static $callbacks = array();

  public static function NewCallback($callback = null, $args = array()){
    $id = uniqid();
    FlexibleCallback::$callbacks[$id] = array(
    'callback' => $callback,
    'args' => $args
    );
    return 'FlexibleCallback::GenericFlexibleCallback_' . $id;
  }

  public static function __callStatic($name, $arguments){
    $arguments = $arguments[0];
    foreach(get_class_methods('FlexibleCallback') as $callback){
      if(substr($name,0,strlen($callback)) == $callback){
        $parts = preg_split("/_/", $name);
        // case 1
        if($callback == 'GenericFlexibleCallback'){
          $id = $parts[1];
          return FlexibleCallback::$callback($id,$arguments);
        }
        // other cases
        for($i = 1; $i < count($parts) ; $i++){
          $arguments[] = $parts[$i];
        }
        return FlexibleCallback::$callback($arguments);
      }
    }
  }

  public static function GenericFlexibleCallback($id,$arguments){
    $callback_info = FlexibleCallback::$callbacks[$id];
    $args = array($arguments,$callback_info['args']);
    return call_user_func($callback_info['callback'],$args);
  }
}

function write_quoted_text($text,$quote_style){
  $quote_match = $quote_style;
  if($quote_style == '"'){
    $quote_match = '\\"';
  }else if($quote_style != "'"){
    throw new Exception('Unhandled quote style');
  }
  $text = preg_replace("/$quote_match/", "\\$quote_style", $text);
  return $text;
}

function getSentenceFromGraphEntity($entity){
  if(!isset($entity->gnid)){
    return null;
  }
  $node_rev = node_load($entity->gnid);
  if($node_rev==null){
    return null;
  }
  return getSentenceFromGID($node_rev->graph_id['und'][0]['target_id']);
}

function getSentenceFromGID($gid){
  if(module_exists("parser") && module_exists("d3js")){
    $row = db_query('SELECT entity_id FROM field_data_field_main_representation WHERE field_main_representation_target_id = :id', array(':id' => $gid))->fetchField();
    if($row == null){
      $row = db_query('SELECT entity_id FROM field_data_field_representations WHERE field_representations_target_id = :id', array(':id' => $gid))->fetchField();
    }
    if($row != null){
      return $row;
    }else{
      return null;
    }
  }else{
    return null;
  }
}


class Logger {
  public static function Log($message,$title = ''){
    $path = DRUPAL_ROOT . '/sites/default/files/';
    $myFile = $path . "log";
    $fh = fopen($myFile, 'a') or die("can't open file");
    $date_array = getdate();
    $time = "[" . $date_array['mday'] . "/" . $date_array['mon'] . "/" . $date_array['year'] . " - " . $date_array['hours'] . ":" . $date_array['minutes'] . ":" . $date_array['seconds'] . "]";
    if(!is_string($message)){
      $message = var_export($message,true);
    }
    fwrite($fh, $time . " " . $title . ' => ' . $message);
    fwrite($fh, "\n");
    fclose($fh);
  }
  
  public static function SaveFile($data,$filename){
    $path = DRUPAL_ROOT . '/sites/default/files/';
    $myFile = $path . $filename;
    $fh = fopen($myFile, 'a') or die("can't open file");
    if(!is_string($data)){
      $data = var_export($data,true);
    }
    fwrite($fh, $data);
    fwrite($fh, "\n");
    fclose($fh);
  }
}


function getNodeCommentCount($nid){
  $count = db_query('SELECT count(cid) FROM comment WHERE nid = :nid',array(':nid'=>$nid))
    ->fetchField();

  return $count;
}

function node_get_attribute($nids, $attribute){
  if(!is_array($nids)){
    $nids = array($nids);
  }
  
  $field_values = db_select('node', 'n')
  ->fields('n', array('nid', $attribute))
  ->condition('nid', $nids, 'IN')
  ->execute()
  ->fetchAllKeyed();
  
  return $field_values;
}

function node_get_field($nids, $attribute, $entity_type = 'node'){
  if(!is_array($nids)){
    $nids = array($nids);
  }
  $field_values = db_select("field_revision_$attribute", 'f')
  ->fields('f', array('entity_id', $attribute . "_value"))
  ->condition('entity_type', $entity_type)
  ->condition('entity_id', $nids, 'IN')
  ->execute()
  ->fetchAllKeyed();
  return $field_values;
}


function node_update_field($nid,$attribute,$value){
  //TODO ?
  //drupal_write_record("field_revision_$attribute", $record,);
}

function createSnippet($sentence, $max_length){
  $length = strlen($sentence);
  if($length < $max_length){
    return $sentence;
  }
  
  for($i = $max_length; $i<$length; $i++){
    if($sentence[$i]==" "){
      return substr($sentence, 0, $i) . '...';
    }
  }
  
  return $sentence;
}

/**
 * Returns true if two simplexml elements are the same, false otherwise
 * @param unknown $elt1
 * @param unknown $elt2
 * @return boolean
 */
function compareSimpleXMLElements($elt1,$elt2){
  if(strcmp($elt1->getName(),$elt2->getName())==0){
    foreach($elt1->attributes() as $key1 => $value1){
      if(strcmp($elt2["$key1"],$value1)!=0)
        return false;
    }
  }else{
    return false;
  }
  if($elt1->count() != $elt2->count()){
    return false;
  }
  foreach($elt1->children() as $child1){
    $foundSame = false;
    foreach($elt2->children() as $child2){
      if(compareSimpleXMLElements($child1,$child2)){
        $foundSame = true;
        break;
      }
    }
    if(!$foundSame){
      return false;
    }
  }
  return true;
}

/**
 * Returns true if an object exist in a table
 * @param unknown $table the table to look into
 * @param unknown $obj the obj-value to search for
 * @param string $attr if defined, the comparison will be made between the $obj and the value of the attribute $attr of all the object of the table
 * @return boolean
 */
function existIn($obj,$table,$attr=null){
  if(!isset($table))
    return false;
  foreach($table as $elt){
    $comparable_obj = $elt;
    if($attr!=null){
      $comparable_obj = $elt["$attr"];
    }
    if($comparable_obj == $obj){
      return true;
    }
  }
  return false;
}

/**
 * Retrieve a node in the mgwiki db, according to a name and a type
 * @param unknown $name
 * @param unknown $type
 * @return Ambigous <A, boolean, mixed>|NULL
 */
function getNode($name,$type){
  $row = db_query('SELECT nid FROM node WHERE title = :mytitle AND type = :type', array(':mytitle' => $name, ':type' => $type))->fetchField();
  if(($row!==false) && isset($row)){
    $node = node_load($row);
    return $node;
  }else{
    return null;
  }
}

/**
 * Retrieve the node id in the mgwiki db, according to a name and a type
 * @param unknown $name
 * @param unknown $type
 * @return A|NULL
 */
function getNodeID($name,$type){
  $ids = &drupal_static(__FUNCTION__ . ':' . $type,array());
  if(isset($ids[$name])){
    return $ids[$name];
  }
  
  $row = db_query('SELECT nid FROM node WHERE title = :mytitle AND type = :type', array(':mytitle' => $name, ':type' => $type))->fetchField();
  if(($row!==false) && isset($row)){
    $ids[$name] = $row;
    return $row;
  }else{
    return null;
  }
}

/**
 * Retrieves an array of node according to a array of name and a type.
 * If the array is null, all node of the type will be retrieved
 * @param unknown $names
 * @param unknown $type
 * @return multitype:A
 */
function getNodes($names,$type){
  $nodes_id = array();
  if($names==null){
    $query_result = db_query('SELECT nid FROM node WHERE type = :type', array(':type' => $type));
    while(($row=$query_result->fetchField())!=null){
      $nodes_id[] = $row;
    }
  }else{
    foreach($names as $name){
      $row = db_query('SELECT nid FROM node WHERE title = :mytitle AND type = :type', array(':mytitle' => $name, ':type' => $type))->fetchField();
      $nodes_id[] = $row;
    }
  }
  return $nodes_id;
}



/**
 * redirect all links to a node, to another nid
 * @param unknown $prev_nid
 * @param unknown $new_nid
 */
function redirectLinks($prev_nid,$new_nid){
  // TODO
  // search all links to this nid in defined pages and replace it with new nid
}

/**
 * Run glob recursively over directories
 * @param unknown $pattern
 * @param number $flags
 * @return multitype:
 */
function glob_recursive($pattern, $flags = 0)
{
  $files = glob($pattern, $flags);
   
  foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir)
  {
    $files = array_merge($files, glob_recursive($dir.'/'.basename($pattern), $flags));
  }
   
  return $files;
}

/**
 * Transform an array into an object
 * @param unknown $array
 * @return stdClass
 */
function array_to_object($array) {
  $obj = new stdClass;
  foreach($array as $k => $v) {
    if(is_array($v)) {
      $obj->{$k} = array_to_object($v); //RECURSION
    } else {
      $obj->{$k} = $v;
    }
  }
  return $obj;
}


/**
 * Get the filename from a path
 * @param unknown $path
 * @return Ambigous <>
 */
function getFileName($path){
  $elts = explode("/",$path);
  return $elts[count($elts)-1];
}

/**
 * @return the name of the file without its extension, null returned if error
 */
function getFileNameWithoutExtension($path){
  $found = preg_match("/(?:\S+?\/)*(\w+)\..*/", $path, $matches);
  if($found){
    return $matches[1];
  }
  return null;
}

/**
 * Convert a name to a machine readable name
 * @param unknown $name
 * @return mixed
 */
function toMachineName($name){
  $machine_readable = strtolower($name);
  $machine_readable = preg_replace('/[^a-z0-9_]+/','_',$machine_readable);
  return $machine_readable;
}

/**
 * Detect encoding of a file given a path.
 * Returns null if encoding was not found.
 * @param unknown $filepath
 * @return Ambigous <>|NULL
 */
function detectFileEncoding($filepath){
  $cmd = "file -i $filepath";
  $output = shell_exec($cmd);
  $matches = array();
  $found = preg_match("/charset=(\S+)/", $output, $matches);
  if($found){
    return $matches[1];
  }else{
    return null;
  }
}

/**
 * Create a file ($newfile) with the encoding ($encoding) from a filepath ($file)
 * @param unknown $file
 * @param unknown $newfile
 * @param unknown $encoding
 */
function saveIntoEncoding($file, $newfile, $encoding){
  $base_encoding = detectFileEncoding($file);
  
  //warning!!!!
  if($base_encoding==null){
    $base_encoding = 'iso-8859-1';
  }
  
  $fp = fopen($file,'r');
  $str = fread($fp, 3);
  if ($str == "\xef\xbb\xbf")
    $str = "";
  while(($tmpstr = fgets($fp))!=null){
    $str .= $tmpstr;
  }
  fclose($fp);
  //$str = file_get_contents($file);
  $str = mb_convert_encoding($str, $encoding, $base_encoding);
  $handle = fopen($newfile, 'w');
  fwrite($handle, $str);
}

/**
 * @return the node type created
 * @param The name of the Content Type $name
 * @param The machine name of the Content Type $machine_name
 */
function createSimpleContentType($name,$machine_name){
  $t = get_t();
  $node_type = array(
      'type' => $machine_name,
      'name' => t($name),
      'base' => 'node_content',
      'custom' => 1,
      'modified' => 1,
      'locked' => 0,
  );
  $node_type = node_type_set_defaults($node_type);
  node_add_body_field($node_type, $t('Body'));
  node_type_save($node_type);

  $installed_fields = call_user_func("_$machine_name" . "_installed_fields");
  foreach ($installed_fields as $field) {
    field_create_field($field);
  }

  $installed_instances = call_user_func("_$machine_name" . "_installed_instances");
  foreach ($installed_instances as $instance) {
    $instance['entity_type'] = 'node';
    $type = $node_type->type;
    $instance['bundle'] = "$type";
    field_create_instance($instance);
  }
}

/**
 * Delete the mgwiki content of nodes with the names and type specified.
 * If the array of name is empty, then all nodes of that type will be deleted
 * @param unknown $names
 * @param unknown $type
 */
function deleteContent($names,$type){
  $nids = array();
  if($names == null){
    $sql = 'SELECT nid FROM {node} n WHERE n.type = :type';
    $result = db_query($sql, array(':type' => $type));
    foreach ($result as $row) {
      $nids[] = $row->nid;
    }
  }else{
    foreach($names as $name){
      $sql = 'SELECT nid FROM {node} n WHERE n.title =:title AND n.type = :type';
      $result = db_query($sql, array(':type' => $type, ':name' => $name));
      foreach ($result as $row) {
        $nids[] = $row->nid;
      }
    }
  }
  node_delete_multiple($nids);
}

/**
 * Delete a content types, (field, nodes, and content type definition)
 * @param unknown $machine_name
 */
function deleteSimpleContentType($machine_name){
  $sql = 'SELECT nid FROM {node} n WHERE n.type = :type';
  $result = db_query($sql, array(':type' => $machine_name));
  $nids = array();
  foreach ($result as $row) {
    $nids[] = $row->nid;
  }

  node_delete_multiple($nids);

  $installed_fields = call_user_func("_$machine_name" . "_installed_fields");
  foreach (array_keys($installed_fields) as $field) {
    field_delete_field($field);
  }

  $instances = field_info_instances('node', $machine_name);
  foreach ($instances as $instance_name => $instance) {
    field_delete_instance($instance);
  }

  node_type_delete($machine_name);

  field_purge_batch(1000);
}

/*********************************************************************/
/*                    Encoding && HTML Cleaning                      */
/*********************************************************************/

function removeUnusedHTMLTags($data){
  $data = preg_replace('/&nbsp;/',"",$data);
  return preg_replace('/(<p(.*?)>|<\/p>|<br \/>|<span(.*?)>|<\/span>)/',"\n",$data);
}

function chr_utf8($code)
{
  if ($code < 0) return false;
  elseif ($code < 128) return chr($code);
  elseif ($code < 160) // Remove Windows Illegals Cars
  {
    if ($code==128) $code=8364;
    elseif ($code==129) $code=160; // not affected
    elseif ($code==130) $code=8218;
    elseif ($code==131) $code=402;
    elseif ($code==132) $code=8222;
    elseif ($code==133) $code=8230;
    elseif ($code==134) $code=8224;
    elseif ($code==135) $code=8225;
    elseif ($code==136) $code=710;
    elseif ($code==137) $code=8240;
    elseif ($code==138) $code=352;
    elseif ($code==139) $code=8249;
    elseif ($code==140) $code=338;
    elseif ($code==141) $code=160; // not affected
    elseif ($code==142) $code=381;
    elseif ($code==143) $code=160; // not affected
    elseif ($code==144) $code=160; // not affected
    elseif ($code==145) $code=8216;
    elseif ($code==146) $code=8217;
    elseif ($code==147) $code=8220;
    elseif ($code==148) $code=8221;
    elseif ($code==149) $code=8226;
    elseif ($code==150) $code=8211;
    elseif ($code==151) $code=8212;
    elseif ($code==152) $code=732;
    elseif ($code==153) $code=8482;
    elseif ($code==154) $code=353;
    elseif ($code==155) $code=8250;
    elseif ($code==156) $code=339;
    elseif ($code==157) $code=160; // not affected
    elseif ($code==158) $code=382;
    elseif ($code==159) $code=376;
  }
  if ($code < 2048) return chr(192 | ($code >> 6)) . chr(128 | ($code & 63));
  elseif ($code < 65536) return chr(224 | ($code >> 12)) . chr(128 | (($code >> 6) & 63)) . chr(128 | ($code & 63));
  else return chr(240 | ($code >> 18)) . chr(128 | (($code >> 12) & 63)) . chr(128 | (($code >> 6) & 63)) . chr(128 | ($code & 63));
}

// Callback for preg_replace_callback('~&(#(x?))?([^;]+);~', 'html_entity_replace', $str);
function html_entity_replace($matches)
{
  if ($matches[2])
  {
    return chr_utf8(hexdec($matches[3]));
  } elseif ($matches[1])
  {
    return chr_utf8($matches[3]);
  }
  switch ($matches[3])
  {
    case "nbsp": return chr_utf8(160);
    case "iexcl": return chr_utf8(161);
    case "cent": return chr_utf8(162);
    case "pound": return chr_utf8(163);
    case "curren": return chr_utf8(164);
    case "yen": return chr_utf8(165);
    case "quot" : return chr_utf8(34);
    case "gt" : return chr_utf8(62);
    case "lt" : return chr_utf8(60);
    case "amp" : return chr_utf8(38);
  }
  return false;
}

function htmlentities2utf8 ($string) // because of the html_entity_decode() bug with UTF-8
{
  if($string){
    $string = preg_replace_callback('~&(#(x?))?([^;]+);~', 'html_entity_replace', $string);
    return $string;
  }else
    return null;
}
