<?php

function class_overview_display($class = "",$options = array()){
  $basepath = drupal_get_path('module','d3js');
  drupal_add_js($basepath . '/templates/classoverview/classoverview.js');
  drupal_add_css($basepath . '/templates/classoverview/classoverview.css');
  
  
  
  $json_data = getForceData();
  
  global $base_url;
  $uid = uniqid();
  
  
  $options['baseUrl']= $base_url;
  $options['uid'] = $uid;
  $options['frmg_class'] = $class;
  $options['help_url']=$base_url.'/mgwiki/help/classoverview';
  
  $json_options = json_encode($options);
  
  $html = '

      <div id="container-'.$uid.'"></div>';
  $html .= '<script>
      depgraphlib.overview = new depgraphlib.ClassOverviewApp("container-'.$uid.'",'.$json_data.','.$json_options.');
      
      
      
      </script>';
  
  return $html;
  
}


function getForceData(){
  
  $json_data = variable_get('class_overview_json');
  if(isset($json_data)){
    $data = json_decode($json_data,true);
    foreach ($data['nodes'] as &$node){
      $node['x'] = floatval($node['x']);
      $node['y'] = floatval($node['y']);
      unset($node['index']);
      unset($node['weight']);
      unset($node['px']);
      unset($node['py']);
    }
    
    foreach($data['links'] as &$link){
      $link['source'] = intval($link['source']['index']);
      $link['target'] = intval($link['target']['index']);
    }
    
    foreach($data['ressourcelinks'] as &$link){
      $link['source'] = intval($link['source']['index']);
      $link['target'] = intval($link['target']['index']);
    }
    $json_data = json_encode($data);
    return $json_data;
  }
  
  $result = db_select('node', 'n')
  ->fields('n', array('nid'))
  ->condition('type','frmg_class')
  ->execute();
  
  $heap = array();
  $trees = array();
  $nids = array();
  $data['nodes']=array();
  $data['links']=array();
  $data['ressourcelinks'] = array();
  $i = 0;
  foreach ($result as $row){
    $nid = $row->nid;
    $nids[$nid] = $i;
    $node = node_load($nid);
    $data['nodes'][]=array('x'=>0,'y'=>0,'name'=>$node->title);
    if(isset($node->frmg_class_parent['und']) && count($node->frmg_class_parent['und'])){
      foreach($node->frmg_class_parent['und'] as $parent){
        $data['links'][] = array('source'=>$nid,'target'=>$parent['target_id'],'type'=>'parent');

        if(isset($heap[$parent['target_id']])){
          $heap[$parent['target_id']][1][] = array($nid,array());
        }else{
          $heap[$parent['target_id']] = array($parent['target_id'],array(array($nid,array())));
        }
        
      }
    }else{
      $trees[] = array($nid,array());
    }
    if(isset($node->frmg_class_needed['und'])){
      foreach($node->frmg_class_needed['und'] as $need){
        $data['ressourcelinks'][] = array('source'=>$nid,'target'=>$need['target_id'],'type'=>'ressource');
      }
    }
    $i++;
  }
  
  $info = array(0=>array('max'=>0));
  foreach ($trees as &$tree){
    buildTree($tree, $heap, $info,0);
  }
//  setNodePositions($trees,$nids,$data);
  
  foreach($data['links'] as &$link){
    $link['source'] = $nids[$link['source']];
    $link['target'] = $nids[$link['target']];
  }
  
  foreach($data['ressourcelinks'] as &$link){
    $link['source'] = $nids[$link['source']];
    $link['target'] = $nids[$link['target']];
  }
  
  $json_data = json_encode($data);
  return $json_data;
  
}


function buildTree(&$tree,&$heap,&$info,$curLevel){
  foreach($heap as &$node){
    if($node[0]==$tree[0]){
      $tree[1] = $node[1];
      unset($heap[$node[0]]);
      break;
    }
  }
  if(!isset($info[$curLevel]) || $info[$curLevel]['max']<count($tree[1])){
    $info[$curLevel]['max']=count($tree[1]);
  }
  foreach($tree[1] as &$child){
    buildTree($child, $heap,$info,$curLevel+1);
  }
}

function setNodePositions($trees,$nids,&$data,$offsetX=0,$offsetY=0){
  $i = 0;
  $l = 50;
  $nl = count($trees)*$l;
  $mid = $nl/2;
  $L = 150; 

  $treeRange = array($offsetX,$offsetX);
  
  if(!$nl){
    return $treeRange;
  }
  
  $lastPos = $offsetX;
    
  foreach($trees as $tree){
    $node = &$data['nodes'][$nids[$tree[0]]]; 
    
    $range = setNodePositions($tree[1],$nids,$data,$lastPos+$l,$offsetY+$L);
    $position = ($range[0]+$range[1])/2;
    
    $node['x'] = $position;
    $node['y'] = $offsetY;
    
    $lastPos = $node['x'];
    $i++;
    
  }
  
  $treeRange[1] = $lastPos;
  return $treeRange;
  
}
