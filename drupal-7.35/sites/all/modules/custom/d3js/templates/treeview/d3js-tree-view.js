/**
 * d3js-tree-view.js
 * This part of the library contains the function
 * for displaying trees.
 * 
 * Author : Paul Bui-Quang
 * INRIA
 */
(function(treegraphlib,$,undefined){

  treegraphlib.TreeGraph = function(viewer,json_data,options){
    this.viewer = viewer;
    this.imagemode = false;
    var maxLexLength = 20;
    treegraphlib.TreeGraph.instances[viewer.uid] = this;
    
    var maxNodePerDepth = this.maxNodePerDepth = options.maxNodePerDepth;
    var treeHeight = this.treeHeight = options.treeHeight;
    this.viewer.container.css('display','block');
    if(options.width != null){
      viewer.setWidth(options.width);
    }else{
      viewer.setWidth('100%');
    }
    if(options.height != null){
      viewer.setHeight(options.height);
    }else{
      viewer.setHeight(600);
    }
    if(options.miniature == true){
      viewer.setImageMode(true);
      this.imagemode = true;
    }
        
    this.width = this.viewer.chart.width();
    this.height = this.viewer.chart.height();
    this.scale = Math.min(this.width/(70*(maxNodePerDepth+1)),this.height/(100*(treeHeight)));
    var scale = this.scale;
    this.margin = {top: viewer.margin.top + Math.abs((this.height-scale*(100*(treeHeight)))/2), 
                  right: viewer.margin.right, 
                  bottom: viewer.margin.bottom, 
                  left: viewer.margin.left + Math.abs((this.width-scale*70*(maxNodePerDepth))/2)};
    this.i = 0;
    this.duration = 500;
  
    this.tree = d3.layout.tree()
        .size([70*maxNodePerDepth, 100*treeHeight])
        .separation(separation);
    
    this.svg = d3.select(viewer.chart[0]).append("svg")
        .attr("width", "100%")
        .attr("height", "100%");
    this.zoomAndPanEnable(this.imagemode);
    this.vis = this.svg.append("g")
        .attr("transform", "translate(" + this.margin.left  + "," + this.margin.top + ") scale("+this.scale+")");
    
    this.root = json_data;
    this.root.x0 = this.height / 2;
    this.root.y0 = 0;
    
    viewer.addToolbarItem({name:'reset-graph',callback:function(){treegraphlib.TreeGraph.getInstance(this).reset_graph();},style:'home'});
    viewer.addToolbarItem({name:'ht',callback:function(){depgraphlib.GraphViewer.getInstance(this).viewAltContent('ht-content');},style:'info'});
    
    var selected_node_info = viewer.createDiv('selected-node-info');
    viewer.extraDiv.append(selected_node_info);
    var sni_title = viewer.createDiv('sni-title','sni-title');
    var sni_features = viewer.createDiv('sni-features','sni-features');
    var sni_guards = viewer.createDiv('sni-guards','sni-guards');
    selected_node_info.append(sni_title, '<h2>Features</h2>',sni_features, '<h2>Guards</h2>', sni_guards);
    
    viewer.callbacks.fullscreen.oncomplete.push({
      method:this.rescale,
      caller:this
      }
    );
    viewer.callbacks.fullscreen.onclose.push({
      method:this.rescale,
      caller:this
      }
    );
    viewer.callbacks.showaltpanel.push({
      method:function(args){
        this.removeToolbarItem(args[0]);
      },
      caller:viewer,
      args:['ht']
      }
    );
    viewer.callbacks.showaltpanel.push({
      method:function(args){
        this.removeToolbarItem(args[0]);
      },
      caller:viewer,
      args:['reset-graph']
      }
    );
    viewer.callbacks.hidealtpanel.push({
      method:function(args){
        this.addToolbarItem({name:args[0],callback:args[1],style:args[2]});
      },
      caller:viewer,
      args:['reset-graph',function(){treegraphlib.TreeGraph.getInstance(this).reset_graph();},'home']
      }
    );
    viewer.callbacks.hidealtpanel.push({
      method:function(args){
        this.addToolbarItem({name:args[0],callback:args[1],style:args[2]});
      },
      caller:viewer,
      args:['ht',function(){depgraphlib.GraphViewer.getInstance(this).viewAltContent('ht-content');},'info']
      }
    );
  
    this.svgStyle = new Object();
    this.svgStyle.setStyle = setStyle;
    
    function showToolTip(d,i){
      var fixedBBox = screenCoordsForElt(this);
      viewer.loadTooltipContent('guards-'+d.uid);
      d.info_roll = true;
      if(d.hasFeatures){
        d.info_roll = false;
        viewer.loadTooltipContent('decoration-'+d.uid);
      }
      viewer.showTooltip(fixedBBox.se);
    }
  
  
    function screenCoordsForElt(elt){
      while(Object.prototype.toString.call( elt ) === '[object Array]'){
        elt = elt[0];
      }
      var rect = elt.getBBox();
      var svg = viewer.chart[0].querySelector('svg');
      var pt  = svg.createSVGPoint();
      var corners = {};
      var matrix  = elt.getScreenCTM();
      pt.x = rect.x;
      pt.y = rect.y;
      corners.nw = pt.matrixTransform(matrix);
      pt.x += rect.width;
      corners.ne = pt.matrixTransform(matrix);
      pt.y += rect.height;
      corners.se = pt.matrixTransform(matrix);
      pt.x -= rect.width;
      corners.sw = pt.matrixTransform(matrix);
      return corners;
    }
    
    function setStyle(data,index){
      var node = d3.select(this);
      if(data.nodeType == 'node'){
        if(data.type == 'skip'){
          skipNodeStyle(node,data,index);
        }
        else{
          stdNodeStyle(node,data,index);
        }
      }
      else{
        groupNodeStyle(node,data);
      }
      
      addFolderButton(node,data);
    }
  
    function stdNodeStyle(node, data, index){
      addCircle(node,data);
      var text = node.append("text")
      .attr("y", '0px')
      .style("fill-opacity", 1e-6)
      .on("click",showNodeInfo)
      .on("mouseover",showToolTip)
      .on("mouseout",viewer.hideToolTip);
      
      stdNodeDecoration(text, data);
    }
  
    function stdNodeDecoration(text,data){
      var style = nodeTextStyle(data);
      
      text.append("tspan")
      .attr("fill",style.color)
      .style("font-size","0.75em")
      .attr("dx",-6*style.prepend.length/2 + 'px')
      .text(style.prepend);
      
      var match = variableDecompose(style.text);
      if(match!=null){
        style.text = match[1].trim();
      }
      
      var shortened = null;
      if(match == null && data.type=='lex' && style.text.length > maxLexLength){
        shortened = style.text;
        style.text = "lex";
      }
      
      var tspan = text.append("tspan")
      .attr("fill",style.color)
      .attr("x",-6*style.text.length/2 + 'px')
      .attr("dy",function(){return style.prepend ? "1em" : "0px" ;})
      .text(style.text);
      
      if(shortened!=null){
        tspan.attr("title",shortened);
      }
      addTitle(match,tspan);
      
      match = variableDecompose(style.sub);
      if(match!=null){
        style.sub = match[1].trim();
      }
      
      tspan = text.append("tspan")
      .text(style.sub)
      .attr("dx",-6*style.text.length/2 + 'px')
      .attr("dy","1.25em")
      .style("font-size","0.5em");
      
      addTitle(match,tspan);
      
      data.sni_title = data.type.charAt(0).toUpperCase() + data.type.slice(1) + " node " + style.text + 
      " (" + " " + data.uid +")";
    }
  
    function variableDecompose(text){
      var variableRegex = /\^(.*?)\^(.*)/;
      var match = variableRegex.exec(text);
      return match;
    }
  
    function addTitle(match,tspan){
      if(match!=null){
        var title = match[2].trim();
        title = trimLex(title);
        tspan.attr("title",title);
      }
    }
  
    function trimLex(lex){
      lex = lex.replace(/\s/g," | ");
      return lex;
    }
  
    function nodeTextStyle(d){
      var style = {color:"black",prepend:"",text:d.cat,sub:d.name,offset:0};
      
      if(d.type == 'coanchor'){
        style.color = 'magenta';
        style.prepend = "<=>";
      }else if(d.type == 'anchor'){
        style.color = 'orange';
        style.prepend = "<>";
      }else if(d.type == 'subst'){
        style.color = 'blue';
      }else if(d.type == 'lex'){
        style.color = 'magenta';
        style.text = d.lex;
        if(!d.name){
          style.text = "ø";
          style.sub = "lex";
        }
      }else if(d.type == 'foot'){
        style.color = 'blue';
        style.text = '↓' + d.cat;
      }
      
      return style;
    }
  
    function groupNodeText(node,data){
      var def = {x:'-1px',y:'0px',size:'10px',content:''};
      
      if(data.nodeType == 'alternative'){
        def.content = '|';
        def.x = '-1.5px';
        def.y = '2px';
      }else if(data.nodeType == 'sequence'){
        def.content = '.';
        def.y = '0px';
      }else if(data.nodeType == 'optional' && data.n==0){
        def.content = '?';
        def.x = '-2.5px';
        def.y = '3px';
      }else if(data.nodeType == 'optional' && data.n=='*'){
        def.content = '@*';
        def.x = '-6px';
        def.y = '3px';
        def.size = '8px';
      }else if(data.nodeType == 'interleave'){
        def.content = '##';
        def.x = '-6px';
        def.y = '2px';
        def.size = '7px';
      }
      return def;
    }
  
    function groupNodeStyle(node, data){
      var text = groupNodeText(node,data);
      
      addRect(node,data)
      .on("click",showNodeInfo)
      .on("mouseover",showToolTip)
      .on("mouseout",viewer.hideToolTip);
      
      node.append("text")
      .on("click",showNodeInfo)
      .on("mouseover",showToolTip)
      .on("mouseout",viewer.hideToolTip)
      .attr("x", text.x)
      .attr("y", text.y)
      .text(text.content)
      .style("font-size",text.size)
      .style("fill-opacity", 1e-6);
      
      data.sni_title = "Group node " + data.nodeType + "(" + data.uid + ")";
    }
  
    function skipNodeStyle(node, data, index){
      node.append("text")
      .on("click",showNodeInfo)
      .on("mouseover",showToolTip)
      .on("mouseout",viewer.hideToolTip)
      .attr("x", '0px')
      .attr("y", '0px')
      .text('ε')
      .style("fill-opacity", 1e-6);
      
      data.sni_title = "Skip node ("+data.uid+")";
    }
  
    function addRect(node,data){
      var rect = node.append("rect")
      .attr("y",'-7px')
      .attr("x", '-7px')
      .attr("width",1e-6)
      .attr("height",1e-6)
      .on("click",showNodeInfo)
      .attr("fill",decorationGuard)
      .attr("transform","rotate(45)");
      return rect;
    }
  
    function decorationGuard(d){
      if(d.hasGuard){
        return 'url(#allguards)';
      }
      return '#ffffff';
    }
  
    function addCircle(node,data){
      var circle = node.append("circle")
      .attr("class",'textMask')
      .attr("r", 8)
      .style("fill", "#ffffff")
      .on("click", toggleFold);
      
      
      return circle;
    }
  
    function addFolderButton(node,data){
      if(!data.children && !data._children){
        return;
      }
      
      node.append("text")
      .text(function(d) { return d._children ? "+" : "-"; })
      .attr("class",'foldButton')
      .attr("x", function(d){
        var elt = node.select("text");
        if(d.nodeType != 'node'){
          return '15px';
        }
        while(Object.prototype.toString.call( elt ) === '[object Array]'){
          elt = elt[0];
        }
        var rect = elt.getBBox();
        return  rect.x + rect.width + 3 + 'px';
       })
      .attr("y", function(d){
        return d.nodeType == 'node' ? '-1px' : '2px';
       })
      .style("font-size","0.75em")
      .style('font-weight','bold')
      .style("fill","red")
      .on("click", toggleFold);
    }
  
    function showNodeInfo(d,i){
      viewer.debugpanelinfo.html(d.sni_title);
      sni_title.html("<h1>" + d.sni_title + "</h1>");
      sni_guards.html(jQuery('#guards-'+d.uid+viewer.appendOwnID('')).html());
      sni_features.html(jQuery('#decoration-'+d.uid+viewer.appendOwnID('')).html());
      viewer.debugpanelcontent.html(selected_node_info.html());
      d.info_roll = !d.info_roll;
      if(d.info_roll || !d.hasFeatures){
        viewer.loadTooltipContent('guards-'+d.uid);
      }else{
        viewer.loadTooltipContent('decoration-'+d.uid);
      }
    }
    
  };
  
  
  treegraphlib.TreeGraph.instances = treegraphlib.TreeGraph.instances || [];
  
  treegraphlib.TreeGraph.getInstance = function(fromdiv){
    if(treegraphlib.TreeGraph.prototype.isPrototypeOf(fromdiv)){
      return fromdiv;
    }else if(fromdiv.ownerSVGElement != null){
      fromdiv = fromdiv.ownerSVGElement.parentNode.id;
    }else if(typeof fromdiv == 'object' && fromdiv.id != null){
      fromdiv = fromdiv.id;
    }
    
    regex = /.*-(\w+)/;
    var match = regex.exec(fromdiv);
    if(match != null){
      return treegraphlib.TreeGraph.instances[match[1]];
    }
    return null;
  };
  
  
  treegraphlib.TreeGraph.prototype.zoomAndPanEnable = function(value){
    if(!value){
      this.svg.call(d3.behavior.zoom().on("zoom", this.redraw));
    }
  };
  
  treegraphlib.TreeGraph.prototype.redraw = function() {
    
    var me = treegraphlib.TreeGraph.getInstance(this.parentNode.id); // here we should have svg
    var vis = me.vis;
    var minScale = 0.05;
    var scaleSpeed = 8;
    var newScaling = (me.scale * ( Math.pow(d3.event.scale,scaleSpeed)));
    if(newScaling<minScale){
      return;
    }
     
    var movementX = d3.event.sourceEvent.movementX       ||
      d3.event.sourceEvent.mozMovementX    ||
      d3.event.sourceEvent.webkitMovementX ||
      0,
       
    movementY = d3.event.sourceEvent.movementY       ||
      d3.event.sourceEvent.mozMovementY    ||
      d3.event.sourceEvent.webkitMovementY ||
      0;
     
    var previousValues = getVisTransformCurrentValues(vis);
    var S = newScaling/previousValues.scale;
    
    if(d3.event.sourceEvent.type == 'dblclick'){ // don't want to do anything with double click
      var oldScaling = Math.pow(previousValues.scale/me.scale,1.0/scaleSpeed);
      d3.event.target.scale(oldScaling);
      return;
    }
    
    if(S==1){
      var x = parseFloat(previousValues.translate[0]) + movementX;
      var y = parseFloat(previousValues.translate[1]) + movementY;
      vis.attr("transform",
          "translate(" + x + "," + y + ")"
          + " scale(" + newScaling + ")");
      return;
    }
  
     
    var M = {x:d3.event.sourceEvent.layerX,y:d3.event.sourceEvent.layerY};
    var G0 = d3.select(me.viewer.chart[0]).select("svg")[0][0].getBBox();
    var G00 = {x:G0.x-previousValues.translate[0],y:G0.y-previousValues.translate[1]};
    var G1 = {
        x:G0.x*S,
        y:G0.y*S,
        w:G0.width*S,
        h:G0.height*S
    };
    var G11 = {
        x:G00.x*S,
        y:G00.y*S
    };
    var diff = {
        x:M.x*(S-1),
        y:M.y*(S-1)
    };
     
    var diff3 = {
        x:G1.x-diff.x-G11.x,
        y:G1.y-diff.y-G11.y
    };
  
    vis.attr("transform",
        "translate(" + diff3.x + "," +
        diff3.y + ")"
        + " scale(" + newScaling + ")");
  };
  
  
  
  treegraphlib.TreeGraph.prototype.reset_graph = function(){
    var me = treegraphlib.TreeGraph.getInstance(this);
    me.zoomAndPanEnable(this.imagemode);
    me.vis.attr("transform", "translate(" + me.margin.left  + "," + me.margin.top + ") scale("+me.scale+")");
  };
  
  
  
  treegraphlib.TreeGraph.prototype.rescale = function(){
    var chart = this.viewer.chart;
    var previousValues = getVisTransformCurrentValues(this.vis);
    previousValues.scale/=this.scale;
    var scaleChangeFactor = this.scale;
    this.scale = Math.min(chart.width()/(70*(this.maxNodePerDepth+1)),chart.height()/(100*(this.treeHeight)));
    scaleChangeFactor=this.scale/scaleChangeFactor;
    var newmargin = {top: chart.height()/10 + (this.imagemode?0:20) + this.viewer.basemargin + Math.abs((chart.height()-this.scale*(100*(this.treeHeight)))/2), 
        right: this.viewer.margin.right, 
        bottom: this.viewer.margin.bottom, 
        left: this.viewer.margin.left + Math.abs((chart.width()-this.scale*70*(this.maxNodePerDepth))/2)};
    var x = newmargin.left + scaleChangeFactor*(parseFloat(previousValues.translate[0]) - this.margin.left);
    var y = newmargin.top + scaleChangeFactor*(parseFloat(previousValues.translate[1]) - this.margin.top);
    this.vis.attr("transform",
        "translate(" + x + "," + y + ")"
        + " scale(" + (this.scale * previousValues.scale)+ ")");
    this.margin = newmargin;
  };
    
  function separation(a,b){
    return a.parent==b.parent ? 1 : 2;
  }
  
  function getVisTransformCurrentValues(elt){
    var value = elt.attr("transform");
    var pairRegex = /(\w+)\((.*?)\)/g;
    var result = new Object();
    var tmp;
    while((tmp = pairRegex.exec(value)) != null){
      var valuesRegex = /(-*\w+\.*\w*)/g;
      result[tmp[1]] = [];
      var values;
      while((values= valuesRegex.exec(tmp[2]))!=null){
        result[tmp[1]].push(values[1]);
      }
    }
    
    return result;
  }
  
  /**
   * Set transform attribute from an object of the type {x:,y:,scale}
   * If a value is null, then the old value is preserved.
   */
  function setVisTransformCurrentValues(elt,values){
    var prevValues;
    if(values.scale == null || values.x == null || values.y == null){
      prevValues = getVisTransformCurrentValues(elt);
      values.scale = values.scale || prevValues.scale;
      values.x = values.x || prevValues.x;
      values.y = values.y || prevValues.y;
    }
    elt.attr("transform",
        "translate(" + x + "," + y + ")"
        + " scale(" + scale + ")");
  }
  
  function collapse(d) {
    if (d.children) {
      d._children = d.children;
      d._children.forEach(collapse);
      d.children = null;
    }
  }
  
  treegraphlib.TreeGraph.prototype.update = function(source) {
    var line = d3.svg.line()
    .x(function(d) { return d.x; })
    .y(function(d) { return d.y; })
    .interpolate("linear");
    
    // Compute the new tree layout.
    var nodes = this.tree.nodes(this.root).reverse();
    
    // Normalize for fixed-depth.
    nodes.forEach(function(d) { d.y = d.depth * 75; });
    var i = this.i;
    // Update the nodes…
    var node = this.vis.selectAll("g.node")
        .data(nodes, function(d) {
          var id = d.id || (d.id = ++i); 
          return id;
          });
    
    var nodeEnter = node.enter().append("g")
        .attr("class", "node")
        .attr("transform", function(d) { return "translate(" + source.x0 + "," + source.y0 + ")"; });
    
    node.selectAll("text.foldButton")
    .text(function(d,i){
       return d._children ? "+" : "-";
    });
    
    //alert(nodes.toSource());
    nodeEnter.each(this.svgStyle.setStyle);
    
    // Transition nodes to their new position.
    var nodeUpdate = node.transition()
        .duration(this.duration)
        .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
    
    nodeUpdate.selectAll("circle.foldButton")
        .attr("r", 2)
        .style("fill", "#ffffff");
    
    nodeUpdate.selectAll("circle.textMask")
    .attr("r", 8)
    .style("fill", "#ffffff");
    
    nodeUpdate.select("rect")
    .attr("width",14)
    .attr("height", 14);
    
    nodeUpdate.selectAll("text").style("fill-opacity", 1);
    
    var nodeExit = node.exit().transition()
    .duration(this.duration)
    .attr("transform", function(d) { return "translate(" + source.x + "," + source.y + ")"; })
    .remove();
    
    nodeExit.select("circle")
    .attr("r", 1e-6);
    
    nodeExit.select("rect")
    .attr("width", 1e-6)
    .attr("height", 1e-6);
    
    nodeExit.select("text")
    .style("fill-opacity", 1e-6);
    
        
    // Update the links…
    var link = this.vis.selectAll("path.link")
        .data(this.tree.links(nodes), function(d) { return d.target.id; });
    
    link.enter().insert("path", "g")
        .attr("class", "link")
        .attr("d", function(d) {
          var tmp = line([{x: source.x0, y: source.y0}]);
          
          return tmp;
        });
    
    // Transition links to their new position.
    link.transition()
        .duration(this.duration)
        .attr("d", function(d){
          var tmp = line([{x: d.source.x, y: d.source.y},{x:d.target.x,y:d.target.y}]);
          
          return tmp;
        });
    
    link.exit().transition()
        .duration(this.duration)
        .attr("d", function(d) {
          return line([{x:d.source.x, y:d.source.y}]);
        })
        .remove();
    
    // Stash the old positions for transition.
    nodes.forEach(function(d) {
      d.x0 = d.x;
      d.y0 = d.y;
    });
  };
  
  // Toggle children on click.
  function toggleFold(d) {
    if (d.children) {
      d._children = d.children;
      d.children = null;
    } else {
      d.children = d._children;
      d._children = null;
    }
    var me = treegraphlib.TreeGraph.getInstance(this);
    me.update(d);
  }
  
  /**************************************************************/
  /**                      Tools                               **/
  /**************************************************************/
  
  
  function getSVGOwnerContainerID(node){
    var svg = node.ownerSVGElement;
    if(svg != null){
      return svg.parentElement.id;
    }else{
      return '';
    }
  }
  
}(window.treegraphlib = window.treegraphlib || {}, jQuery));