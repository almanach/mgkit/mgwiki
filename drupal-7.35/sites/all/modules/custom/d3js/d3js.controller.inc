<?php

class D3jsController extends DrupalDefaultEntityController{
  
  public function create($type = '') {
    return (object) array(
        'gid' => '',
        'type' => $type,
        'title' => '',
    );
  }
  
  public function save($entity,$sync=true) {
    // If our entity has no basic_id, then we need to give it a
    // time of creation.
    if (empty($entity->gid)) {
      $entity->created = time();
    }
    // Invoke hook_entity_presave().
    module_invoke_all('entity_presave', $entity, 'd3js');
    // The 'primary_keys' argument determines whether this will be an insert
    // or an update. So if the entity already has an ID, we'll specify
    // basic_id as the key.
    $primary_keys = $entity->gid ? 'gid' : array();
    // Write out the entity record.
    drupal_write_record('d3js', $entity, $primary_keys);
    // We're going to invoke either hook_entity_update() or
    // hook_entity_insert(), depending on whether or not this is a
    // new entity. We'll just store the name of hook_entity_insert()
    // and change it if we need to.
    $invocation = 'entity_insert';
    // Now we need to either insert or update the fields which are
    // attached to this entity. We use the same primary_keys logic
    // to determine whether to update or insert, and which hook we
    // need to invoke.
    if (empty($primary_keys)) {
      field_attach_insert('d3js', $entity);
    }
    else {
      field_attach_update('d3js', $entity);
      $invocation = 'entity_update';
    }
    // Invoke either hook_entity_update() or hook_entity_insert().
    module_invoke_all($invocation, $entity, 'd3js');
    if($sync){
        if($entity->type == "parser_output_graph" || $entity->type == "mgwiki_generic_depgraph"){
            d3js_depgraph_sync($entity);
        }
        
    }

    return $entity;
  }
  
}