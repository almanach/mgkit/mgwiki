<?php
/**
 * This file define function that help building dpath signature from graph data
 */

/**
 * Convert an array of highlightings edge ids to an array of highlightings edge structure definitions
 * @param unknown $xml
 * @param unknown $highlightings
 * @return mixed
 */
function d3js_frmg_highlighting_id2struct($xml,$highlightings=null){
  $hstructs = array();

  // if none is given, then transform the highlightings edges given in the entity graph 
  if(!isset($highlightings)){
    $highlightings = (string)$xml['highlighting'];
    $highlightings = explode(",",$highlightings);
  }
  
  foreach($highlightings as $highlighting){
    $edges = $xml->xpath("//edge[@id = '$highlighting']");
    
    $edge = getTheOnlyEltInArrayOrReturnFalse($edges);
    
    if($edge === false){
      Logger::Log($highlighting);
      Logger::Log("couldn't find matching edge");
      continue;
    }

    $source_id = $edge['source'];
    $target_id = $edge['target'];
    $source = getTheOnlyEltInArrayOrReturnFalse($xml->xpath("//node[@id = '$source_id']"));
    $target = getTheOnlyEltInArrayOrReturnFalse($xml->xpath("//node[@id = '$target_id']"));
    
    if($source === false && $target === false){
      Logger::Log("couldn't find the source or target of the edge");
      continue;
    }
    
    $hstructs[] = array(
        (string)$source['cat'],
        (string)$source['form'],
        (string)$source['lemma'],
        (string)$edge['label'],
        (string)$edge['type'],
        (string)$target['cat'],
        (string)$target['form'],
        (string)$target['lemma'],
    );
    
  }
  
  return $hstructs;
}


/**
 * try to find an existing node source and add the current edge to its edge list
 * @param  [type] $graph   the graph structure of highlights
 * @param  [type] $nsid    the node source id of the edge
 * @param  [type] $newlink the new edge
 * @return a boolean indicating if the edge has been attached to an existing node
 */
function d3js_frmg_edge_try_attach_right(&$graph,$nsid,&$newlink){
  foreach($graph as &$elt){
    if($elt == null){
      continue;
    }
    $node;
    if(!isset($elt["ns"])){
      $node = &$elt['nt']; 
    }else{
      $node = &$elt;
    }
    
    if($node["ns"] == $nsid){
      $node["links"][] = $newlink;
      return true;
    }else if($node["links"]!=null){
      if(d3js_frmg_edge_try_attach_right($node["links"],$nsid,$newlink)){
        return true;
      }
    }
  }
  return false;
}

/**
 * extract highlights from a depgraph formatted data into a structured data
 * @param  [type] $json the graph data in the depgraph json format
 * @return a tree structure of highlights found in the graph
 */
function d3js_extract_highlightings2matchingRule($json){
  $graphMatch = array();
  
  if(!isset($json['graph'])){
    return array();
  }

  // construct indexed by id node table
  $nodes = array();
  if(isset($json['graph']['words'])){
    $p = 0;
    foreach($json['graph']['words'] as $word){
      $id = $word['id'];
      $nodes[$id] = $word;
      if(!isset($word['#position'])){
        $word['#position']=$p;
      }
      $p++;
    }
  }
  

  // get highlights
  if(isset($json['graph']['links'])){
    $linkId = 0;
    foreach($json['graph']['links'] as $link){
      Logger::Log($link);
      if(isset($link['#style']) && isset($link['#style']['highlighted']) && $link['#style']['highlighted']==true){
        $source = isset($nodes[$link['source']])?$nodes[$link['source']]:null;
        $target = isset($nodes[$link['target']])?$nodes[$link['target']]:null;
        if(!isset($source) && !isset($target)){
          Logger::Log("couldn't find the source or target of the edge");
          continue;
        }
        $dir = "right";
        if($source['#position']>$target['#position']){
          $dir = "left";
        }
        $cat = (isset($target['#data']['pos']))?(string)$target['#data']['pos']:(string)$target['#data']['cat'];
        $newlink = array(
          "nt" => array(
              "ns" => $link['target'], 
              "info" => array(
                  "cat"=>$cat,
                  "token"=>(string)$target['label'],
                  "lemma"=>(string)$target['#data']['lemma'],
                  "position"=>$target['#position']
                  ),
              "links" => array()),
          "info"=>array(
              "id"=>$linkId++,
              "label" => (string)$link['label'],
              "type" =>null,
              "dir"=>$dir
              )
          );
      
        for($i = 0; $i<count($graphMatch); $i++){
          $elt = $graphMatch[$i];
          if($elt!=null && $elt["ns"] == $link['target']){
            $newlink["nt"] = $elt;
            $graphMatch[$i] = null;
          }
        }
        
        
        $success = d3js_frmg_edge_try_attach_right($graphMatch,$link['source'],$newlink);
        $cat = (isset($source['#data']['pos']))?(string)$source['#data']['pos']:(string)$source['#data']['cat'];
        if(!$success){
          $graphMatch[] = array(
            "ns" => $link['source'],
            "info" =>  array(
                    "cat"=>$cat,
                    "token"=>(string)$source['label'],
                    "lemma"=>(string)$source['#data']['lemma'],
                    "position"=>$source['#position']
             ),
            "links"=> array($newlink)
          );
        }
      }
    }
  }
  


  return $graphMatch;
}

/**
 * extracts hightlights from graph data formatted in depxml into a structure data
 * @param  [type] $xml           the graph data in depxml format
 * @param  [type] $highlightings (optional) list of highlighted edges (if null, then highlightings are found in the metadata propery 'highlightings' of the xml)
 * @param  array  $nomatch an array to store the highlightings id that couldn't be found in the graph data    
 * @return a tree structure of highlights found in the graph
 */
function d3js_frmg_highlightings2matchingRule($xml,$highlightings=null,&$nomatch = array()){
  $graphMatch = array();
  
  if(!isset($highlightings)){
    $highlightings = (string)$xml['highlighting'];
    $highlightings = explode(",",$highlightings);
  }
  
  foreach($highlightings as $highlighting){
    $edges = $xml->xpath("//edge[@id = '$highlighting']");
  
    $edge = getTheOnlyEltInArrayOrReturnFalse($edges);
  
    if($edge === false){
      Logger::Log($highlighting);
      $nomatch[] = $highlighting;
      Logger::Log("couldn't find matching edge");
      continue;
    }
  
    $source_id = (string)$edge['source'];
    $target_id = (string)$edge['target'];
    $source = getTheOnlyEltInArrayOrReturnFalse($xml->xpath("//node[@id = '$source_id']"));
    $target = getTheOnlyEltInArrayOrReturnFalse($xml->xpath("//node[@id = '$target_id']"));
  
    

    if($source === false && $target === false){
      Logger::Log("couldn't find the source or target of the edge");
      continue;
    }

    $sc = explode("_",(string)$source['cluster']);
    $tc = explode("_", (string)$target['cluster']);
    $dir = comp_node_position_2($source,$target);
  
    $newlink = array(
        "nt" => array(
            "ns" => $target_id, 
            "info" => array(
                "cat"=>(string)$target['cat'],
                "token"=>(string)$target['form'],
                "lemma"=>(string)$target['lemma'],
                "position"=>$tc[1]
                ),
            "links" => array()),
        "info"=>array(
            "id" => (string)$edge['id'],
            "label" => (string)$edge['label'],
            "type" => (string)$edge['type'],
            "dir" => $dir
            )
        );
    
    //Logger::Log($newlink,"the new link");
    //Logger::Log($source_id);
    
    for($i = 0; $i<count($graphMatch); $i++){
      $elt = $graphMatch[$i];
      if($elt!=null && $elt["ns"] == $target_id){
        $newlink["nt"] = $elt;
        $graphMatch[$i] = null;
      }
    }
    
    
    $success = d3js_frmg_edge_try_attach_right($graphMatch,$source_id,$newlink);
    
    if(!$success){
      $graphMatch[] = array(
        "ns" => $source_id,
        "info" =>  array(
                "cat"=>(string)$source['cat'],
                "token"=>(string)$source['form'],
                "lemma"=>(string)$source['lemma'],
                "position"=>$sc[1]
         ),
        "links"=> array($newlink)
      );
    }
  
  }
  return $graphMatch;
}

/**
 * compare the position of two nodes and determine the direction from one to the other
 * @param  [type] $a the source node
 * @param  [type] $b the target node
 * @return the direction "left" or "right"
 */
function comp_node_position_2($a,$b){
  $sc = explode("_",(string)$a['cluster']);
  $tc = explode("_", (string)$b['cluster']);
  $dir = "right";
  if($sc[1]==$tc[1]){
    if($sc[2]==$tc[2]){
      if((string)$a['id']<(string)$b['id']){
        $dir = "right";
      }else{
        $dir = "left";
      }
    }else if($sc[2]<$tc[2]){
      $dir = "right";
    }else{
      $dir = "left";
    }
  }else if($sc[1]<$tc[1]){
    $dir = "right";
  }else{
    $dir = "left";
  }

  return $dir;
}

/**
 * quote a string (and apply proper escapes in string)
 * @param  [type] $string the string to quote
 * @return [type]         the string quoted with '"'
 */
function _d3js_quote_string($string){
  if(preg_match('/"/', $string)!=0){
    return "\"" . str_replace("\"", "\\\"", $string) . "\"";
  }else{
    return '"' . $string . '"';
  }
}

/**
 * transform a tree structure of highlightings into OGRE graph match pattern
 * @param  [type] $graphMatch the tree structure of highlightings
 * @return the OGRE representation of this signature
 */
function d3js_frmg_matchingRule2String($graphMatch){
  $string = "match{";
  foreach($graphMatch as $elt){
    if($elt == null){
      continue;
    }
    $string .= d3js_frmg_matchingRule2String_elt($elt) . ","; 
  }
  return substr($string, 0, strlen($string)-1) . "}";
}

/**
 * transform an element of the signature/highlightings tree structure into a OGRE match element
 * @param  [type] $elt    a node in the highlightings tree structure
 * @param  string $string the current string of the OGRE match
 * @return the part of string OGRE match
 */
function d3js_frmg_matchingRule2String_elt($elt,&$string=""){
  $string .= " ["
    ."cat:"._d3js_quote_string($elt['info']['cat']).","
    ."lemma:"._d3js_quote_string($elt['info']['lemma']).","
    ."token:"._d3js_quote_string($elt['info']['token'])
    ."] ";
  
  $n = count($elt["links"]);
  
  if($n > 1){
    $string.="(";  
  }
  
  for($i = 0 ; $i < $n ; $i++){
    $link = $elt["links"][$i];
    $string.= "-["
      ."label:"._d3js_quote_string($link['info']["label"]).","
      ."type:"._d3js_quote_string($link['info']["type"])
      ."]->";
    
    
    d3js_frmg_matchingRule2String_elt($link["nt"],$string);
    
    if($i < ($n-1)){
      $string .= ",";
    }
    
    
  }
  
  if($n > 1){
    $string.=") ";
  }
    
  return $string;
}

/**
 * transform a tree structure of highlightings into a dpath query/signature
 * @param  [type]  $graphMatch  the tree structure of the signature
 * @param  boolean $lemmatized  boolean indicating if the signature should contain lemma constraint
 * @param  boolean $lexicalized boolean indicating if the signature should contain token constraint
 * @return a dpath query/signature
 */
function d3js_frmg_matchingRule2Dpath($graphMatch,$lemmatized=false,$lexicalized=false){
  $string = "nodes";
  foreach($graphMatch as $elt){
    if($elt == null){
      continue;
    }
    $string .= d3js_frmg_matchingRule2Dpath_elt($elt,$lemmatized,$lexicalized) . " AND nodes"; 
  }
  return substr($string, 0, strlen($string)-10) . ";"; 
}

/**
 * transform a tree structure element into a dpath query part
 * @param  [type]  $elt         the current node in the tree structure
 * @param  boolean $lemmatized  boolean indicating if the signature should contain lemma constraint
 * @param  boolean $lexicalized boolean indicating if the signature should contain token constraint
 * @param  string  $string      the current string part of the dpath signature to be built
 * @return the part of the dpath signature for this element
 */
function d3js_frmg_matchingRule2Dpath_elt($elt,$lemmatized=false,$lexicalized=false,&$string=""){
  $string .= " [@cat="._d3js_quote_string($elt['info']['cat'])."] ";
  if($lemmatized){
    $string .= "[@lemma="._d3js_quote_string($elt['info']['lemma'])."] ";
  }
  if($lexicalized){
   $string .= "[@form="._d3js_quote_string($elt['info']['token'])."] "; 
  }

  $n = count($elt["links"]);
  usort($elt["links"],function($a,$b){return d3js_link_sort_string($a)< d3js_link_sort_string($b) ? 1 : -1;});
  for($i = 0 ; $i < $n ; $i++){
    $link = $elt["links"][$i];
    Logger::Log($link['info']['id']);
    $dir = (isset($link['info']['dir']))?"[dir="._d3js_quote_string($link['info']['dir'])."]":"";
    $string .= ".(out ".$dir." [@label="._d3js_quote_string($link['info']["label"])."] target";
    
    d3js_frmg_matchingRule2Dpath_elt($link["nt"],$lemmatized,$lexicalized,$string);
    $string .= ")";
    
    
  }
  
  return $string;
}

/**
 * transform a tree structure of highlightings into a dpath query/signature
 * @param  [type]  $graphMatch  the tree structure of the signature
 * @param  boolean $lemmatized  boolean indicating if the signature should contain lemma constraint
 * @param  boolean $lexicalized boolean indicating if the signature should contain token constraint
 * @param  $type the type of signature to produce (depconll|depxml style (cat) or conll|passage style (pos))
 * @return a dpath query/signature
 */
function d3js_matchingRule2Dpath($graphMatch,$lemmatized=false,$lexicalized=false,$type){
  $string = "nodes";
  foreach($graphMatch as $elt){
    if($elt == null){
      continue;
    }
    $dummy = "";
    $string .= d3js_matchingRule2Dpath_elt($elt,$lemmatized,$lexicalized,$dummy,$type) . " AND nodes"; 
  }
  return substr($string, 0, strlen($string)-10) . ";"; 
}

/**
 * transform a tree structure element into a dpath query part
 * @param  [type]  $elt         the current node in the tree structure
 * @param  boolean $lemmatized  boolean indicating if the signature should contain lemma constraint
 * @param  boolean $lexicalized boolean indicating if the signature should contain token constraint
 * @param  string  $string      the current string part of the dpath signature to be built
 * @param  $type the type of signature to produce (depconll|depxml style (cat) or conll|passage style (pos))
 * @return the part of the dpath signature for this element
 */
function d3js_matchingRule2Dpath_elt($elt,$lemmatized=false,$lexicalized=false,&$string="",$type){
  if(!isset($type) || $type == "depxml" || $type == "depconll"){
    return d3js_frmg_matchingRule2Dpath_elt($elt,$lemmatized,$lexicalized,$string);
  }else{
    return d3js_conll_matchingRule2Dpath_elt($elt,$lemmatized,$lexicalized,$string);
  }
}

/**
 * create a specific string denoting an edge for sorting purpose
 * @param  [type] $link a link node in the highlightings tree structure
 * @return a string that will be used to sort links 
 */
function d3js_link_sort_string($link){
  return $link['info']['type'] . $link['info']['label'] . $link['nt']['info']['cat'] . $link['nt']['info']['token'];
}

/**
 * transform a tree structure element into a dpath query part
 * @param  [type]  $elt         the current node in the tree structure
 * @param  boolean $lemmatized  boolean indicating if the signature should contain lemma constraint
 * @param  boolean $lexicalized boolean indicating if the signature should contain token constraint
 * @param  string  $string      the current string part of the dpath signature to be built
 * @return the part of the dpath signature for this element
 */
function d3js_conll_matchingRule2Dpath_elt($elt,$lemmatized=false,$lexicalized=false,&$string=""){
  $string .= " [@pos="._d3js_quote_string($elt['info']['cat'])."] ";
  if($lemmatized){
    $string .= "[@lemma="._d3js_quote_string($elt['info']['lemma'])."] ";
  }
  if($lexicalized){
   $string .= "[@form="._d3js_quote_string($elt['info']['token'])."] "; 
  }

  $n = count($elt["links"]);
  usort($elt["links"],function($a,$b){return d3js_link_sort_string($a)< d3js_link_sort_string($b) ? 1 : -1;});
  for($i = 0 ; $i < $n ; $i++){
    $link = $elt["links"][$i];
    $dir = (isset($link['info']['dir']))?"[dir="._d3js_quote_string($link['info']['dir'])."]":"";
    $string .= ".(out ".$dir." [@label="._d3js_quote_string($link['info']["label"])."] target";
    
    d3js_frmg_matchingRule2Dpath_elt($link["nt"],$lemmatized,$lexicalized,$string);
    $string .= ")";
    
    
  }
  
  return $string;
}


/**
 * transform a list of edges definition into a list of edges id considering a depxml graph data
 * this is used when updating depxml graph
 * @param  [type] $xmldata       the graph data to match the link against
 * @param  [type] $highlightings the list of edges definition to transform
 * @return the list of edges ids corresponding to the entry definitions list
 */
function d3js_frmg_detailedHighlight2id(&$xmldata,$highlightings){
  $highlightingids = array();
  foreach($xmldata->edge as $edge){
    if(count($highlightings)==0){
      break;
    }

    for($i = 0; $i<count($highlightings); $i++){
      $highlighting = $highlightings[$i];
      $cond0 = (string) $edge['label'] == $highlighting['info']['label'] && (string) $edge['type'] == $highlighting['info']['type'];
      if($cond0){
        $source_id = (string)$edge['source'];
        $source = getTheOnlyEltInArrayOrReturnFalse($xmldata->xpath("//node[@id = '$source_id']"));
        $sc = explode("_",(string)$source['cluster']);
        
        $cond1 = (string)$source['cat'] == $highlighting['source']['cat'] &&
          (string)$source['form'] == $highlighting['source']['token'] &&
          (string)$source['lemma'] == $highlighting['source']['lemma'] &&
          (string)$sc[1] == $highlighting['source']['position'];

        if($cond1){
          $target_id = (string)$edge['target'];
          $target = getTheOnlyEltInArrayOrReturnFalse($xmldata->xpath("//node[@id = '$target_id']"));
          $tc = explode("_", (string)$target['cluster']);

          $cond2 = (string)$target['cat'] == $highlighting['target']['cat'] &&
            (string)$target['form'] == $highlighting['target']['token'] &&
            (string)$target['lemma'] == $highlighting['target']['lemma'] &&
            (string)$tc[1] == $highlighting['target']['position'];

          if($cond2){
            $highlightingids[] = (string) $edge['id'];
            array_splice($highlightings, $i,1);
            break;
          }
        }

        
      }
    }    
  }

  return $highlightingids;
}

/**
 * retrieve the list of edge definitions that are found in the signature tree structure
 * @param  [type] $matchingRule the signature tree structure
 * @return the list of edge definitions found in the tree structure
 */
function d3js_frmg_matchingRule2detailedHighlight($matchingRule){
  $highlightings = array();
  foreach($matchingRule as $elt){
    if($elt == null){
      continue;
    }
    d3js_frmg_matchingRule2detailedHighlight_elt($elt,$highlightings); 
  }
  return $highlightings;
}

/**
 * create an edge definition from a signature tree structure node
 * @param  [type] $elt           the signature tree structure node
 * @param  [type] $highlightings the list of highlightings to populate
 */
function d3js_frmg_matchingRule2detailedHighlight_elt($elt,&$highlightings){
  $highlight = null;
  $n = count($elt["links"]);
  if($n>0){
    
  }

  for($i = 0 ; $i < $n ; $i++){
    $link = $elt["links"][$i];
    $highlight = &$highlightings[];
    $highlight['target'] = array(
      "cat"=>$link['nt']['info']['cat'],
      "lemma"=>$link['nt']['info']['lemma'],
      "token"=>$link['nt']['info']['token'],
      "position"=>$link['nt']['info']['position']
      );
    $highlight['info'] = array(
      "label"=>$link['info']["label"],
      "type"=>$link['info']["type"]
      );
    $highlight['source'] = array(
      "cat"=>$elt['info']['cat'],
      "lemma"=>$elt['info']['lemma'],
      "token"=>$elt['info']['token'],
      "position"=>$elt['info']['position']
      );
    
    d3js_frmg_matchingRule2detailedHighlight_elt($link["nt"],$highlightings);
    
  }
}

/**
 * compare two depxml data
 * @param  [type] $data1 the data of a graph
 * @param  [type] $data2 the data of another graph
 * @return a boolean indicating if theses graph are similars
 */
function d3js_frmg_compare($data1,$data2){
  if(isset($data1)!=isset($data2)){
    return false;
  }
  $xmldata1 = new SimpleXMLElement($data1);
  $xmldata2 = new SimpleXMLElement($data2);
  $edges1 = array();
  foreach($xmldata1->edge as $edge){
    $edges1[] = (string)$edge['id'];
  }
  $edges2 = array();
  foreach($xmldata2->edge as $edge){
    $edges2[] = (string)$edge['id'];
  }
  $sig1 = d3js_frmg_matchingRule2Dpath(d3js_frmg_highlightings2matchingRule(new SimpleXMLElement($data1),$edges1));
  $sig2 = d3js_frmg_matchingRule2Dpath(d3js_frmg_highlightings2matchingRule(new SimpleXMLElement($data2),$edges2));
  Logger::Log($sig1);
  Logger::Log($sig2);
  return $sig1 == $sig2;
}

/**
 * get the detailed disamb param for a graph
 * @param  [type] $xml    the graph data
 * @param  [type] $params the disamb params expressed with constraints on edge ids
 * @return the disamb params expressed with constraints on edge properties
 */
function d3js_frmg_get_detailedparams($xml,$params){
  $cb = FlexibleCallback::NewCallback(function($arguments){
    $match = $arguments[0];
    $xml = $arguments[1][0];
    $edges = $xml->xpath("//edge[@id = 'E1e".$match[1]."']");
    $edge = getTheOnlyEltInArrayOrReturnFalse($edges);
    $source_id = (string)$edge['source'];
    $target_id = (string)$edge['target'];
    $source = getTheOnlyEltInArrayOrReturnFalse($xml->xpath("//node[@id = '$source_id']"));
    $target = getTheOnlyEltInArrayOrReturnFalse($xml->xpath("//node[@id = '$target_id']"));
    $newparam = "edge=".(string)$source['lemma']."&".(string)$source['cat']."&".(string)$edge['label']."&".(string)$target['lemma']."&".(string)$target['cat']."&".$match[2];
    return $newparam;
  },array($xml));
  return preg_replace_callback("/eid=(\d+)&([\+-]\d+)/", $cb , $params);
}

/**
 * get the id constraint based disamb param for a graph
 * @param  [type] $xml             the graph data
 * @param  [type] $detailedoptions the disamb params expressed with constraints on edge properties
 * @return the disamb params expressed with constraints on edge ids
 */
function d3js_frmg_detailedparams2id($xml,$detailedoptions){
  $cb = FlexibleCallback::NewCallback(function($arguments){
    $match = $arguments[0];
    $xml = $arguments[1][0];
    foreach($xml->edge as $edge){
      $cond0 = (string) $edge['label'] == $match[3];
      if($cond0){
        $source_id = (string)$edge['source'];
        $source = getTheOnlyEltInArrayOrReturnFalse($xml->xpath("//node[@id = '$source_id']"));
        $sc = explode("_",(string)$source['cluster']);
        
        $cond1 = (string)$source['cat'] == $match[2] &&
          (string)$source['lemma'] == $match[1];

        if($cond1){
          $target_id = (string)$edge['target'];
          $target = getTheOnlyEltInArrayOrReturnFalse($xml->xpath("//node[@id = '$target_id']"));
          $tc = explode("_", (string)$target['cluster']);

          $cond2 = (string)$target['cat'] == $match[4] &&
            (string)$target['lemma'] == $match[5];

          if($cond2){
            $newparam = "eid=".preg_replace("/E\d+e(\d+)/", "$1", (string) $edge['id'])."&".$match[6];
            return $newparam;
          }
        }
      }
    }
  },array($xml));
  return preg_replace_callback("/edge=(\S*?)&(\S*?)&(\S*?)&(\S*?)&(\S*?)&([\+-]\d+)/", $cb, $detailedoptions);
}
