<?php

// add the js application ressources
$basepath = drupal_get_path('module','mgwiki_corpus') ;
drupal_add_js($basepath."/js/cma/build/cma.js");
drupal_add_css($basepath."/js/cma/build/style/cma.css");
drupal_add_js($basepath."/js/cma/lib/jquery-textcomplete-master/jquery.textcomplete.js");
drupal_add_css($basepath."/js/cma/lib/jquery-textcomplete-master/jquery.textcomplete.css");




global $user;
$user_light = array(
  'name'=>$user->name,
  'uid'=>$user->uid
);
global $base_url;
$ws_url = $base_url . '/mgwiki_corpus/';

$options = array(
    "ws_url"=>$ws_url,
    "exit_url" => $base_url,
    "host_url"=>$base_url,
    "websocket_url"=>"alpage.inria.fr:8081/userconnexion", // change to marc|passage.inria.fr/frmgwiki/
    "ws"=>array(
      "userlist"=> $base_url.'/mgwiki/ws/userlist',
    ),
    "user"=>$user_light,
 );

?>
<!--<div id="cma-container">Coming back soon.. :)</div>-->
<script>
  new cma.App(null,<?php echo json_encode($options);?>);
</script>



