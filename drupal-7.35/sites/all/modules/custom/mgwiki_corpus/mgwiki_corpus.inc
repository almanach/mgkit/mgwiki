<?php 

define("FRMGCM_HOST","http://localhost:8082/");
define("FRMGCM_CORPUS_HOST","http://localhost:8082/corpus/");

class MGWikiCorpusController extends DrupalDefaultEntityController{

  public function create($type = 'default') {
    global $user;
    return (object) array(
        'cid' => '',
        'type' => $type,
        'title' => '',
        'owner'=>$user->uid
    );
  }

  public function save($entity) {
    // If our entity has no basic_id, then we need to give it a
    // time of creation.
    if (empty($entity->gid)) {
      $entity->created = time();
    }
    // Invoke hook_entity_presave().
    module_invoke_all('entity_presave', $entity, 'mgwiki_corpus');
    // The 'primary_keys' argument determines whether this will be an insert
    // or an update. So if the entity already has an ID, we'll specify
    // basic_id as the key.
    $primary_keys = $entity->cid ? 'cid' : array();
    // Write out the entity record.
    drupal_write_record('mgwiki_corpus', $entity, $primary_keys);
    // We're going to invoke either hook_entity_update() or
    // hook_entity_insert(), depending on whether or not this is a
    // new entity. We'll just store the name of hook_entity_insert()
    // and change it if we need to.
    $invocation = 'entity_insert';
    // Now we need to either insert or update the fields which are
    // attached to this entity. We use the same primary_keys logic
    // to determine whether to update or insert, and which hook we
    // need to invoke.
    if (empty($primary_keys)) {
      field_attach_insert('mgwiki_corpus', $entity);
    }
    else {
      field_attach_update('mgwiki_corpus', $entity);
      $invocation = 'entity_update';
    }
    // Invoke either hook_entity_update() or hook_entity_insert().
    module_invoke_all($invocation, $entity, 'mgwiki_corpus');
    return $entity;
  }

}

/**
 * Enumeration of corpus possible status
 * @author buiquang
 *
 */
class CorpusStatus{
  const UNKNOWN = 0;
  const UPLOADING = 1; // upload in progress
  const WAITING = 2; // analysis process waiting (due to cluster reservation node)
  const AVAILABLE = 3; // analysis finished with success
  const UPLOAD_ERROR = 4;
  const ANALYSIS_ERROR = 5;
  const DEFAULT_ERROR = 6; 
  const PREPROCESSING = 7; // unpacking, format conversion, dag creation
  const PROCESSING = 8; // analysis process
}

class CorpusActions{
  const DELETE = 'delete';
  const PROCESS = 'process';
  const DOWNLOAD = 'download';
}


class Corpus{
  
  const ALPAGE_PREFIX = '/home/rioc/buiquang/exportbuild';
  const RIOC_PATH = '/scratch/buiquang/mgwiki/';
  
  const MAX_SIZE = 2147483648;
  const TMP_DIR = '/tmp/';
  const FINAL_DIR = '/corpus/';
  private $public_path = null;

  public /*CorpusStatus*/ $status = CorpusStatus::UNKNOWN;  // corpus status
  public $path = null;                                      // root directory path of the corpus (private_files/corpus/cid/)
  public $cid = null;
  public $name = null;
  public $dbobject = null;
  public $actions = array();
  public $format = null;
  
  /**
   * Create corpus object from corpus id (exist in db)
   * @param unknown $cid
   */
  public function __construct($cid = null,$nodb = false){
    $this->public_path = drupal_realpath('public://');
    if(isset($cid) && !$nodb){
      $entity = reset(entity_load('mgwiki_corpus', array($cid), array(), false));
    }else{
      return $this;
    }
    
    $this->init($entity);
    

    
    
    return $this;
  }
  
  public function init(&$entity){
    $cid = $entity->cid;
    $this->dbobject = &$entity;
    $this->path = $entity->path;
    $this->prepareContainerFolder();
    $this->cid = $cid;
    $this->name = $entity->title;
    
    $this->ownerName = user_load($this->dbobject->owner)->name;
    
    $corpus = retrieve_corpus($cid);
    $cpmCorpus = json_decode($corpus,true);
    Logger::Log($corpus);
    if(isset($cpmCorpus['error'])){
      $this->status = "uploading"; // most probable case
    }else{
      $this->status = $cpmCorpus['status'];
      if(!$this->isProcessLaunched()){
        $this->status = "waiting";
      }
      if(isset($cpmCorpus['outputFormat'])){
        $this->format = $cpmCorpus['outputFormat'];
      }
    }
    
    if($this->status == "available"){
      $this->actions[] = "download";
    }
    global $user;
    if($user->uid == $this->dbobject->owner || isAdmin()){
      if($this->status == "waiting"){
        //$this->actions[] = "process";
      }
      $this->actions[] = "delete";
    
      if($this->dbobject->permissions == 777){
        $this->permissionText = t("Make this corpus private");
      }else{
        $this->permissionText = t("Make this corpus public");
      }
    }
  }
  
  
  public function setName($name){
    $this->name = $name;
    $this->dbobject->title = $name;
    entity_get_controller('mgwiki_corpus')->save($this->dbobject);
  }
  
  /////////////////////////////////////////////////////////////////////////////////
  //                          UPLOAD                                             //
  /////////////////////////////////////////////////////////////////////////////////
  
  
  /**
   * Retrieve a corpus in uploading stage
   * @param unknown $key the file key tempname of the corpus
   */
  public static function GetUploading($key){
    if(!isset($key)) {
      $key = mt_rand() . '.tmp';
    }
    $cid = variable_get('corpus_upload_'.$key,null);
    $corpus = new Corpus($cid,true);
    if(!isset($cid)){
      variable_set('corpus_upload_'.$key, $corpus->cid);
    }
    $corpus->uploadTmpName = $key;
    return $corpus;
  }
  
  public function uploadFile(){
    Logger::Log("uploading chunk of file");
    //Make sure the total file we're writing to hasn't surpassed the file size limit
    if(file_exists($this->public_path . Corpus::TMP_DIR . $this->uploadTmpName)) {
      if(filesize($this->public_path . Corpus::TMP_DIR . $this->uploadTmpName) > self::MAX_SIZE) {
        $this->abortUpload();
        return json_encode(array(
            'errorStatus' => 1,
            'errorText' => 'File is too large.'
        ));
      }
    }
    
    //Open the raw POST data from php://input
    $fileData = file_get_contents('php://input');
    
    //Write the actual chunk to the larger file
    $handle = fopen($this->public_path . Corpus::TMP_DIR . $this->uploadTmpName, 'a');
    
    fwrite($handle, $fileData);
    fclose($handle);
    
    return json_encode(array(
        'key' => $this->uploadTmpName,
        'errorStatus' => 0
    ));
  }
  
  public function abortUpload() {
    if(unlink($this->public_path . Corpus::TMP_DIR . $this->uploadTmpName)) {
      return json_encode(array('errorStatus' => 0));
    }
    else {
  
      return json_encode(array(
          'errorStatus' => 1,
          'errorText' => 'Unable to delete temporary file.'
      ));
    }
  }
  
  /**
   * Delete the variable set for corpus uploading stage
   * @param unknown $key
   */
  public function finishUpload($name){
    try{
      $name = standardize_string($name);
      // create corpus in mgwiki db
      $entity = entity_get_controller('mgwiki_corpus')->create();
      entity_get_controller('mgwiki_corpus')->save($entity); // get cid;
      $this->cid = $cid = $entity->cid;
      $entity->path = drupal_realpath('public://').'/corpus/'.$cid;
      entity_get_controller('mgwiki_corpus')->save($entity);
      $this->init($entity);
      
      variable_del('corpus_upload_'.$this->uploadTmpName);
      $tmpFile = drupal_realpath('public://').Corpus::TMP_DIR. $this->uploadTmpName;
      rename($tmpFile, $this->path.'/original/'.$name);
      // default private corpus
      $this->setName($name);
      create_corpus($this->cid, $this->path,$name);
      return json_encode(array('errorStatus' => 0));
    }
    catch(Exception $e){
      Logger::Log($e);
      return json_encode(array(
          'errorStatus' => 1,
          'errorText' => $e->getMessage()
      ));
    }
  }
  
  public function postUnsupported() {
    $name = $_FILES['bigUploadFile']['name'];
    $name = standardize_string($name);
    $size = $_FILES['bigUploadFile']['size'];
    $tempName = $_FILES['bigUploadFile']['tmp_name'];
  
    if(filesize($tempName) > self::MAX_SIZE) {
      return 'File is too large.';
    }
  
    if(move_uploaded_file($tempName, $this->getMainDirectory() . $name)) {
      return 'File uploaded.';
    }
    else {
      return 'There was an error uploading the file';
    }
  
  }
  
  /////////////////////////////////////////////////////////////////////////////////
  //                          WEBSERVICE HANDLERS                                //
  /////////////////////////////////////////////////////////////////////////////////
  
  
  public function hasPermission($permissionType,$theuser=null){
    if(!isset($theuser)){
      global $user;
      $theuser = $user;
    }
    
    if(isAdmin($theuser)){
      return true;
    }
    
    $permission;
    
   $rights = db_query('SELECT pid, type FROM mgwiki_corpus_permissions WHERE user = :uid AND cid= :cid', array(':uid' => $theuser->uid,':cid'=>$this->cid))
      ->fetchAssoc();
   if($rights !== false){
      $permission = $rights['type'];  
    }else{
      $rights = db_query('SELECT pid, type FROM mgwiki_corpus_permissions WHERE user = :uid AND cid= :cid', array(':uid' => 0,':cid'=>$this->cid))
      ->fetchAssoc();
      $permission = $rights['type'];
    }
    
    switch($permissionType){
      case 'write' :
        return ($permission == 3 || $permission == 5 || $permission == 7 || $permission == 9);
      case 'read':
        return ($permission == 2 || $permission == 5 || $permission == 6 || $permission == 9);
      case 'admin':
        return ($permission == 4 || $permission == 6 || $permission == 7 || $permission == 9);
    }
    
    
    return false;
  }
  
  public function updatePermissions($permissions){
    if(!$this->hasPermission('admin')){
      return '{"error":"unauthorized access"}';
    }
    /*db_delete('mgwiki_corpus_permissions')
    ->condition('cid',$this->cid)
    ->execute();*/
    foreach($permissions as $permission){
      $uid = false;
      
      if($permission['user']=='public'){
        $uid =0;
      }else{
        $uid = db_query('SELECT uid FROM users WHERE name = :name', array(':name' => $permission['user']))->fetchField();
      }
      
      
      if($uid === false){
        return '{"error":"unknown user"}';
      }
      
      $exist = db_query('SELECT pid FROM mgwiki_corpus_permissions WHERE user = :uid AND cid= :cid', array(':uid' => $uid,':cid'=>$this->cid))->fetchField();

      
      $record = array(
        "user"=>$uid,
        "cid"=>$this->cid,
        "type"=>$permission['rights'],
      );
      $primaryKeys = array();
      if($exist !== false){
        $primaryKeys = array('pid');
        $record['pid']=$exist;
      }
      drupal_write_record('mgwiki_corpus_permissions', $record, $primaryKeys);
    }
    
    return '{"status":"ok"}';
  }
  
  public function getPermissions(){
    global $user;
    $editable = $this->hasPermission('admin');
    $result = db_select('mgwiki_corpus_permissions','p')
    ->fields('p',array('cid','type','user'))
    ->condition('p.cid',$this->cid)
    ->orderBy('user','asc')
    ->execute()
    ->fetchAllAssoc("user");
    
    $permissions = array();
    $hasPublic = false;
    $hasOwner = false;
    foreach($result as &$permission){
      $permissions[] = $permission;
      if($permission->user == $this->dbobject->owner){
        $hasOwner = true;
      }
      $permission->user = db_query('SELECT name FROM users WHERE uid = :uid', array(':uid' => $permission->user))->fetchField();
      if(false === $permission->user || empty($permission->user)){
        $permission->user = "public";
      }
      if($permission->user == "public"){
        $hasPublic = true;  
      }
      $permission->read = array("value"=>($permission->type == 2 || $permission->type == 5 || $permission->type == 6 || $permission->type == 9),"editable"=>$editable);
      $permission->write = array("value"=>($permission->type == 3 || $permission->type == 5 || $permission->type == 7 || $permission->type == 9),"editable"=>$editable);
      $permission->admin = array("value"=>($permission->type == 4 || $permission->type == 6 || $permission->type == 7 || $permission->type == 9),"editable"=>$editable && !$owner);
    }
   if(!$hasPublic){
      $precord = array("user"=>0,
          "cid"=>$this->cid,
          "type"=>"0");
      drupal_write_record('mgwiki_corpus_permissions',$precord, array());
    }
    
    if(!$hasOwner){
      $orecord = array("user"=>$this->dbobject->owner,
          "cid"=>$this->cid,
          "type"=>"9");
      drupal_write_record('mgwiki_corpus_permissions',$orecord, array());
    }
    return $permissions;
    
  }
  
  
  public function getOARSubID(){
    $task_id = variable_get("cpm_task_".$this->cid,null);
    if(isset($task_id)){
      $task = new CPMTask($task_id);
      $task_status = $task->get_output(8);
      $tmp = explode("OAR_JOB_ID=", $task_status[0]['output']);
      if(count($tmp)==2){
        return $tmp[1];
      }else{
        return "error";
      }
    }else{
      return "error";
    }
  }
  
  
  public function getTaskOutput($resource){
    $resourceInfo = explode("-", $resource);
    $errFile = false;
    if($resourceInfo[0]!="stdout"){
      $errFile = true;
    }
    $output = file_get_contents(FRMGCM_CORPUS_HOST.$this->cid."/task/".$resourceInfo[1]."/".$resourceInfo[2].($errFile?"/stderr":"/stdout"));
    return file_get_contents(trim($output));
  }

  public function isProcessLaunched(){
    $output = file_get_contents(FRMGCM_CORPUS_HOST.$this->cid."/task");
    $json_data = trim($output);
    $data = json_decode($json_data,true);
    for($i=0;$i<count($data);$i++){
      for($j=0;$j<count($data[$i]);$j++){
        if($data[$i][$j]['name']=="raw2dag"){
          if($data[$i][$j]['status'] != "waiting")
            return true;
          else
            return false;
        }     
      }
    }
}
  
  public function getTaskStatus(){
    $output = file_get_contents(FRMGCM_CORPUS_HOST.$this->cid."/task");
    $json_data = trim($output);
    $data = json_decode($json_data,true);
    $index = 0;
    for($i=0;$i<count($data);$i++){
      for($j=0;$j<count($data[$i]);$j++){
        $data[$i][$j]["stderr"]=$i."-".$j;
        $data[$i][$j]["stdout"]=$i."-".$j;
      }
    }
    return json_encode($data);
  }
  
  public function getReport(){
    $output = file_get_contents(FRMGCM_CORPUS_HOST.$this->cid."/report");
    $outputlines = preg_split("/\n/", $output);
    $start = 0;
    for($i=0;$i<count($outputlines);$i++){
      if(startsWith($outputlines[$i],"Report")){
        $start = $i;
        $outputlines[$i] = "<h2 style=\"margin-top:0px\">Corpus Analysis Report :</h2>";
      }
    }
    $trimmedOutput = "";
    for($i=$start;$i<count($outputlines);$i++){
      $trimmedOutput .= $outputlines[$i]."<br>";
    }
    return trim($trimmedOutput);
  }
  
  
  public function resultAvailable(){
    /*$file = Corpus::RIOC_PATH . $this->cid . '/results/status/done';
    $cmd = "if [ ! -f $file ]; then echo \"file not found\"; else echo \"found\"; fi";
    $task = new CPMTask();
    $task->append_function("check if result are here", "riocExec", array($cmd));
    $task->run();
    while(!$task->is_completed()){
      sleep(1);
    }
    $output = $task->get_output();
    return "found" === $output[0]['output'];*/
    return true;
  }
  

  

  
  function retrieveResults(){
  	$options = array("reset"=>true);
    $options['mgwiki_user']='admin';
    $options['pid']=2;
  	
  	$output = rest_helper(FRMGCM_CORPUS_HOST.$this->cid."/_process",$options,'POST','html');
  	return trim($output);
  }
  
  
   
  /**
   * Returns an array of original documents
   * array keys :
   * - relative path
   * - type
   * - 
   */
  public function getDocuments(){
    $output = rest_helper(FRMGCM_CORPUS_HOST.$this->cid."/document",array(),'POST','html');
  	return trim($output);
  }
  
 
  public function getAnnotatedHTML($did){
    
    $response = rest_helper(FRMGCM_CORPUS_HOST.$this->cid."/document/".$did."/annotated",array(),'GET','html');
    return trim(utf8_encode($response));
  }
  
  public function downloadResultURL(){
    global $base_url;
    $response = rest_helper(FRMGCM_CORPUS_HOST.$this->cid."/_download",array(),'GET');
    return $base_url."/sites/default/files/corpus/".$this->cid."/cache/results.tar.gz";
  }
  
  public function getGraph($did,$gid,$vid){
    $data_array = array(
        "gid"=>$gid,
        "vid"=>$vid
    );
   $response = rest_helper(FRMGCM_CORPUS_HOST.$this->cid."/document/".$did."/graph",$data_array,'POST','txt');
    $response  = trim($response);
    if($response != ""){
      $response_data = json_decode($response,true);
      $graph_file_path = $response_data['filepath'];
      $format = $response_data['format'];
      if($format=="dis_xmldep"){
        $format = "depxml";
      }
      $data = file_get_contents_utf8($graph_file_path);
      $json_data = d3js_convert_to_depgraph_format($data, $format);
      return $json_data;
    }else{ 
      return "no graph found";
    }
  }
  
  public function getGraphFragments($did,$tokenStart,$tokenEnd,$sentences){
    $data_array = array(
      "ts"=>$tokenStart,
      "te"=>$tokenEnd,
      "slist"=>json_encode($sentences),
    );
    $response = rest_helper(FRMGCM_CORPUS_HOST.$this->cid."/document/".$did."/graph",$data_array,'POST','txt');
    $response  = trim($response);
    if($response != ""){
      $response_data = json_decode($response,true);
      $graph_data = $response_data['data'];
      $format = $response_data['format'];
      $json_data = d3js_convert_to_depgraph_format($graph_data, $format);
      return $json_data;
    }else{
      return "no graph found";
    }
  }
  
  /**
   * Create the folder in which the corpus data and info will be stored
   * - /
   * ----- info.xml
   * ----- original
   * ----- raw
   * ----- dag
   * ----- results
   * ----- log
   */
  private function prepareContainerFolder(){
    if(!is_dir($this->path)){
      umask(0);
      mkdir($this->path.'/original',0777,true);
      mkdir($this->path.'/uncompressed',0777,true);
      mkdir($this->path.'/raw',0777,true);
      mkdir($this->path.'/aligns',0777,true);
      mkdir($this->path.'/results',0777,true);
      mkdir($this->path.'/log',0777,true);
      mkdir($this->path.'/cache',0777,true);
    }
  }

  
}


function create_corpus($cid,$path,$name){
  // add default permission for owner & public
  global $user;
  $precord = array("user"=>0,
    "cid"=>$cid,
    "type"=>"0");
  drupal_write_record('mgwiki_corpus_permissions',$precord, array());
  $orecord = array("user"=>$user->uid,
    "cid"=>$cid,
    "type"=>"9");
  drupal_write_record('mgwiki_corpus_permissions',$orecord, array());
  // -- create corpus in cpm
  $data_array = array(
      "cid"=>$cid,
      "path"=>$path,
      "name"=>$name
  );
  $output = rest_helper(FRMGCM_CORPUS_HOST."_create",$data_array,'POST','txt');
  return trim($output);
}

function retrieve_corpus($cid){
  $output = file_get_contents(FRMGCM_CORPUS_HOST.$cid);
  $output = utf8_decode($output);
  return trim($output);
}


function external_process_manager_call($cmd,$data){

  $message = $cmd . "\n" . $data . "\nCPM_MESSAGE_END\n";

  $output = "";

  $fp = stream_socket_client("tcp://localhost:8568", $errno, $errstr, 30);
  if (!$fp) {
    return "$errstr ($errno)<br />\n";
  } else {
    fwrite($fp, $message);
    $timeout = time();
    while (!feof($fp)) {
      $output .= fgets($fp, 1024);
      if(time()-$timeout>10000){
        break;
      }
    }

    fclose($fp);
  }

  return $output;
}

function riocExec($cmd){
  $session = ssh2_connect('rioc');
  ssh2_auth_password($session, 'buiquang', 'Ts4hptFj');
  $stream = ssh2_exec($session, $cmd);
  stream_set_blocking($stream, true);
  $content = stream_get_contents($stream);
  return $content;
}

function riocPushFile($source,$dest){
  $base_path = drupal_realpath(drupal_get_path('module', 'mgwiki_corpus'));
  $pkey = $base_path . '/resources/id_rioc';
  
  $cmd = "scp -r -i $pkey " . $source .' buiquang@rioc:' . $dest;
  shell_exec($cmd);
  
}

function riocGetFile($source,$dest){
  $base_path = drupal_realpath(drupal_get_path('module', 'mgwiki_corpus'));
  $pkey = $base_path . '/resources/id_rioc';
  
  $cmd = "scp -r -i $pkey " . 'buiquang@rioc:' . $source . ' ' . $dest;
  shell_exec($cmd);
}

?>
