(function(cma){
  
  cma.toolbox = cma.toolbox || {};

  cma.toolbox.Permissions = function(){
    this.initView();
  }

  cma.toolbox.Permissions.prototype.initView = function(){
    var me = this;
    this.view = jQuery(cma.toolbox.Permissions.templateMain);

    this.autocomplete = new cma.ui.AutoComplete(cma.ui.get("cma-permissions-add-user-input",this.view),cma.TheApp.options.ws.userlist);
    
    this.table = new cma.ui.Table({headers:[{title:"User",nameid:"user"},
      {title:"Read",nameid:"read",format:function(name,value,data){return formatPermission(name,value,data);},unformat:function(value){return unFormatPermission(value);}},
      {title:"Write",nameid:"write",format:function(name,value,data){return formatPermission(name,value,data);},unformat:function(value){return unFormatPermission(value);}},
      {title:"Admin",nameid:"admin",format:function(name,value,data){return formatPermission(name,value,data);},unformat:function(value){return unFormatPermission(value);}}]}
      );

    cma.ui.get('cma-permissions-table',this.view).html(this.table.view);

    this.addUserPermissionsbutton = new cma.ui.CallbackButton("Add User Permissions");
    cma.ui.get('cma-permissions-add-user-submit',this.view).append(this.addUserPermissionsbutton.view);
    
    this.savebutton = new cma.ui.CallbackButton("Update Permissions");
    cma.ui.get('cma-permissions-update-button',this.view).append(this.savebutton.view);

    this.update();
  };

  cma.toolbox.Permissions.prototype.update = function(doAfter){
    var me = this;
    cma.TheApp.corpus.getAndDo("permissions",{},function(data){
      
      me.table.update(data);

      jQuery('input[type="checkbox"]',me.view).change(function() {
        regex = /cma-permissions-(\w+)-input-(.*)/;
        var match = regex.exec(this.id);
        if (match != null) {
          permissionsInputsClean(match[1],match[2],this.checked);
        }

      });

      if(doAfter != null){
        doAfter.call();
      }
    });
  };

  cma.toolbox.Permissions.prototype.haveRights = function(activity,okcallback,notokcallback){
    var me = this;
    if(!this.table.originalData){
      this.update(function(){me.haveRights(activity,okcallback,notokcallback);})
      return;
    }

    var username = cma.TheApp.options.user.name;
    // theses permissions are only ui level. server check of identity and permissions is done for
    // any actions.
    if(username == 'admin'){
      okcallback.call();
      return true; 
    }
    var rights = null;
    for(var i in this.table.originalData){
      if(this.table.originalData[i].user == username){
        rights = this.table.originalData[i];
        break;
      }
    }
    if(!rights){
      rights = this.table.originalData[0];
    }

    if(activity == 'process'){
      if(rights.admin.value){
        okcallback.call();
        return true;
      }
    }else if(activity == 'edit'){
      if(rights.write.value){
        okcallback.call();
        return true;
      }
    }else if(activity == 'updatePermissions'){
      if(rights.admin.value){
        okcallback.call();
        return true;
      }
    }else{
      alert('error : unknown activity');  
    }
    
    if(notokcallback!=null){
      notokcallback.call();
    }
    return false;
  }

  cma.toolbox.Permissions.prototype.submit = function(button){
    var me = this;
    var data = this.table.getData();
    for(var i in data){
      data[i].cid = cma.TheApp.corpus.cid;
      data[i].rights = data[i].admin ? 9 : (data[i].write ? 5 : (data[i].read ? 2 : 0));
    }
    cma.TheApp.corpus.callAndDo('func',{func:"updatePermissions",permissions:data},function(data){
        console.log(data);
        button.success();
        me.update();
      },function(data){
        console.log(data);
        button.error();
      });
  };
  
  cma.toolbox.Permissions.prototype.init = function(){
    var me = this;

    this.haveRights('updatePermissions',function(){},function(){
      cma.ui.get('cma-permissions-add-user',me.view).hide();
      cma.ui.get('cma-permissions-update-button',me.view).hide();
      jQuery('input',me.view).prop('disabled', true);
    });


    this.addUserPermissionsbutton.init(function(button,caller){
      var read = cma.ui.get('cma-permissions-add-user-read',caller.view)[0].checked;
      var write = cma.ui.get('cma-permissions-add-user-write',caller.view)[0].checked;
      var admin = cma.ui.get('cma-permissions-add-user-admin',caller.view)[0].checked;
      var rights = admin ? 9 : (write ? 5 : (read ? 2 : 0));
      var permission = {
        user:cma.ui.get('cma-permissions-add-user-input').val(),
        rights:rights
      }
      cma.TheApp.corpus.callAndDo('func',{func:"updatePermissions",permissions:[permission]},function(data){
        if(data.error){
          alert(data.error);
          button.error();
        }else{
          caller.update();
          button.success();  
        }
      });
    },this);

    this.savebutton.init(function(button,caller){ 
      me.submit(button,caller);
    },this);

    this.autocomplete.init();
  };

  cma.toolbox.Permissions.templateMain = '<div id="cma-tb-permission">'+
    '<div id="cma-permissions-table"></div>'+
    '<div id="cma-permissions-update-button"></div>'+
    '<div id="cma-permissions-add-user">'+
    '<input id="cma-permissions-add-user-input" type="text">'+
    '<input id="cma-permissions-add-user-read" checked name="read" value="r" type="checkbox">read'+
    '<input id="cma-permissions-add-user-write" name="write" value="w" type="checkbox">write'+
    '<input id="cma-permissions-add-user-admin" name="admin" value="a" type="checkbox">admin'+
    '<span id="cma-permissions-add-user-submit"></span>'+
    '</div></div>';

  function formatPermission(name,item,data){
    var lock = (name == "write" && data.admin.value) || (name == "read" && data.write.value);
    return '<input id="cma-permissions-'+name+'-input-'+data.user+'" '+(item.value?' checked ':'')+((!item.editable || lock)?' disabled ':'')+' type="checkbox">';
  }

  function unFormatPermission(value){
    return jQuery(value)[0].checked;
  }

  function permissionsInputsClean(name,user,value){
    var read = jQuery('input[id="cma-permissions-read-input-'+user+'"]');
    var write = jQuery('input[id="cma-permissions-write-input-'+user+'"]');
    if(name == "admin"){
      if(value){
        write.prop("checked",true);
        read.prop("checked",true);
      }
      write.prop("disabled",value);
      read.prop("disabled",value);
    }else if(name == "write"){
      if(value){
        read.prop("checked",true);
      }
      read.prop("disabled",value);
    }
  }

}(window.cma = window.cma || {}));
