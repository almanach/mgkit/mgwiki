(function(cma){
  
  cma.ui = cma.ui || {};

  cma.ui.AutoComplete = function(element,uri,restrict){
    this.view = element;
    this.initView();
    this.uri = uri;
    this.update();
  }



  cma.ui.AutoComplete.prototype.initView = function(){
  }

  cma.ui.AutoComplete.prototype.init = function(){
/*    this.view.keyup(function(){
      console.log(jQuery(this).val());
    });*/
  }

  cma.ui.AutoComplete.prototype.update = function(){
    if(!this.uri){
      this.entries = [];
      return;
    }
      

    var me = this;
    jQuery.ajax({
      type: 'GET', 
      url: this.uri,
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        me.entries = data;
        if(me.view.autocomplete){
          me.view.autocomplete({
            source: data,
          });
          me.view.data("uiAutocomplete")._resizeMenu = function () {
            var ul = this.menu.element;
            ul.outerWidth(this.element.outerWidth());
          };
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        
      }
    });
  }

  
  
}(window.cma = window.cma || {}));
