(function(cma){


  cma.toolbox = cma.toolbox || {};

  /**
  * 
  * Search features:
  *   - sort results
      - filter results
      - limit result to 100 (performance) => mode query all après
      - go to sentence
      - export result
      - export query to a webservice (return url) => create in db url for query handler (push new changes, get query result)
  *
  *
  *
  *
  *
  **/

  cma.toolbox.Search = function(){
    this.max = 20;
    this.from = 0;
    this.getTagSet();
    this.initView();
  };

  cma.toolbox.Search.prototype.initView = function(){
    var me = this;
    this.view = jQuery(cma.toolbox.Search.templateMain);
    var table = new cma.ui.Table({headers:[{title:"ID",nameid:"sid",format:function(name,value){return formatSID(name,value);},unformat:function(value){return unFormatSID(value);}},
      {title:"Sentence",nameid:"sentence",format:function(name,value,data){return formatSentence(name,value,data);},unformat:function(value){return unFormatSentence(value);}},
      {title:"Score",nameid:"_score"},
      {title:"Document",nameid:"did",format:function(name,value){return formatDocument(name,value);},unformat:function(value){return unFormatDocument(value);}}]
    });
    me.resultsTable = table;

    this.searchbutton = new cma.ui.CallbackButton("Search");
    cma.ui.get('cma-search-submit-div',this.view).append(this.searchbutton.view);

    var searchTypes = '<select name="type">'
        //+'<option value="id">id</option>'
        +'<option value="dpath" selected>dpath</option>'
        +'<option value="sentence">sentence keywords</option>'
        +'</select>';

    cma.ui.get('cma-search-type',this.view).html(searchTypes);

    var textarea = cma.ui.get('cma-search-input',this.view);
    this.textarea = textarea;

    var infoDpath = 'Description of dpath language here..';
    var foldableDiv = new cma.ui.FoldableDiv(infoDpath);
    foldableDiv.setTitle('Help');
    textarea.after(foldableDiv.view);


    var strategies = this.autocompleteStrategies();
    textarea.textcomplete(strategies);

    textarea.on("keypress",function(e){
      if(e.keyCode == 9){
        e.preventDefault();
        console.log("autocomplete");

      }
      cma.utils.getTextareaCarretPosition(this);
    });

    this.macroLib = new cma.toolbox.SearchLib(textarea);
    this.history = new cma.toolbox.SearchHistory(this);

    jQuery('.cma-search-insert-macro',this.view).click(function(){
      me.macroLib.show();
    });

    jQuery('.cma-search-history',this.view).click(function(){
      me.history.show();
    });

    jQuery('.cma-search-visual',this.view).click(function(){
      me.visualSignatureForm();
    });

    jQuery('.cma-search-save-macro',this.view).click(function(){
      var val = textarea[0].textContent;
      me.macroLib.elementForm({query:val});
    });

  };

  cma.toolbox.Search.prototype.getTagSet = function(){
    var me = this;
    var data = {
      proxy_url:"corpus/"+cma.TheApp.corpus.cid+"/_tagset"
    };
    jQuery.ajax({
      type: 'POST', 
      url: cma.TheApp.ws_url+'frmgcm_proxy',
      data: data,
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        if(data.name){
          me.tagset = data;
          me.props = [];
          for (var i in data.node) {
            me.props.push(i);
          };
          for (var i in data.edge) {
            me.props.push(i);
          };  
        }
        
      },
      error: function(jqXHR, textStatus, errorThrown) {
      }
    });
  };

  cma.toolbox.Search.prototype.autocompleteStrategies = function(){
    var me = this;
    var strategies = [];
    strategies.push({
      match: /@(\w+)\s*=(")?(\w*)$/,
      index:3,
      search: function (term, callback) {
        var prop = this.currentMatch[1];
        var propVal = [];
        if(me.tagset && me.tagset.edge[prop]){
          propVal = me.tagset.edge[prop];
        }else if(me.tagset && me.tagset.node[prop]){
          propVal = me.tagset.node[prop];
        }
        
        callback(jQuery.map(propVal, function (element) {
                return element.indexOf(term) === 0 ? element : null;
            }));
      },
      replace: function (value) {
        return '@$1="' + value +'"';
      },
      cache: true,
      maxCount:99
    });
    strategies.push({
      match: /(\$)(\w*)$/,
      search: function (term, callback) {
        var macros = me.macroLib.macros;
        callback(jQuery.map(macros, function (element) {
                return element.indexOf(term) === 0 ? element : null;
            }));
      },
      replace: function (value) {
        return '$1' + value + ' ';
      },
      cache: true,
      maxCount:99
    });
    strategies.push({
      match: /(@)(\w*)$/,
      search: function (term, callback) {
        var props = me.props || [];
        callback(jQuery.map(props, function (element) {
                return element.indexOf(term) === 0 ? element : null;
            }));
      },
      replace: function (value) {
        return '$1' + value ;
      },
      cache: true,
      maxCount:99
    });

    return strategies;
  };


  cma.toolbox.Search.prototype.init = function(did){
    var me = this;
    this.did = did;

    this.searchbutton.init(function(button,caller){ 
      var textarea = cma.ui.get('cma-search-input',me.view);
      var val = textarea[0].textContent;
      me.from = 0;
      var select = jQuery('select',cma.ui.get('cma-search-type',me.view));
      var type = select[0].options[select[0].selectedIndex].value;
      me.search(val,type,me.from,me.max,function(){button.success();},function(){button.error();});
    },this);

    
  };

  cma.toolbox.Search.prototype.search = function(query,searchType,from,max,callbacksuccess,callbackerror){
    var me = this;
    cma.TheApp.corpus.callAndDo('_search',{query:query,searchType:searchType,from:from,max:max,did:this.did},function(data){
      me.processResults(data);

      var total = data.output.total || ((data.output.hits)?data.output.hits.total:0);
      var header = "Total : "+total+'<input id="cma-search-export" style="display:inline-block; margin-left:15px;" type="button" value="Export">';

      var resulthtml = '<div><input id="cma-search-result-prev" type="button" value="prev"><input id="cma-search-result-next" type="button" value="next"></div>';
      var resultObj = jQuery(resulthtml).prepend(me.resultsTable.view).prepend(header);
      cma.ui.get('cma-search-result',me.view).html(resultObj);
      cma.ui.get('cma-search-export',me.view).click(function(){
        var postVal = {cid:cma.TheApp.corpus.cid,export:true,query:query,searchType:searchType,from:from};
        if(me.did){
          postVal.did = me.did;
        }
        depgraphlib.windowOpenPost(postVal,cma.TheApp.ws_url+"_search");
      },null,'html');
      cma.ui.get('cma-search-result-next',me.view).click(function(){
        me.from+=me.max;
        me.search(query,searchType,me.from,me.max);
      });
      cma.ui.get('cma-search-result-prev',me.view).click(function(){
        me.from-=me.max;
        me.search(query,searchType,me.from,me.max);
      });
      
      if((me.from+me.max)>me.total){
        cma.ui.get('cma-search-result-next',me.view).hide();  
      }
      if(me.from==0){
        cma.ui.get('cma-search-result-prev',me.view).hide();  
      }
      if(callbacksuccess){
        callbacksuccess.call();  
      }
      
      
    },callbackerror);
  };

  cma.toolbox.Search.prototype.processResults = function(data){
    if(data.output && data.output.hits){
      var hits;
      if(data.output.dpath){
        hits = data.output.hits;
        this.total = data.output.total;
      }else{
        hits = data.output.hits.hits;
        this.total = data.output.hits.total;
      }
      for(var i in hits){
        var hit = hits[i];
        hit.sid = hit._source.sid;
        hit.did = hit._source.did;
        if(hit._source.sentence){
          hit.sentence = hit._source.sentence.substr(0,200);  
        }else{
          hit.sentence = "error : could not find sentence content";
        }
        
        delete hit._index; 
        delete hit._type; 
        delete hit._id; 
      }  
      this.resultsTable.update(hits);
    }
    console.log(data);
  };

  cma.toolbox.Search.templateMain = '<div><div id="cma-search-box">'+
    '<div id="cma-search-type"></div>'+
    '<div class="cma-search-utils"><div class="cma-search-insert-macro"></div><div class="cma-search-save-macro"></div><div class="cma-search-visual"></div><div class="cma-search-history"></div></div>'+
    '<div id="cma-search-input" contenteditable=true></div> '+
    //'<textarea id="cma-search-input" rows="4" cols="45"></textarea>'+
    '<div id="cma-search-submit-div"></div>'+
    '</div>'+
    '<div id="cma-search-result"></div></div>';

  function formatDocument(name,value){
    for (var did in cma.TheApp.corpus.paths) {
      if(cma.TheApp.corpus.paths[did].scid == value){
        return '<a did="'+value+'" href="#corpus/'+cma.TheApp.corpus.cid+'/view/'+did+'">'+cma.TheApp.corpus.paths[did].name+'</a>';  
      }
    };
    return value;
  }

  function unFormatDocument(value){
    return jQuery(value).attr('did');
  }

  function formatSID(name,value){
    return value;
    //return (value.indexOf('E') == 0)?value:'E'+String(value);
  }

  function unFormatSID(value){
    return value;
  }

  function formatSentence(name,value,data){
    var sid = data['sid'];//(data['sid'].indexOf('E') == 0)?data['sid']:'E'+String(data['sid']);
    var html =  'Sentence : <a style="cursor:pointer;">'+value+'</a>';
    var obj = jQuery(html).click(function(){
      cma.TheApp.corpus.doc.showGraph("frmgwiki",sid);
    });
    return obj;
  }

  function unFormatSentence(value){
    return jQuery(value).html();
  }

  cma.toolbox.Search.prototype.visualSignatureForm = function(){
    var me = this;
    var width = cma.TheApp.view.width();
    var height = cma.TheApp.view.height();
    var offset = cma.TheApp.view.offset();
    
    var uniqid = Date.now();
    var div = jQuery('<div><div id="graph-container-'+uniqid+'"></div></div>');
  
    // Box || r.window => base sentence
    // data creation (+datamodel)
    
    var sentence = prompt("Please enter a sentence.","");

    if (sentence==null || sentence == ""){
      sentence =".";
    }

    var data = {
      graph:{
        words:[]
      }
    };


    var words = sentence.split(/\s+/);
    for (var i = 0 ; i < words.length ; i++) {
      data.graph.words.push({id:i+1,label:words[i]});
    };
    

    var box = new depgraphlib.Box({closeButton:true,draggable:true}).setContent(div).open();
    box.setHeader("test");
    var depGraph = new depgraphlib.DepGraph(jQuery("#graph-container-"+uniqid),data,{"uid":uniqid,viewmode:"full",maxwidth:cma.TheApp.view.width()-100});
    var viewer = depGraph.viewer;
    depGraph.wsurl = cma.TheApp.ws_url+"graph";
    if(depGraph.options.viewmode == 'full'){
      viewer.noBorders();
    }

    var boxcontent = jQuery('.depgraphlib-box-content',box.object);
    var dgwidth = viewer.mainpanel.width();
    var dgheight = viewer.mainpanel.height();
    boxcontent.width(dgwidth+16);
    boxcontent.height(dgheight+16);
    boxcontent.resizable({
      stop:function(e,data){
        depGraph.setWidthLimitedViewMode(data.size.width-16,true);
        viewer.setHeight(data.size.height-46);
      }
    });

    var point = {x:offset.left+width/2-dgwidth/2,y:offset.top+height/2-dgheight/2};
    box.move(point);

    depGraph.sentenceLink = '#';

    depGraph.sentence = 'sentence content here';
    depGraph.editObject.mode['default'].save = depgraphlib.EditObject.DefaultModeLib.save;
    depGraph.dataFormat = 'json';

    var dataModel = {
      words:[
      {name:"label",view:'label'},
      {name:"lemma",view:"sublabel/0"},
      {name:"pos",values:me.tagset.node["pos"],view:"sublabel/1"}
      ],
      links:[
      {name:"label",values:me.tagset.edge["label"],view:"label"},
      ],
    };

    depGraph.editObject.setDataModel(dataModel);
    

    viewer.setFixedToolbar();
    var formatTmp = (me.model.tagset.node["pos"])?"conll":"depxml";
    viewer.addToolbarItem({name:'export_signature',callback:function(){
      var ddata = depGraph.cleanData();
      jQuery.ajax({
        type: 'POST', 
        url: cma.TheApp.ws_url+"graph",
        data: {
          action:'_getSig',
          data:ddata,
          format:formatTmp
        },
        dataType : 'text',
        success: function(data, textStatus, jqXHR) {
          console.log(data);
          var prev = me.textarea.html();
          me.textarea.html(prev + data);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          alert(textStatus);
        }
      });
    },tooltip:'Export Signature',style:'mgwiki-export','static':true});

    depGraph.editObject.setEditMode('default',false);
    /*depGraph.editObject.mode[depGraph.editObject.editMode].save = function(depgraph){
    }*/
  };

  /*********************************************************************/
  /*                 DPath Search Library                              */
  /*********************************************************************/

  cma.toolbox.SearchLib = function(textarea){
    this.view = jQuery(cma.toolbox.SearchLib.template);
    this.init();
    this.textarea = textarea;
  };

  cma.toolbox.SearchLib.prototype.initView = function(){
    if(this.currentSelect){
      this.currentSelect.removeClass('cma-searchlib-list-item-selected');
    }
    this.currentHover = null;
    this.currentSelect = null;
    var me = this;
    jQuery('.cma-searchlib-list',this.view).on("mouseover",function(e){
      var selection = jQuery(e.target).parents('tr');
      if(me.currentHover){
        me.currentHover.removeClass('cma-searchlib-list-item-hover');
      }
      if(selection.hasClass('cma-searchlib-list-title')){
        me.currentHover = null;
        return;
      }
      me.currentHover = selection;
      selection.addClass('cma-searchlib-list-item-hover');
    });
    jQuery('.cma-searchlib-list',this.view).on("mouseout",function(e){
      if(me.currentHover){
        me.currentHover.removeClass('cma-searchlib-list-item-hover');
        me.currentHover = null;
      }
    });
    jQuery('.cma-searchlib-list',me.view).on("click",function(e){
      var selection = jQuery(e.target).parents('tr');
      if(selection.hasClass('cma-searchlib-list-title')){
        return;
      }
      if(me.currentSelect){
        me.currentSelect.removeClass('cma-searchlib-list-item-selected');
      }
      if(me.currentSelect == selection){
        return;
      }
      me.currentSelect = selection;
      selection.addClass('cma-searchlib-list-item-selected');
    });
    jQuery('.cma-searchlib-insert',me.view).click(function(){
      if(me.currentSelect != null){
        var children = me.currentSelect.children();
        var macro = "$"+children[0].innerHTML;
        var val = me.textarea.html();
        me.textarea.html(val+'<span>'+macro+'</span>');
      }
    });
  };

  cma.toolbox.SearchLib.prototype.init = function(){
    var me = this;
    var data = {
      proxy_url:"dpathlib/_all"
    };
    jQuery.ajax({
      type: 'POST', 
      url: cma.TheApp.ws_url+'frmgcm_proxy',
      data: data,
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        me.macros = [];
        for (var i = data.length - 1; i >= 0; i--) {
          me.macros.push(data[i].name);
        };
        me.fillTable(data);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        
      }
    });
  };

  /**
   * Fill the table with data retrieved by the server
   * @param  {[type]} data [description]
   * @return {[type]}      [description]
   */
  cma.toolbox.SearchLib.prototype.fillTable = function(data){
    var me = this;
    var tbody = jQuery('.cma-searchlib-list-body',this.view);
    tbody.html("");
    for (var i = data.length - 1; i >= 0; i--) {
      tbody.append('<tr><td>'+data[i].name+'</td><td>'+data[i].query+'</td><td>'+data[i].description+'</td><td>'+data[i].user+'</td><td>'+getActions(data[i])+'</td></tr>');
    };
    jQuery('.cma-searchlib-action-delete',me.view).click(function(e){
      var selection = jQuery(e.target).parents('tr');
      var children = selection.children();
      var macro = children[0].innerHTML;
      var r = window.confirm(cma.text.get("delete-macrodef-confirm"));
      if(r){
        me.deleteElement(macro,function(){
          selection.remove();
        });
      }
      
    });
    jQuery('.cma-searchlib-action-edit',me.view).click(function(e){
      var selection = jQuery(e.target).parents('tr');
      var children = selection.children();
      var ispublic = jQuery(children[4]).find('.cma-searchlib-action-public');
      var publicv = false;
      if(ispublic.length){
        publicv = true;
      }
      var macro = {
        name:children[0].innerHTML,
        query:children[1].innerHTML,
        description:children[2].innerHTML,
        publicv:publicv
      };
      me.elementForm(macro);
    });
    jQuery('.cma-searchlib-action-public',me.view).click(function(e){
      var selection = jQuery(e.target).parents('tr');
      var children = selection.children();
      var macro = {
        name:children[0].innerHTML,
        query:children[1].innerHTML,
        description:children[2].innerHTML,
        publicv:false
      };
      me.syncElement(macro.name,macro,function(data){
        if(data.success){
          me.init();
        }else{
          alert(data.message);
        }
      });
    });
    jQuery('.cma-searchlib-action-private',me.view).click(function(e){
      var selection = jQuery(e.target).parents('tr');
      var children = selection.children();
      var macro = {
        name:children[0].innerHTML,
        query:children[1].innerHTML,
        description:children[2].innerHTML,
        publicv:true
      };
      me.syncElement(macro.name,macro,function(data){
        if(data.success){
          me.init();
        }else{
          alert(data.message);
        }
      });
    });
  };

  function getActions(macro){
    var actions = "";
    if(macro.user == cma.TheApp.user){
      actions += '<span class="cma-searchlib-action cma-searchlib-action-edit"></span><span class="cma-searchlib-action cma-searchlib-action-delete"></span>';
    }
    if(macro.ispublic){
      actions += '<span class="cma-searchlib-action cma-searchlib-action-public"></span>';
    }else{
      actions += '<span class="cma-searchlib-action cma-searchlib-action-private"></span>';
    }
    return actions;
  }

  /**
   * expand a macro to the query
   * @param  {[type]} name [description]
   * @return {[type]}      [description]
   */
  cma.toolbox.SearchLib.prototype.expand = function(name){

  };

  /**
   * show the form to add a new macro to the library
   * @param {[type]} name  [description]
   * @param {[type]} query [description]
   */
  cma.toolbox.SearchLib.prototype.elementForm = function(macroInfo){
    var me = this;
    var width = jQuery(window).width();
    var height = jQuery(window).height();
    var box = new depgraphlib.Box({closeButton:true,draggable:true,position:{x:width/2-200,y:height/2-200}});
    box.setFixedSize(400,400);
    box.open();
    box.setHeader("DPath Query Library");

    var $el = jQuery('.depgraphlib-box-content',box.object);
    $el.append(cma.toolbox.SearchLib.addNewFormTemplate);

    if(macroInfo.query){
      $el.find('.cma-searchlib-addnewform-query').val(macroInfo.query);  
    }

    if(macroInfo.description){
      $el.find('.cma-searchlib-addnewform-desc').val(macroInfo.description);  
    }

    if(macroInfo.name){
      $el.find('.cma-searchlib-addnewform-name').val(macroInfo.name);  
    }

    if(macroInfo.publicv){
      $el.find('.cma-searchlib-addnewform-public').prop("checked",true);  
    }
    
    $el.find('.cma-searchlib-addnewform-cancel').click(function(){
      box.destroy();
      
    });

    $el.find('.cma-searchlib-addnewform-submit').click(function(){
      var owner = cma.TheApp.user || 'unknown';
      var query = $el.find('.cma-searchlib-addnewform-query').val();
      var name = $el.find('.cma-searchlib-addnewform-name').val();
      var desc = $el.find('.cma-searchlib-addnewform-desc').val();

      var publicv = $el.find('.cma-searchlib-addnewform-public')[0].checked;
      
      var macroDefintion = {
        name:name,
        query:query,
        owner:owner,
        description:desc,
        publicv:publicv
      };
      me.syncElement(macroInfo.name,macroDefintion,function(data){
        if(data.success){
          me.init();
          box.destroy();  
        }else{
          alert(data.message);
        }
      });
      
    });
  };

  cma.toolbox.SearchLib.prototype.deleteElement = function(macroName,callbackResult){
    var proxy_url = "dpathlib/"+macroName+"/_delete";
    var me = this;
    var data = {
      proxy_url:proxy_url,
      name:macroName,
    };
    jQuery.ajax({
      type: 'POST', 
      url: cma.TheApp.ws_url+'frmgcm_proxy',
      data: data,
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        callbackResult.call(me,data);
        console.log(data);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        
      }
    });
  };

  cma.toolbox.SearchLib.prototype.syncElement = function(macroName,macroDefinition,resultCallback){
    var proxy_url = "dpathlib/_create";
    if(macroName){
      proxy_url = "dpathlib/"+macroName+"/_edit";
    }
    var me = this;
    var data = {
      proxy_url:proxy_url,
      name:macroDefinition.name,
      query:macroDefinition.query,
      owner:macroDefinition.owner,
      description:macroDefinition.description,
      publicv:macroDefinition.publicv
    };
    console.log(data);
    jQuery.ajax({
      type: 'POST', 
      url: cma.TheApp.ws_url+'frmgcm_proxy',
      data: data,
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        resultCallback.call(me,data);
        console.log(data);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        
      }
    });
  };

  cma.toolbox.SearchLib.addNewFormTemplate = '<div class="cma-searchlib-addnewform">'+
    '<h2>Save Query macro</h2>'+
    '<fieldset>'+
      '<legend>Required:</legend>'+
      'Name: <input class="cma-searchlib-addnewform-name" type="text"><br>'+
      'Query: <input class="cma-searchlib-addnewform-query" type="text"><br>'+
    '</fieldset>'+
    '<fieldset>'+
      '<legend>Optional:</legend>'+
      'Description: <textarea class="cma-searchlib-addnewform-desc"></textarea><br>'+
      'Public Visibility: <input type="checkbox" class="cma-searchlib-addnewform-public"><br>'+
    '</fieldset>'+
    '<input type="submit" class="cma-searchlib-addnewform-submit" value="Save"><input type="submit" class="cma-searchlib-addnewform-cancel" value="Cancel">'
  '</div>';

  /**
   * delete a macro
   * @param  {[type]} name [description]
   * @return {[type]}      [description]
   */
  cma.toolbox.SearchLib.prototype.remove = function(name){

  };

  /**
   * show the macro library
   * @return {[type]} [description]
   */
  cma.toolbox.SearchLib.prototype.show = function(){
    var box = new depgraphlib.Box({closeButton:true,draggable:true,position:{x:50,y:50}});
    box.setMaxSize(null,500);
    box.open();
    box.setHeader("DPath Query Library");

    var $el = jQuery('.depgraphlib-box-content',box.object);
    this.initView();
    $el.append(this.view);
  };

  /**
   * hide the macro library
   * @return {[type]} [description]
   */
  cma.toolbox.SearchLib.prototype.hide = function(){

  };
  
  cma.toolbox.SearchLib.template = '<div>'+
    //'<div class="cma-searchlib-filter"><input type="text" class="cma-searchlib-filter-input"></div>'+
    '<table class="cma-searchlib-list"><thead><tr class="cma-searchlib-list-title"><th>Name</th><th>Query</th><th>Description</th><th>Owner</th><th>Actions</th></tr></thead><tbody class="cma-searchlib-list-body"></tbody></table>'+
    '<div class="cma-searchlib-insert"><button type="submit">Insert Selected Macro</button></div>'+
  '</div>';


  /*********************************************************************/
  /*                 DPath Search History                              */
  /*********************************************************************/

  cma.toolbox.SearchHistory = function(searchmodule){
    this.view = jQuery('<table class="cma-searchhistory-list"><thead><tr class="cma-searchhistory-list-title"><th>Query</th><th></th></tr></thead><tbody class="cma-searchhistory-list-body"></tbody></table>');
    this.searchmodule = searchmodule;
  };

  cma.toolbox.SearchHistory.prototype.show = function(){
    var box = new depgraphlib.Box({closeButton:true,draggable:true,position:{x:50,y:50}});
    box.setMaxSize(null,500);
    box.open();
    box.setHeader("DPath Query History");

    var $el = jQuery('.depgraphlib-box-content',box.object);
    this.refreshView();
    $el.append(this.view);
  };

  cma.toolbox.SearchHistory.prototype.refreshView = function(){
    var me = this;
    var data = {
      proxy_url:"dpathlib/_history"
    };
    jQuery.ajax({
      type: 'POST', 
      url: cma.TheApp.ws_url+'frmgcm_proxy',
      data: data,
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        me.history = [];
        for (var i = data.length - 1; i >= 0; i--) {
          me.history.push(data[i]);
          var tbody = jQuery('.cma-searchhistory-list-body',me.view);
          tbody.html("");
          for (var i = data.length - 1; i >= 0; i--) {
            tbody.append('<tr><td>'+data[i]+'</td><td><span class="cma-searchhistory-save"></span><span class="cma-searchhistory-insert"></span></td></tr>');
          };
          jQuery('.cma-searchhistory-save',me.view).click(function(e){
            var selection = jQuery(e.target).parents('tr');
            var children = selection.children();
            var macro = {
              query:children[0].innerHTML,
            };
            me.searchmodule.macroLib.elementForm(macro);
          });
          jQuery('.cma-searchhistory-insert',me.view).click(function(e){
            var selection = jQuery(e.target).parents('tr');
            var children = selection.children();
            var query = children[0].innerHTML;
            var val = me.searchmodule.textarea.html();
            me.searchmodule.textarea.html(val+'<span>'+query+'</span>');
          });
        };
      },
      error: function(jqXHR, textStatus, errorThrown) {
        
      }
    });
  };


}(window.cma = window.cma || {}));
