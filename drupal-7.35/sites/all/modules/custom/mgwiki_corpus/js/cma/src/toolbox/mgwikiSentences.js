(function(cma){


  cma.toolbox = cma.toolbox || {};


  cma.toolbox.MgwikiSentences = function(){
    this.initView();
  };

  cma.toolbox.MgwikiSentences.prototype.initView = function(){
    var me = this;
    this.view = jQuery(cma.toolbox.MgwikiSentences.templateMain);
    var table = new cma.ui.Table({headers:[
      {title:"Sentence",nameid:"title",format:formatTitle},
      {title:"Graphs",nameid:"graphs",format:formatGraphs},
    ]});
    me.listSentences = table;
    
    
  };
  
  cma.toolbox.MgwikiSentences.prototype.init = function(){
    var me = this;
    this.view.html(me.listSentences.view);
    data = {};
    data.func = "listSentences";
    dataType = 'json';
    jQuery.ajax({
      type: 'POST', 
      url: cma.TheApp.ws_url+'func',
      data: data,
      dataType : dataType,
      success: function(data, textStatus, jqXHR) {
        me.listSentences.update(data);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  };


  cma.toolbox.MgwikiSentences.templateMain = '<div id="cma-mgwiki-sentences"></div>';

  cma.toolbox.MgwikiSentences.showGraph = function(data){
    var width = cma.TheApp.view.width();
    var height = cma.TheApp.view.height();
    var offset = cma.TheApp.view.offset();
    var point = {x:offset.left+width/2-400,y:offset.top+height/2-200};
    var uniqid = Date.now();
    var div = jQuery('<div><div id="graph-container-'+uniqid+'"></div></div>');
  
    var box = new depgraphlib.Box({closeButton:true,draggable:true}).setContent(div).open(point);
    var depGraph = new depgraphlib.DepGraph(jQuery("#graph-container-"+uniqid),data,{"uid":uniqid,"maxwidth":800});
    var viewer = depGraph.viewer;
    depGraph.wsurl = cma.TheApp.ws_url+"graph";
    if(depGraph.options.viewmode == 'full'){
      viewer.noBorders();
    }
    point.x = offset.left+width/2-viewer._width/2;
    point.y = offset.top+height/2-viewer._height/2;
    box.move(point);
  }
 
  function formatTitle(name,value,data){
    var html = jQuery(value).attr("target","_blank");
    return html;
  }
  

  function formatGraphs(name,value,data){
    var html = "<ul>";
    for(i in value){
      var graph = value[i];
      html += '<li gid="'+graph['gid']+'">graph '+graph['title']+"</li>";
    }
    html += "</ul>";
    html = jQuery(html);
    jQuery('li',html).click(function(element){
      jQuery.ajax({
        type: 'GET', 
        url: cma.TheApp.options.host_url+'/d3js/'+jQuery(this).attr('gid')+'/edit/export/json',
        dataType : 'json',
        success: function(data, textStatus, jqXHR) {
          cma.toolbox.MgwikiSentences.showGraph(data);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          alert(textStatus);
        }
      });
    });
    return html;
  }

}(window.cma = window.cma || {}));