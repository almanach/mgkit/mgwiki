(function(cma){

  cma.ui = cma.ui || {};

  cma.ui.Table = function(definition,data){
    this.view = jQuery('<table></table>');
    this.model = definition;
    this.model.bindings = {};
    this.model.style = this.model.style || {};

    this.view.addClass(this.model.style.table);
    this.initView();
    if(data){
      this.update(data);
    }
  }


  cma.ui.Table.prototype.clear = function(){
    this.view.html("");
    this.initView();
  }

  cma.ui.Table.prototype.getData = function(){
    var data = [];
    var rows = jQuery('tr',this.view);
    for(var i =1;i<rows.length;i++){
      var cells = jQuery(rows[i]).children();
      var item = {};
      data.push(item);
      for(var j = 0;j<cells.length;j++){
        item[this.model.headers[j].nameid] = (typeof this.model.headers[j].unformat == 'function')?this.model.headers[j].unformat(jQuery(cells[j]).children()[0]):jQuery(cells[j]).html();
      }
    }
    return data;
  }

  cma.ui.Table.prototype.update = function(data){
    this.clear();
    var styletr0 = (this.model.style.tr0)?' class="'+this.model.style.tr0+'" ':"";
    var styletr1 = (this.model.style.tr1)?' class="'+this.model.style.tr1+'" ':"";
    var styletd = (this.model.style.td)?' class="'+this.model.style.td+'" ':"";
    var ncols = this.model.headers.length;
    this.originalData = data;
    for(var i in data){
      var row = '<tr'+((i%2)?styletr1:styletr0)+'>';
      for(var k=0;k<ncols;k++){
        row += '<td'+styletd+'></td>';
      }
      row += '</tr>';
      row = jQuery(row);
      var cells = row.children();
      for(var name in data[i]){
        if(this.model.bindings[name] !== undefined){
          var content = (typeof this.model.headers[this.model.bindings[name]].format == 'function')?this.model.headers[this.model.bindings[name]].format(name,data[i][name],data[i]):data[i][name];
          jQuery(cells[this.model.bindings[name]]).html(content);
        }
      }
      this.view.append(row);
    }
  }

  cma.ui.Table.prototype.initView = function(){
    var styleth = (this.model.style.th)?' class="'+this.model.style.th+'" ':"";
    var stylethr = (this.model.style.thr)?' class="'+this.model.style.thr+'" ':"";
    var tableheader = '<tr'+stylethr+'>';
    for(var i in this.model.headers){
      var header = this.model.headers[i];
      tableheader += '<th'+styleth+'>'+header.title+'</th>';
      if(header.nameid){
        this.model.bindings[header.nameid] = i;  
      }
    }
    tableheader += '</tr>';

    this.view.append(tableheader);
  }
  
  
}(window.cma = window.cma || {}));