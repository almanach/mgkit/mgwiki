(function(cma){
  
  cma.toolbox = cma.toolbox || {};

  cma.toolbox.Misc = function(){
    this.initView();
  }

  cma.toolbox.Misc.prototype.initView = function(){
    var me = this;
    this.view = jQuery(cma.toolbox.Misc.templateMain);

    jQuery('#cma-misc-download-results',this.view).click(function(){
      cma.TheApp.corpus.callAndDo('download',{},function(data){
          window.open(data.url);
      });
    });

    jQuery('#cma-misc-delete',this.view).click(function(){
      var r = window.confirm(cma.text.get("delete-corpus-confirm"));
      if(r){
        cma.TheApp.corpus.callAndDo('delete',{},function(data){
          console.log(data);
          cma.TheApp.goTo("");
        });
      }
      
    });

    /*if(cma.TheApp.corpus.status != "waiting"){
      jQuery('#cma-process-default-analysis',this.view).hide();
    }*/
    
  };

  cma.toolbox.Misc.templateMain = '<div><h3>Misc</h3>'+
    '<h4>Download</h4>'+
    '<input type="button" id="cma-misc-download-results" value="Download Analysis Results">'+
    '<h4>Other</h4>'+
    '<input type="button" id="cma-misc-delete" value="Delete this corpus."><br>Warning this can\'t be undone!'+
    '</div>';
  
}(window.cma = window.cma || {}));