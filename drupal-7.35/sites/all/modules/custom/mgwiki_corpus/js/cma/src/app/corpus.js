(function(cma){
  
  cma.Corpus = function(cid){
    this.cid = cid;
    this.paths = {};
    this.doc = null;
    this.docsTree = null;
    this.view = jQuery(cma.template.corpusViewMain);
    cma.AppView.setTemplate(this.view);
    cma.Log("corpus loaded");
  };




 cma.Corpus.prototype.init = function(doAfterInit){
  var me = this;
  this.permissions = new cma.toolbox.Permissions();
  this.getAndDo('info',{},function(data){
    me.name = data.name;
    me.status = data.status;    
    if(data.status == "available" || data.status == "processing"){
      me.initialized = true;
      me.getAndDo('documents',{},function(documentInfo){
        me.docsTree = documentInfo;
        me.getDocumentsInfo(documentInfo);
        doAfterInit.call();
      });
    }else{
      doAfterInit.call();
    }
  });

 };

 
  cma.Corpus.prototype.initView = function(){
    var me = this;

    me.prepareView(this.status);
    //me.initContactForm();
    me.updateHeader();

    if(this.initialized){
      var documents = jQuery('<div style="display:inline-block;"><h2 style="margin-top:0;">Corpus Files</h2></div>');
      var documentsTree = jQuery('<div class="cma-documents-tree"></div>');
      documents.append(documentsTree);
      cma.AppView.getMainLayoutCell(0,0).append(documents);
      me.displayDocuments(this.docsTree,documentsTree);
      jQuery(".cma-folder-toggle",document).click(function(event){
        var togglerIcon = jQuery(event.target);
        if(togglerIcon.hasClass("cma-folder-collapse")){
          togglerIcon.addClass("cma-folder-expand");
          togglerIcon.removeClass("cma-folder-collapse");
        }else{
          togglerIcon.addClass("cma-folder-collapse");
          togglerIcon.removeClass("cma-folder-expand");
        }
        jQuery(".cma-document-tree",jQuery(event.target).parent()).toggle();
        event.stopPropagation();
      });  
    }else{
      cma.AppView.getMainLayoutCell(0,0).append("not ready");
    }
    

  };
  
  cma.Corpus.prototype.prepareView = function(status){
    var me = this;

    cma.AppView.setTitle(jQuery(cma.template.corpusViewHeader));
    cma.ui.initMutable();
    
    var cmaContentContainer = cma.ui.get('cma-content');
    var height = cma.ui.get('cma-app-container').height();
    //cmaContentContainer.height(height-100);


    var layout = [];
    var row = [{name:"firstCol",width:30},{name:"secondCol",width:65}];
    layout.push(row);
    cma.AppView.setMainLayout(layout);

    
    
    this.initCorpusToolBox();
    
    
  };
  
  cma.Corpus.prototype.load = function(cid){
    
  };
  
  cma.Corpus.prototype.show = function(documentInfo){
    
  };
  
  cma.Corpus.prototype.showStatus = function(){
    
  };
  
  cma.Corpus.prototype.initCorpusToolBox = function(){
    var me = this; 
    var cell = cma.AppView.getMainLayoutCell(0,1);

    var toolboxobj = new cma.ui.ToolBox();
    cell.html(toolboxobj.view);

    var permissions = this.permissions;
    var miscActions = new cma.toolbox.Misc();

   permissions.haveRights('process',function(){
      var process = me.process = new cma.toolbox.Process();
      toolboxobj.addBoxItem('corpus-process',process.view,'Process'); 
   });
    
    var search = new cma.toolbox.Search();
    toolboxobj.addBoxItem('document-search',search.view,'Search',{startOpen:true});
    search.init();
    
    
    
    toolboxobj.addBoxItem('corpus-report','<div id="cma-corpus-status"></div>','Corpus Analysis Report',{startOpen:true});
    toolboxobj.addBoxItem('corpus-permissions',permissions.view,'Permissions',{startOpen:true});
    toolboxobj.addBoxItem('corpus-log','<div id="cma-corpus-log"></div>','Corpus Log');
    toolboxobj.addBoxItem('mgwiki_sentences',cma.TheApp.mgwiki_sentences.view,'MGWiki Sentences');
    toolboxobj.addBoxItem('corpus-misc-actions',miscActions.view,'Misc');

    cma.TheApp.mgwiki_sentences.init();
    
    

    permissions.init();

    this.getAndDo('tasks',{},function(data){
      console.log(data);
      var tasks = me.setTasks(data);
      cma.ui.get('cma-corpus-log').html('');
      cma.ui.get('cma-corpus-log').append(tasks);
    });

    this.getAndDo("report",{},function(data){

      cma.ui.get('cma-corpus-status').html(data);
    },null,'html');

    
  }

  
  
  cma.Corpus.prototype.updateHeader = function(){
    var me = this;
    this.getAndDo('info',{},function(data){
      me.name = data.name;
      cma.ui.get('cma-title').html(data.name);
      var format = "";
      if(data.format){
        format = " ("+data.format+")";
      }
      cma.ui.get('cma-status').html(data.status+format);
      
      me.setActions(data);
      cma.ui.get('cma-app').removeClass('hidden');
      cma.TheApp.ajaxLoader(false);
    });
  };
  

  cma.Corpus.prototype.setActions = function(data){
    var ui = cma.ui.get('cma-actions');
    var html = '';
    for(var i=0;i<data.actions.length;++i){
      html+= '<div class="cma-app-button"><a href="#corpus/'+this.cid+'/'+data.actions[i]+'" class="icon-action-'+data.actions[i]+'"></a></div>';
    }
    ui.html(html);
  };

  cma.Corpus.prototype.getDocumentsInfo = function(data){
    if(data.file){
      this.paths[data.did] = {filepath:data.fullpath,scid:data.scid,name:data.path};
    }else{
      for(i in data.children){
        this.getDocumentsInfo(data.children[i]);
      }
    }
  }
  
  cma.Corpus.prototype.displayDocuments = function(data,container){
    if(data.file){
      this.paths[data.did] = {filepath:data.fullpath,name:data.path};
      container.append('<a href="#corpus/'+this.cid+'/view/'+data.did+'" class="cma-document-file">'+data.path+'</a>');
    }else{
      var folder = jQuery('<div class="cma-document-folder"><div class="cma-folder-toggle cma-folder-collapse"></div><div class="cma-document-folder-file">'+data.path+'</div></div>');
      var newcontainer = jQuery('<div class="cma-document-tree"></div>');
      container.append(folder);
      folder.append(newcontainer);
      for(i in data.children){
        this.displayDocuments(data.children[i],newcontainer);
      }
    }
  };
  
  cma.Corpus.prototype.displayDocument = function(did,filename){
    var me = this;
    this.doc = new cma.Document(did);
    jQuery.ajax({
      type: 'POST', 
      url:  cma.TheApp.ws_url+'frmgcm_proxy',
      data : {proxy_url: 'corpus/'+me.cid+'/document/'+did+'/annotated'},
      dataType : 'html',
      success: function(data, textStatus, jqXHR) {
        me.doc.initView(filename);
        
        me.doc.init(data);

        cma.TheApp.ajaxLoader(false);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        if(errorCallback){
          errorCallback(jqXHR, textStatus, errorThrown);
        }else{
          alert(textStatus);
        }
        cma.TheApp.ajaxLoader(false);
      }
    });
    
  };






  
  cma.Corpus.prototype.setTasks = function(data){
    var me = this;
    this.taskstatus = data;
    var tasksui = jQuery(cma.template.tasks);
    var table = jQuery("table",tasksui);
    for(var i=0; i<data.length; i++){
      var task = data[i];
      for(var j=0;j<task.length;j++){
        var taskunit = task[j];
        var tr = '<tr><td class="cma-app">'+taskunit.name
          +'</td><td class="cma-app">'+taskunit.status
          +'</td><td class="cma-app"><a style="text-decoration: underline; cursor:pointer;" ref="stdout-'+taskunit.stdout+'">view</a>'
          +'</td><td class="cma-app"><a style="text-decoration: underline; cursor:pointer;" ref="stderr-'+taskunit.stderr+'">view</a>'
          +'</td></tr>';
        table.append(tr);
      }
    }
    jQuery('a',table).click(function(){
      me.showTask(jQuery(this).attr('ref'));
    });
    return tasksui;
  };

  cma.Corpus.prototype.showTask = function(task){
    this.getAndDo('output',{resource:task},function(data){
      var width = cma.TheApp.view.width();
      var height = cma.TheApp.view.height();
      var offset = cma.TheApp.view.offset();
      var point = {x:offset.left+width/2,y:offset.top+height/2};
      var div = jQuery('<div>'+data+'</div>');
      var box = new depgraphlib.Box({closeButton:true,draggable:true}).setContent(div).setFixedSize(500).open(point);
    },null,'html');
  }

  cma.Corpus.prototype.initContactForm = function(){
    var me = this;
    var contactForm = cma.ui.get("cma-corpus-contact-form");
    contactForm.html(cma.template.contactForm);

    jQuery("#cma-contact-form-submit").click(function(event){
      var form = jQuery(jQuery(event.target).parent());
      var subject = jQuery("#cma-contact-form-subject",form).val();
      var body = jQuery("#cma-contact-form-body",form).val();
      me.callAndDo("sendMail",{subject:subject,body:body},function(data){
        console.log(data);
      });
    });
  }
  

  
  cma.Corpus.prototype.callAndDo = function(action,data,callback,errorCallback,dataType){
    var me = this;
    data = data || {};
    data.cid = me.cid;
    dataType = dataType || 'json';
    jQuery.ajax({
      type: 'POST', 
      url: cma.TheApp.ws_url+action,
      data: data,
      dataType : dataType,
      success: function(data, textStatus, jqXHR) {
        callback(data,textStatus,jqXHR);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        if(errorCallback){
          errorCallback(jqXHR, textStatus, errorThrown);
        }else{
          alert(textStatus);
        }
      }
    });
  };

  
  cma.Corpus.prototype.getAndDo = function(action,data,callback,errorCallback,dataType){
    var me = this;
    data = data || {};
    data.cid = me.cid;
    data.action = action;
    dataType = dataType || 'json';
    jQuery.ajax({
      type: 'POST', 
      url: cma.TheApp.ws_url+'get',
      data: data,
      dataType : dataType,
      success: function(data, textStatus, jqXHR) {
        callback(data,textStatus,jqXHR);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        if(errorCallback){
          errorCallback(jqXHR, textStatus, errorThrown);
        }else{
          alert(textStatus);
        }
      }
    });
  };
  

  
  
  
  
  
}(window.cma = window.cma || {}));
