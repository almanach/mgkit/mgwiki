(function(cma){
  
  /*******************************************************************/
  /*                              TODO                               */ 
  /*******************************************************************/
  /*
    
    - add on app close (=route out of app range, page refresh) : unauthenticate user
    - add open mgwiki in new window button (a coté de home)
    






  */
  /*******************************************************************/
  
  cma.App = function(container,options){
    // set the singleton reference
    cma.TheApp = this;

    // options dispatching
    // refactor with no dispatching
    this.ws_url = options.ws_url;
    this.options = options;
    this.options.defaultLang = this.options.defaultLang || "fr";
    this.exit_url = options.exit_url;

    // set up the app layout
    this.view = jQuery(cma.template.main);

    // inserting the view in dom
    if(!container){
      container = jQuery('<div id="cma-overlay"></div>');
      jQuery("body").append(container);
    }
    container.append(this.view);
    
    // logic init
    this.init();

    // modules
    this.mgwiki_sentences = new cma.toolbox.MgwikiSentences();
    
  };
  
  // history navigation
  cma.App.prototype.routeHistory = [];
  
  // logic init
  cma.App.prototype.init = function(){
    var me = this;

    jQuery.ajax({
      type: 'GET', 
      url: cma.TheApp.ws_url+'auth',
      data: {},
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        me.authkey = data.authkey;
        me.user = data.user;
        console.log(me.user);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        alert(textStatus);
      }
    });

    // init view parameters (depends on screen size, etc..)
    this.initView();

    // init web socket / authentication user
   this.websocket_listeners={
      "onopen":{},
      "onmessage":{},
      "onclose":{},
      "onerror":{}
    };
    this.initWebSocket(this.options.websocket_url);
    
    // navigation callback
    jQuery(window).on("hashchange",function(e){
      me.go(location.hash);
    });
    jQuery(window).hashchange();
  };

  cma.App.prototype.initWebSocket = function(url){
    var me = this;
    if(WebSocket == undefined){
      this.websocket_enabled = false;
      return;
    }else{
      this.websocket_enabled = true;
    }

    // request authentication by mgwiki first
    // mgwiki send a secret key to the ws server to accept the ws connexion
    try{
       this.socket = new WebSocket("ws://"+url);
    }catch(err){
      console.log(err);
      return;
    }
   
    jQuery.ajax({
      type: 'GET', 
      url: cma.TheApp.ws_url+'auth',
      data: {},
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        me.authkey = data.authkey;
        me.user = data.user;

        me.websocket_listeners['onopen']['authenticate'] = function(){
          var message = {authkey:cma.TheApp.authkey};
          cma.TheApp.socket.send(JSON.stringify(message));
        };
          

        me.socket.onopen = function() { me.websocketListenersCall("onopen"); };
        me.socket.onmessage = function (e) { 
          me.websocketListenersCall("onmessage",e.data); 
        };
        me.socket.onclose = function() { me.websocketListenersCall("onclose");};
        me.socket.onerror = function() { me.websocketListenersCall("onerror");};

      },
      error: function(jqXHR, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
    

    
  };

  cma.App.prototype.websocketListenersCall = function(theevent,data){
    for(listener in this.websocket_listeners[theevent]){
      if(this.websocket_listeners[theevent][listener] != null){
        this.websocket_listeners[theevent][listener].call(null,data);
      }
    }
  }

  cma.App.prototype.websocketAddListener = function(theevent,name,callback){
    this.websocket_listeners[theevent][name] = callback;
  }
  

  cma.App.prototype.initView = function(){
    var container = jQuery('#cma-app-container');
    var height = jQuery(window).height() - this.view.offset().top; 
    var width = this.view.width();
    var appmainview = cma.ui.get("cma-app");
    var paddingtop = 10;
    var paddingleft = 20;
    appmainview.height(height-130-paddingtop);
    appmainview.width(width-50-paddingleft);
    this.view.height(height);
    container.width(width-50);
    container.height(height-50);
    this.ajaxLoader(true);
    this.initHeaderMenu();

  };

  /**
   * @function help
   * @description launch IntroJS interactive help
   * @memberOf cma.App
   */
  cma.App.prototype.help = function(){
    var me = this;
    var helpType = 0; // default main page help
    var path = location.hash.trim().split("/");
    if(path[0]=="#corpus"){
      if(path[2]=="view"){
        helpType = 2; // document help page
      }else{
        helpType = 1; // corpus help page
      }
    }
    
    var intro = introJs();
    if(helpType==0){
      var uploadForm = jQuery('#corpusUploadForm');
      uploadForm = uploadForm[0];
      intro.setOptions({
        steps:[
        {
          intro:"<h1>Bienvenue!</h1>"
        },
        {
          intro:"Ici vous pouvez uploader votre corpus",
          element:uploadForm
        },
        {
          intro:"Les corpus sont ensuite listés ici",
          element:document.querySelector("#corpus-list-view")
        }
        ]
      });

    }else if(helpType==1){
      intro.setOptions({
        steps:[
        {
          intro:"Vous retrouvez l'ensemble des documents chargés pour ce corpus ici. En cliquant sur un document vous pouvez accéder à une visualisation des résultats par document.",
          element:document.querySelector(".cma-documents-tree")
        },
        {
          intro:"Cet section fournit les actions possible ou l'état du traitement de corpus.",
          element:me.view[0].querySelector('.cma-toolboxitem-container[name="corpus-process"]')
        },
        {
          intro:"Ici vous pouvez effectuer une recherche dans le corpus.",
          element:me.view[0].querySelector('.cma-toolboxitem-container[name="document-search"]')
        },
        {
          intro:"Ici se trouve le rapport du processus de traitement de corpus.",
          element:me.view[0].querySelector('.cma-toolboxitem-container[name="corpus-report"]')
        },
        {
          intro:"Ici vous pouvez modifier les permissions accordées aux utilisateurs.",
          element:me.view[0].querySelector('.cma-toolboxitem-container[name="corpus-permissions"]')
        },
        {
          intro:"Ici se trouve le log des différentes taches constituant le processus de traitement de corpus.",
          element:me.view[0].querySelector('.cma-toolboxitem-container[name="corpus-log"]')
        },
        {
          intro:"Ici se trouve la liste des phrases d'exemples de MGWiki.",
          element:me.view[0].querySelector('.cma-toolboxitem-container[name="mgwiki_sentences"]')
        },
        {
          intro:"Ici se trouvent les fonctions pour télécharger les résultats ou supprimer le corpus.",
          element:me.view[0].querySelector('.cma-toolboxitem-container[name="corpus-misc-actions"]')
        }
        ]
      });
    }else if(helpType==2){
      intro.setOptions({
        steps:[
        {
          intro:"Ici se trouve le texte original. Les phrases analysées (noir et bleu pour une analyse partielle) sont cliquable et ouvre une fenêtre de visualisation du graphe associé à la phrase. Il est aussi possible de sélectionner seulement un fragment de phrase.",
          element:me.view[0].querySelector('.cma-toolboxitem-container[name="corpus-process"]')
        },
        {
          intro:"Ici vous pouvez effectuer une recherche dans ce document.",
          element:me.view[0].querySelector('.cma-toolboxitem-container[name="document-search"]')
        },
        {
          intro:"Vous pouvez ici si vous avez les droits activer le mode édition qui permet d'éditer les sorties d'analyse à partir de l'interface de visualisation des phrases.",
          element:me.view[0].querySelector('.document-edition"]')
        }
        ]
      });
    }
    intro.start();
  };
  
  cma.App.prototype.initHeaderMenu = function(){
    var me = this;
    cma.ui.get("cma-quit").attr("href",this.exit_url);
    cma.ui.get("cma-feedback").click(function(){
      cma.TheApp.feedBackForm();
    });
    cma.ui.get("cma-help").click(function(){
      me.help();
    });
    var menuBox = cma.ui.get("cma-header-menu");
    var menu = '<div id="cma-back" style="display:none;" onclick="cma.TheApp.goBack();"></div><div id="cma-home" onclick="cma.TheApp.goTo(\'\')"></div><div id="cma-mgwiki-link" onclick="window.open(cma.TheApp.options.exit_url);"></div>';
    menuBox.html(menu);
  };
  
  cma.App.prototype.goBack = function(){
    this.routeHistory.pop();
    if(this.routeHistory.length==1){
      cma.ui.get("cma-back").hide();
    }
    this.goTo(this.routeHistory.pop());
  };
  
  cma.App.prototype.goTo = function(route){
    if(location.hash!=route){
      cma.TheApp.ajaxLoader(true);
    }
    location.hash=route;
  };
  
  cma.App.prototype.go = function(route){
    var backbutton = cma.ui.get("cma-back");
    if(backbutton.is(":hidden") && this.routeHistory.length > 0){
      backbutton.show();
    }
    
    if(this.routeHistory[this.routeHistory.length-1] != route){
      this.routeHistory.push(route);
    }

    cma.TheApp.ajaxLoader(true);

        
    var path = route.trim().split("/");
    if(!path[0]){
      cma.AppView.setTemplate(cma.template.upload);
      if(this.options.user.uid == 0){
        jQuery('.corpusUpload').hide();
      }
      cma.AppView.setTitle('<h1>Corpus Management Application</h1>');

      var helpUpload = new cma.ui.FoldableDiv(cma.text.get("uploadHelp"));
      helpUpload.setTitle('Infos');
      cma.ui.get('cma-upload-title').after(helpUpload.view);

      this.corpus_list_view = new cma.CorpusListView('corpus-list-view');
      this.corpus_list_view.refreshData();

      this.corpusUpload = new cma.corpusUpload();
      this.corpusUpload.settings.scriptPath = this.ws_url+"upload";
      this.corpusUpload.settings.corpusListView = this.corpus_list_view; 

      cma.ui.get('cma-app').removeClass('hidden');

      this.corpus = null;

    }
    else if(path[0]=="#corpus"){
      var cid = path[1];
      var action = path[2];
      if(cid !== null){
        if(action=="delete"){
          this.corpus = new cma.Corpus(cid);
          this.routeHistory.pop();
          this.corpus.callAndDo('delete',{},function(data){
            console.log(data);
          });
          this.goTo("");
          return;
        }
        if(!this.corpus || this.corpus.cid != cid){
          console.log("loading corpus "+cid);
          this.corpus = new cma.Corpus(cid);
          this.corpus.init(function (){cma.TheApp.go(route);});
          return;
        }
      }else{
        this.page404(route);
      }
      
      if(action == "process"){
        this.goBack();
      }else if(action == "delete"){
        this.routeHistory.pop();
        this.corpus.callAndDo('delete',{},function(data){
          console.log(data);
        });
        this.goTo("");
      }else if(action == "download"){
        this.corpus.callAndDo('download',{},function(data){
          window.open(data.url);
        });
        this.goBack();
      }else if(action == "view"){
        var me = this;
        var file = me.corpus.paths[path[3]].filepath;
        me.corpus.displayDocument(path[3],file);
      }else{
        this.corpus.initView();
      }
      
      
    }else{
      this.goTo("");
    }
  
  };

  cma.App.prototype.page404 = function(){

  };

  cma.App.prototype.feedBackForm = function(){
    var width = cma.TheApp.view.width();
    var height = cma.TheApp.view.height();
    var offset = cma.TheApp.view.offset();
    var point = {x:offset.left+width/2,y:offset.top+height/2};
    var div = jQuery('<div><label>'+cma.text.get('feedbackLabel')+'</label><textarea id="cma-feedback-content" name="issue" rows="4" cols="50"></textarea><br><input id="cma-feedback-submit" type="button" value="Send"></div>');
    jQuery("#cma-feedback-submit",div).click(function(){
      var textarea = jQuery('#cma-feedback-content',div);
      var data = textarea.val();
      var box = depgraphlib.Box.getBox(textarea[0]);
      jQuery.ajax({
        type: 'POST', 
        url: cma.TheApp.options.host_url+"/mgwiki/ws/sendMail",
        data: {
          message:data,
        },
        dataType : 'json',
        success: function(data, textStatus, jqXHR) {
          box.setContent(cma.text.get("success-message-feedback"));
          setTimeout(function(){box.close(true);},2000);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          alert(textStatus);
        }
      });
    });
    var box = new depgraphlib.Box({closeButton:true,draggable:true}).setContent(div).setHeader('FeedBack Form').setFixedSize(500).open(point);
  };

  cma.App.prototype.helpPanel = function(){
    var width = cma.TheApp.view.width();
    var height = cma.TheApp.view.height();
    var offset = cma.TheApp.view.offset();
    var point = {x:offset.left+width/2,y:offset.top+height/2};
    var div = jQuery('<div>Add mgwiki cma help content page here!</div>');
    var box = new depgraphlib.Box({closeButton:true,draggable:true}).setContent(div).setFixedSize(500).open(point);
  };
  
  cma.App.prototype.ajaxLoader = function(status){
    var ajaxLoader = jQuery('#ajax-loader');
    if(status){
      var container = jQuery('#cma-app-container');
      var height = jQuery(window).height() - this.view.offset().top; 
      var width = this.view.width();
      ajaxLoader.height(height-130);
      ajaxLoader.width(width-50);
      ajaxLoader.css("top",54);
      ajaxLoader.removeClass("hidden");
    }else{
      ajaxLoader.addClass("hidden");
    }
  };

  cma.App.prototype.callProxy = function(path,data,callback,errorCallback,dataType){
    var me = this;
    data = data || {};
    data.proxy_url = path;
    dataType = dataType || 'json';
    jQuery.ajax({
      type: 'POST', 
      url: cma.TheApp.ws_url+'frmgcm_proxy',
      data: data,
      dataType : dataType,
      success: function(data, textStatus, jqXHR) {
        callback(data,textStatus,jqXHR);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        if(errorCallback){
          errorCallback(jqXHR, textStatus, errorThrown);
        }else{
          alert(textStatus);
        }
      }
    });
  };

  
  
  
}(window.cma = window.cma || {}));
