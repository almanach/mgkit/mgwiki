(function(cma){
  
  cma.text = {};

  cma.text.fr = {

    "uploadHelp":"<p>Le service accepte les formats de fichiers suivants : pdf, doc, docx, rdf, txt. Les formats html et xml seront bientôt pris également en charge. :) A noter que pour de meilleurs résultats il est conseillé d'utiliser un format de texte brut, c'est à dire 'txt'.</p><p> Dans le cas où votre corpus est composé de plusieurs fichiers séparés (préférable à un seul gros fichier), le service accepte les archives zip et tar.gz.</p><p>Nous imposons pour ce service une limite pour la taille totale des fichiers (10M). Si vous souhaitez traiter un plus large volume de donnée, vous pouvez nous contacter via le formulaire de contact du site principal (Navigation->Contact).</p><p> Merci de ne pas utiliser abusivement ce service.</p>",

    "feedbackLabel":"Entrez votre message : ",
    "success-message-feedback":"Votre message de feedback a bien été envoyé. Merci.",
    "delete-corpus-confirm":"Etes vous sûr de vouloir supprimer ce corpus?",
    "delete-macrodef-confirm":"Etes vous sûr de vouloir supprimer cette macro?"
  };

  cma.text.en = {

    "uploadHelp":"to do",

  };

  cma.text.get = function(item,lang){
    if(!lang){
      lang = cma.TheApp.options.defaultLang;
    }

    return cma.text[lang][item];
  }

  
}(window.cma = window.cma || {}));
