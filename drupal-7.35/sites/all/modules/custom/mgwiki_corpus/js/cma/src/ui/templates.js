(function(cma){
  
  cma.template = {};
  
  cma.template.main= '<div style="width:100%; height:800px;"><div id="cma-app-container">'
    + '<div id="ajax-loader"></div>'
    + '<div id="cma-header-bar"><div id="cma-header-menu"></div><div id="cma-header-title"></div><a id="cma-help"></a><a id="cma-quit" class="exit"></a></div>'
    + '<div id="cma-app" class="hidden">'
    + '</div><div id="cma-status-bar"><div id="cma-status-bar-content"></div><a id="cma-feedback"></a></div></div></div>';
  
  
  cma.template.corpusViewHeader = '<div id="cma-header">'
    + '<span id="cma-title" class="mutable"></span>' // on click transform to input for renaming
    + '<span id="cma-status"></span>'
    + '<span id="cma-actions"></span>'
    + '</div>';

  cma.template.corpusViewMain = '<div id="cma-content">'
    + '</div>';

  cma.template.toolboxPermissions = '<h3>Corpus Permissions</h3>'
    + '<h4>Access Corpus</h4>'
    + '<ul></ul>'
    + '<h4>Edit Corpus</h4>';

  cma.template.contactForm = '<form id="cma-contact-form">'
    + '<label>Subject :</label><input id="cma-contact-form-subject" type="textarea"><br>'
    + '<label>Message :</label><textarea id="cma-contact-form-body" name="body" rows="4" cols="50"></textarea><br>'
    + '<input id="cma-contact-form-submit" name="submit" type="button" value="Send">'
    + '</form>';

  cma.template.tasks = '<div><h2 style="margin-top:0px">Log</h2><table class="cma-app"><tr><th class="cma-app">Task</th><th class="cma-app">Status</th><th class="cma-app">StdOut</th><th class="cma-app">StdErr</th></tr>'
    + '</table></div>';
  
  cma.template.upload = '<div class="corpusUpload">'+
    '<div class="corpusUploadContainer cma-container-level-1">'+
    '<h2 id="cma-upload-title" style="margin-top:0px;">Upload new corpus : </h2>'+
    '<form action="corpus_upload/upload" method="post" enctype="multipart/form-data" id="corpusUploadForm">'+
    '<input type="file" id="corpusUploadFile" name="corpusUploadFile" />'+
    '<div>OR</div>'+
    '<label>URL:</label><input type="text" id="corpusUploadUrl" name="corpusUploadUrl" /><br>'+
    '<input type="button" class="corpusUploadButton" value="Start Upload" id="corpusUploadSubmit" onclick="cma.TheApp.corpusUpload.fire();" />'+
    '</form>'+
    '<div id="corpusUploadProgressBarContainer">'+
    '<div id="corpusUploadProgressBarFilled"></div>'+
    '</div>'+
    '<div id="corpusUploadTimeRemaining"></div>'+
    '<div id="corpusUploadResponse"></div>'+
    '</div>'+
    '</div>'+
    '<div class="cma-container-level-1" >'+
    '<h2>Corpus List</h2>'+
    '<div id="corpus-list-view"></div></div>';
  
  cma.template.toolboxitem_inset = '<div class="cma-toolboxitem-container">'+
      '<div class="cma-toolboxitem-header"><div class="cma-icon cma-toolbox-item-detach"></div><div class="cma-icon cma-toolbox-item-expand"></div><div class="cma-toolbox-item-title"></div></div>'+
      '<div class="cma-toolboxitem-content"></div>'+
    '</div>';

  cma.template.toolboxitem_outset = '<div id="cma-toolboxitem_outset"></div>';

  cma.template.toolbox = '<div class="cma-toolbox cma-container-level-1">'+
      '<div class="cma-toolbox-header"><h2 style="margin-top:0px;">Toolbox</h2></div>'+
      '<div class="cma-toolboxitems-container"></div>'+
      '<div class="cma-toolbox-footer"></div>'+
    '</div>';



  
}(window.cma = window.cma || {}));
