(function(mgwiki,$){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.SearchService = function(app,$el,settings){
    this.app = app;
    this.settings = settings;
    this.view = new mgwiki.cma.SearchServiceView(this,$el);
    this.init();
  };

  mgwiki.cma.SearchService.max = 20;

  mgwiki.cma.SearchService.prototype.init = function(){
    var me = this;
    this.corpusList = [];
    this.results = {};
    this.fetchCorpusList(function(){
      me.view.render();
      me.getTagset();
      //me.app.errorLogger.log("Les corpus EP, FrWiki et WkS sont en cours d\'indexation. Les résultats fournis pour les requêtes appliquées à ces corpus seront donc incomplets pour le moment.");
      me.searchlib = new mgwiki.cma.SearchLib({proxy_url:me.settings.proxy_url,user:me.settings.user,textarea:me.view.$el.find('.cma-search-input')});
      me.history = new mgwiki.cma.SearchHistory({ws_url:me.settings.proxy_url,macroLib:me.searchlib,textarea:me.view.$el.find('.cma-search-input')});
    },function(){
      me.app.errorLogger.log("error while loading corpus list");
    });
    window.onbeforeunload = function(e){
      for (var i in me.results) {
        for(var j in me.results[i]){
          if(!me.results[i][j].finished){
            me.results[i][j].abortSearch();
          }
        }
      }
    };
  };

  mgwiki.cma.SearchService.prototype.getFormat =function(){
    var formats = [];
    for(var i=0;i<this.corpusList.length;i++){
      if(this.corpusList[i].name == this.targetCorpus){
        formats = this.corpusList[i].formats;
        break;
      }
    }
    if(!formats){
      return "conll";
    }
    return formats[0];
  }


  mgwiki.cma.SearchService.prototype.getTagset = function(){
    var me = this;
    jQuery.ajax({
      type: 'POST', 
      url: me.settings.proxy_url+'sc/_tagset',
      data:{
        format:me.getFormat()
      },
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        if(data.name){
          me.tagset = data;
          me.props = [];
          for (var i in data.node) {
            me.props.push(i);
          };
          for (var i in data.edge) {
            me.props.push(i);
          };  
        }
        
      },
      error: function(jqXHR, textStatus, errorThrown) {
      }
    });
  };

  mgwiki.cma.SearchService.prototype.getSearchInfo = function(collectionName){
    var searchInfo = {};
    for(var i=0;i<this.corpusList.length;i++){
      if(this.corpusList[i].name == collectionName){
        var corpus = this.corpusList[i];
        if(corpus.cmaCorpus){
          searchInfo.url = "corpus/"+corpus.cid+"/_search";
          searchInfo.cmaCorpus = true;
        }else{
          searchInfo.url = "sc/"+collectionName+"/_search";
        }
      }
    }
    return searchInfo;
  };

  mgwiki.cma.SearchService.prototype.search = function(query,callbackSuccess,callbackFailure){
    var me = this;
    var searchInfo = this.getSearchInfo(me.targetCorpus);
    var data = {
      query:query,
      max:mgwiki.cma.SearchService.max
    };
    if(searchInfo.cmaCorpus){
      data['cma-reloaded']=true;
    }
    $.ajax({
      type: 'POST', 
      url: me.settings.proxy_url + searchInfo.url,
      data: data,
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        console.log(data);
        if(data.error){
          me.app.errorLogger.log(data.message);
          callbackFailure.call();
        }else{
          me.results[me.targetCorpus][query]=new mgwiki.cma.SearchResult(me.app.$el.find('.cma-results-container'),{app:me,query:query,ws_url:me.settings.ws_url,targetCorpus:me.targetCorpus,proxy_url:me.settings.proxy_url,data:data});
          //me.results[me.targetCorpus][query].view.render();
          callbackSuccess.call(me,data);  
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        callbackFailure.call(me,jqXHR);
      }
    });
  }

  mgwiki.cma.SearchService.prototype.dismissResult = function(result){
    delete this.results[result.targetCorpus][result.query];
  };


  mgwiki.cma.SearchService.prototype.fetchCorpusList = function(callbackSuccess,callbackFailure){
    var me = this;
    $.ajax({
      type: 'POST', 
      url: me.settings.proxy_url + "sc/_list",
      data: {
      },
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        me.corpusList = data;
        if(data.length){
          me.targetCorpus = data[0].name;
          for(var i = 0; i<data.length; i++){
            me.results[data[i].name] = me.results[data[i].name] || {};
          }
          callbackSuccess.call(me,data);
        }else{
          me.app.errorLogger.log("no corpus found!");
          callbackFailure.call();
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        callbackFailure.call(me,jqXHR);
      }
    });
  };

  

  
}(window.mgwiki = window.mgwiki || {},jQuery));
