(function(mgwiki,$){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.SentenceCollection = function(app,$el,settings){
    this.app = app;
    this.view = new mgwiki.cma.SentenceCollectionView(this,$el);
    this.max = 5;
    this.from = 0;
  };

  mgwiki.cma.SentenceCollection.prototype.load = function(item){
    var me = this;
    this.collection = item;
    this.goTo(0);
  };

  mgwiki.cma.SentenceCollection.prototype.next = function(callbackSuccess,callbackFailure){
    this.goTo(Math.ceil(this.from/this.max)+1,callbackSuccess,callbackFailure);
  };

  mgwiki.cma.SentenceCollection.prototype.prev = function(callbackSuccess,callbackFailure){
    this.goTo(Math.ceil(this.from/this.max)-1,callbackSuccess,callbackFailure);
  };

  mgwiki.cma.SentenceCollection.prototype.goTo = function(index,callbackSuccess,callbackFailure){
    window.scrollTo(0,0);
    var me = this;
    var data = {
      max:me.max,
      from:index*me.max
    };
    if(this.hideVoted){
      data.hideVoted = true;
    }
    if(this.hideUnVoted){
      data.hideUnVoted = true;
    }
    $.ajax({
      type: 'POST', 
      url: me.app.settings.proxy_url + "sc/"+me.collection.name+"/_graph/_all",
      data: data,
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        me.graphs = [];
        for (var i = data.graphs.length - 1; i >= 0; i--) {
          data.graphs[i].sc = me.collection.name;
          me.graphs.push(new mgwiki.cma.Graph(me.app,null,data.graphs[i]));
        };
        me.from = me.max*index;
        me.total = data.total;
        me.view.render();
      },
      error: function(jqXHR, textStatus, errorThrown) {
        console.log("error while fetching corpus list");
      }
    });
  };
  
}(window.mgwiki = window.mgwiki || {},jQuery));