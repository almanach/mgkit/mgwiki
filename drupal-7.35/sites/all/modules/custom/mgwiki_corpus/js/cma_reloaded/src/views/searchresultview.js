(function(mgwiki,$){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.SearchResultView = function(model,$el){
    this.model = model;
    this.$el = $el || $('<div class="cma-result-container"></div>');
    this.initiated = false;
  };

  mgwiki.cma.SearchResultView.paginationMax = 10;

  mgwiki.cma.SearchResultView.prototype.render = function(){
    var me = this;

    if(!this.initiated){
      me.$el.hide();  
    }
    
    me.$el.html(mgwiki.cma.SearchResultView.template); // does this practice delete all previous dom bidings / data?

    if(me.model.finished){
      me.$el.find('.cma-result-total').html("<b>Total :</b>"+me.model.total);
      me.$el.find('.cma-result-export').click(function(){
        me.model.export();
      });
    }else{
      me.$el.find('.cma-result-total').html('<b>Total (estimation) : </b>'+me.model.e_total+'<div class="cma-throbber"></div>');
      me.$el.find('.cma-result-export').hide();
    }
    

    this.$el.find('.cma-result-query').html("<b>Query : </b>"+this.model.query);

    this.$el.find('.dismiss-message-icon').click(function(){
      me.model.destroy();
      me.$el.fadeOut(400,function(){
        me.$el.remove();  
      });
    });

    if(this.model.data){
      var table = new mgwiki.cma.ui.Table({
        headers:[
          {title:"ID",nameid:"sid"},
          {title:"Sentence",nameid:"sentence"},
          {title:"Document",nameid:"did"}
        ],
        style:{
          tr0:'cma-table-even cma-table-item',
          tr1:'cma-table-odd cma-table-item'
        }
      });
      me.resultsTable = table;
      me.$el.find('.cma-result-data').append(table.view); // here can go a styling effect of table fading out/in
      table.update(processResults(this.model.data));
      table.view.find('tr').click(function(e){
        var sid = this.cells[0].textContent;
        var did = this.cells[2].textContent;
        if(sid=="ID"){
          return;
        }
        me.model.showGraph(sid,did,me.model.targetCorpus);
      });

      var n = Math.ceil(me.model.total/me.model.max);
      var i = Math.floor(this.model.from / this.model.max);
      

      

      if(i>0){
        var startButton = new mgwiki.cma.ui.CallbackButton("Start",{}); // add styles to callback buttons ...
        me.$el.find('.cma-pagination-start').append(startButton.view); // here if i use .html() are callbacks still preserved?
        startButton.init(function(){
          var button = this;
          me.model.goTo(0,function(){button.success();},function(){button.error();});
        },this);

        var prevButton = new mgwiki.cma.ui.CallbackButton("Prev",{}); // add styles to callback buttons ...
        me.$el.find('.cma-pagination-prev').append(prevButton.view); // here if i use .html() are callbacks still preserved?
        prevButton.init(function(){
          var button = this;
          me.model.prev(function(){button.success();},function(){button.error();});
        },this);  
      }

      if(i<n-1){
        if(me.model.finished){
          var endButton = new mgwiki.cma.ui.CallbackButton("End",{}); // add styles to callback buttons ...
          me.$el.find('.cma-pagination-end').append(endButton.view); // here if i use .html() are callbacks still preserved?
          endButton.init(function(){
            var button = this;
            me.model.goTo(n-1,function(){button.success();},function(){button.error();});
          },this);
        }

        var nextButton = new mgwiki.cma.ui.CallbackButton("Next",{}); // add styles to callback buttons ...
        me.$el.find('.cma-pagination-next').append(nextButton.view); // here if i use .html() are callbacks still preserved?
        nextButton.init(function(){
          var button = this;
          me.model.next(function(){button.success();},function(){button.error();});
        },this);
      }

      var k = mgwiki.cma.SearchResultView.paginationMax/2;
      var kp = 0;
      var kn = 0;
      var a = i-k;
      if(a<0){
        kn = -a;
      }
      var b = i+k;
      if(b>=n){
        kp = n-b;
      }
      var ks = (a+kp>0)?(a+kp):0;
      var ke = (b+kn<n)?(b+kn):(n-1);
      for(var j=ks;j<=ke;j++){
        if(j==i){
          me.$el.find('.cma-pagination-pages').append('<span style="margin-left:10px; margin-right:10px;">'+i+'</span>');
          continue;
        }
        var pagginButton = new mgwiki.cma.ui.CallbackButton(j,{}); // add styles to callback buttons ...
        pagginButton.view.attr('index',j);
        me.$el.find('.cma-pagination-pages').append(pagginButton.view); // here if i use .html() are callbacks still preserved?
        pagginButton.init(function(){
          var button = this;
          me.model.goTo(button.view.attr('index'),function(){button.success();},function(){button.error();});
        },this);  
      }
      
      // callback for save/export/dismiss buttons
      
    }else{
      this.$el.find('cma-result-pagination').hide();
    }

    if(!this.initiated){
      me.$el.slideDown(300);  
      this.initiated = true;
    }

  };



  function processResults(data){
    if(data && data.hits){
      var hits;
      if(data.dpath){
        hits = data.hits;
      }else{
        hits = data.hits.hits;
      }
      for(var i in hits){
        var hit = hits[i];
        hit.sid = hit._source.sid;
        hit.did = hit._source.did;
        if(hit._source.sentence){
          hit.sentence = hit._source.sentence.substr(0,100);  
        }else{
          hit.sentence = "error : could not find sentence content";
        }
        
        delete hit._index; 
        delete hit._type; 
        delete hit._id; 
      }  
    }
    return hits;
  }

  mgwiki.cma.SearchResultView.template = '<div class="cma-result-header"><div class="dismiss-message-icon"></div><div class="cma-result-export"></div><span class="cma-result-save"></span><div class="cma-result-query"></div><div class="cma-result-total"></div></div>'+
    '<div class="cma-result-data"></div>'+
    '<div class="cma-result-pagination"><span class="cma-pagination-start"></span><span class="cma-pagination-prev"></span><span class="cma-pagination-pages"></span><span class="cma-pagination-next"></span><span class="cma-pagination-end"></span></div>';

  
}(window.mgwiki = window.mgwiki || {},jQuery));