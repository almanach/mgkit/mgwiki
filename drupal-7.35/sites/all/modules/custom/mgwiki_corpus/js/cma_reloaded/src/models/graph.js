(function(mgwiki,$){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.Graph = function(app,$el,settings){
    this.app = app;
    this.settings = settings;
    this.view = new mgwiki.cma.GraphView(this,$el);
  };

  mgwiki.cma.Graph.prototype.fetchData = function(callbackSuccess,callbackError){
    var me = this;
    $.ajax({
      type: 'POST', 
      url: me.app.settings.proxy_url + "sc/"+me.settings.sc+"/_graph",
      data: {
        complete:true,
        sid:me.settings.id,
      },
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        me.settings.data = data.data;
        me.settings.notes = data.notes;
        me.settings.votes = data.votes;
        me.settings.eid = data.eid;
        me.settings.sentence = data.sentence;
        callbackSuccess.call(me,data);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        callbackError.call(me,textStatus);
      }
    });
  };

  mgwiki.cma.Graph.prototype.export = function(){
    depgraphlib.windowOpenPost(
      {
        sid:me.settings.id
      },
      me.app.settings.proxy_url + "sc/"+me.settings.sc+"/_graph"
    );
  };


  mgwiki.cma.Graph.prototype.vote = function(val){
    var me = this;
    $.ajax({
      type: 'POST', 
      url: me.app.settings.proxy_url + "sc/"+me.settings.sc+"/_graph/_vote/_put",
      data: {
        sid:me.settings.id,
        val:val
      },
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
      },
      error: function(jqXHR, textStatus, errorThrown) {
      }
    });
  };

  mgwiki.cma.Graph.prototype.removeNote = function(note){
    var index = jQuery(note).attr('cid');
    var me = this;
    $.ajax({
      type: 'POST', 
      url: me.app.settings.proxy_url + "sc/"+me.settings.sc+"/_graph/_note/_remove",
      data: {
        sid:me.settings.id,
        index:index
      },
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        jQuery(note.parentNode.parentNode).remove();
        console.log(data);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  };
  
  mgwiki.cma.Graph.prototype.submitNote = function(){
    var me = this;
    var depgraph = this.view.depgraph;
    var textarea = $("#note-text-"+depgraph.options.uid);
    var data = textarea.val();
    var box = depgraphlib.Box.getBox(textarea[0]);
    $.ajax({
      type: 'POST', 
      url: me.app.settings.proxy_url + "sc/"+me.settings.sc+"/_graph/_note/_add",
      data: {
        sid:me.settings.id,
        data:data
      },
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        me.view.render();
      },
      error: function(jqXHR, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
    box.close(true);
  };

}(window.mgwiki = window.mgwiki || {},jQuery));