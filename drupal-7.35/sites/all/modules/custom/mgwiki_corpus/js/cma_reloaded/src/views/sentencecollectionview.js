(function(mgwiki,$){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.SentenceCollectionView = function(model,$el){
    this.model = model;
    this.$el = $el || $('<div class="cma-sentencecollection-container"></div>');
    this.initiated = false;
  };

  mgwiki.cma.SentenceCollectionView.paginationMax = 10;

  mgwiki.cma.SentenceCollectionView.prototype.render = function(){
    var me = this;

    this.$el.html(mgwiki.cma.SentenceCollectionView.template);
    var container = this.$el.find('.cma-sentencecollection-items');

    for(var i = 0; i <me.model.graphs.length;i++){
      var graph = me.model.graphs[i];

      container.append(graph.view.$el);
      graph.view.render();

    }

    var n = Math.ceil(me.model.total/me.model.max);
    var i = Math.floor(this.model.from / this.model.max);


    if(i>0){
      var startButton = new mgwiki.cma.ui.CallbackButton("Start",{}); // add styles to callback buttons ...
      me.$el.find('.cma-pagination-start').append(startButton.view); // here if i use .html() are callbacks still preserved?
      startButton.init(function(){
        var button = this;
        me.model.goTo(0,function(){button.success();},function(){button.error();});
      },this);

      var prevButton = new mgwiki.cma.ui.CallbackButton("Prev",{}); // add styles to callback buttons ...
      me.$el.find('.cma-pagination-prev').append(prevButton.view); // here if i use .html() are callbacks still preserved?
      prevButton.init(function(){
        var button = this;
        me.model.prev(function(){button.success();},function(){button.error();});
      },this);  
    }

    if(i<n-1){
      var endButton = new mgwiki.cma.ui.CallbackButton("End",{}); // add styles to callback buttons ...
      me.$el.find('.cma-pagination-end').append(endButton.view); // here if i use .html() are callbacks still preserved?
      endButton.init(function(){
        var button = this;
        me.model.goTo(n-1,function(){button.success();},function(){button.error();});
      },this);

      var nextButton = new mgwiki.cma.ui.CallbackButton("Next",{}); // add styles to callback buttons ...
      me.$el.find('.cma-pagination-next').append(nextButton.view); // here if i use .html() are callbacks still preserved?
      nextButton.init(function(){
        var button = this;
        me.model.next(function(){button.success();},function(){button.error();});
      },this);
    }

    var k = Math.floor(mgwiki.cma.SentenceCollectionView.paginationMax/2);
    var kp = 0;
    var kn = 0;
    var a = i-k;
    if(a<0){
      kn = -a;
    }
    var b = i+k;
    if(b>=n){
      kp = n-b;
    }
    var ks = (a+kp>0)?(a+kp):0;
    var ke = (b+kn<n)?(b+kn):(n-1);
    for(var j=ks;j<=ke;j++){
      if(j==i){
        me.$el.find('.cma-pagination-pages').append('<span style="margin-left:10px; margin-right:10px;">'+i+'</span>');
        continue;
      }
      var pagginButton = new mgwiki.cma.ui.CallbackButton(j,{}); // add styles to callback buttons ...
      pagginButton.view.attr('index',j);
      me.$el.find('.cma-pagination-pages').append(pagginButton.view); // here if i use .html() are callbacks still preserved?
      pagginButton.init(function(){
        var button = this;
        me.model.goTo(button.view.attr('index'),function(){button.success();},function(){button.error();});
      },this);  
    }
    
  };

  mgwiki.cma.SentenceCollectionView.template = '<div class="cma-sentencecollection-items"></div>'+
    '<div class="cma-result-pagination"><span class="cma-pagination-start"></span><span class="cma-pagination-prev"></span><span class="cma-pagination-pages"></span><span class="cma-pagination-next"></span><span class="cma-pagination-end"></span></div>';

  
}(window.mgwiki = window.mgwiki || {},jQuery));