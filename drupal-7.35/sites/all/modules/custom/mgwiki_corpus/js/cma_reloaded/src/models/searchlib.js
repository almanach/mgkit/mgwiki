(function(mgwiki,$){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma.SearchLib = function(settings){
    this.view = jQuery(mgwiki.cma.SearchLib.template);
    this.textarea = settings.textarea;
    this.user = settings.user;
    this.ws_url = settings.proxy_url;
    this.init();
  };


  mgwiki.cma.SearchLib.prototype.initView = function(){
    if(this.currentSelect){
      this.currentSelect.removeClass('cma-searchlib-list-item-selected');
    }
    this.currentHover = null;
    this.currentSelect = null;
    var me = this;
    jQuery('.cma-searchlib-filter-input',this.view).on("keyup",function(e){
      var obj = this;
      if(me.currentSearch != jQuery(obj).val()){
        jQuery.ajax({
          type: 'POST', 
          url: me.ws_url+'dpathlib/_search',
          data:{
            query:jQuery(obj).val()
          },
          dataType : 'json',
          success: function(data, textStatus, jqXHR) {
            me.fillTable(data);
          },
          error: function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
          }
        });
      }
      me.currentSearch = jQuery(obj).val();
    });
    jQuery('.cma-searchlib-list',this.view).on("mouseover",function(e){
      var selection = jQuery(e.target).parents('tr');
      if(me.currentHover){
        me.currentHover.removeClass('cma-searchlib-list-item-hover');
      }
      if(selection.hasClass('cma-searchlib-list-title')){
        me.currentHover = null;
        return;
      }
      me.currentHover = selection;
      selection.addClass('cma-searchlib-list-item-hover');
    });
    jQuery('.cma-searchlib-list',this.view).on("mouseout",function(e){
      if(me.currentHover){
        me.currentHover.removeClass('cma-searchlib-list-item-hover');
        me.currentHover = null;
      }
    });
    jQuery('.cma-searchlib-list',me.view).on("click",function(e){
      var selection = jQuery(e.target).parents('tr');
      if(selection.hasClass('cma-searchlib-list-title')){
        return;
      }
      if(me.currentSelect){
        me.currentSelect.removeClass('cma-searchlib-list-item-selected');
      }
      if(me.currentSelect == selection){
        return;
      }
      me.currentSelect = selection;
      selection.addClass('cma-searchlib-list-item-selected');
    });
    jQuery('.cma-searchlib-insert',me.view).click(function(){
      if(me.currentSelect != null){
        var children = me.currentSelect.children();
        var macro = "$"+children[0].innerHTML;
        var val = me.textarea.html();
        me.textarea.html(val+'<span>'+macro+'</span>');
      }
    });
    this.setTableActions();
  };

  mgwiki.cma.SearchLib.prototype.init = function(){
    var me = this;
    jQuery.ajax({
      type: 'POST', 
      url: me.ws_url+'dpathlib/_all',
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        me.macros = [];
        for (var i = data.length - 1; i >= 0; i--) {
          me.macros.push(data[i].name);
        };
        me.fillTable(data);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        
      }
    });
  };

  mgwiki.cma.SearchLib.prototype.setTableActions = function(){
    var me = this;
    jQuery('.cma-searchlib-action-delete',me.view).click(function(e){
      var selection = jQuery(e.target).parents('tr');
      var children = selection.children();
      var macro = children[0].innerHTML;
      var r = window.confirm("Êtes vous sur de vouloir supprimer cette recherche?");
      if(r){
        me.deleteElement(macro,function(){
          selection.remove();
        });
      }
      
    });
    jQuery('.cma-searchlib-action-edit',me.view).click(function(e){
      var selection = jQuery(e.target).parents('tr');
      var children = selection.children();
      var ispublic = jQuery(children[4]).find('.cma-searchlib-action-public');
      var publicv = false;
      if(ispublic.length){
        publicv = true;
      }
      var macro = {
        name:children[0].innerHTML,
        query:children[1].innerHTML,
        description:children[2].innerHTML,
        publicv:publicv
      };
      me.elementForm(macro);
    });
    jQuery('.cma-searchlib-action-public',me.view).click(function(e){
      var selection = jQuery(e.target).parents('tr');
      var children = selection.children();
      var macro = {
        name:children[0].innerHTML,
        query:children[1].innerHTML,
        description:children[2].innerHTML,
        publicv:false
      };
      me.syncElement(macro.name,macro,function(data){
        if(data.success){
          me.init();
        }else{
          alert(data.message);
        }
      });
    });
    jQuery('.cma-searchlib-action-private',me.view).click(function(e){
      var selection = jQuery(e.target).parents('tr');
      var children = selection.children();
      var macro = {
        name:children[0].innerHTML,
        query:children[1].innerHTML,
        description:children[2].innerHTML,
        publicv:true
      };
      me.syncElement(macro.name,macro,function(data){
        if(data.success){
          me.init();
        }else{
          alert(data.message);
        }
      });
    });
  };

  /**
   * Fill the table with data retrieved by the server
   * @param  {[type]} data [description]
   * @return {[type]}      [description]
   */
  mgwiki.cma.SearchLib.prototype.fillTable = function(data){
    var me = this;
    var tbody = jQuery('.cma-searchlib-list-body',this.view);
    tbody.html("");
    for (var i = data.length - 1; i >= 0; i--) {
      tbody.append('<tr><td>'+data[i].name+'</td><td>'+data[i].query+'</td><td>'+data[i].description+'</td><td>'+data[i].user+'</td><td>'+getActions(data[i],this.user.name)+'</td></tr>');
    };
    me.setTableActions();
  };

  function getActions(macro,user){
    var actions = "";
    if(macro.user == user){
      actions += '<span class="cma-searchlib-action cma-searchlib-action-edit"></span><span class="cma-searchlib-action cma-searchlib-action-delete"></span>';
    }
    if(macro.ispublic){
      actions += '<span class="cma-searchlib-action cma-searchlib-action-public"></span>';
    }else{
      actions += '<span class="cma-searchlib-action cma-searchlib-action-private"></span>';
    }
    return actions;
  }

  /**
   * expand a macro to the query
   * @param  {[type]} name [description]
   * @return {[type]}      [description]
   */
  mgwiki.cma.SearchLib.prototype.expand = function(name){

  };

  /**
   * show the form to add a new macro to the library
   * @param {[type]} name  [description]
   * @param {[type]} query [description]
   */
  mgwiki.cma.SearchLib.prototype.elementForm = function(macroInfo){
    var me = this;
    var width = jQuery(window).width();
    var height = jQuery(window).height();
    var box = new depgraphlib.Box({closeButton:true,draggable:true,position:{x:width/2-200,y:height/2-200}});
    box.setFixedSize(400,400);
    box.open();
    box.setHeader("DPath Query Library");

    var $el = jQuery('.depgraphlib-box-content',box.object);
    $el.append(mgwiki.cma.SearchLib.addNewFormTemplate);

    if(macroInfo.query){
      $el.find('.cma-searchlib-addnewform-query').val(macroInfo.query);  
    }

    if(macroInfo.description){
      $el.find('.cma-searchlib-addnewform-desc').val(macroInfo.description);  
    }

    if(macroInfo.name){
      $el.find('.cma-searchlib-addnewform-name').val(macroInfo.name);  
    }

    if(macroInfo.publicv){
      $el.find('.cma-searchlib-addnewform-public').prop("checked",true);  
    }
    
    $el.find('.cma-searchlib-addnewform-cancel').click(function(){
      box.destroy();
      
    });

    $el.find('.cma-searchlib-addnewform-submit').click(function(){
      var owner = me.user.name || 'unknown';
      var query = $el.find('.cma-searchlib-addnewform-query').val();
      var name = $el.find('.cma-searchlib-addnewform-name').val();
      var desc = $el.find('.cma-searchlib-addnewform-desc').val();

      var publicv = $el.find('.cma-searchlib-addnewform-public')[0].checked;
      
      var macroDefintion = {
        name:name,
        query:query,
        owner:owner,
        description:desc,
        publicv:publicv
      };
      me.syncElement(macroInfo.name,macroDefintion,function(data){
        if(data.success){
          me.init();
          box.destroy();  
        }else{
          alert(data.message);
        }
      });
      
    });
  };

  mgwiki.cma.SearchLib.prototype.deleteElement = function(macroName,callbackResult){
    var proxy_url = "dpathlib/"+macroName+"/_delete";
    var me = this;
    var data = {
      name:macroName,
    };
    jQuery.ajax({
      type: 'POST', 
      url: me.ws_url+proxy_url,
      data: data,
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        callbackResult.call(me,data);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        
      }
    });
  };

  mgwiki.cma.SearchLib.prototype.syncElement = function(macroName,macroDefinition,resultCallback){
    var proxy_url = "dpathlib/_create";
    if(macroName){
      proxy_url = "dpathlib/"+macroName+"/_edit";
    }
    var me = this;
    var data = {
      name:macroDefinition.name,
      query:macroDefinition.query,
      owner:macroDefinition.owner,
      description:macroDefinition.description,
      publicv:macroDefinition.publicv
    };
    jQuery.ajax({
      type: 'POST', 
      url: me.ws_url+proxy_url,
      data: data,
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        resultCallback.call(me,data);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        
      }
    });
  };

  mgwiki.cma.SearchLib.addNewFormTemplate = '<div class="cma-searchlib-addnewform">'+
    '<h2>Save Query macro</h2>'+
    '<fieldset>'+
      '<legend>Required:</legend>'+
      'Name: <input class="cma-searchlib-addnewform-name" type="text"><br>'+
      'Query: <input class="cma-searchlib-addnewform-query" type="text"><br>'+
    '</fieldset>'+
    '<fieldset>'+
      '<legend>Optional:</legend>'+
      'Description: <textarea class="cma-searchlib-addnewform-desc"></textarea><br>'+
      'Public Visibility: <input type="checkbox" class="cma-searchlib-addnewform-public"><br>'+
    '</fieldset>'+
    '<input type="submit" class="cma-searchlib-addnewform-submit" value="Save"><input type="submit" class="cma-searchlib-addnewform-cancel" value="Cancel">'
  '</div>';

  /**
   * delete a macro
   * @param  {[type]} name [description]
   * @return {[type]}      [description]
   */
  mgwiki.cma.SearchLib.prototype.remove = function(name){

  };

  /**
   * show the macro library
   * @return {[type]} [description]
   */
  mgwiki.cma.SearchLib.prototype.show = function(){
    var box = new depgraphlib.Box({closeButton:true,draggable:true,position:{x:50,y:50}});
    box.setMaxSize(null,500);
    box.open();
    box.setHeader("DPath Query Library");

    var $el = jQuery('.depgraphlib-box-content',box.object);
    this.initView();
    $el.append(this.view);
  };

  mgwiki.cma.SearchLib.template = '<div>'+
    '<div><span class="cma-searchlib-filter"><input type="text" class="cma-searchlib-filter-input"></span><span class="cma-searchlib-insert"><button type="submit">Insert Selected Macro</button></span></div>'+
    '<table class="cma-searchlib-list"><thead><tr class="cma-searchlib-list-title"><th>Name</th><th>Query</th><th>Description</th><th>Owner</th><th>Actions</th></tr></thead><tbody class="cma-searchlib-list-body"></tbody></table>'+
  '</div>';


  mgwiki.cma.SearchHistory = function(settings){
    this.view = jQuery('<table class="cma-searchhistory-list"><thead><tr class="cma-searchhistory-list-title"><th>Query</th><th></th></tr></thead><tbody class="cma-searchhistory-list-body"></tbody></table>');
    this.settings = settings;
  };

  mgwiki.cma.SearchHistory.prototype.show = function(){
    var box = new depgraphlib.Box({closeButton:true,draggable:true,position:{x:50,y:50}});
    box.setMaxSize(null,500);
    box.open();
    box.setHeader("DPath Query History");

    var $el = jQuery('.depgraphlib-box-content',box.object);
    this.refreshView();
    $el.append(this.view);
  };

  mgwiki.cma.SearchHistory.prototype.refreshView = function(){
    var me = this;
    var proxy_url="dpathlib/_history"
    jQuery.ajax({
      type: 'POST', 
      url: me.settings.ws_url+proxy_url,
      dataType : 'json',
      success: function(data, textStatus, jqXHR) {
        me.history = [];
        for (var i = data.length - 1; i >= 0; i--) {
          me.history.push(data[i]);
          var tbody = jQuery('.cma-searchhistory-list-body',me.view);
          tbody.html("");
          for (var i = data.length - 1; i >= 0; i--) {
            tbody.append('<tr><td>'+data[i]+'</td><td><span class="cma-searchhistory-save"></span><span class="cma-searchhistory-insert"></span></td></tr>');
          };
          jQuery('.cma-searchhistory-save',me.view).click(function(e){
            var selection = jQuery(e.target).parents('tr');
            var children = selection.children();
            var macro = {
              query:children[0].innerHTML,
            };
            me.settings.macroLib.elementForm(macro);
          });
          jQuery('.cma-searchhistory-insert',me.view).click(function(e){
            var selection = jQuery(e.target).parents('tr');
            var children = selection.children();
            var query = children[0].innerHTML;
            var val = me.settings.textarea.html();
            me.settings.textarea.html(val+'<span>'+query+'</span>');
          });
        };
      },
      error: function(jqXHR, textStatus, errorThrown) {
        
      }
    });
  };
  
}(window.mgwiki = window.mgwiki || {},jQuery));

  