(function(cma){

  mgwiki.cma.ui = mgwiki.cma.ui || {};

  mgwiki.cma.ui.FoldableDiv = function(content,startexpanded){
    this.view = jQuery(mgwiki.cma.ui.FoldableDiv.templateMain);
    this.content = jQuery('.cma-foldable-content',this.view);
    this.content.append(content);
    this.expanded = startexpanded;
    if(!this.expanded){
      jQuery('.cma-foldable-head',this.view).addClass('cma-foldable-open');
      this.content.hide();
    }else{
      jQuery('.cma-foldable-head',this.view).addClass('cma-foldable-close');
    }
    this.initView();
  }

  mgwiki.cma.ui.FoldableDiv.prototype.setTitle = function(title){
    jQuery('.cma-foldable-head',this.view).find('div').html(title);
  }

  mgwiki.cma.ui.FoldableDiv.prototype.initView = function(){
    var me = this;
    jQuery('.cma-foldable-head',this.view).click(function(){
      if(me.expanded){
        jQuery(this).addClass('cma-foldable-open');
        jQuery(this).removeClass('cma-foldable-close');
        me.content.hide();
      }else{
        jQuery(this).addClass('cma-foldable-close');
        jQuery(this).removeClass('cma-foldable-open');
        me.content.show();  
      }
      me.expanded = !me.expanded;
    });
  }

  mgwiki.cma.ui.FoldableDiv.templateMain = '<div class="cma-foldable-div"><div class="cma-foldable-head"><div style="border-top:1px solid #bbbbbb; margin-left:20px; margin-top:2px; font-size:10px;"></div></div>'+
    '<div class="cma-foldable-content"></div></div>';
  
}(window.cma = window.cma || {}));