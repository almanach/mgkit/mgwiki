(function(mgwiki,$){
  
  /**
   * CMA Reloaded module.
   * This module is a ui for lightweight corpus management provided to mgwiki.
   * @type {Object}
   */
  mgwiki.cma = {};

  mgwiki.cma.App = function($el,config){
    this.$el = $el || $('<div></div>');
    this.settings = config || {};
    this.init();
  };

  mgwiki.cma.App.prototype.init = function(){
    var me = this;

    this.$el.html(mgwiki.cma.App.template);

    this.errorLogger = new mgwiki.cma.ErrorLogger(this.$el.find('.cma-error-message'));
    this.searchService = new mgwiki.cma.SearchService(this,this.$el.find('.cma-search-box'),this.settings);


  };

  mgwiki.cma.App.template = 
    '<div class="cma-error-message"></div>'+
    '<div class="cma-search-box"></div>'+
    '<div class="cma-results-container"></div>';


  mgwiki.cma.App2 = function($el,settings){
    this.$el = $el || $('<div></div>');
    this.settings = settings;
    this.init();
  };

  mgwiki.cma.App2.prototype.init = function(){
    var me = this;

    this.$el.html(mgwiki.cma.App2.template);

    this.corpuslist = new mgwiki.cma.CorpusList(this,this.$el.find('.cma-col-left'));
    this.sentenceCollection = new mgwiki.cma.SentenceCollection(this,this.$el.find('.cma-sentencecollection-container'));
  };

  mgwiki.cma.App2.prototype.use = function(item){
    this.sentenceCollection.load(item);
  };

  mgwiki.cma.App2.template = 
    '<div class="cma-col-left"></div>'+
    '<div class="cma-sentencecollection-container"></div>';


}(window.mgwiki = window.mgwiki || {},jQuery));