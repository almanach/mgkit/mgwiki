<?php

/**
 * @file
 * Default theme implementation to present the feedback form.
 *
 * @see template_preprocess_feedback_form_display()
 */
global $base_path;
$basepath = drupal_get_path('module','feedback');
?>
<div id="block-feedback-form">
  <h2><span class="feedback-link"><img
        src='<?php print $base_path.$basepath;?>/images/feedback_simple.gif'
        height='100px'
        width='35px' /></span></h2>
  <div class="content"><?php print drupal_render($content); ?></div>
</div>
