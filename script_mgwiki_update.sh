#! /bin/bash

mgwiki_home="/www/html/frmgwiki/"

cd "$mgwiki_home"sites/all/modules/custom/d3js/js/depgraph
grunt build_all
grunt style
cd "$mgwiki_home"sites/all/modules/custom/parser/js/parser
grunt build_all
cd "$mgwiki_home"sites/all/modules/custom/mgwiki_corpus/js/cma
grunt build_all
cd "$mgwiki_home"sites/all/modules/custom/mgwiki_corpus/js/cma_reloaded
grunt build_all
